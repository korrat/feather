/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.ExternalGroup
import dev.maximilian.feather.ExternalUser
import dev.maximilian.feather.IExternalCredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.authorization.internals.ldap.GroupOfNames
import dev.maximilian.feather.authorization.internals.ldap.InetOrgPerson
import dev.maximilian.feather.authorization.internals.ldap.getUUID
import dev.maximilian.feather.authorization.internals.ldap.toEntity
import dev.maximilian.feather.authorization.internals.ldap.toLdapEntry
import org.apache.directory.api.ldap.model.constants.LdapSecurityConstants
import org.apache.directory.api.ldap.model.constants.SchemaConstants
import org.apache.directory.api.ldap.model.entry.DefaultModification
import org.apache.directory.api.ldap.model.entry.Modification
import org.apache.directory.api.ldap.model.entry.ModificationOperation
import org.apache.directory.api.ldap.model.message.SearchScope
import org.apache.directory.api.ldap.model.password.PasswordUtil
import java.io.Closeable
import java.util.Locale

class ExternalLdapProvider(public val ldap: ILdapProvider) : IExternalCredentialProvider, Closeable by ldap {
    private val groupObjectClass =
        if (ldap.uniqueGroups) SchemaConstants.GROUP_OF_UNIQUE_NAMES_OC else SchemaConstants.GROUP_OF_NAMES_OC
    private val groupMemberAttribute =
        if (ldap.uniqueGroups) SchemaConstants.UNIQUE_MEMBER_AT else SchemaConstants.MEMBER_AT

    override fun getUsersAndGroups(): Pair<Collection<ExternalUser>, Collection<ExternalGroup>> {
        val ldapUsers =
            ldap.searchEntries(
                dn = ldap.baseDnUsers,
                scope = SearchScope.ONELEVEL,
                filter = "(objectClass=inetOrgPerson)",
            ).map { it.toEntity<InetOrgPerson>(ldap) }
        val ldapGroups =
            ldap.searchEntries(
                dn = ldap.baseDnGroups,
                filter = "(objectClass=$groupObjectClass)",
            ).filter { it.dn.toString() != ldap.baseDnGroups }
                .map { it.toEntity<GroupOfNames>(ldap) }

        val cachedUserDNs: Map<String, String> =
            ldapUsers.associate { it.dn to it.entryUUID }
        val cachedGroupDNs: Map<String, String> =
            ldapGroups.associate { it.dn to it.entryUUID }

        val disabled =
            ldap.searchEntries(
                dn = ldap.baseDnUsers,
                scope = SearchScope.ONELEVEL,
                filter = "(objectClass=inetOrgPerson)",
                attributeList = arrayOf(ldap.uuidType, "nsAccountLock"),
            ).associate {
                it.getUUID(ldap) to (it.get("nsAccountLock")?.string == "TRUE")
            }

        val users =
            ldapUsers.map {
                ExternalUser(
                    externalId = it.entryUUID,
                    username = it.rdn,
                    firstname = it.givenName ?: "",
                    surname = it.sn,
                    mail = it.mail?.firstOrNull() ?: "",
                    displayName = it.displayName ?: "${it.givenName} ${it.sn}",
                    registeredSince = it.createTimestamp,
                    groups =
                    ldapGroups.filter { group -> group.member.contains(it.dn) }
                        .map { group -> group.entryUUID }.toSet(),
                    ownedGroups =
                    ldapGroups.filter { group -> group.owner.contains(it.dn) }
                        .map { group -> group.entryUUID }.toSet(),
                    disabled = disabled[it.entryUUID] ?: false,
                )
            }

        val groups =
            ldapGroups.map {
                ExternalGroup(
                    externalId = it.entryUUID,
                    name = it.rdn,
                    description = it.description ?: it.rdn,
                    userMembers = it.member.mapNotNull { member -> cachedUserDNs[member] }.toSet(),
                    groupMembers = it.member.mapNotNull { member -> cachedGroupDNs[member] }.toSet(),
                    owners = it.owner.mapNotNull { owner -> cachedUserDNs[owner] }.toSet(),
                    ownerGroups = it.owner.mapNotNull { owner -> cachedGroupDNs[owner] }.toSet(),
                    parentGroups =
                    ldapGroups.filter { otherGroup ->
                        otherGroup.member.contains(it.dn)
                    }.map { otherGroup -> otherGroup.entryUUID }.toSet(),
                    ownedGroups =
                    ldapGroups.filter { otherGroup ->
                        otherGroup.owner.contains(it.dn)
                    }.map { otherGroup -> otherGroup.entryUUID }.toSet(),
                )
            }

        return users to groups
    }

    override fun getPermissions(): Map<String, Set<Permission>> {
        val ldapUserUuidByDn: Map<String, String> =
            ldap.searchEntries(
                dn = ldap.baseDnUsers,
                scope = SearchScope.ONELEVEL,
                filter = "(objectClass=inetOrgPerson)",
                attributeList = arrayOf(ldap.uuidType),
            ).associate {
                it.dn.toString() to it.getUUID(ldap)
            }

        val ldapPermissions: List<GroupOfNames> =
            ldap.searchEntries(dn = ldap.baseDnPermissions).filter { it.dn.toString() != ldap.baseDnPermissions }
                .map { it.toEntity(ldap) }

        val permissions: Map<Permission, Set<String>> =
            ldapPermissions.filter {
                enumContains<Permission>(it.rdn.uppercase(Locale.getDefault()))
            }.associate {
                Permission.valueOf(it.rdn.uppercase(Locale.getDefault())) to it.member
            }

        return ldapUserUuidByDn.map { (userDn, userUUID) ->
            userUUID to
                permissions.filter { p -> p.value.contains(userDn) }.map {
                    it.key
                }.toSet()
        }.toMap()
    }

    /**
     * Creates the user in the external ldap backend
     * The following fields are ignored while creating the user:
     * permissions, lastLoginAt
     * They are returned as given
     */
    override fun createUser(
        user: ExternalUser,
        password: String?,
    ): ExternalUser {
        require(user.externalId.isBlank()) {
            "The given external id is not blank, user already created?"
        }

        val cn = if (ldap.rdnUser != "cn") user.displayName else null
        val passwordToBeSend =
            if (ldap.plainTextPasswords) {
                password
            } else {
                password?.let {
                    String(PasswordUtil.createStoragePassword(it, LdapSecurityConstants.HASH_METHOD_CRYPT_SHA256))
                }
            }
        val created =
            ldap.create(
                InetOrgPerson(
                    dn = "${ldap.rdnUser}=${user.username},${ldap.baseDnUsers}",
                    cn = cn,
                    rdn = user.username,
                    mail = setOf(user.mail),
                    displayName = user.displayName,
                    sn = user.surname,
                    givenName = user.firstname,
                    entryUUID = "",
                    employeeNumber = null,
                    userPassword = passwordToBeSend,
                ).toLdapEntry(ldap),
            ).toEntity<InetOrgPerson>(ldap)

        val groupFilter =
            (user.groups + user.ownedGroups).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")",
            )
        ldap.searchEntries(
            ldap.baseDnGroups,
            "(&(${SchemaConstants.OBJECT_CLASS_AT}=$groupObjectClass)(|$groupFilter))",
        )
            .map { it.toEntity<GroupOfNames>(ldap) }.forEach {
                val groupModifications = mutableListOf<Modification>()

                // Add user to all groups he/she is owner or member
                // User is always member of groups he/she is owner
                if (!it.member.contains(created.dn)) {
                    groupModifications.add(
                        attributeReplaceModificationOperation(
                            groupMemberAttribute,
                            it.member + created.dn,
                        ),
                    )
                }

                // Add user to groups only where he/she is owner
                if (!it.owner.contains(created.dn) && user.ownedGroups.contains(it.entryUUID)) {
                    groupModifications.add(
                        attributeReplaceModificationOperation(
                            SchemaConstants.OWNER_AT,
                            it.owner + created.dn,
                        ),
                    )
                }

                if (groupModifications.isNotEmpty()) {
                    ldap.modify(it.dn, groupModifications)
                }
            }

        return user.copy(
            externalId = created.entryUUID,
            registeredSince = created.createTimestamp,
            groups = user.groups + user.ownedGroups,
        )
    }

    override fun getUserById(userId: String): ExternalUser? {
        require(userId.isNotBlank()) {
            "The given external id is blank"
        }

        val externalUser =
            ldap.searchOneEntry(
                ldap.baseDnUsers,
                "(&(${SchemaConstants.OBJECT_CLASS_AT}=${SchemaConstants.INET_ORG_PERSON_OC})(${ldap.uuidType}=$userId))",
                SearchScope.ONELEVEL,
            )?.toEntity<InetOrgPerson>(ldap) ?: return null

        val memberGroups =
            ldap.searchEntries(
                dn = ldap.baseDnGroups,
                filter = "(&(${SchemaConstants.OBJECT_CLASS_AT}=$groupObjectClass)($groupMemberAttribute=${externalUser.dn}))",
            ).map { e -> e.getUUID(ldap) }

        val ownedGroups =
            ldap.searchEntries(
                dn = ldap.baseDnGroups,
                filter = "(&(${SchemaConstants.OBJECT_CLASS_AT}=$groupObjectClass)(${SchemaConstants.OWNER_AT}=${externalUser.dn}))",
            ).map { e -> e.getUUID(ldap) }

        val disabled =
            ldap.getByUUID(
                externalUser.entryUUID,
                ldap.baseDnUsers,
                arrayOf("nsAccountLock"),
            )?.get("nsAccountLock")?.let { attr ->
                attr.string == "TRUE"
            }

        return ExternalUser(
            externalId = userId,
            displayName = externalUser.displayName ?: "${externalUser.givenName} ${externalUser.sn}",
            firstname = externalUser.givenName ?: "",
            mail = externalUser.mail?.firstOrNull() ?: "",
            registeredSince = externalUser.createTimestamp,
            surname = externalUser.sn,
            username = externalUser.rdn,
            groups = memberGroups.toSet(),
            ownedGroups = ownedGroups.toSet(),
            disabled = disabled ?: false,
        )
    }

    override fun authenticateUserByUsernameOrMail(
        usernameOrMail: String,
        password: String,
    ): String? =
        ldap.searchOneEntry(
            ldap.baseDnUsers,
            "(&(objectClass=inetOrgPerson)(|(${ldap.rdnUser}=$usernameOrMail)(mail=$usernameOrMail)))",
            SearchScope.ONELEVEL,
            attributeList = arrayOf(ldap.uuidType),
        )?.takeIf { ldap.bind(it.dn.toString(), password) }?.getUUID(ldap)

    override fun getPhotoForUser(externalId: String): ByteArray? =
        ldap.getByUUID(externalId, ldap.baseDnUsers)!!.toEntity<InetOrgPerson>(ldap).jpegPhoto

    override fun updatePhotoForUser(
        externalId: String,
        image: ByteArray,
    ) {
        val entry = ldap.getByUUID(externalId, ldap.baseDnUsers, arrayOf(SchemaConstants.JPEG_PHOTO_AT))
        requireNotNull(entry)

        val operation =
            if (entry.containsAttribute(SchemaConstants.JPEG_PHOTO_AT)) {
                ModificationOperation.REPLACE_ATTRIBUTE
            } else {
                ModificationOperation.ADD_ATTRIBUTE
            }

        ldap.modify(
            entry.dn.toString(),
            DefaultModification(
                operation,
                SchemaConstants.JPEG_PHOTO_AT,
                image,
            ),
        )
    }

    override fun deletePhotoForUser(externalId: String) {
        val entry =
            ldap.getByUUID(externalId, ldap.baseDnUsers, arrayOf(SchemaConstants.JPEG_PHOTO_AT))?.takeIf {
                it.containsAttribute(SchemaConstants.JPEG_PHOTO_AT)
            } ?: return

        ldap.modify(
            entry.dn.toString(),
            DefaultModification(ModificationOperation.REMOVE_ATTRIBUTE, SchemaConstants.JPEG_PHOTO_AT),
        )
    }

    override fun updateUserPassword(
        externalId: String,
        newPassword: String,
    ): Boolean {
        val entry = ldap.getByUUID(externalId, ldap.baseDnUsers, arrayOf(SchemaConstants.USER_PASSWORD_AT)) ?: return false

        val operation =
            if (entry.containsAttribute(
                    SchemaConstants.USER_PASSWORD_AT,
                )
            ) {
                ModificationOperation.REPLACE_ATTRIBUTE
            } else {
                ModificationOperation.ADD_ATTRIBUTE
            }

        val passwordToBeSend =
            if (ldap.plainTextPasswords) {
                newPassword
            } else {
                String(PasswordUtil.createStoragePassword(newPassword, LdapSecurityConstants.HASH_METHOD_CRYPT_SHA256))
            }

        ldap.modify(
            entry.dn.toString(),
            DefaultModification(
                operation,
                SchemaConstants.USER_PASSWORD_AT,
                passwordToBeSend,
            ),
        )

        return true
    }

    override fun deleteUser(externalId: String) {
        require(externalId.isNotBlank()) {
            "The given external id is blank"
        }

        ldap.getByUUID(externalId, ldap.baseDnUsers)?.apply {
            ldap.searchEntries(
                ldap.baseDnGroups,
                "(&(${SchemaConstants.OBJECT_CLASS_AT}=$groupObjectClass)(|(${SchemaConstants.OWNER_AT}=$dn)($groupMemberAttribute=$dn)))",
            ).forEach { entry ->
                val newMembers: Set<String> =
                    entry[groupMemberAttribute]?.map { e -> e.string }?.let { it + ldap.dummyAccountDn - dn.toString() }
                        ?.toSet() ?: setOf(ldap.dummyAccountDn)
                val newOwners: Set<String> =
                    entry[SchemaConstants.OWNER_AT]?.map { e -> e.string }
                        ?.let { it + ldap.dummyAccountDn - dn.toString() }?.toSet() ?: setOf(ldap.dummyAccountDn)
                val modifications =
                    listOf(
                        attributeReplaceModificationOperation(groupMemberAttribute, newMembers),
                        attributeReplaceModificationOperation(SchemaConstants.OWNER_AT, newOwners),
                    )

                ldap.modify(entry.dn.toString(), modifications)
            }

            ldap.delete(this)
        }
    }

    override fun updateUser(user: ExternalUser): ExternalUser? {
        require(user.externalId.isNotBlank()) {
            "The given external id is blank"
        }

        // Special case, user (not longer) existing, ignoring update request
        val savedUser = getUserById(user.externalId) ?: return null

        // Special case rdn modification
        if (savedUser.username != user.username) {
            ldap.rename("${ldap.rdnUser}=${savedUser.username},${ldap.baseDnUsers}", "${ldap.rdnUser}=${user.username}")
        }

        val userDn = "${ldap.rdnUser}=${user.username},${ldap.baseDnUsers}"
        val modifications =
            listOfNotNull(
                attributeModificationOperation(SchemaConstants.DISPLAY_NAME_AT, savedUser.displayName, user.displayName),
                attributeModificationOperation(SchemaConstants.GIVENNAME_AT, savedUser.firstname, user.firstname),
                attributeModificationOperation(SchemaConstants.SURNAME_AT, savedUser.surname, user.surname),
                attributeModificationOperation(SchemaConstants.MAIL_AT, savedUser.mail, user.mail),
            ).toMutableList()

        val nsAccountLockAsString =
            ldap.getByUUID(
                user.externalId,
                ldap.baseDnUsers,
                arrayOf("nsAccountLock"),
            )?.get("nsAccountLock")?.string

        if (nsAccountLockAsString != null || user.disabled) {
            attributeModificationOperation(
                "nsAccountLock",
                nsAccountLockAsString,
                if (user.disabled) "TRUE" else "FALSE",
            )
                ?.also { modifications.add(it) }
        }

        if (modifications.isNotEmpty()) {
            ldap.modify(userDn, modifications)
        }

        val allOldUserGroups = savedUser.groups + savedUser.ownedGroups
        val allNewUserGroups = user.groups + user.ownedGroups

        allOldUserGroups.plus(allNewUserGroups).mapNotNull { getGroupById(it) }.forEach {
            if (user.ownedGroups.contains(it.externalId) && !it.owners.contains(user.externalId)) {
                // add this user as owner (and member) to that group
                updateGroup(it.copy(owners = it.owners + user.externalId, userMembers = it.userMembers + user.externalId))
            } else if (!allNewUserGroups.contains(it.externalId) && it.userMembers.contains(user.externalId)) {
                // remove this user as member (and owner) from that group
                updateGroup(it.copy(owners = it.owners - user.externalId, userMembers = it.userMembers - user.externalId))
            } else if (allNewUserGroups.contains(it.externalId) && !it.userMembers.contains(user.externalId)) {
                // add this user as member to that group
                updateGroup(it.copy(userMembers = it.userMembers + user.externalId))
            } else if (it.userMembers.contains(user.externalId) && !allNewUserGroups.contains(it.externalId)) {
                // remove this user as member from that group
                updateGroup(it.copy(userMembers = it.userMembers - user.externalId))
            }
        }

        // Registered since is not updated
        // User is member in all owned groups
        return user.copy(groups = user.groups + user.ownedGroups, registeredSince = savedUser.registeredSince)
    }

    override fun createGroup(group: ExternalGroup): ExternalGroup {
        val personFilter =
            (group.userMembers + group.owners).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")",
            )
        val personUIDs =
            ldap.searchEntries(
                dn = ldap.baseDnUsers,
                filter = "(&(objectClass=inetOrgPerson)(|$personFilter))",
            ).associate { e -> e.getUUID(ldap) to e.dn.toString() }

        val groupFilter =
            (group.groupMembers + group.ownerGroups).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")",
            )
        val groupUIDs =
            ldap.searchEntries(
                dn = ldap.baseDnGroups,
                filter = "(&(objectClass=$groupObjectClass)(|$groupFilter))",
            ).associate { e -> e.getUUID(ldap) to e.dn.toString() }

        val userMembers =
            (group.userMembers.mapNotNull { personUIDs[it] } + group.owners.mapNotNull { personUIDs[it] } + ldap.dummyAccountDn).toSet()
        val groupMembers =
            (group.groupMembers.mapNotNull { groupUIDs[it] } + group.ownerGroups.mapNotNull { groupUIDs[it] }).toSet()
        val userOwners = (group.owners.mapNotNull { personUIDs[it] } + ldap.dummyAccountDn).toSet()
        val groupOwners = group.ownerGroups.mapNotNull { groupUIDs[it] }.toSet()
        val created =
            ldap.create(
                GroupOfNames(
                    dn = "${ldap.rdnGroup}=${group.name},${ldap.baseDnGroups}",
                    rdn = group.name,
                    member = userMembers + groupMembers,
                    owner = userOwners + groupOwners,
                    description = group.description,
                    entryUUID = "",
                ).toLdapEntry(ldap),
            )

        (group.parentGroups - group.ownedGroups).mapNotNull { getGroupById(it) }.forEach {
            // add this group as member to that other group
            updateGroup(it.copy(groupMembers = it.groupMembers + created.getUUID(ldap)))
        }

        group.ownedGroups.mapNotNull { getGroupById(it) }.forEach {
            // add this group as owner to that other group
            updateGroup(it.copy(ownerGroups = it.ownerGroups + created.getUUID(ldap)))
        }

        return group.copy(
            externalId = created.getUUID(ldap),
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups,
        )
    }

    override fun getGroupById(id: String): ExternalGroup? =
        ldap.searchOneEntry(
            ldap.baseDnGroups,
            "(&(objectClass=$groupObjectClass)(${ldap.uuidType}=$id))",
            SearchScope.SUBTREE,
        )?.toEntity<GroupOfNames>(ldap)?.let {
            val memberAndOwnerFilter =
                (it.member + it.owner).joinToString(separator = ")(entryDN=", prefix = "(entryDN=", postfix = ")")
            val personUIDs =
                ldap.searchEntries(
                    dn = ldap.baseDnUsers,
                    filter = "(&(objectClass=inetOrgPerson)(|$memberAndOwnerFilter))",
                ).associate { e -> e.dn.toString() to e.getUUID(ldap) }
            val groupUIDs =
                ldap.searchEntries(
                    dn = ldap.baseDnGroups,
                    filter = "(&(objectClass=$groupObjectClass)(|$memberAndOwnerFilter))",
                ).associate { e -> e.dn.toString() to e.getUUID(ldap) }

            val memberGroups =
                ldap.searchEntries(
                    dn = ldap.baseDnGroups,
                    filter = "(&(objectClass=$groupObjectClass)($groupMemberAttribute=${it.dn}))",
                ).map { e -> e.getUUID(ldap) }

            val ownedGroups =
                ldap.searchEntries(
                    dn = ldap.baseDnGroups,
                    filter = "(&(objectClass=$groupObjectClass)(owner=${it.dn}))",
                ).map { e -> e.getUUID(ldap) }

            ExternalGroup(
                externalId = it.entryUUID,
                name = it.rdn,
                description = it.description ?: it.rdn,
                owners = it.owner.mapNotNull { e -> personUIDs[e] }.toSet(),
                ownerGroups = it.owner.mapNotNull { e -> groupUIDs[e] }.toSet(),
                userMembers = it.member.mapNotNull { e -> personUIDs[e] }.toSet(),
                groupMembers = it.member.mapNotNull { e -> groupUIDs[e] }.toSet(),
                parentGroups = memberGroups.toSet(),
                ownedGroups = ownedGroups.toSet(),
            )
        }

    override fun deleteGroup(externalId: String) {
        require(externalId.isNotBlank()) {
            "The given external id is blank"
        }

        ldap.getByUUID(externalId, ldap.baseDnGroups)?.apply {
            ldap.searchEntries(
                ldap.baseDnGroups,
                "(&(${SchemaConstants.OBJECT_CLASS_AT}=$groupObjectClass)(|(${SchemaConstants.OWNER_AT}=$dn)($groupMemberAttribute=$dn)))",
            ).forEach { entry ->
                val newMembers: Set<String> =
                    entry[groupMemberAttribute]?.map { e -> e.string }?.let { it + ldap.dummyAccountDn - dn.toString() }
                        ?.toSet() ?: setOf(ldap.dummyAccountDn)
                val newOwners: Set<String> =
                    entry[SchemaConstants.OWNER_AT]?.map { e -> e.string }
                        ?.let { it + ldap.dummyAccountDn - dn.toString() }?.toSet() ?: setOf(ldap.dummyAccountDn)
                val modifications =
                    listOf(
                        attributeReplaceModificationOperation(groupMemberAttribute, newMembers),
                        attributeReplaceModificationOperation(SchemaConstants.OWNER_AT, newOwners),
                    )

                ldap.modify(entry.dn.toString(), modifications)
            }

            ldap.delete(this)
        }
    }

    override fun updateGroup(group: ExternalGroup): ExternalGroup? {
        // Special case, group (no longer) existing, ignoring update request
        val savedGroup = getGroupById(group.externalId) ?: return null

        // Special case rdn modification
        if (group.name != savedGroup.name) {
            ldap.rename("${ldap.rdnGroup}=${savedGroup.name},${ldap.baseDnGroups}", "${ldap.rdnGroup}=${group.name}")
        }

        val personFilter =
            (group.userMembers + group.owners).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")",
            )
        val personUIDs =
            ldap.searchEntries(
                dn = ldap.baseDnUsers,
                filter = "(&(objectClass=inetOrgPerson)(|$personFilter))",
            ).associate { e -> e.getUUID(ldap) to e.dn.toString() }

        val groupFilter =
            (group.groupMembers + group.ownerGroups).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")",
            )
        val groupUIDs =
            ldap.searchEntries(
                dn = ldap.baseDnGroups,
                filter = "(&(objectClass=$groupObjectClass)(|$groupFilter))",
            ).associate { e -> e.getUUID(ldap) to e.dn.toString() }

        val modifications = mutableListOf<Modification>()
        attributeModificationOperation(SchemaConstants.DESCRIPTION_AT, savedGroup.description, group.description)
            ?.also { modifications.add(it) }

        if (((group.userMembers + group.owners) != savedGroup.userMembers) ||
            ((group.groupMembers + group.ownerGroups) != savedGroup.groupMembers)
        ) {
            val uniqueMembers =
                (
                    group.userMembers.mapNotNull { personUIDs[it] } +
                        group.owners.mapNotNull { personUIDs[it] } +
                        group.groupMembers.mapNotNull { groupUIDs[it] } +
                        group.ownerGroups.mapNotNull { groupUIDs[it] } +
                        ldap.dummyAccountDn
                    ).toSet()
            modifications.add(
                DefaultModification(
                    ModificationOperation.REPLACE_ATTRIBUTE,
                    groupMemberAttribute,
                    *uniqueMembers.toTypedArray(),
                ),
            )
        }

        if ((group.owners != savedGroup.owners) ||
            (group.ownerGroups != savedGroup.ownerGroups)
        ) {
            val uniqueOwners =
                (
                    group.owners.mapNotNull { personUIDs[it] } +
                        group.ownerGroups.mapNotNull { groupUIDs[it] } +
                        ldap.dummyAccountDn
                    ).toSet()
            modifications.add(
                DefaultModification(
                    ModificationOperation.REPLACE_ATTRIBUTE,
                    SchemaConstants.OWNER_AT,
                    *uniqueOwners.toTypedArray(),
                ),
            )
        }

        if (modifications.isNotEmpty()) {
            ldap.modify("${ldap.rdnGroup}=${group.name},${ldap.baseDnGroups}", modifications)
        }

        // The order here matters
        // First remove all owner group stuff, if it is done vice versa, the member group is added again :(
        if (group.ownedGroups != savedGroup.ownedGroups) {
            group.ownedGroups.plus(savedGroup.ownedGroups).mapNotNull { getGroupById(it) }.forEach {
                if (group.ownedGroups.contains(it.externalId) && !savedGroup.ownedGroups.contains(it.externalId)) {
                    // add this group as owner to that other group
                    updateGroup(it.copy(ownerGroups = it.ownerGroups + group.externalId))
                } else if (!group.ownedGroups.contains(it.externalId) && savedGroup.ownedGroups.contains(it.externalId)) {
                    // remove this group as owner from that other group
                    updateGroup(it.copy(ownerGroups = it.ownerGroups - group.externalId))
                }
            }
        }

        if (group.parentGroups != savedGroup.parentGroups) {
            group.parentGroups.plus(savedGroup.parentGroups).mapNotNull { getGroupById(it) }.forEach {
                if (group.parentGroups.contains(it.externalId) && !savedGroup.parentGroups.contains(it.externalId)) {
                    // add this group as member to that other group
                    updateGroup(it.copy(groupMembers = it.groupMembers + group.externalId))
                } else if (!group.parentGroups.contains(it.externalId) && savedGroup.parentGroups.contains(it.externalId)) {
                    // remove this group as member from that other group
                    updateGroup(it.copy(groupMembers = it.groupMembers - group.externalId))
                }
            }
        }

        return group.copy(
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups,
        )
    }

    private fun attributeModificationOperation(
        attribute: String,
        oldValue: String?,
        newValue: String?,
    ): Modification? =
        when {
            oldValue == newValue -> null
            oldValue == null ->
                DefaultModification(
                    ModificationOperation.ADD_ATTRIBUTE,
                    attribute,
                    newValue,
                )
            newValue == null ->
                DefaultModification(
                    ModificationOperation.REMOVE_ATTRIBUTE,
                    attribute,
                )
            else -> DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attribute, newValue)
        }

    private fun attributeReplaceModificationOperation(
        attribute: String,
        newValue: Iterable<String>,
    ): Modification = DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attribute, *newValue.toList().toTypedArray())
}
