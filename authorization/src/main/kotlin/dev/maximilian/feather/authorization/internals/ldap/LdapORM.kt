/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.internals.ldap

import dev.maximilian.feather.authorization.ILdapProvider
import org.apache.directory.api.ldap.model.constants.SchemaConstants
import org.apache.directory.api.ldap.model.entry.Attribute
import org.apache.directory.api.ldap.model.entry.DefaultAttribute
import org.apache.directory.api.ldap.model.entry.DefaultEntry
import org.apache.directory.api.ldap.model.entry.Entry
import org.apache.directory.api.ldap.model.entry.Value
import org.apache.directory.api.ldap.model.name.Dn
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.KType
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor

internal object LdapORM {
    val ldapIsoFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss'Z'").withZone(ZoneOffset.UTC)

    fun toLdapEntry(
        entity: LdapEntity<*>,
        ldap: ILdapProvider,
    ): Entry =
        DefaultEntry(entity.dn).apply {
            val clazz = entity::class

            require(clazz.isData) { "Only data classes are supported" }
            require(entity.className(ldap) != null) { "This does not work on anonymous classes" }

            clazz.memberProperties.filter { it.name != "dn" && it.name != "rdn" }.forEach {
                val value = it.getter.call(entity)

                // TODO implement here more primitive types
                require(value == null || value is String || value is Iterable<*> || value is Instant) {
                    "${it.name} is ${it.returnType}, which is currently not supported"
                }

                if (value != null) {
                    if (value is String) {
                        add(DefaultAttribute(it.name, Value(value)))
                    } else if (value is Iterable<*> && value.firstOrNull() != null) {
                        require(value.all { e -> e is String }) { "Iterable values must be String" }

                        // Special handling :(
                        if (entity is GroupOfNames && ldap.uniqueGroups && it.name == "member") {
                            add(DefaultAttribute("uniqueMember", *value.map { e -> Value(e.toString()) }.toTypedArray()))
                        } else {
                            add(DefaultAttribute(it.name, *value.map { e -> Value(e.toString()) }.toTypedArray()))
                        }
                    } else if (value is Instant) {
                        add(DefaultAttribute(it.name, Value(ldapIsoFormatter.format(value))))
                    }
                }
            }

            add(DefaultAttribute(Dn(entity.dn).rdn.type, entity.rdn))
            add(DefaultAttribute(SchemaConstants.OBJECT_CLASS_AT, entity.className(ldap)))
        }

    fun <T : LdapEntity<T>> fromLdapEntry(
        clazz: KClass<out T>,
        ldap: ILdapProvider,
        entry: Entry,
    ): T {
        require(clazz.isData) { "Only data classes are supported" }
        val primaryConstructor = requireNotNull(clazz.primaryConstructor) { "Need a primary constructor" }
        val paramMap: Map<KParameter, Any?> =
            primaryConstructor.parameters.associate { parameter ->
                val name: String =
                    requireNotNull(parameter.name) { "Constructor parameters must be named" }.let {
                        // Special handling :(
                        if (it == "member" && ldap.uniqueGroups) {
                            "uniqueMember"
                        } else {
                            it
                        }
                    }

                when (name) {
                    "dn" -> {
                        parameter to entry.dn.toString()
                    }
                    "entryUUID" -> {
                        parameter to entry.getUUID(ldap)
                    }
                    "rdn" -> {
                        parameter to entry.dn.rdn.value
                    }
                    else -> {
                        val type: KType = parameter.type
                        val optional: Boolean = parameter.isOptional || type.isMarkedNullable
                        requireNotNull(parameter.kind.takeIf { it == KParameter.Kind.VALUE }) { "Parameter must be a value kind" }

                        val primitive: Any? =
                            when (requireNotNull(type.classifier as? KClass<*>) { "Type must have a class" }) {
                                String::class -> entry.get(name)?.requireSingle(entry.dn)?.first()?.string
                                Set::class -> entry.get(name)?.map { it.string }?.toSet() ?: emptySet<Nothing>()
                                MutableSet::class -> entry.get(name)?.map { it.string }?.toMutableSet()
                                Iterable::class -> entry.get(name)?.map { it.string }?.toList() ?: emptyList<Nothing>()
                                MutableIterable::class -> entry.get(name)?.map { it.string }?.toMutableList()
                                Instant::class ->
                                    entry.get(name)
                                        ?.let {
                                            LocalDateTime.parse(
                                                it.string,
                                                ldapIsoFormatter,
                                            ).toInstant(ZoneOffset.UTC)
                                        }
                                ByteArray::class -> {
                                    entry.get(name)?.bytes
                                }
                                // TODO implement here more primitive types
                                else -> null
                            }

                        require(optional || primitive != null) {
                            "$name is required but not provided by entry \"${entry.dn}\""
                        }

                        parameter to primitive
                    }
                }
            }

        return primaryConstructor.callBy(paramMap)
    }

    private fun Attribute.requireSingle(dn: Dn) =
        requireNotNull(takeIf { this.count() == 1 }) { "Attribute \"$id\" does not hold a single value by entry \"$dn\"" }
}

internal fun <T : LdapEntity<T>> LdapEntity<T>.toLdapEntry(ldap: ILdapProvider) = LdapORM.toLdapEntry(this, ldap)

internal fun <T : LdapEntity<T>> Entry.toEntity(
    clazz: KClass<out T>,
    ldap: ILdapProvider,
): T = LdapORM.fromLdapEntry(clazz, ldap, this)

internal inline fun <reified T : LdapEntity<T>> Entry.toEntity(ldap: ILdapProvider): T = this.toEntity(T::class, ldap)

internal fun Entry.getUUID(ldap: ILdapProvider): String =
    requireNotNull(this[ldap.uuidType]?.string) {
        "An entry must have an ${ldap.uuidType}."
    }
