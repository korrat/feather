/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.Permission
import org.apache.directory.api.ldap.model.constants.LdapConstants
import org.apache.directory.api.ldap.model.constants.SchemaConstants
import org.apache.directory.api.ldap.model.entry.Entry
import org.apache.directory.api.ldap.model.entry.Modification
import org.apache.directory.api.ldap.model.message.SearchScope
import java.io.Closeable

interface ILdapProvider : Closeable {
    val baseDn: String
    val baseDnUsers: String
    val baseDnGroups: String
    val baseDnPermissions: String

    val rdnUser: String
    val rdnGroup: String
    val uniqueGroups: Boolean
    val plainTextPasswords: Boolean

    val dummyAccountDn: String
        get() = "cn=dummy,$baseDn"

    val uuidType: String

    fun reconnect()

    fun getByUUID(
        uuid: String,
        baseDn: String,
        attributeList: Array<String> = SchemaConstants.ALL_ATTRIBUTES_ARRAY,
    ): Entry? = searchOneEntry(baseDn, scope = SearchScope.SUBTREE, filter = "($uuidType=$uuid)", attributeList = attributeList)

    fun searchOneEntry(
        dn: String,
        filter: String = LdapConstants.OBJECT_CLASS_STAR,
        scope: SearchScope = SearchScope.OBJECT,
        attributeList: Array<String> = SchemaConstants.ALL_ATTRIBUTES_ARRAY,
    ): Entry? = searchEntries(dn, filter, scope, attributeList).singleOrNull()

    fun searchEntries(
        dn: String = baseDn,
        filter: String = LdapConstants.OBJECT_CLASS_STAR,
        scope: SearchScope = SearchScope.SUBTREE,
        attribute: String,
    ): List<Entry> = searchEntries(dn, filter, scope, arrayOf(attribute))

    fun searchEntries(
        dn: String = baseDn,
        filter: String = LdapConstants.OBJECT_CLASS_STAR,
        scope: SearchScope = SearchScope.SUBTREE,
        attributeList: Array<String> = SchemaConstants.ALL_ATTRIBUTES_ARRAY,
    ): List<Entry>

    fun create(entry: Entry): Entry

    fun delete(entry: Entry)

    fun modify(
        dn: String,
        vararg modifications: Modification,
    )

    fun modify(
        dn: String,
        modifications: Collection<Modification>,
    ) = modify(dn, *(modifications.toTypedArray()))

    fun bind(
        dn: String,
        password: String,
    ): Boolean

    fun rename(
        dn: String,
        newRdnValue: String,
    )

    fun getUserPermissions(userDn: String): Set<Permission>
}
