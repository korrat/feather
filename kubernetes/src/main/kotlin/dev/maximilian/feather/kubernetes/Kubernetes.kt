package dev.maximilian.feather.kubernetes

import io.fabric8.kubernetes.client.KubernetesClient
import io.fabric8.kubernetes.client.KubernetesClientBuilder
import io.fabric8.kubernetes.client.readiness.Readiness
import mu.KotlinLogging
import java.io.ByteArrayOutputStream
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

public class Kubernetes : IKubernetes {
    private val logger = KotlinLogging.logger { }
    private var client: KubernetesClient? = null

    private fun getClient(): KubernetesClient =
        client ?: KubernetesClientBuilder().build().also {
            client = it
        }

    private inline fun <R> failOnErrorAndThrowAwayClient(block: () -> R): R =
        runCatching(block).onFailure {
            if (it !is IllegalArgumentException) {
                this@Kubernetes.client = null
            }
        }.getOrThrow()

    init {
        require(getClient().apiServices().list().items.isNotEmpty()) {
            "Cannot list kubernetes resources. Possibly misconfigured?"
        }
    }

    override fun getNamespaces(): List<Namespace> =
        failOnErrorAndThrowAwayClient {
            logger.debug { "Getting namespace list" }
            getClient().namespaces().list().items.map { Namespace(it.metadata.name) }
        }

    override fun getNamespaceByName(name: String): Namespace? =
        failOnErrorAndThrowAwayClient {
            logger.debug { "Retrieving namespace $name" }
            getClient().namespaces().withName(name).get()?.let { Namespace(it.metadata.name) }
        }

    override fun getDeployments(
        namespace: Namespace,
        withLabels: Map<String, String>,
    ): List<Deployment> =
        failOnErrorAndThrowAwayClient {
            logger.debug {
                "Retrieving list of deployment in namespace ${namespace.name} with labels ${
                    withLabels.entries.joinToString(prefix = "[", postfix = "]")
                } "
            }

            getClient().apps().deployments()
                .inNamespace(namespace.name)
                .withLabels(withLabels)
                .list()
                .items
                .map { Deployment(it.metadata.name, namespace) }
        }

    override fun getDeploymentByName(
        namespace: Namespace,
        name: String,
    ): Deployment? =
        failOnErrorAndThrowAwayClient {
            logger.debug { "Retrieving deployment $name in namespace ${namespace.name}" }

            getClient().apps().deployments()
                .inNamespace(namespace.name)
                .withName(name)
                .get()?.let { Deployment(it.metadata.name, namespace) }
        }

    override fun getReplicaSetsByDeployment(deployment: Deployment): List<ReplicaSet> =
        failOnErrorAndThrowAwayClient {
            logger.debug {
                "Retrieving list of replica sets from deployment ${deployment.name} in namespace ${deployment.namespace.name}"
            }

            getClient().apps().replicaSets().inNamespace(deployment.namespace.name).list().items.filter { rs ->
                rs.metadata.ownerReferences.any { ref -> ref.name == deployment.name }
            }.map {
                ReplicaSet(
                    it.metadata.name,
                    deployment.namespace,
                    deployment,
                    it.metadata.annotations["deployment.kubernetes.io/revision"]?.toIntOrNull() ?: 0,
                )
            }
        }

    override fun getPodsByReplicaSet(replicaSet: ReplicaSet): List<Pod> =
        failOnErrorAndThrowAwayClient {
            logger.debug {
                "Retrieving list of pods from replica set ${replicaSet.name} from deployment ${replicaSet.deployment.namespace} in namespace ${replicaSet.namespace.name}"
            }

            getClient().pods().inNamespace(replicaSet.namespace.name).list().items.filter { rs ->
                rs.metadata.ownerReferences.any { ref -> ref.name == replicaSet.name }
            }.map {
                Pod(
                    it.metadata.name,
                    replicaSet.namespace,
                    replicaSet,
                    replicaSet.deployment,
                    Readiness.isPodReady(it),
                    it.spec.containers.map { c -> c.name },
                )
            }
        }

    override fun executeCommand(
        pod: Pod,
        containerName: String?,
        commands: Array<String>,
        timeoutInSeconds: Long,
    ): CommandOutput {
        logger.debug {
            "Running command ${commands.joinToString()} in pod ${pod.name} from replica set ${pod.replicaSet.name} from deployment ${pod.deployment.namespace} in namespace ${pod.namespace.name}"
        }

        var failure = false
        var messages = arrayOf<String>()

        try {
            val baos = ByteArrayOutputStream()
            val data = CompletableFuture<String>()

            getClient().pods()
                .inNamespace(pod.namespace.name)
                .withName(pod.name)
                .inContainer(
                    containerName
                        ?: requireNotNull(pod.containers.firstOrNull()) { "This pod does not have a container and so, no command can be executed in" },
                )
                .writingOutput(baos)
                .writingError(baos)
                .usingListener(SimpleListener(data, baos))
                .exec(*commands)

            val output = data.get(timeoutInSeconds, TimeUnit.SECONDS)

            messages = arrayOf(output)
        } catch (e: ExecutionException) {
            failure = true
            e.message?.let {
                messages = arrayOf(it)
            }
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

        logger.debug {
            "Running command ${commands.joinToString()} in pod ${pod.name} from replica set ${pod.replicaSet.name} from deployment ${pod.deployment.namespace} in namespace ${pod.namespace.name} ${if (failure) "NOT " else ""}successful"
        }

        return CommandOutput(failure, messages.toList())
    }
}
