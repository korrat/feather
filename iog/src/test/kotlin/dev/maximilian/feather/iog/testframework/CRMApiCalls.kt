/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.BackgroundJobWithStrings
import dev.maximilian.feather.iog.api.bindings.IogGroupCreateRequest
import dev.maximilian.feather.iog.api.bindings.IogGroupDeleteRequest
import dev.maximilian.feather.multiservice.BackgroundJob
import dev.maximilian.feather.multiservice.BackgroundJobManager
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpRequestWithBody
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import org.eclipse.jetty.http.HttpStatus
import kotlin.test.assertEquals

internal class CRMApiCalls(private val apiTestUtilities: ApiTestUtilities, private val backgroundJobManager: BackgroundJobManager) {
    internal fun createGroupByApi(g: IogGroupCreateRequest): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .put("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/groups")
            .body(g)

    internal fun createCRMSystemBlocking(realInstallation: Boolean) {
        val resultWithJob = createCRMByApi(realInstallation).asObject<BackgroundJobWithStrings>()

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(resultWithJob.body.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)

        assertEquals(
            HttpStatus.CREATED_201,
            resultWithJob.status,
            "createCRMSystemBlocking($realInstallation) did not respond with created.",
        )
    }

    internal fun deleteCMRSystemBlocking() {
        val resultWithJob = deleteCRMByApi().asObject<BackgroundJobWithStrings>()

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(resultWithJob.body.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)

        assertEquals(HttpStatus.CREATED_201, resultWithJob.status, "deleteCRMSystemBlocking did not respond with created.")
    }

    private fun createCRMByApi(realInstallation: Boolean): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/crm")
            .body(realInstallation)

    private fun deleteCRMByApi(): HttpRequestWithBody =
        apiTestUtilities
            .restConnection
            .delete("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/crm")

    internal fun deleteGroupByApi(
        g: IogGroupDeleteRequest,
        id: Int,
    ): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .delete("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/groups/$id")
            .body(g)

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

    fun addUserToGroupByApi(
        groupID: Int,
        userID: Int,
    ): HttpResponse<Group> {
        return apiTestUtilities.restConnection.put(
            "${apiTestUtilities.scenario!!.basePath}/groups/$groupID/member",
        ).body(setOf(userID)).asObject()
    }

    fun deleteUserFromGroupByApi(
        groupID: Int,
        userID: Int,
    ): HttpResponse<Group> {
        return apiTestUtilities.restConnection.delete("${apiTestUtilities.scenario!!.basePath}/groups/$groupID/member/$userID").asObject()
    }
}
