/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.group

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.IogGroupCreateRequest
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.internal.settings.OPRoleNames
import dev.maximilian.feather.iog.internal.tools.PublicLinkConverter
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectPrincipalType
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateGroupTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val api = ApiTestUtilities()
    private var testUser: User?
    private val rgString = GroupKind.REGIONAL_GROUP.toString()

    private val subFolderList = listOf(NextcloudFolders.inputFolder, NextcloudFolders.groupInternal, NextcloudFolders.publicFolder)
    private val groupInternalList = NextcloudFolders.standardFolders

    init {
        api.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        api.createStandardGroups()
        runBlocking {
            api.scenario!!.opUtilities.createStandardProjects()
        }
        testUser = ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.ADMIN_USER.username)
    }

    @Test
    fun `Interested, Member, Reader, Node admin, Project admin role present in OP`() {
        val r = runBlocking { api.scenario!!.openProject!!.getRoles() }
        assertTrue(r.any { it.name == OPRoleNames.INTERESTED }, "Interested role is missing in OP.")
        assertTrue(r.any { it.name == OPRoleNames.MEMBER }, "Member role is missing in OP.")
        assertTrue(r.any { it.name == OPRoleNames.READER }, "Reader role is missing in OP.")
        assertTrue(r.any { it.name == OPRoleNames.NODE_ADMIN }, "Node admin role is missing in OP.")
        assertTrue(r.any { it.name == OPRoleNames.PROJECT_ADMIN }, "Project admin role is missing in OP.")
    }

    @Test
    fun `Create REGIONAL_GROUP without verification returns BAD REQUEST`() {
        val request = IogGroupCreateRequest("rg-test1", "RG Test1", rgString, setOf(testUser!!.id), false)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        val res = putCreateGroup(request).asEmpty()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
    }

    @Test
    fun `Create REGIONAL_GROUP without admin returns FORBIDDEN`() {
        val request = IogGroupCreateRequest("rg-test2", "RG Test2", rgString, setOf(testUser!!.id), false)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        api.loginWithStandardUser()
        val res = putCreateGroup(request).asEmpty()
        api.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, res.status)
    }

    @Test
    fun `Create WRONG_TYPE returns BAD_REQUEST`() {
        val request = IogGroupCreateRequest("rg-test3", "RG Test3", "WRONG_TYPE", setOf(testUser!!.id), true)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        val res = putCreateGroup(request).asEmpty()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
    }

    @Test
    fun `Create with empty description returns BAD_REQUEST`() {
        val request = IogGroupCreateRequest("rg-test4", "RG Test4", rgString, setOf(testUser!!.id), true)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        val res = putCreateGroup(request.copy(description = "")).asObject<List<String>>()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
        assertTrue(res.body.contains("Eine Beschreibung ist benötigt"), "Die Fehlerbeschreibung ist falsch.")
    }

    @Test
    fun `Create without node admin returns BAD_REQUEST`() {
        val request = IogGroupCreateRequest("rg-test5", "RG Test5", rgString, emptySet(), true)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        val res = putCreateGroup(request).asObject<List<String>>()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
    }

    @Test
    fun `Create with empty display name returns BAD_REQUEST and error message`() {
        val request = IogGroupCreateRequest("rg-test6", "RG Test6", rgString, setOf(testUser!!.id), true)
        api.removeMemberAdminInterested(
            request.displayName,
            request.description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        val res = putCreateGroup(request.copy(displayName = "")).asObject<List<String>>()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
        assertTrue(res.body.contains("Ein Anzeigename ist benötigt"), "Fehlerbeschreibung falsch.")
    }

    @Test
    fun `Create with capitalized description returns BAD_REQUEST`() {
        val res = putCreateGroup(IogGroupCreateRequest("RG-TEST7", "RG TEST7", rgString, setOf(testUser!!.id), true)).asEmpty()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
    }

    @Test
    fun `Create with UMLAUT display returns BAD_REQUEST`() {
        val res = putCreateGroup(IogGroupCreateRequest("rg-täst8", "RG Taest8", rgString, setOf(testUser!!.id), true)).asEmpty()
        assertEquals(HttpStatus.BAD_REQUEST_400, res.status)
    }

    @Test
    fun `Create regional group with UMLAUT description returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "rg-test9"
        val description = "RG TäüöÖÜÄst9"
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}"
        api.removeMemberAdminInterested(id, description, path)
        api.scenario!!.groupSyncMock.reset()
        val res =
            putCreateGroup(
                IogGroupCreateRequest(id, description, GroupKind.REGIONAL_GROUP.toString(), setOf(testUser!!.id), true),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val targetList = getMemberCrmTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        // test OP content
        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.REGIONAL_GROUP, op)

        // test for ldap sync calls
        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        // test for correct memberships in OP
        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.READER, "$id${IogPluginConstants.INTERESTED_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsMemberInterested(id, "$path/$description")
    }

    @Test
    fun `Create project returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "iog-tet12"
        val description = "IOG-TET12"
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}"
        api.removeMemberAdminInterested(id, description, path)
        api.scenario!!.groupSyncMock.reset()
        val r = putCreateGroup(IogGroupCreateRequest(id, description, GroupKind.PROJECT.toString(), setOf(testUser!!.id), true)).asEmpty()
        assertEquals(HttpStatus.CREATED_201, r.status)

        val targetList = getMemberInterestedTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.PROJECTS, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.READER, "$id${IogPluginConstants.INTERESTED_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.NODE_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.PROJECT_ADMIN, OPGroupNames.PROJECT_ADMINS),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsMemberInterested(id, "$path/$description")
    }

    @Test
    fun `Create competence group returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "kg-taest15"
        val description = "KG Täst15"
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.KG}"
        api.removeKG(id, description)
        api.scenario!!.groupSyncMock.reset()
        val r =
            putCreateGroup(
                IogGroupCreateRequest(id, description, GroupKind.COMPETENCE_GROUP.toString(), setOf(testUser!!.id), true),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, r.status)

        val targetList = getMemberAdminTrialTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.COMPETENCE_GROUP, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsSimple(id, "$path/$description")
    }

    @Test
    fun `Create PR-FR group returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "rg-test17"
        val descriptionParent = "RG Test17"
        val description = "RG Test17 (PR-FR)"
        api.removeMemberAdminInterested(
            "$id${IogPluginConstants.PR_FR_SUFFIX}",
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.PrFr}",
        )
        api.removeMemberAdminInterested(id, descriptionParent, "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}")
        putCreateGroup(IogGroupCreateRequest(id, descriptionParent, rgString, setOf(testUser!!.id), true)).asEmpty()
        api.scenario!!.groupSyncMock.reset()
        val res = putCreateGroup(IogGroupCreateRequest(id, description, GroupKind.PR_FR_GROUP.toString(), emptySet(), true)).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val targetList = getMemberCrmTargetList(id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, "$id${IogPluginConstants.PR_FR_SUFFIX}", description, OPPName.PR_FR, op)

        assertEquals(0, api.scenario!!.groupSyncMock.createCalls.size)

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.READER, "$id${IogPluginConstants.INTERESTED_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsMemberInterested(id, "${NextcloudFolders.GroupShare}/${NextcloudFolders.PrFr}/$description")
    }

    @Test
    fun `Create BILA returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "rg-test18"
        val descriptionParent = "RG Test18"
        val description = "RG Test18 (BiLa)"
        api.removeMemberAdminInterested(
            "$id${IogPluginConstants.BILA_SUFFIX}",
            description,
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.BiLa}",
        )
        api.removeMemberAdminInterested(id, descriptionParent, "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}")
        putCreateGroup(IogGroupCreateRequest(id, descriptionParent, rgString, setOf(testUser!!.id), true)).asEmpty()
        api.scenario!!.groupSyncMock.reset()
        val res = putCreateGroup(IogGroupCreateRequest(id, description, GroupKind.BILA_GROUP.toString(), emptySet(), true)).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)
        val targetList = getMemberCrmTargetList(id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, "$id${IogPluginConstants.BILA_SUFFIX}", description, OPPName.BILA, op)

        assertEquals(0, api.scenario!!.groupSyncMock.createCalls.size)

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.READER, "$id${IogPluginConstants.INTERESTED_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsMemberInterested(id, "${NextcloudFolders.GroupShare}/${NextcloudFolders.BiLa}/$description")
    }

    @Test
    fun `Create COMMITTEE returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "as-test"
        val description = "AS TEST"
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.KG}"
        api.removeKG(id, description)
        api.scenario!!.groupSyncMock.reset()
        val body = IogGroupCreateRequest(id, description, GroupKind.COMMITTEE.toString(), setOf(testUser!!.id), true)
        val res = putCreateGroup(body).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status, "Falscher Returncode. Body der Antwort: ${res.body}")

        val targetList = getMemberAdminTrialTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.COMMITTEE, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsSimple(id, "$path/$description")
    }

    @Test
    fun `Create COLLABORATION returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "suub"
        val description = "SUUB"
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.collaboration}"
        api.removeMemberAdminInterested(id, description, path)
        api.scenario!!.groupSyncMock.reset()
        val res =
            putCreateGroup(
                IogGroupCreateRequest(id, description, GroupKind.COLLABORATION.toString(), setOf(testUser!!.id), true),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val targetList = getSimpleTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.COLLABORATION, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.MEMBER, id),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsSimple(id, "$path/$description")
    }

    @Test
    fun `Create NO_SPECIALITY_MEMBER_ADMIN_SECRET returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "oms2"
        val description = "OMS2"
        val path = NextcloudFolders.GroupShare
        api.removeSimpleAdmin(id, description, path)
        api.scenario!!.groupSyncMock.reset()
        val res =
            putCreateGroup(
                IogGroupCreateRequest(id, description, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET.toString(), setOf(testUser!!.id), true),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val targetList = getSimpleTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.OTHER, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.INTERESTED, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.MEMBER, id),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsSimple(id, "$path/$description")
    }

    @Test
    fun `Create NO_SPECIALITY_MEMBER_ADMIN_PUBLIC returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "oms3"
        val description = "OMS3"
        val path = NextcloudFolders.GroupShare
        api.removeSimpleAdmin(id, description, NextcloudFolders.GroupShare)
        api.scenario!!.groupSyncMock.reset()
        val res =
            putCreateGroup(
                IogGroupCreateRequest(id, description, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC.toString(), setOf(testUser!!.id), true),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)

        val targetList = getSimpleTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.OTHER, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.MEMBER, id),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsSimple(id, "$path/$description")
    }

    @Test
    fun `Create NO_SPECIALITY_MEMBER_ADMIN_INTERESTED returns CREATED, creates OP project with memberships and ldap sync properly as well as NC folders`() {
        val id = "oms4"
        val description = "OMS4"
        val path = NextcloudFolders.GroupShare
        api.removeSimpleAdmin(id, description, path)
        api.scenario!!.groupSyncMock.reset()
        val res =
            putCreateGroup(
                IogGroupCreateRequest(
                    id,
                    description,
                    GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED.toString(),
                    setOf(testUser!!.id),
                    true,
                ),
            ).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)
        val targetList = getMemberInterestedTargetList(id)
        assertLDAPgroupsCreated(targetList, id)

        val op = api.scenario!!.openProject!!
        assertProjectEqual(targetList, id, description, OPPName.OTHER, op)

        assertEquals(1, api.scenario!!.groupSyncMock.createCalls.size)
        compareSyncCreateCalls(targetList, api.scenario!!.groupSyncMock.createCalls.first())

        val expectedMemberships =
            listOf(
                Pair(OPRoleNames.INTERESTED, OPGroupNames.INTERESTED_PEOPLE),
                Pair(OPRoleNames.READER, OPGroupNames.IOG_MEMBERS),
                Pair(OPRoleNames.READER, "$id${IogPluginConstants.INTERESTED_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.MEMBER_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, OPGroupNames.CENTRAL_OFFICE),
                Pair(OPRoleNames.PROJECT_ADMIN, "$id${IogPluginConstants.ADMIN_SUFFIX}"),
                Pair(OPRoleNames.MEMBER, "$id${IogPluginConstants.TRIAL_SUFFIX}"),
            ).sortedBy { it.second }

        val memberships = getMemberships(id, op)
        assertEquals(expectedMemberships, memberships)

        assertNCsettingsMemberInterested(id, "$path/$description")
    }

    @Test
    fun `Create LDAP_ONLY returns CREATED and creates ldap groups`() {
        val id = "oms5"
        val description = "OMS5"
        api.removeSimpleAdmin(id, description, NextcloudFolders.GroupShare)
        val res = putCreateGroup(IogGroupCreateRequest(id, description, GroupKind.LDAP_ONLY.toString(), emptySet(), true)).asEmpty()
        assertEquals(HttpStatus.CREATED_201, res.status)
        assertNotNull(ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(id), "The LDAP group $id was not created.")
    }

    private fun assertProjectEqual(
        targetList: List<String>,
        id: String,
        description: String,
        parent: String,
        op: IOpenProject,
    ) {
        targetList.onEach { runBlocking { op.getGroupByName(it)!! } }
        val p = runBlocking { op.getProjectByIdentifierOrName(id) }
        assertEquals(description, p!!.name, "OpenProject description is not correct.")
        assertEquals(id, p.identifier, "OpenProject identifier is not correct")
        if (parent == "") {
            assertNull(p.parent, "Parent of OpenProjectProject is set, but should not.")
        } else {
            assertEquals(parent, p.parent!!.value!!.identifier, "Parent of OpenProject-Project is not correct.")
        }
    }

    private fun getMemberships(
        id: String,
        op: IOpenProject,
    ): List<Pair<String, String>> {
        var q: List<Pair<String, String>>
        runBlocking {
            val roles = op.getRoles()
            q =
                op.getMemberships(roles)
                    .filter { it.project.value?.identifier == id && it.principal.type == OpenProjectPrincipalType.GROUP }
                    .map {
                        Pair(
                            it.roles.map { it.name }.toString().replace("[", "").replace("]", ""),
                            (it.principal.value as OpenProjectGroup).name,
                        )
                    }
                    .sortedBy { it.second }
        }
        return q
    }

    private fun compareSyncCreateCalls(
        targetList: List<String>,
        createCalls: GroupSyncMock.CreateCall,
    ) {
        val syncedGroupsLDAP = createCalls.createdLdapGroups.map { it.name }.sorted()
        val syncedGroupsOP = createCalls.newOpenProjectGroups.map { it.name }.sorted()
        assertEquals(targetList.sorted(), syncedGroupsLDAP, "Synchronized LDAP group names are not correct.")
        assertEquals(
            expected = targetList.sorted(),
            actual = syncedGroupsOP,
            message = "Synchronized OP group names are not correct.",
        )
    }

    private fun assertFolderStructureExists(path: String) {
        val nc = api.scenario!!.nextcloud!!
        val folders = nc.listFolders(path)
        subFolderList.onEach { assertTrue(folders.contains(it), "The subfolder $it is not contained in $path on Nextcloud.") }
        val files = nc.listFiles(path)
        assertTrue(files.contains("Readme.md"), "Readme.md does not exist in $path on Nextcloud.")
        val content = nc.getFileContent("$path/Readme.md")
        val subfolders = nc.listFolders("$path/${NextcloudFolders.groupInternal}")
        groupInternalList.onEach {
            assertTrue(subfolders.contains(it), "Folder $it is not contained in $path/${NextcloudFolders.groupInternal} on Nextcloud.")
        }
        val myshare = nc.getAllShares().find { it.path == "/$path/Eingang" }
        val converter = PublicLinkConverter(api.nextcloudSettings._publicUrl)
        val newUrl = converter.convert(myshare!!.url!!)
        assertTrue(
            content.endsWith("Eingang: <$newUrl>"),
            "Content of the README.md file does not end with \"Eingang: $newUrl\". Content: \"$content\".",
        )
        assertNotNull(myshare, "Share for input folder $path/Eingang was not created!")
        assertNull(myshare.expiration, "Share has expiration date set.")
        assertEquals(ShareType.PublicLink, myshare.shareType)
        assertEquals(setOf(PermissionType.Create), myshare.permissions)
        assertEquals("folder", myshare.itemType)
    }

    private fun assertNCpermissionSet(membersToAdd: List<String>) {
        val vvv = api.scenario!!.nextcloud!!.findGroupFolder(NextcloudFolders.GroupShare)
        membersToAdd.forEach {
            assertTrue(vvv!!.groups!!.containsKey(it), "$it was not added to GroupFolders.")
            val permissions = vvv.groups!![it]!!
            assertEquals(
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                permissions,
                "Groupfolder permission for $it is not set to readwrite permission.",
            )
        }
    }

    private fun putCreateGroup(req: IogGroupCreateRequest): RequestBodyEntity {
        return api
            .restConnection
            .put("${api.scenario!!.basePath}/bindings/multiservice/groups").body(req)
    }

    private fun assertLDAPgroupsCreated(
        targetList: List<String>,
        id: String,
    ) {
        targetList.onEach { assertNotNull(ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(it), "The LDAP group $it was not created.") }
        assertTrue(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName("$id${IogPluginConstants.ADMIN_SUFFIX}")!!.userMembers.contains(testUser!!.id),
            "Created LDAPgroups $id${IogPluginConstants.ADMIN_SUFFIX} does not contain test user.",
        )
    }

    private fun assertNCsettingsMemberInterested(
        id: String,
        path: String,
    ) {
        assertNCpermissionSet(
            listOf(
                "$id${IogPluginConstants.MEMBER_SUFFIX}",
                "$id${IogPluginConstants.INTERESTED_SUFFIX}",
                "$id${IogPluginConstants.TRIAL_SUFFIX}",
            ),
        )
        assertFolderStructureExists(path)
    }

    private fun assertNCsettingsSimple(
        id: String,
        path: String,
    ) {
        assertNCpermissionSet(listOf(id))
        assertFolderStructureExists(path)
    }

    private fun getMemberInterestedTargetList(id: String): List<String> =
        listOf(
            id,
            "$id${IogPluginConstants.MEMBER_SUFFIX}",
            "$id${IogPluginConstants.ADMIN_SUFFIX}",
            "$id${IogPluginConstants.INTERESTED_SUFFIX}",
            "$id${IogPluginConstants.TRIAL_SUFFIX}",
        )

    private fun getMemberCrmTargetList(id: String): List<String> =
        listOf(
            id,
            "$id${IogPluginConstants.MEMBER_SUFFIX}",
            "$id${IogPluginConstants.ADMIN_SUFFIX}",
            "$id${IogPluginConstants.CRM_SUFFIX}",
            "$id${IogPluginConstants.INTERESTED_SUFFIX}",
            "$id${IogPluginConstants.TRIAL_SUFFIX}",
        )

    private fun getSimpleTargetList(id: String): List<String> = listOf(id, "$id${IogPluginConstants.ADMIN_SUFFIX}")

    private fun getMemberAdminTrialTargetList(id: String): List<String> =
        listOf(
            id,
            "$id${IogPluginConstants.ADMIN_SUFFIX}",
            "$id${IogPluginConstants.MEMBER_SUFFIX}",
            "$id${IogPluginConstants.TRIAL_SUFFIX}",
        )

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
