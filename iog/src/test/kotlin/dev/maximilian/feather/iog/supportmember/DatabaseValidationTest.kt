/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.supportmember

import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import kotlin.test.Test
import kotlin.test.assertEquals

class DatabaseValidationTest {
    @Test
    fun `Test validate accepts 5 correct support members`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail2@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail4@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    5,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.OK, result)
    }

    @Test
    fun `Test validate accepts 5 members with unique mail adresses in different fields`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash(""),
                    dv.makeHash("mail2@"),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail4@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    5,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.OK, result)
    }

    @Test
    fun `Test validate rejects updates with less then 5 support member`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail2@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail4@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.NOT_ENOUGH_SUPPORT_MEMBER, result)
    }

    @Test
    fun `Test validate detects duplicate external id`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail2@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail4@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.EXTERNAL_ID_NOT_UNIQUE, result)
    }

    @Test
    fun `Test validate detects not enough unique mail adresses`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    5,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.NOT_ENOUGH_UNIQUE_MAIL_ADDRESS, result)
    }

    @Test
    fun `Test detects invalid string length in name entry`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(1, "blabala1", dv.makeHash("mail1@"), dv.makeHash("")),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.NAME_HASH_SIZE_INVALID, result)
    }

    @Test
    fun `Test validate detects empty name hash`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(1, dv.makeHash(""), dv.makeHash("mail1@"), dv.makeHash("")),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.NAME_HASH_EMPTY, result)
    }

    @Test
    fun `Test validate detects invalid string length in first mail entry`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(1, dv.makeHash("blabala1"), "mail1@", dv.makeHash("")),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.MAIL_HASH_SIZE_INVALID, result)
    }

    @Test
    fun `Test validate detects invalid string length in second mail entry `() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(1, dv.makeHash("blabala1"), dv.makeHash("mail1@"), "asdasd"),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail1@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.MAIL_HASH_SIZE_INVALID, result)
    }

    @Test
    fun `Test validate accepts two empty email addresses`() {
        val dv = DatabaseValidation()
        val q =
            mutableListOf(
                SupportMemberHashRow(
                    1,
                    dv.makeHash("blabala1"),
                    dv.makeHash(""),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    2,
                    dv.makeHash("blabala2"),
                    dv.makeHash("mail2@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    3,
                    dv.makeHash("blabala3"),
                    dv.makeHash("mail3@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    4,
                    dv.makeHash("blabala4"),
                    dv.makeHash("mail4@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    5,
                    dv.makeHash("blabala5"),
                    dv.makeHash("mail5@"),
                    dv.makeHash(""),
                ),
                SupportMemberHashRow(
                    6,
                    dv.makeHash("blabala6"),
                    dv.makeHash("mail6@"),
                    dv.makeHash(""),
                ),
            )
        val result = dv.validate(q.toList())
        assertEquals(DatabaseValidation.ValidationResult.OK, result)
    }
}
