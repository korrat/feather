/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.chapter

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.BackgroundJobWithStrings
import dev.maximilian.feather.iog.api.bindings.ChapterDetails
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJob
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateChapterDBTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val api = ApiTestUtilities()
    private var testUser: User?
    private val onc = OPNameConfig(mutableMapOf())

    init {
        api.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager)
        api.createStandardGroups()
        runBlocking {
            api.scenario!!.opUtilities.createStandardProjects()
        }
        testUser = ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.ADMIN_USER.username)
        api.createRGTest()
        api.createProject("deu-iog55", "DEU-IOG55")
        api.loginWithAdminUser()
        createChapterDBblocking()
    }

    @Test
    fun `Create CHAPTERS without admin returns FORBIDDEN`() {
        api.loginWithStandardUser()
        val res = createChapterDB().asEmpty()
        api.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, res.status)
    }

    @Test
    fun `Create CHAPTER DB detects Regional group correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName("rg-test")
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                "rg-test",
                IogGroupSchema.PatternTypes.MemberCrmInterested,
                GroupKind.REGIONAL_GROUP,
                "IOG/RG-Intern/RG Test",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                onc.regionalGroupParentProject,
                "rg-test",
                "RG Test",
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create CHAPTER DB detects proko correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName(LdapNames.PROKO)
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                LdapNames.PROKO,
                IogGroupSchema.PatternTypes.SimpleMemberAdmin,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                "",
                emptySet(),
                LdapNames.CENTRAL_OFFICE,
                LdapNames.PROKO,
                "PROKO",
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create CHAPTER DB detects project group correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName("deu-iog55")
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                "deu-iog55",
                IogGroupSchema.PatternTypes.MemberAdminInterested,
                GroupKind.PROJECT,
                "IOG/Projekte/DEU-IOG55",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                onc.projectParentProject,
                "deu-iog55",
                "DEU-IOG55",
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create CHAPTER DB detects OMS correctly`() {
        api.loginWithAdminUser()

        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName(LdapNames.OMS)
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                LdapNames.OMS,
                IogGroupSchema.PatternTypes.SimpleMemberAdmin,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                "${NextcloudFolders.GroupShare}/${NextcloudFolders.OMS}",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                "ingenieure-ohne-grenzen",
                LdapNames.OMS,
                NextcloudFolders.OMS,
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create Chapter DB detects geschaeftstelle correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName(LdapNames.CENTRAL_OFFICE)
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                LdapNames.CENTRAL_OFFICE,
                IogGroupSchema.PatternTypes.SimpleMemberAdmin,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                "IOG/${NextcloudFolders.CentralOffice}",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                "ingenieure-ohne-grenzen",
                LdapNames.CENTRAL_OFFICE,
                NextcloudFolders.CentralOffice,
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create CHAPTER DB detects Vorstand correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName(LdapNames.ASSOCIATION_BOARD)

        assertEquals(
            ChapterDetails(
                detail!!.uid,
                LdapNames.ASSOCIATION_BOARD,
                IogGroupSchema.PatternTypes.SimpleMemberAdmin,
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                "IOG/${NextcloudFolders.AssociationBoard}",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                "ingenieure-ohne-grenzen",
                LdapNames.ASSOCIATION_BOARD,
                NextcloudFolders.AssociationBoard,
                false,
            ),
            detail,
        )
    }

    @Test
    fun `Create CHAPTER DB detects AP-SprecherInnen correctly`() {
        api.loginWithAdminUser()
        val detail = api.scenario!!.iogPlugin!!.chapterDB.getChapterByName(LdapNames.AP_SPRECHERINNEN)
        assertEquals(
            ChapterDetails(
                detail!!.uid,
                LdapNames.AP_SPRECHERINNEN,
                IogGroupSchema.PatternTypes.SimpleMemberAdmin,
                GroupKind.COLLABORATION,
                "IOG/Projektübergreifende Zusammenarbeit/${NextcloudFolders.APSprecherInnen}",
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
                onc.collaborationParentProject,
                LdapNames.AP_SPRECHERINNEN,
                NextcloudFolders.APSprecherInnen,
                false,
            ),
            detail,
        )
    }

    private fun createChapterDB(): RequestBodyEntity {
        return api
            .restConnection
            .post("${api.scenario!!.basePath}/bindings/iog/chapter/db").body(false)
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

    private fun createChapterDBblocking() {
        val resultWithJob = createChapterDB().asObject<BackgroundJobWithStrings>()

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(resultWithJob.body.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)

        assertEquals(HttpStatus.CREATED_201, resultWithJob.status)
    }
}
