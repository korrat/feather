/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.iog.internal.group.plausibility.SelfCheckingFileSystem
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType

internal class NCUtilities(val nextcloud: Nextcloud) {
    internal fun addGroupToGroupFolder(name: String) {
        var mainGroupFolder = nextcloud.findGroupFolder(NextcloudFolders.GroupShare)
        if (mainGroupFolder == null) {
            nextcloud.createGroupFolder(
                NextcloudFolders.GroupShare,
                1000,
                OPGroupNames.INTERESTED_PEOPLE,
                permissions = PermissionType.values().toSet(),
            )
            mainGroupFolder = nextcloud.findGroupFolder(NextcloudFolders.GroupShare)
        }
        if (name == LdapNames.INTERESTED_PEOPLE) {
            if (!nextcloud.getAllGroups().contains(name)) {
                nextcloud.addGroupToGroupFolder(mainGroupFolder!!, name, PermissionType.values().toSet())
            }
        } else if (!nextcloud.getAllGroups().contains(name)) {
            nextcloud.addGroupToGroupFolder(
                mainGroupFolder!!,
                name,
                setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete),
            )
        }
    }

    private fun createFoldersIfNotExists(
        path: String,
        folders: List<String>,
    ) {
        val parentFolders = nextcloud.listFolders(path)
        folders.forEach {
            if (!parentFolders.contains(it)) {
                nextcloud.createFolder("$path/$it")
            }
        }
    }

    fun createStandardFolders() {
        val mainFolder = nextcloud.listFolders("")
        if (!mainFolder.contains(NextcloudFolders.GroupShare)) {
            nextcloud.createFolder(NextcloudFolders.GroupShare)
        }

        val stdStructure =
            mutableListOf(
                NextcloudFolders.OMS,
                NextcloudFolders.CentralOffice,
                NextcloudFolders.AssociationBoard,
            )
        createFoldersIfNotExists(
            NextcloudFolders.GroupShare,
            listOf(
                NextcloudFolders.RgIntern,
                NextcloudFolders.BiLa,
                NextcloudFolders.KG,
                NextcloudFolders.Projects,
                NextcloudFolders.PrFr,
                NextcloudFolders.collaboration,
            ).plus(stdStructure),
        )
        createFoldersIfNotExists(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}",
            listOf(NextcloudFolders.template),
        )
        createFoldersIfNotExists(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.collaboration}",
            listOf(NextcloudFolders.APSprecherInnen),
        )
        stdStructure.add("${NextcloudFolders.Projects}/${NextcloudFolders.template}")
        stdStructure.add("${NextcloudFolders.collaboration}/${NextcloudFolders.APSprecherInnen}")
        stdStructure.forEach { createFolderStructureIfNecessary(NextcloudFolders.GroupShare, it) }
    }

    fun removeFolderIfExists(
        path: String,
        folder: String,
    ) {
        try {
            val q = nextcloud.listFolders(path)
            if (q.contains(folder)) {
                nextcloud.delete("$path/$folder")
            }
        } catch (ex: Exception) {
        }
    }

    fun reset() {
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.RgIntern)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.KG)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.PrFr)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.BiLa)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.Projects)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.OMS)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.CentralOffice)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.AssociationBoard)
        removeFolderIfExists(NextcloudFolders.GroupShare, NextcloudFolders.collaboration)
    }

    fun createFolderStructureIfNecessary(
        path: String,
        description: String,
    ) {
        val nfc = SelfCheckingFileSystem(nextcloud)

        nfc.createFolderRecursive("$path/$description/${NextcloudFolders.publicFolder}")
        nfc.createFolderRecursive("$path/$description/${NextcloudFolders.groupInternal}")
        nfc.createFolderRecursive("$path/$description/${NextcloudFolders.inputFolder}")

        NextcloudFolders.standardFolders.forEach {
            nfc.createFolderRecursive("$path/$description/${NextcloudFolders.groupInternal}/$it")
        }

        val files = nextcloud.listFiles("$path/$description")
        if (!files.contains("Readme.md")) {
            nextcloud.createFile("$path/$description/Readme.md", "Dies ist ein Eingangsordner von $description")
        }
    }

    fun removeGeneralTrialMembers() {
        nextcloud.removeGroupFromGroupFolder(
            nextcloud.findGroupFolder(NextcloudFolders.GroupShare)!!,
            LdapNames.TRIAL_MEMBERS,
        )
    }

    fun addGeneralTrialMembers() {
        nextcloud.addGroupToGroupFolder(
            nextcloud.findGroupFolder(NextcloudFolders.GroupShare)!!,
            LdapNames.TRIAL_MEMBERS,
            PermissionType.values().toSet(),
        )
    }
}
