/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.civicrm

import dev.maximilian.feather.iog.api.bindings.IogGroupCreateRequest
import dev.maximilian.feather.iog.api.bindings.IogGroupDeleteRequest
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.CRMApiCalls
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeleteGroupCiviCRMTest {
    @Test
    fun `Delete group also delete CiviCRM group and acl`() {
        val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
        val apiTestUtilities = ApiTestUtilities()

        val specialGroupName = "rg-schnickschnackb"
        val specialGroupNameCRM = "$specialGroupName-crm"
        apiTestUtilities.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }

        apiTestUtilities.loginWithAdminUser()
        val crm = CRMApiCalls(apiTestUtilities, backgroundJobManager)

        val testUser =
            ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.NORMAL_USER.username)
                ?: ServiceConfig.CREDENTIAL_PROVIDER.createUser(TestUser.NORMAL_USER)

        val g =
            IogGroupCreateRequest(
                specialGroupName,
                "SuperGroupB",
                GroupKind.REGIONAL_GROUP.name,
                setOf(testUser.id),
                true,
            )
        val createGroupResponse = crm.createGroupByApi(g).asObject<String>()
        assertEquals(
            HttpStatus.CREATED_201,
            createGroupResponse.status,
            "Wrong status code of createGroup. Body: ${createGroupResponse.body}",
        )

        val t = ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(specialGroupName)
        assertNotNull(t, "$specialGroupName was not created in ldap")
        val d = IogGroupDeleteRequest(TestUser.ADMIN_USER_PASSWORD, true)
        val response = crm.deleteGroupByApi(d, t.id).asObject<String>()
        assertEquals(HttpStatus.CREATED_201, response.status, "Wrong status code of delete group. Body: ${response.body}")

        runBlocking {
            val groups = apiTestUtilities.scenario?.civiCRM?.civicrm?.getGroups()
            assertFalse(groups!!.any { it.name == specialGroupNameCRM }, "$specialGroupNameCRM was not deleted in CIVICRM")
            val aclEntities = apiTestUtilities.scenario?.civiCRM?.civicrm?.getACLs()!!
            assertFalse(
                aclEntities.any { it.name.contains(specialGroupNameCRM) },
                "ACL for $specialGroupNameCRM was not deleted in CIVICRM",
            )
        }
        crm.deleteCMRSystemBlocking()
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
