/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.trial

import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.TrialDecision
import dev.maximilian.feather.iog.api.bindings.TrialRequest
import dev.maximilian.feather.iog.api.bindings.TrialRequestResponse
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.GetRequest
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpRequestWithBody
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import java.time.Instant
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TrialMemberTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER

    @Test
    fun `GET trial requests returns for standard user FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = getAllTrials(apiTestUtilities).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `GET trial requests returns for ADMIN OK (200)`() {
        apiTestUtilities.loginWithAdminUser()
        val response = getAllTrials(apiTestUtilities).asEmpty()
        assertEquals(HttpStatus.OK_200, response.status)
    }

    @Test
    fun `accept trial for member`() {
        apiTestUtilities.loginWithAdminUser()
        val response = getAllTrials(apiTestUtilities).asEmpty()
        assertEquals(HttpStatus.OK_200, response.status)
    }

    @Test
    fun `POST requests trial return for standard User NO_CONTENT (204)`() {
        apiTestUtilities.loginWithStandardUser()

        val response = requestTrials(apiTestUtilities).asEmpty()
        val password = UUID.randomUUID().toString()
        val user = ServiceConfig.ACCOUNT_CONTROLLER.createUser(TestUser.generateTestUser(), password)
        apiTestUtilities.loginWithUser(user.username, password)
        processTrial(apiTestUtilities, user.id, false).asEmpty()
        assertEquals(HttpStatus.NO_CONTENT_204, response.status)
    }

    @Test
    fun `GET trial requests returns correct entries list for admin`() {
        apiTestUtilities.loginWithStandardUser()
        apiTestUtilities.scenario!!.iogPlugin!!.trial!!.deleteAll()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        val response = getAllTrials(apiTestUtilities).asObject<TrialRequestResponse>()
        val id = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        processTrial(apiTestUtilities, id, false).asEmpty()
        assertEquals(1, response.body.openRequests.size)
        assertEquals(
            TrialRequest(id, TestUser.NORMAL_USER.username, TestUser.NORMAL_USER.displayName, 0, response.body.openRequests.first().started),
            response.body.openRequests.first(),
        )
        val q = response.body.openRequests.first().started
        assertTrue(Instant.now().isAfter(q))
        assertTrue(Instant.now().minusSeconds(60).isBefore(q))
    }

    @Test
    fun `GET trial requests returns correct entries list for node admin`() {
        apiTestUtilities.loginWithStandardUser()
        apiTestUtilities.createRGTest()
        val std = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!
        credentials.updateUser(
            credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.copy(
                groups = setOf(credentials.getGroupByName("rg-test-interested")!!.id),
            ),
        )
        val nodeAdmin =
            credentials.createUser(
                User(
                    0,
                    "rg.admin.test",
                    "Admin Test",
                    "RG",
                    "Admin",
                    "rg@admin.test",
                    setOf(credentials.getGroupByName("rg-test-admin")!!.id),
                    Instant.now(),
                    setOf(),
                    emptySet(),
                    false,
                    lastLoginAt = Instant.ofEpochSecond(0),
                ),
                "secret",
            )
        credentials.updateGroup(credentials.getGroupByName("rg-test-interested")!!.copy(userMembers = setOf(std.id)))
        apiTestUtilities.scenario!!.iogPlugin!!.trial!!.deleteAll()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithUser("rg.admin.test", "secret")
        val response = getAllTrials(apiTestUtilities).asObject<TrialRequestResponse>()
        processTrial(apiTestUtilities, std.id, false).asEmpty()
        credentials.deleteUser(nodeAdmin)
        apiTestUtilities.removeRGTest()
        assertEquals(1, response.body.openRequests.size)
        val q = response.body.openRequests.first().started
        assertTrue(Instant.now().isAfter(q))
        assertTrue(Instant.now().minusSeconds(60).isBefore(q))
    }

    @Test
    fun `Node admin can accept trials of interested`() {
        apiTestUtilities.loginWithStandardUser()
        apiTestUtilities.createRGTest()
        val std = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!
        credentials.updateUser(
            credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.copy(
                groups = setOf(credentials.getGroupByName("rg-test-interested")!!.id),
            ),
        )
        val nodeAdmin =
            credentials.createUser(
                User(
                    0,
                    "rg.admin.test.2",
                    "Admin Test",
                    "RG",
                    "Admin",
                    "rg.admin.test.2@example.org",
                    setOf(credentials.getGroupByName("rg-test-admin")!!.id),
                    Instant.now(),
                    setOf(),
                    emptySet(),
                    false,
                    lastLoginAt = Instant.ofEpochSecond(0),
                ),
                "secret",
            )
        credentials.updateGroup(credentials.getGroupByName("rg-test-interested")!!.copy(userMembers = setOf(std.id)))
        apiTestUtilities.scenario!!.iogPlugin!!.trial!!.deleteAll()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithUser("rg.admin.test.2", "secret")
        val response = processTrial(apiTestUtilities, std.id, true).asEmpty()
        credentials.deleteUser(nodeAdmin)
        apiTestUtilities.removeRGTest()
        assertEquals(HttpStatus.NO_CONTENT_204, response.status)
    }

    @Test
    fun `Member cannot accept trials of interested`() {
        apiTestUtilities.loginWithStandardUser()
        apiTestUtilities.createRGTest()
        val std = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!
        credentials.updateUser(
            credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.copy(
                groups = setOf(credentials.getGroupByName("rg-test-interested")!!.id),
            ),
        )
        val noNodeAdmin =
            credentials.createUser(
                User(
                    0,
                    "rg.member.test",
                    "Member Test",
                    "RG",
                    "No Admin",
                    "rg@example.org",
                    setOf(credentials.getGroupByName("rg-test-member")!!.id),
                    Instant.now(),
                    setOf(),
                    emptySet(),
                    false,
                    lastLoginAt = Instant.ofEpochSecond(0),
                ),
                "secret",
            )
        credentials.updateGroup(credentials.getGroupByName("rg-test-interested")!!.copy(userMembers = setOf(std.id)))
        apiTestUtilities.scenario!!.iogPlugin!!.trial!!.deleteAll()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithUser("rg.member.test", "secret")
        val response = processTrial(apiTestUtilities, std.id, true).asEmpty()
        credentials.deleteUser(noNodeAdmin)
        apiTestUtilities.removeRGTest()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `REJECT trial returns for ADMIN NO_CONTENT (204)`() {
        apiTestUtilities.loginWithAdminUser()

        apiTestUtilities.loginWithStandardUser()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        val stdID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        val response = processTrial(apiTestUtilities, stdID, false).asEmpty()
        assertEquals(HttpStatus.NO_CONTENT_204, response.status)
    }

    @Test
    fun `REJECT trial for invalid ldap ID returns NOT_FOUND (404)`() {
        apiTestUtilities.loginWithStandardUser()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        val response = processTrial(apiTestUtilities, -42, false).asEmpty()
        assertEquals(HttpStatus.NOT_FOUND_404, response.status)
    }

    @Test
    fun `REJECT trial for standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        requestTrials(apiTestUtilities).asEmpty()
        val stdID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        val response = processTrial(apiTestUtilities, stdID, false).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `REJECT trial remove request from list`() {
        apiTestUtilities.loginWithStandardUser()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        val stdID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        processTrial(apiTestUtilities, stdID, false).asEmpty()
        val response = getAllTrials(apiTestUtilities).asObject<TrialRequestResponse>()
        assertEquals(0, response.body.openRequests.size)
    }

    @Test
    fun `ACCEPT trial removes request from list`() {
        apiTestUtilities.loginWithStandardUser()
        requestTrials(apiTestUtilities).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        val stdID = credentials.getUserByUsername(TestUser.NORMAL_USER.username)!!.id
        processTrial(apiTestUtilities, stdID, true).asEmpty()
        val response = getAllTrials(apiTestUtilities).asObject<TrialRequestResponse>()
        assertEquals(0, response.body.openRequests.size)
    }

    private fun getAllTrials(apiTestUtilities: ApiTestUtilities): GetRequest {
        return apiTestUtilities
            .restConnection
            .get("${apiTestUtilities.scenario!!.basePath}/bindings/iog/users/getAllTrialRequests")
    }

    private fun requestTrials(apiTestUtilities: ApiTestUtilities): HttpRequestWithBody {
        return apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/users/requestTrial")
    }

    private fun processTrial(
        apiTestUtilities: ApiTestUtilities,
        id: Int,
        decision: Boolean,
    ): RequestBodyEntity {
        return apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/users/processTrialRequest").body(
                TrialDecision(
                    listOf(id),
                    decision,
                ),
            )
    }

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
