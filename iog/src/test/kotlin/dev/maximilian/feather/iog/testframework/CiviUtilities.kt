/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.iog.internal.chapter.CiviGroupScheme
import dev.maximilian.feather.iog.internal.chapter.DeleteCrmGroups
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import kotlinx.coroutines.runBlocking
import kotlin.test.assertNotNull

class CiviUtilities(private val civicrm: ICiviCRM, private val credentials: ICredentialProvider) {
    private val groupScheme = CiviGroupScheme(credentials, civicrm)

    fun reset() {
        runBlocking {
            DeleteCrmGroups.deleteCivi(civicrm)
        }
    }

    internal fun createStandardGroups() {
        groupScheme.createStandardGroups()
    }

    fun createChapterWithRolesAndSubgroups(name: String) {
        runBlocking {
            val userGroupId = civicrm.getGroupByName(CiviCRMNames.USER_GROUP_NAME)?.id
            assertNotNull(userGroupId, "User group ${CiviCRMNames.USER_GROUP_NAME} not found")
            val credentialGroup = credentials.getGroupByName(name)
            assertNotNull(credentialGroup, "credentialGroup $name not found")
            val personContacts = civicrm.getGroupByName(CiviCRMNames.PERSON_GROUP_NAME)?.id
            assertNotNull(personContacts, "Person contacts ${CiviCRMNames.PERSON_GROUP_NAME} not found")
            val orgaContacts = civicrm.getGroupByName(CiviCRMNames.ORGANIZATION_GROUP_NAME)?.id
            assertNotNull(orgaContacts, "Orga contacts ${CiviCRMNames.ORGANIZATION_GROUP_NAME} not found")
            groupScheme.createSingleCiviChapter(credentialGroup, userGroupId, orgaContacts, personContacts)
        }
    }

    fun removeChapter(groupName: String) {
        groupScheme.deleteChapter(groupName)
    }
}
