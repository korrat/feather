/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.civicrm

import dev.maximilian.feather.civicrm.entities.ACL
import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.entities.OptionValue
import dev.maximilian.feather.civicrm.internal.civicrm.ACLProtocol
import dev.maximilian.feather.iog.api.bindings.IogGroupCreateRequest
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.CRMApiCalls
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateGroupCiviCRMTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()
    private var createGroupResponse: HttpResponse<String>? = null
    private val rgGroupName = "rg-schnickschnackc"
    private val rgGroupNameCRM = "$rgGroupName-crm"
    private var civiGroup: List<Group> = emptyList()
    private var optionValues: List<OptionValue> = emptyList()
    private var aclEntityRoles: List<ACLEntityRole> = emptyList()
    private var aclEntities = emptyList<ACL>()

    init {
        apiTestUtilities.startIogPlugin(admin = true, backgroundJobManager = backgroundJobManager, true)
        val crm = CRMApiCalls(apiTestUtilities, backgroundJobManager)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
        apiTestUtilities.createRGTest()
        apiTestUtilities.createProject("deu-iog66", "DEU-IOG66")
        apiTestUtilities.loginWithAdminUser()
        ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName("rg-test-crm")
            ?.let { ServiceConfig.CREDENTIAL_PROVIDER.deleteGroup(it) }

        crm.deleteCMRSystemBlocking()
        assertPreconditionsForCiviCRM()
        crm.createCRMSystemBlocking(true)

        val testUser =
            ServiceConfig.CREDENTIAL_PROVIDER.getUserByUsername(TestUser.NORMAL_USER.username)
                ?: ServiceConfig.CREDENTIAL_PROVIDER.createUser(TestUser.NORMAL_USER)

        val g =
            IogGroupCreateRequest(
                rgGroupName,
                "SuperGroup",
                GroupKind.REGIONAL_GROUP.name,
                setOf(testUser.id),
                true,
            )
        createGroupResponse = crm.createGroupByApi(g).asObject<String>()

        runBlocking {
            apiTestUtilities.scenario?.civiCRM?.civicrm!!.let {
                    civi ->
                val cg = civi.getGroups()
                assertNotNull(cg, "CiviCRM::getGroups returned null")
                civiGroup = cg

                val ov = civi.getOptionValues()
                assertNotNull(ov, "CiviCRM::getOptionValues returned null")
                optionValues = ov

                val aclER = civi.getACLEntityRoles()
                assertNotNull(aclER, "CiviCRM::getACLEntityRoles returned null")
                aclEntityRoles = aclER

                val aclE = civi.getACLs()
                assertNotNull(aclE, "CiviCRM::getACLs returned null")
                aclEntities = aclE
            }
        }

        crm.deleteCMRSystemBlocking()
    }

    @Test
    fun `Create group responded with CREATED`() {
        assertNotNull(createGroupResponse, "Create Group Response is null")

        assertEquals(
            HttpStatus.CREATED_201,
            createGroupResponse!!.status,
            "Wrong status code of create Group Response. Body: ${createGroupResponse!!.body}",
        )
    }

    @Test
    fun `Create group generates rg-testname in CiviCRM`() {
        Assertions.assertTrue(
            civiGroup.any { it.name == rgGroupNameCRM },
            "$rgGroupNameCRM was not created in CIVICRM",
        )
    }

    @Test
    fun `Create group generates rg-testname-orga group in CiviCRM`() {
        Assertions.assertTrue(
            civiGroup.any { it.name == rgGroupNameCRM + IogPluginConstants.ORGA_SUFFIX },
            "Orga group for $rgGroupNameCRM was not created in CIVICRM",
        )
    }

    @Test
    fun `Create group generates rg-testname-contact group in CiviCRM`() {
        assertNotNull(civiGroup, "CiviGroup is null")
        Assertions.assertTrue(
            civiGroup.any { it.name == rgGroupNameCRM + IogPluginConstants.CONTACT_SUFFIX },
            "Contact group for $rgGroupNameCRM was not created in CIVICRM",
        )
    }

    @Test
    fun `Create group generates ACLs, OptionValues, Orga and Contact groups in CiviCRM`() {
        val orgaGroupId =
            civiGroup.single { it.name == rgGroupNameCRM + IogPluginConstants.ORGA_SUFFIX }.id
        val contactGroupID =
            civiGroup.single { it.name == rgGroupNameCRM + IogPluginConstants.CONTACT_SUFFIX }.id

        Assertions.assertTrue(
            optionValues.any { it.name == rgGroupNameCRM && it.description == "Rolle für $rgGroupNameCRM" },
            "No OptionValue created. Created OptionValues are: ${optionValues.joinToString { (it.name ?: "No name") + ";" + (it.description ?: "No description") }}",
        )

        val groupOptionValue = optionValues.first { it.name == rgGroupNameCRM }
        val roleID = groupOptionValue.value?.toInt()

        assertNotNull(roleID, "Role ID in OptionValue is null")

        assertEquals(
            1,
            civiGroup.count { g -> g.name == rgGroupNameCRM },
            "Numbers of groups with name $rgGroupNameCRM should be 1.",
        )
        val specialGroupID = civiGroup.first { g -> g.name == rgGroupNameCRM }.id

        Assertions.assertTrue(
            aclEntityRoles.any { role ->
                role.active && role.entityId == specialGroupID && role.entityTable == ACLProtocol.ACLEntityType.CRM_GROUP.protocolName
            },
            "No Role for $rgGroupNameCRM. Id for specialGroup is $specialGroupID. Existing Roles: ${aclEntityRoles.joinToString { it.active.toString() + ";" + it.entityTable + ";" + it.entityTable }}",
        )

        Assertions.assertTrue(
            aclEntities.any { acl ->
                acl.operation == ACLProtocol.Operations.EDIT.protocolName &&
                    acl.entityTable == "civicrm_acl_role" &&
                    acl.objectTable == "civicrm_saved_search" &&
                    acl.entityId == roleID &&
                    acl.objectId == orgaGroupId
            },
            "No ACL for role of $rgGroupNameCRM assigned to Orga Group with id $orgaGroupId. Role ID for specialGroup is $roleID. Existing ACLs: ${aclEntities.joinToString { it.name + ";" + it.operation + ";" + it.entityId + ";" + it.entityTable + ";" + (it.objectTable ?: "NULL") + ";" + it.objectId?.toString() }}",
        )
        Assertions.assertTrue(
            aclEntities.any { acl ->
                acl.operation == ACLProtocol.Operations.EDIT.protocolName &&
                    acl.entityTable == "civicrm_acl_role" &&
                    acl.objectTable == "civicrm_saved_search" &&
                    acl.entityId == roleID &&
                    acl.objectId == contactGroupID
            },
            "No ACL for role of $rgGroupNameCRM assigned to Contact Group with id $orgaGroupId. Role ID for specialGroup is $roleID. Existing ACLs: ${aclEntities.joinToString { it.name + ";" + it.operation + ";" + it.entityId + ";" + it.entityTable + ";" + (it.objectTable ?: "NULL") + ";" + it.objectId?.toString() }}",
        )

        val orgaAcl =
            aclEntities.first { acl ->
                acl.operation == ACLProtocol.Operations.EDIT.protocolName &&
                    acl.entityTable == "civicrm_acl_role" &&
                    acl.objectTable == "civicrm_saved_search" &&
                    acl.entityId == roleID
                acl.objectId == orgaGroupId
            }
        assertEquals(
            "Von $rgGroupNameCRM verwaltete Organisationen",
            orgaAcl.name,
            "Name of orga group ACL is wrong.",
        )

        val contactAcl =
            aclEntities.first { acl ->
                acl.operation == ACLProtocol.Operations.EDIT.protocolName &&
                    acl.entityTable == "civicrm_acl_role" &&
                    acl.objectTable == "civicrm_saved_search" &&
                    acl.entityId == roleID
                acl.objectId == contactGroupID
            }
        assertEquals(
            "Von $rgGroupNameCRM verwaltete Personenkontakte",
            contactAcl.name,
            "Name of contact group ACL is wrong.",
        )
    }

    private fun assertPreconditionsForCiviCRM() {
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.IOG_MEMBERS),
            "IOG Member not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.INTERESTED_PEOPLE),
            "INTERESTED_PEOPLE not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.TRIAL_MEMBERS),
            "TRIAL_MEMBERS not created before precondition",
        )
        assertNotNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(LdapNames.ETHIC_VALIDATION_GROUP),
            "ETHIC_VALIDATION_GROUP not created before precondition",
        )

        ServiceConfig.CREDENTIAL_PROVIDER.getGroups().filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) &&
                it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix),
                "Konnte Basisgroupp $groupNameWithoutPrefix nicht finden",
            )
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX),
                "Konnte Admingruppe ${groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX} nicht finden",
            )
            assertNotNull(
                ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX),
                "Konnte Trialgruppe ${groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX} nicht finden",
            )
        }
        ServiceConfig.CREDENTIAL_PROVIDER.getGroups().filter { g -> g.name.endsWith(IogPluginConstants.CRM_SUFFIX) }.forEach {
            Assertions.assertTrue(
                false,
                "CRM-Gruppe ${it.name} existiert bereits. Bitte alle CRM-Gruppen vor Erstellung löschen.",
            )
        }

        assertNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(CiviCRMNames.USER_GROUP_NAME),
            "${CiviCRMNames.USER_GROUP_NAME} in civicrm gefunden. Diese muss vorher gelöscht sein.",
        )
        assertNull(
            ServiceConfig.CREDENTIAL_PROVIDER.getGroupByName(CiviCRMNames.CONTACT_GROUP_NAME),
            "${CiviCRMNames.CONTACT_GROUP_NAME} in civicrm gefunden. Diese muss vorher gelöscht sein.",
        )

        try {
            apiTestUtilities.scenario?.civiCRM?.civicrm?.findIndexOfACLRole()
        } catch (ex: Exception) {
            Assertions.assertTrue(false, "ACL Rolle muss in civicrm erfüllt sein")
        }
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
