/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.supportmember

import dev.maximilian.feather.Group
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.internal.chapter.ChapterDB
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheck
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.trials.TrialMemberDB
import dev.maximilian.feather.iog.internal.trials.TrialProtocol
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.IOGGroupScenario
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.TestInstance
import java.time.Instant
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MemberCheckTest {
    companion object {
        private val centralOfficeGroupName by lazy { OPGroupNames.CENTRAL_OFFICE.trimIndent() }

        private val credentialProvider by lazy { ServiceConfig.CREDENTIAL_PROVIDER }

        private val db by lazy { ApiTestUtilities.db }
        private val scenarioLoader by lazy { IOGGroupScenario(credentialProvider) }
    }

    @Test
    fun `Test bug Openproject#22943 was fixed correctly for competence groups`() {
        val supportMemberDb = SupportMemberDB(ApiTestUtilities.db, ServiceConfig.ACCOUNT_CONTROLLER)
        val trialMemberDB = TrialMemberDB(ApiTestUtilities.db, ServiceConfig.ACCOUNT_CONTROLLER)
        val trialProtocol = TrialProtocol(trialMemberDB, credentialProvider, null)
        val memberCheck = MemberCheck(
            credentialProvider,
            supportMemberDb,
            trialProtocol,
            null,
            credentialProvider::getUsers,
            IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM,
            LdapNames.CENTRAL_OFFICE,
        )

        val chapterDb = ChapterDB(db)
        val iogGroupSchema = IogGroupSchema(credentialProvider)
        val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
        val createGroup = CreateGroup(
            null, null, null, OPNameConfig(mutableMapOf()),
            object : GroupSynchronizationEvent {
                override val k8sSyncEnabled: Boolean = false

                override suspend fun createOPSync(
                    openproject: IOpenProject,
                    createdLdapGroups: List<Group>,
                    newOpenProjectGroups: List<OpenProjectGroup>,
                ) {
                    /* No-OP */
                }

                override fun synchronize() {
                    /* No-OP */
                }
            },
            backgroundJobManager, iogGroupSchema, chapterDb, credentialProvider,
        )
        val testUUID = UUID.randomUUID().toString()
        val groupAdmin = credentialProvider.createUser(TestUser.generateTestUser())
        val createGroupConfig =
            CreateGroupConfig(testUUID, testUUID, setOf(groupAdmin.id), GroupKind.COMPETENCE_GROUP, "")

        scenarioLoader.createBaseGroups()
        val resultGroups = runBlocking { createGroup.createOPandNCandLDAPandCiviCRM(null, createGroupConfig) }
        val trialGroup = resultGroups.first { it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) }

        val trialMember = credentialProvider.createUser(TestUser.generateTestUser())
        credentialProvider.updateGroup(trialGroup.copy(userMembers = trialGroup.userMembers + trialMember.id))
        val updatedTrialMember = requireNotNull(credentialProvider.getUser(trialMember.id))
        memberCheck.userCreated(updatedTrialMember)

        assertEquals(
            MemberCheck.RegistrationState.REGISTERED_TRIAL_MEMBER,
            memberCheck.getRegistrationState(updatedTrialMember),
        )
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-member is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for rg-test-interested is INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-interested with expired trial is INTERESTED_SM_UPGRADE_CAN_BE_OFFERED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id, Instant.now().minusSeconds(mc.trial.halfYearInSeconds * 2 + 1))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id, Instant.now().minusSeconds(mc.trial.halfYearInSeconds + 1))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_SM_UPGRADE_CAN_BE_OFFERED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-trial is TRIAL_REQUIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))
        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.TRIAL_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for gs is NO_SUPPORT_MEMBERSHIP_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase(centralOfficeGroupName, "Geschäftsstelle")
        scenarioLoader.createRG("uga-iog10", "uga-IOG10")
        val user = createTestUserAndGetLDAPID(setOf(centralOfficeGroupName, "uga-iog10-member"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for gs-admin ist NO_SUPPORT_MEMBERSHIP_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase(centralOfficeGroupName, "Geschäftsstelle")
        val user = createTestUserAndGetLDAPID(setOf("$centralOfficeGroupName-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-admin is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered kg-test-admin is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("kg-test", "KG Test")
        val user = createTestUserAndGetLDAPID(setOf("kg-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered kg-member is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("kg-test", "KG Test")
        val user = createTestUserAndGetLDAPID(setOf("kg-test"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for duplicate mail hash in support member database is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("other@example.org"),
            mc.makeHash(user.mail),
        )
        mc.smr.storeHash(
            999,
            mc.makeHash("HaraldDuck01.01.1998"),
            mc.makeHash("other2@example.org"),
            mc.makeHash(user.mail),
        )

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered as-admin is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test", "KG Test")
        val user = createTestUserAndGetLDAPID(setOf("as-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered as-member is SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test", "AS Test")
        val user = createTestUserAndGetLDAPID(setOf("as-test-admin"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for rg-member with second mail in DB is SM_MAIL_REGISTRATION_POSSIBLE `() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("other@example.org"),
            mc.makeHash(user.mail),
        )

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for rg-interested with first mail in DB leads to INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE `() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))

        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("other@example.org"),
            mc.makeHash(user.mail),
        )

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for rg-trial with first mail in DB leads to TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE `() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))

        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("other@example.org"),
            mc.makeHash(user.mail),
        )

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for rg-member with first mail in DB is SM_MAIL_REGISTRATION_POSSIBLE `() {
        val mc = loadDefaults()
        createRGTest()
        val ldapid = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash(ldapid.mail),
            mc.makeHash("other@example.org"),
        )

        val result = mc.getRegistrationState(ldapid)
        assertEquals(MemberCheck.RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for registered rg-test-member is REGISTERED_SUPPORT_MEMBER`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("harald@example.org"),
            mc.makeHash("other@example.org-"),
        )

        val result = mc.registerByNameAndBirthdate(mc.makeHash("HaraldTest01.01.1995"), user.id)
        assertTrue(result)
        val result2 = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.REGISTERED_SUPPORT_MEMBER, result2)
    }

    @Test
    fun `Test getRegistrationState for trial registered rg-test-interested is INTERESTED_TRIAL_UPGRADE_POSSIBLE`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id)

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_TRIAL_UPGRADE_POSSIBLE, result)
    }

    @Test
    fun `Test getRegistrationState for unregistered rg-test-interested is INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED, result)
    }

    @Test
    fun `Test getRegistrationState for accepted rg-test-trial is REGISTERED_TRIAL_MEMBER`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id)

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.REGISTERED_TRIAL_MEMBER, result)
    }

    @Test
    fun `Test getRegistrationState for requested rg-test-trial is INTERESTED_TRIAL_PENDING`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        mc.trial.requestTrial(user.id)

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.INTERESTED_TRIAL_PENDING, result)
    }

    @Test
    fun `Test getRegistrationState for first expired rg-test-trial is TRIAL_REQUIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id, Instant.now().minusSeconds(mc.trial.halfYearInSeconds + 1))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.TRIAL_REQUIRED, result)
    }

    @Test
    fun `Test getRegistrationState for second expired rg-test-trial is TRIAL_EXPIRED`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id, Instant.now().minusSeconds(mc.trial.halfYearInSeconds * 2 + 1))
        mc.trial.requestTrial(user.id)
        mc.trial.acceptTrialRequest(user.id, Instant.now().minusSeconds(mc.trial.halfYearInSeconds + 1))

        val result = mc.getRegistrationState(user)
        assertEquals(MemberCheck.RegistrationState.TRIAL_EXPIRED, result)
    }

    @Test
    fun `Test registerByMail for registered rg-test-member leads to FALSE`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("harald@example.org"),
            mc.makeHash("other@example.org"),
        )
        mc.registerByMail(user)
        val result2 = mc.registerByMail(user)
        assertFalse(result2)
    }

    @Test
    fun `Test registerByMail for registered rg-test-member doesnt fills database`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash(user.mail),
            mc.makeHash("other@example.org"),
        )
        mc.registerByMail(user)
        val databaseUserBefore = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserBefore.count())
        mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserAfter.count())
    }

    @Test
    fun `Test registerByMail for unregistered rg-test-member fills database and is TRUE`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash(user.mail),
            mc.makeHash("other@example.org"),
        )

        val result = mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertTrue(result)
        assertEquals(1, databaseUserAfter.count())
    }

    @Test
    fun `Test deleteFromSupportMemberRegistration really deletes`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("harald@example.org"),
            mc.makeHash("other@example.org"),
        )

        mc.registerByMail(user)
        mc.userDeleted(user)
        val database = mc.getAllRegisteredSupportMember()
        assertEquals(0, database.count())
    }

    @Test
    fun `Test getAllSupportMembers delivers correct results`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash(user.mail),
            mc.makeHash("other@example.org"),
        )
        mc.smr.storeHash(
            999,
            mc.makeHash("HaraldDuck01.01.1998"),
            mc.makeHash("other2@example.org"),
            mc.makeHash("other3@example.org"),
        )

        mc.registerByMail(user)
        val databaseUserAfter = mc.getAllRegisteredSupportMember()
        assertEquals(1, databaseUserAfter.count())
        assertEquals(user.firstname, databaseUserAfter[0].firstName)
        assertEquals(user.surname, databaseUserAfter[0].lastName)
        assertEquals(user.id, databaseUserAfter[0].ldapID)
        assertEquals(777, databaseUserAfter[0].externalID)
    }

    @Test
    fun `Test getAllSupportMembersOfFundraisingBox delivers correct results`() {
        val mc = loadDefaults()
        createRGTest()
        createTestUserAndGetLDAPID(setOf("rg-test-member"))
        mc.smr.storeHash(
            777,
            mc.makeHash("HaraldTest01.01.1995"),
            mc.makeHash("notworking@example.org"),
            mc.makeHash("other@example.org"),
        )

        val databaseUserAfter = mc.getAllSupportMembersOfFundraisingBox()
        assertEquals(1, databaseUserAfter.count())
        assertEquals(mc.makeHash("HaraldTest01.01.1995"), databaseUserAfter[0].NameBirthdayHash)
        assertEquals(mc.makeHash("notworking@example.org"), databaseUserAfter[0].MailHash1)
        assertEquals(mc.makeHash("other@example.org"), databaseUserAfter[0].MailHash2)
        assertEquals(777, databaseUserAfter[0].ExternalID)
    }

    @Test
    fun `Test upgrade from rg-test-interested leads to rg-test-member`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))

        mc.upgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)
        val targetID = getIDFromGroupsList("rg-test-member")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test upgrade from rg-test-trial leads to rg-test-member`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))

        mc.upgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)
        val targetID = getIDFromGroupsList("rg-test-member")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `improve to member after trial requests and removes from trial request database`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-interested"))
        mc.trial.requestTrial(user.id)
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(
            SupportMemberHashRow(
                7,
                mc.makeHash("askdfjas2"),
                mc.makeHash(user.mail),
                mc.makeHash("SDFsdf99"),
            ),
        )
        mc.updateDatabase(registered)
        mc.registerByMail(user)

        val t = mc.trial.getAllRequests(emptyList(), true)
        assertEquals(0, t.size)
    }

    @Test
    fun `Test downgrade from rg-test-member leads to rg-test-interested`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)
        val targetID = getIDFromGroupsList("rg-test-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test downgrade from rg-test-admin leads to rg-test-interested`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-admin"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)

        val targetID = getIDFromGroupsList("rg-test-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test downgrade from rg-test-member with as-membership leads to rg-test-interested`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test1", "AS Test1")
        createRGTest2()
        val user = createTestUserAndGetLDAPID(setOf("as-test1", "rg-test2-member"))

        mc.downgrade(user.id)
        val user2 = credentialProvider.getUser(user.id)

        val targetID = getIDFromGroupsList("rg-test2-interested")
        assertEquals(targetID, user2?.groups?.first())
    }

    @Test
    fun `Test updateDatabase with 3 members leads to filled databse with 3 members`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(
            SupportMemberHashRow(
                19,
                mc.makeHash("askdfjas2"),
                mc.makeHash("asfdg"),
                mc.makeHash("SDFsdf99"),
            ),
        )
        registered.add(
            SupportMemberHashRow(
                22,
                mc.makeHash("askdfjas3"),
                mc.makeHash("asfdg12"),
                mc.makeHash("SDFsdf94"),
            ),
        )

        mc.updateDatabase(registered)
        assertEquals(3, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test number of support member after two updateDatabase calls`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(
            SupportMemberHashRow(
                19,
                mc.makeHash("askdfjas2"),
                mc.makeHash("asfdg"),
                mc.makeHash("SDFsdf99"),
            ),
        )
        registered.add(
            SupportMemberHashRow(
                22,
                mc.makeHash("askdfjas3"),
                mc.makeHash("asfdg12"),
                mc.makeHash("SDFsdf94"),
            ),
        )

        mc.updateDatabase(registered)
        registered.add(SupportMemberHashRow(25, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(
            SupportMemberHashRow(
                26,
                mc.makeHash("askdfjas2"),
                mc.makeHash("asfdg"),
                mc.makeHash("SDFsdf99"),
            ),
        )
        registered.add(
            SupportMemberHashRow(
                77,
                mc.makeHash("askdfjas3"),
                mc.makeHash("asfdg12"),
                mc.makeHash("SDFsdf94"),
            ),
        )

        mc.updateDatabase(registered)
        assertEquals(6, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test updateDatabase removes an unregistered support member`() {
        val mc = loadDefaults()
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(
            SupportMemberHashRow(
                19,
                mc.makeHash("askdfjas2"),
                mc.makeHash("asfdg"),
                mc.makeHash("SDFsdf99"),
            ),
        )
        registered.add(
            SupportMemberHashRow(
                22,
                mc.makeHash("askdfjas3"),
                mc.makeHash("asfdg12"),
                mc.makeHash("SDFsdf94"),
            ),
        )

        mc.updateDatabase(registered)
        registered.clear()
        registered.add(SupportMemberHashRow(15, mc.makeHash("askdfjas"), mc.makeHash("asfdg4"), mc.makeHash("SDFsdf5")))
        registered.add(
            SupportMemberHashRow(
                26,
                mc.makeHash("askdfjas2"),
                mc.makeHash("asfdg"),
                mc.makeHash("SDFsdf99"),
            ),
        )
        mc.updateDatabase(registered)
        assertEquals(2, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test updateDatabase removes a registered support member`() {
        val mc = loadDefaults()
        scenarioLoader.createSimplePatternForBase("as-test1", "AS Test1")
        createRGTest2()
        val user = createTestUserAndGetLDAPID(setOf("as-test1", "rg-test2-member"))
        val registered = mutableListOf<SupportMemberHashRow>()
        registered.add(randomHashRowFromSeed(mc, 7))
        registered.add(
            SupportMemberHashRow(
                15,
                mc.makeHash("testuser"),
                mc.makeHash(user.mail),
                mc.makeHash("SDFsdf5"),
            ),
        )
        registered.add(randomHashRowFromSeed(mc, 22))

        mc.updateDatabase(registered)
        mc.registerByMail(user)
        assertEquals(3, mc.smr.countSupportMember())
        assertEquals(1, mc.smr.countRegisteredDKPUser())

        registered.clear()
        registered.add(randomHashRowFromSeed(mc, 99))
        registered.add(randomHashRowFromSeed(mc, 26))
        mc.updateDatabase(registered)
        assertEquals(2, mc.smr.countSupportMember())
        assertEquals(0, mc.smr.countRegisteredDKPUser())
    }

    @Test
    fun `Test updateDatabase updates the last-updated metadata`() {
        val mc = loadDefaults()
        assertNull(mc.getLastDatabaseUpdate())

        val registered = (1..3).map { i -> randomHashRowFromSeed(mc, i) }
        val tolerance = ChronoUnit.HOURS
        val start = ZonedDateTime.now().truncatedTo(tolerance)
        mc.updateDatabase(registered)
        val stop = ZonedDateTime.now().plus(1, tolerance).truncatedTo(tolerance) // rounding up

        val lastUpdateTime = mc.getLastDatabaseUpdate()
        assertNotNull(lastUpdateTime)
        assert(lastUpdateTime >= start)
        assert(lastUpdateTime <= stop)
    }

    private fun randomHashRowFromSeed(
        mc: MemberCheck,
        externalId: Int,
    ) = SupportMemberHashRow(
        externalId,
        mc.makeHash(externalId.toString()),
        mc.makeHash((externalId + 1).toString()),
        mc.makeHash((externalId + 10).toString()),
    )

    private fun getIDFromGroupsList(name: String): Int {
        val allGroups = credentialProvider.getGroups()
        return allGroups.first { it.name == name }.id
    }

    private fun createTestUserAndGetLDAPID(groupOfTestUserNames: Set<String>): User {
        val groupsOfTestUser = mutableListOf<Int>()
        groupOfTestUserNames.forEach {
            groupsOfTestUser.add(getIDFromGroupsList(it))
        }

        val newUser = TestUser.generateTestUser(groups = groupsOfTestUser.toSet())
        return credentialProvider.createUser(newUser)
    }

    private fun loadDefaults(): MemberCheck {
        val user = credentialProvider.getUsers()
        user.forEach { credentialProvider.deleteUser(it) }
        val groups = credentialProvider.getGroups()
        groups.forEach { credentialProvider.deleteGroup(it) }
        val supportMemberRegister = SupportMemberDB(db, ServiceConfig.ACCOUNT_CONTROLLER)
        supportMemberRegister.deleteAll()
        val actionController = ActionController(db, ServiceConfig.ACCOUNT_CONTROLLER)
        val trial =
            TrialProtocol(TrialMemberDB(db, ServiceConfig.ACCOUNT_CONTROLLER), credentialProvider, actionController)
        trial.deleteAll()
        val mc = MemberCheck(
            credentialProvider,
            supportMemberRegister,
            trial,
            actionController,
            credentialProvider::getUsers,
            IogPluginConstants.SUPPORT_MEMBERSHIP_HASH_ALGORITHM,
            LdapNames.CENTRAL_OFFICE,
        )
        actionController.setupActions()
        mc.databaseValidation.minSupportMemberCount = 1
        scenarioLoader.createBaseGroups()
        return mc
    }

    @Test
    fun `Test change register state is removing old TRIAL_REQUESTED action from action db`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-trial"))
        val g = credentialProvider.getGroupByName("rg-test-trial")!!
        mc.addActionsToUsers()
        val result = mc.actionController!!.getOpenUserActions(user)
        val blockingAction = result.first { it.name == "membership" }
        assertEquals(
            "membership",
            blockingAction.name,
            "the list of actions does not contain blocking membership after adding to trial",
        )
        assertEquals(
            "TRIAL_REQUIRED",
            blockingAction.payload.toString(),
            "the list of actions does not contain blocking membership with payload TRIAL_REQUIRED",
        )
        credentialProvider.updateUser(user.copy(groups = setOf(credentialProvider.getGroupByName("rg-test-interested")!!.id)))
        mc.userRemovedFromGroup(user.id, g.id)
        val result2 = mc.actionController.getOpenUserActions(user)
        assertTrue(
            result2.none { it.name == "membership" },
            "the list of actions is not cleared after remove of its action. remaining: ${result.first().payload} with name ${result.first().name}",
        )
        mc.actionController.deleteActionsByNameAndUser("membership", user)
    }

    @Test
    fun `Test change register state is removing old action SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED action db`() {
        val mc = loadDefaults()
        createRGTest()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        val g = credentialProvider.getGroupByName("rg-test-member")!!
        mc.addActionsToUsers()
        val result = mc.actionController!!.getOpenUserActions(user)
        val blockingAction = result.first { it.name == "membership" }
        assertEquals(
            "membership",
            blockingAction.name,
            "the list of actions does not contain blocking membership after adding to trial",
        )
        assertEquals(
            "SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED",
            blockingAction.payload.toString(),
            "the list of actions does not contain blocking membership with payload SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED",
        )
        credentialProvider.updateUser(user.copy(groups = setOf(credentialProvider.getGroupByName("rg-test-interested")!!.id)))
        mc.userRemovedFromGroup(user.id, g.id)
        val result2 = mc.actionController.getOpenUserActions(user)
        assertTrue(
            result2.none { it.name == "membership" },
            "the list of actions is not cleared after remove of its action. remaining: ${result.first().payload} with name ${result.first().name}",
        )
        mc.actionController.deleteActionsByNameAndUser("membership", user)
    }

    @Test
    fun `Test change register state is not removing action after removing in one of two groups`() {
        val mc = loadDefaults()
        createRGTest()
        createRGTest2()
        val user = createTestUserAndGetLDAPID(setOf("rg-test-member"))
        credentialProvider.updateUser(
            user.copy(
                groups = setOf(
                    credentialProvider.getGroupByName("rg-test-member")!!.id,
                    credentialProvider.getGroupByName("rg-test2-interested")!!.id,
                ),
            ),
        )
        val g = credentialProvider.getGroupByName("rg-test-member")!!
        mc.addActionsToUsers()
        val result = mc.actionController!!.getOpenUserActions(user)
        val blockingAction = result.first { it.name == "membership" }
        assertEquals(
            "membership",
            blockingAction.name,
            "the list of actions does not contain blocking membership after adding to trial",
        )
        assertEquals(
            "SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED",
            blockingAction.payload.toString(),
            "the list of actions does not contain blocking membership with payload SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED",
        )
        credentialProvider.updateUser(user.copy(groups = setOf(credentialProvider.getGroupByName("rg-test2-member")!!.id)))
        mc.userRemovedFromGroup(user.id, g.id)
        val blockingAction2 = result.first { it.name == "membership" }
        assertEquals(
            "SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED",
            blockingAction2.payload.toString(),
            "the blocking actions is wrong after deleting one of two membership membership",
        )
        mc.actionController.deleteActionsByNameAndUser("membership", user)
    }

    private fun createRGTest() {
        scenarioLoader.createRG("rg-test", "RG Test")
    }

    private fun createRGTest2() {
        scenarioLoader.createRG("rg-test2", "RG Test2")
    }
}
