/*
 *    Copyright [2021-2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.iog.api.AutoRepair
import dev.maximilian.feather.iog.api.GroupPlausibilityConfig
import dev.maximilian.feather.iog.internal.group.GroupPlausibility
import dev.maximilian.feather.iog.internal.group.GroupPlausibilityResults
import dev.maximilian.feather.iog.internal.group.plausibility.RepairMessages
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestUser
import dev.maximilian.feather.testutils.getEnv
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

data class BackgroundJobWithStrings(
    val id: UUID,
    val permission: String?, // as String because unirest fails to decode enum correctly
    val created: String, // as String because unirest fails to decode Instant correctly
    val status: String,
    val completed: String? = null, // as String because unirest fails to decode Instant correctly
    val exception: String? = null,
)

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GroupPlausibilityTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER
    private val standardGroupNumber = 15
    private val rgGroupNumber = 6

    private val groupPlausibility =
        GroupPlausibility(
            credentials,
            backgroundJobManager,
            apiTestUtilities.scenario!!.iogPlugin!!.iogGroupSchema,
            apiTestUtilities.scenario!!.openProjectService!!,
            apiTestUtilities.scenario!!.groupSyncMock,
            OPNameConfig(mutableMapOf()),
            apiTestUtilities.scenario!!.nextcloud!!,
            getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082"),
        )

    @Test
    fun `GET group plausibility by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = getPlausibility().asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `GET group plausibility returns correct body`() {
        apiTestUtilities.loginWithAdminUser()
        val response = getPlausibility().asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response.status)
        val job = response.body
        assertNotNull(job)
        // assertEquals(Permission.ADMIN, job.permission)
        assertEquals("ADMIN", job.permission)
        assertEquals("started", job.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)
        do {
            val jobWorking = backgroundJobManager.getJobStatus(job.id)
            checkNotNull(jobWorking)
        } while (jobWorking?.completed == null)
    }

    @Test
    fun `Group plausibility for regional group returns OK in OP only mode`() {
        apiTestUtilities.createRGTest()
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("OK", res.overallStatus)
        assertEquals(standardGroupNumber + rgGroupNumber, res.entries.count())
    }

    @Test
    fun `Group plausibility for regional group returns OK`() {
        apiTestUtilities.createRGTest()
        val res = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        assertEquals("OK", res.overallStatus, "Something differs: {${res.entries.find { it.generalStatus != "OK"}?.generalStatus}")
        assertEquals(
            standardGroupNumber + rgGroupNumber,
            res.entries.count(),
            "Group number differ: ${res.entries.joinToString { it.group.name }}",
        )
    }

    @Test
    fun `Group plausibility for regional group returns OK in LDAP only mode`() {
        apiTestUtilities.createRGTest()
        val res = runCheckLDAPOnly()
        apiTestUtilities.removeRGTest()
        assertEquals("OK", res.overallStatus)
        assertEquals(standardGroupNumber + rgGroupNumber, res.entries.count())
    }

    @Test
    fun `Group plausibility detects missing interested group in regional group of ldap`() {
        apiTestUtilities.createRGTest()
        credentials.deleteGroup(credentials.getGroupByName("rg-test-interested")!!)
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("Not OK", res.overallStatus)
        assertEquals(standardGroupNumber + rgGroupNumber, res.entries.count())
        assertTrue(res.entries.find { it.group.name == "rg-test-interested" }!!.generalStatus.startsWith("LDAP-Gruppe fehlt"))
    }

    @Test
    fun `Group plausibility can detect missing nextcloud folder in regional group and repair missing folders`() {
        runBlocking {
            launch { apiTestUtilities.createRG("rg-test1", "RG Test1") }
            launch { apiTestUtilities.createRG("rg-test2", "RG Test2") }
            launch { apiTestUtilities.createRG("rg-test3", "RG Test3") }
            launch { apiTestUtilities.createRG("rg-test4", "RG Test4") }
        }

        apiTestUtilities.scenario!!.ncUtilities.nextcloud.delete("${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}/RG Test1")
        apiTestUtilities.scenario!!.ncUtilities.nextcloud.delete(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}/RG Test2/Gruppenintern",
        )
        apiTestUtilities.scenario!!.ncUtilities.nextcloud.delete(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}/RG Test3/Eingang",
        )
        apiTestUtilities.scenario!!.ncUtilities.nextcloud.delete(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}/RG Test4/Öffentlich",
        )
        val res = runCheckWithAll()
        val resRepaired = repairCheck()
        val resConfirmed = runCheckWithAll()

        runBlocking {
            launch {
                apiTestUtilities.removeMemberCrmInterested(
                    "rg-test1",
                    "RG Test1",
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
                )
            }
            launch {
                apiTestUtilities.removeMemberCrmInterested(
                    "rg-test2",
                    "RG Test2",
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
                )
            }
            launch {
                apiTestUtilities.removeMemberCrmInterested(
                    "rg-test3",
                    "RG Test3",
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
                )
            }
            launch {
                apiTestUtilities.removeMemberCrmInterested(
                    "rg-test4",
                    "RG Test4",
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
                )
            }
        }

        assertEquals(
            "Konnte in NC folgendes Verzeichnis nicht finden: <IOG/RG-Intern/RG Test1> ${RepairMessages.MANUAL}.",
            res.entries.find { it.group.name == "rg-test1" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test2/Gruppenintern> ${RepairMessages.AUTO}.",
            res.entries.find { it.group.name == "rg-test2" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test3/Eingang> ${RepairMessages.AUTO}.",
            res.entries.find { it.group.name == "rg-test3" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test4/Öffentlich> ${RepairMessages.AUTO}.",
            res.entries.find { it.group.name == "rg-test4" }!!.generalStatus,
        )
        assertEquals("Not OK", res.overallStatus)

        assertEquals(
            "Konnte in NC folgendes Verzeichnis nicht finden: <IOG/RG-Intern/RG Test1> ${RepairMessages.MANUAL}.",
            resRepaired.entries.find { it.group.name == "rg-test1" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test2/Gruppenintern> ${RepairMessages.REPAIR_DONE}. NC Permission Skript nicht vergessen!!!",
            resRepaired.entries.find { it.group.name == "rg-test2" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test3/Eingang> ${RepairMessages.REPAIR_DONE}. NC Permission Skript nicht vergessen!!!",
            resRepaired.entries.find { it.group.name == "rg-test3" }!!.generalStatus,
        )
        assertEquals(
            "Konnte Nextcloud-Ordner nicht finden: <IOG/RG-Intern/RG Test4/Öffentlich> ${RepairMessages.REPAIR_DONE}. NC Permission Skript nicht vergessen!!!",
            resRepaired.entries.find { it.group.name == "rg-test4" }!!.generalStatus,
        )
        assertEquals("Not OK", resRepaired.overallStatus)

        assertEquals(
            "Konnte in NC folgendes Verzeichnis nicht finden: <IOG/RG-Intern/RG Test1> ${RepairMessages.MANUAL}.",
            resConfirmed.entries.find { it.group.name == "rg-test1" }!!.generalStatus,
        )
        assertEquals("OK", resConfirmed.entries.find { it.group.name == "rg-test2" }!!.generalStatus)
        assertEquals("OK", resConfirmed.entries.find { it.group.name == "rg-test3" }!!.generalStatus)
        assertEquals("OK", resConfirmed.entries.find { it.group.name == "rg-test4" }!!.generalStatus)
    }

    @Test
    fun `Group plausibility can detect missing Readme md in regional group and repair it`() {
        apiTestUtilities.createRGTest()
        apiTestUtilities.scenario!!.ncUtilities.nextcloud.delete(
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}/RG Test/Readme.md",
        )
        val res = runCheckWithAll()
        val resRepaired = repairCheck()
        val resConfirm = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        assertEquals(
            "Konnte Readme.md nicht finden, erwarteter Ort : <IOG/RG-Intern/RG Test/Readme.md> ${RepairMessages.AUTO}.",
            res.entries.find {
                it.group.name == "rg-test"
            }!!.generalStatus,
        )
        assertEquals(
            "Konnte Readme.md nicht finden, erwarteter Ort : <IOG/RG-Intern/RG Test/Readme.md> ${RepairMessages.REPAIR_DONE}.",
            resRepaired.entries.find {
                it.group.name == "rg-test"
            }!!.generalStatus,
        )
        assertEquals("OK", resConfirm.entries.find { it.group.name == "rg-test" }!!.generalStatus)
    }

    @Test
    fun `Group plausibility for committee group returns OK`() {
        apiTestUtilities.createKG("kg-test", "KG Test")
        val res = runCheckWithAll()
        apiTestUtilities.removeKG(
            "kg-test",
            "KG Test",
        )
        assertEquals(
            "OK",
            res.entries.find { it.group.name == "kg-test" }!!.generalStatus,
            "Status of kg-test is not OK.",
        )
        assertEquals("OK", res.overallStatus, "Overall status is not OK. ${res.entries.find { it.generalStatus != "OK" }?.generalStatus}")
    }

    @Test
    fun `Group plausibility detects missing oms-admin group in ldap`() {
        val oms = credentials.getGroupByName("oms-admin")!!
        credentials.deleteGroup(oms)

        val res = runCheckWithOP()
        credentials.createGroup(oms.copy(id = 0))
        assertEquals("LDAP-Gruppe fehlt ${RepairMessages.MANUAL}.", res.entries.find { it.group.name == "oms-admin" }!!.generalStatus)
    }

    @Test
    fun `Group plausibility detects missing vorstand group in ldap`() {
        val vorstand = credentials.getGroupByName("vorstand")!!
        credentials.deleteGroup(vorstand)
        val res = runCheckWithOP()
        credentials.createGroup(vorstand.copy(id = 0))
        assertEquals("Not OK", res.overallStatus)
        assertEquals("LDAP-Gruppe fehlt ${RepairMessages.MANUAL}.", res.entries.find { it.group.name == "vorstand" }!!.generalStatus)
    }

    @Test
    fun `Group plausibility detects members in main group of memberAdminInterested`() {
        apiTestUtilities.createRGTest()
        val oms = credentials.getGroupByName("rg-test")!!
        credentials.updateGroup(
            oms.copy(
                userMembers =
                setOf(
                    credentials.getUserByUsername(
                        TestUser.NORMAL_USER.username,
                    )!!.id,
                ),
            ),
        )
        repairCheck()
        val res = runCheckWithOP()
        credentials.updateGroup(oms)
        apiTestUtilities.removeRGTest()
        assertEquals(
            "Diese Gruppe darf keine direkten Member beinhalten ${RepairMessages.MANUAL}.",
            res.entries.find { it.group.name == "rg-test" }!!.generalStatus,
        )
    }

    @Test
    fun `Group plausibility detects wrong ldap description of RG and can autorepair it`() {
        apiTestUtilities.createRGTest()
        val g = credentials.getGroupByName("rg-test-admin")
        credentials.updateGroup(g!!.copy(description = "Administratoren von RG Tost"))
        val res = runCheckWithOP()
        val resRepaired = repairCheck()
        val resFinish = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        assertEquals(
            "Die Beschreibung weicht von der geforderten Beschreibung ab. Gefordert ist <Administratoren von RG Test> ${RepairMessages.AUTO}.",
            res.entries.find { it.group.name == "rg-test-admin" }!!.generalStatus,
        )
        assertEquals(
            "Die Beschreibung weicht von der geforderten Beschreibung ab. Gefordert ist <Administratoren von RG Test> ${RepairMessages.REPAIR_DONE}.",
            resRepaired.entries.find { it.group.name == "rg-test-admin" }!!.generalStatus,
        )
        assertEquals(
            "OK",
            resFinish.entries.find { it.group.name == "rg-test-admin" }!!.generalStatus,
        )
    }

    @Test
    fun `Group plausibility detects ldap description of rg-paderborn-bielefeld correctly`() {
        apiTestUtilities.createRG("rg-paderborn-bielefeld", "RG Paderborn-Bielefeld")
        val res = runCheckWithOP()
        apiTestUtilities.removeMemberCrmInterested(
            "rg-paderborn-bielefeld",
            "RG Paderborn-Bielefeld",
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detects ldap description of rg-koeln correctly`() {
        apiTestUtilities.createRG("rg-koeln", "RG Köln")
        val res = runCheckWithOP()
        apiTestUtilities.removeMemberCrmInterested(
            "rg-koeln",
            "RG Köln",
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}",
        )
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detects ldap description of iog-aeu10 correctly`() {
        apiTestUtilities.createProject("iog-aeu10", "IOG-AEU10")
        val res = runCheckWithOP()
        apiTestUtilities.removeMemberAdminInterested(
            "iog-aeu10",
            "RG Köln",
            "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}",
        )
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility can autorepair wrong ldap description of RG`() {
        apiTestUtilities.createRGTest()
        val g = credentials.getGroupByName("rg-test-admin")
        credentials.updateGroup(g!!.copy(description = "Administratoren von RG Tost"))
        repairCheck()
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detect project in OP OK`() {
        apiTestUtilities.createRGTest()
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detect missing rg-test project in OP`() {
        apiTestUtilities.createRGTest()
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProjectService!!.openproject
            val q = op.getProjectByIdentifierOrName("rg-test")!!
            op.deleteProject(q)
        }
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertContains(
            res.entries.find { it.group.name == "rg-test" }!!.generalStatus,
            "Es gibt kein OP Projekt, dass rg-test als Identifier hat ${RepairMessages.MANUAL}.",
        )
        assertEquals("Not OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detect missing rg-test group in OP`() {
        apiTestUtilities.createRGTest()
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProjectService!!.openproject
            val q = op.getGroupByName("rg-test")!!
            op.deleteGroup(q)
        }
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("Not OK", res.overallStatus)
        assertEquals(
            "Die Gruppe rg-test existiert nicht in OpenProject ${RepairMessages.MANUAL}.",
            res.entries.find { it.group.name == "rg-test" }!!.generalStatus,
        )
    }

    @Test
    fun `Group plausibility can detect not registered groupmember in OP`() {
        apiTestUtilities.createRGTest()
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProjectService!!.openproject
            val q = op.getGroupByName("rg-test")!!
            op.addGroupMember(q, op.getUserByLogin("sso_admin")!!)
        }
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        val status = res.entries.find { it.group.name == "rg-test" }!!.generalStatus
        assertTrue(
            status.contains(" aus der OP-Gruppe rg-test existiert nicht in LDAP"),
            "Es wurde eine Fehlermeldung zu einem nicht existierenden LDAP User erwartet. Jedoch gemeldet: <$status>",
        )
        assertEquals("Not OK", res.overallStatus)
    }

    @Test
    fun `Group plausibility detect missing interested-role in OP`() {
        apiTestUtilities.createRGTest()
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProjectService!!.openproject
            val q = op.getMemberships(op.getRoles().filter { it.name == "Interested" })
            val q2 =
                q.filter {
                    it.project.value?.identifier == "rg-test" && ((it.principal.value as? OpenProjectGroup?)?.name == "rg-test-interested")
                }
            op.deleteMembership(q2.first())
        }
        val res = runCheckWithOP()
        apiTestUtilities.removeRGTest()
        assertEquals("Not OK", res.overallStatus)
        assertEquals(
            "Die Reader-Rolle ist in OP nicht an rg-test-interested vergeben.",
            res.entries.find { it.group.name == "rg-test" }!!.generalStatus,
        )
    }

    @Test
    fun `Group plausibility detects missing and wrong NC groupfolder registration`() {
        apiTestUtilities.createRGTest()
        val nc = apiTestUtilities.scenario!!.nextcloud!!
        val share = nc.findGroupFolder(NextcloudFolders.GroupShare)!!
        nc.removeGroupFromGroupFolder(share, "rg-test-interested")
        nc.removeGroupFromGroupFolder(share, "rg-test-member")
        nc.addGroupToGroupFolder(share, "rg-test-member", setOf(PermissionType.Read))
        val res = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        val status = res.entries.find { it.group.name == "rg-test" }!!.generalStatus
        assertTrue(
            status.contains("rg-test-interested wurde nicht zum Groupfolder in Nextcloud hinzugefügt."),
            "Status von rg-test enthält Meldung zur fehlenden Groupfolder-Registrierung nicht. Stattdessen: $status",
        )
        assertTrue(
            status.contains(
                "Berechtigung von rg-test-member in GroupFolders ist falsch. Es sollte ${setOf(
                    PermissionType.Read,
                    PermissionType.Update,
                    PermissionType.Create,
                    PermissionType.Delete,
                )} sein, ist jedoch ${setOf(PermissionType.Read)}.",
            ),
            "Status von rg-test enthält Meldung zur falschen Groupfolder-Registrierung nicht. Stattdessen: $status",
        )
    }

    @Test
    fun `Group plausibility detects missing GS membership in regional group`() {
        apiTestUtilities.createRGTest()
        val op = apiTestUtilities.scenario!!.openProject!!

        runBlocking {
            val projectMemberships = op.getMemberships(op.getRoles()).filter { it.project.value?.identifier == "rg-test" }
            op.deleteMembership(projectMemberships.first { (it.principal.value as? OpenProjectGroup)?.name == "geschaeftsstelle" })
        }
        val res = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        val status = res.entries.find { it.group.name == "rg-test" }!!.generalStatus
        assertTrue(
            status.contains("Die Member-Rolle ist in OP nicht an geschaeftsstelle vergeben."),
            "Status von rg-test enthält Meldung zur fehlenden Membershipmeldung nicht. Stattdessen: $status",
        )
    }

    @Test
    fun `Group plausibility can autorepair missing and wrong NC groupfolder registration`() {
        apiTestUtilities.createRGTest()
        val nc = apiTestUtilities.scenario!!.nextcloud!!
        val share = nc.findGroupFolder(NextcloudFolders.GroupShare)!!
        nc.removeGroupFromGroupFolder(share, "rg-test-interested")
        nc.removeGroupFromGroupFolder(share, "rg-test-member")
        nc.addGroupToGroupFolder(share, "rg-test-member", setOf(PermissionType.Read))
        repairCheck()
        val res = runCheckWithAll()
        apiTestUtilities.removeRGTest()
        val status = res.entries.find { it.group.name == "rg-test" }!!.generalStatus
        assertTrue(
            status.contains("OK"),
            "Status ist wider erwarten nicht OK: $status",
        )
        assertEquals("OK", res.overallStatus)
    }

    @Test
    fun `list template folders with brackets, spaces and underscore`() {
        val nc = apiTestUtilities.scenario!!.nextcloud!!
        val path = "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}/_Testvorlage"
        val parentFolder = "$path/${NextcloudFolders.groupInternal}"
        nc.createFolder(path)
        nc.createFolder(parentFolder)
        nc.createFolder("$parentFolder/01 Erkundung(intern)")
        nc.createFolder("$parentFolder/02 PR-FR (intern)")
        val result = nc.listFolders(parentFolder)
        nc.delete(path)
        assertEquals(listOf("01 Erkundung(intern)", "02 PR-FR (intern)"), result)
    }

    @Test
    fun `Group plausibility detects missing trial group in regional group`() {
        apiTestUtilities.createRG("rg-test1", "RG Test1")
        credentials.deleteGroup(credentials.getGroupByName("rg-test1-trial")!!)
        val res = runCheckWithAll()

        apiTestUtilities.removeMemberCrmInterested("rg-test1", "RG Test1", "${NextcloudFolders.GroupShare}/${NextcloudFolders.RgIntern}")

        // leute aus trial hat member rechte im eigenen projekt und interested in anderen
        assertEquals("LDAP-Gruppe fehlt ${RepairMessages.MANUAL}.", res.entries.find { it.group.name == "rg-test1-trial" }!!.generalStatus)
        assertEquals("Not OK", res.overallStatus)
    }

    private fun getPlausibility(): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/plausibility/checkgroups")
            .body(GroupPlausibilityConfig(AutoRepair.JUST_CHECK))

    private fun runCheckLDAPOnly(): GroupPlausibilityResults =
        runBlocking {
            groupPlausibility.checkGroupPlausibility(
                UUID.randomUUID(),
                GroupPlausibilityConfig(AutoRepair.JUST_CHECK, null, false, false),
            )
        }

    private fun runCheckWithOP(): GroupPlausibilityResults =
        runBlocking {
            groupPlausibility.checkGroupPlausibility(
                UUID.randomUUID(),
                GroupPlausibilityConfig(AutoRepair.JUST_CHECK, null, true, false),
            )
        }

    private fun runCheckWithAll(): GroupPlausibilityResults =
        runBlocking {
            groupPlausibility.checkGroupPlausibility(
                UUID.randomUUID(),
                GroupPlausibilityConfig(AutoRepair.JUST_CHECK, null, true, true),
            )
        }

    private fun repairCheck(): GroupPlausibilityResults =
        runBlocking {
            groupPlausibility.checkGroupPlausibility(
                UUID.randomUUID(),
                GroupPlausibilityConfig(AutoRepair.AUTOREPAIR_ALL, null, true, true),
            )
        }

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
