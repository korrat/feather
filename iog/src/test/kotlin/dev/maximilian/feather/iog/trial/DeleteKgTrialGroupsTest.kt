/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.trial

import dev.maximilian.feather.iog.BackgroundJobWithStrings
import dev.maximilian.feather.iog.internal.chapter.TrialOperationResult
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.multiservice.BackgroundJob
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.testutils.ServiceConfig
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpRequestWithBody
import kong.unirest.core.HttpResponse
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.TestInstance
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeleteKgTrialGroupsTest {
    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager, true)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = ServiceConfig.CREDENTIAL_PROVIDER

    @Test
    fun `Delete kg-trial groups by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = deleteKgTrialByApi().asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Delete kg-trial group returns correct body and removes all trial groups`() {
        apiTestUtilities.loginWithAdminUser()
        apiTestUtilities.createKG("kg-test", "KG Test")
        val response = deleteKgTrialByApi().asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response.status)
        val job = response.body
        assertNotNull(job, "Background job may not be null.")
        // assertEquals(Permission.ADMIN, job.permission)
        assertEquals("ADMIN", job.permission)
        assertEquals("started", job.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)
        var jobWorking: BackgroundJob
        do {
            jobWorking = backgroundJobManager.getJobStatus(job.id)!!
            Thread.sleep(100)
        } while (jobWorking.completed == null)
        Thread.sleep(500)
        assertEquals("OK", backgroundJobManager.getJobResults<TrialOperationResult>(jobWorking)!!.status)
        val g = credentials.getGroups()
        assertFalse(g.any { it.name == "kg-test-member" }, "Es wurde nicht die KG Member-Gruppe gelöscht.")
        assertTrue(g.any { it.name == "kg-test" }, "Es wurde die KG HauptGruppe gelöscht.")
        assertTrue(
            g.none {
                it.name.startsWith(IogPluginConstants.KG_PREFIX) &&
                    it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX)
            },
            "Es wurde die KG Trial-Gruppe nicht gelöscht.",
        )
        runBlocking {
            val groups = async { apiTestUtilities.scenario!!.openProject!!.getGroups() }
            val roles = async { apiTestUtilities.scenario!!.openProject!!.getRoles() }

            assertTrue(
                groups.await().none { it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) },
                "Es wurde die KG Trial-Gruppe nicht in OP gelöscht.",
            )

            assertTrue(
                groups.await().none { it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.MEMBER_SUFFIX) },
                "Es wurde die KG Member-Gruppe nicht in OP gelöscht.",
            )
            val r =
                apiTestUtilities.scenario!!.openProject!!.getMemberships(
                    listOf(roles.await().find { it.name == OPNameConfig(mutableMapOf()).memberRoleName }!!),
                )
            val t =
                r.mapNotNull {
                    (it.principal.value as? OpenProjectGroup)?.name
                }.filter { it.startsWith(IogPluginConstants.KG_PREFIX) && it.endsWith(IogPluginConstants.TRIAL_SUFFIX) }
            assertTrue(t.isEmpty(), "Es gibt noch Trial Memberships in OP: " + t.joinToString())
        }
        val ncGroups =
            apiTestUtilities.scenario!!.ncUtilities.nextcloud.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull {
                it.key
            } ?: emptyList()

        assertTrue(ncGroups.none { it.startsWith(IogPluginConstants.KG_PREFIX) && it.endsWith(IogPluginConstants.TRIAL_SUFFIX) })
        apiTestUtilities.removeKG("kg-test", "KG Test")
    }

    private fun deleteKgTrialByApi(): HttpRequestWithBody =
        apiTestUtilities
            .restConnection
            .delete("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/kgtrials")

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})
}
