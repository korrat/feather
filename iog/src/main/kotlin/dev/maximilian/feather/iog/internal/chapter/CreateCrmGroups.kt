/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.GetGroupDetails
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.groupPattern.MemberCrmInterestedPattern
import dev.maximilian.feather.iog.internal.groupPattern.SimpleMemberAdminCRM
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.UUID

private data class CrmPreconditionResult(
    val errorMessages: List<String>,
)

internal data class CrmOperationResult(
    val status: String,
    val errorMessages: List<String>,
)

internal class CreateCrmGroups(
    private val credentials: ICredentialProvider,
    private val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val onc: OPNameConfig,
    private val nextcloudPublicURL: String,
    private val civicrm: ICiviCRM,
) {
    var groups: Collection<Group> = emptyList()

    companion object : KLogging()

    fun createCrmSystem(
        jobId: UUID,
        autoRepair: Boolean,
    ): CrmOperationResult {
        val errorMessages = mutableListOf<String>()
        var status = "NOK"
        logger.info { "CreateCrmGroups::createCrmGroups fetching all groups " }
        backgroundJobManager.setJobStatus(jobId, "Fetching all users and groups from LDAP")

        val userAndGroups = credentials.getUsersAndGroups()
        groups = userAndGroups.second
        backgroundJobManager.setJobStatus(jobId, "Check read OpenProject projects and memberships")
        logger.info { "CreateCrmGroups::createCrmGroups create OPC" }
        val opc = OpenProjectPCreator(openProjectService.openproject, onc, jobId, backgroundJobManager)
        val opp = OpenProjectPlausibility(openProjectService.openproject, credentials, opc, onc, userAndGroups.first)

        logger.info { "CreateCrmGroups::createCrmGroups check preconditions" }
        backgroundJobManager.setJobStatus(jobId, "Check preconditions")

        val cachedResults = checkPreconditions()
        if (cachedResults.errorMessages.isNotEmpty()) {
            errorMessages.addAll(cachedResults.errorMessages)
            backgroundJobManager.setJobStatus(jobId, "Prüfung mit Fehlern abgeschlossen")
        } else if (autoRepair) {
            backgroundJobManager.setJobStatus(jobId, "Add crm groups")
            logger.warn { "CreateCrmGroups::createCrmGroups convert" }
            createCrmSystem(opp, jobId)
            backgroundJobManager.setJobStatus(jobId, "Die CRM-Konvertierung wurde erfolgreich durchgeführt.")
            status = "OK"
        } else {
            backgroundJobManager.setJobStatus(jobId, "Vorbedingungen für eine CRM-Konvertierung sind erfüllt.")
            status = "OK"
        }
        GetGroupDetails.logger.warn {
            "CreateCrmGroups::createCrmGroups status: $status result: ${
                errorMessages.joinToString(
                    ". ",
                )
            }"
        }
        return CrmOperationResult(status, errorMessages)
    }

    private fun checkPreconditions(): CrmPreconditionResult {
        val errorMessages = mutableListOf<String>()

        if (credentials.getGroupByName(LdapNames.IOG_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.IOG_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE) == null) errorMessages += "Gruppe '${LdapNames.INTERESTED_PEOPLE}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.TRIAL_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.TRIAL_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.ETHIC_VALIDATION_GROUP) == null) errorMessages += "Gruppe '${LdapNames.ETHIC_VALIDATION_GROUP}' existiert nicht"

        // if (civ == null) errorMessages += "CiviCRMService nicht parametriert."

        groups.filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) &&
                it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            if (credentials.getGroupByName(groupNameWithoutPrefix) == null) {
                errorMessages += "Konnte Basisgruppe für $groupNameWithoutPrefix nicht findet. Gebildet aus ${it.name}"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX) == null) {
                errorMessages += "Admin Gruppe <${groupNameWithoutPrefix + IogPluginConstants.ADMIN_SUFFIX} existiert nicht!"
            }
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX) == null) {
                errorMessages += "Trial Gruppe <${groupNameWithoutPrefix + IogPluginConstants.TRIAL_SUFFIX} existiert nicht!"
            }
        }
        groups.filter { it.name.endsWith(IogPluginConstants.CRM_SUFFIX) }.forEach {
            errorMessages += "CRM-Gruppe ${it.name} existiert bereits. Bitte alle CRM-Gruppen vor Erstellung löschen."
        }

        runBlocking {
            civicrm.getGroupByName(CiviCRMNames.USER_GROUP_NAME)?.let {
                logger.info { "CreateCrmGroups::checkPreconditions ${CiviCRMNames.USER_GROUP_NAME} in civicrm gefunden. Diese muss vorher gelöscht sein." }
                errorMessages += "Die Gruppe ${CiviCRMNames.USER_GROUP_NAME} ist bereits definiert."
            }
            civicrm.getGroupByName(CiviCRMNames.CONTACT_GROUP_NAME)?.let {
                logger.info { "CreateCrmGroups::checkPreconditions ${CiviCRMConstants.ACL_ROLE} in civicrm gefunden. Diese muss vorher gelöscht sein." }
                errorMessages += "Die Gruppe ${CiviCRMNames.CONTACT_GROUP_NAME} ist bereits definiert."
            }
            try {
                civicrm.findIndexOfACLRole()
                logger.info { "CreateCrmGroups::checkPreconditions ${CiviCRMConstants.ACL_ROLE} gefunden" }
            } catch (ex: Exception) {
                errorMessages += "${CiviCRMConstants.ACL_ROLE} nicht in CiviCRM definiert."
            }
        }
        return CrmPreconditionResult(errorMessages)
    }

    private fun createCrmSystem(
        opPlausibility: OpenProjectPlausibility,
        jobId: UUID,
    ) {
        logger.info("CreateCrmGroups::createCrmSystem Start creation of CRM - System")
        backgroundJobManager.setJobStatus(jobId, "Create crmmember-group in ldap and users-group in in civicrm ")
        logger.info("CreateCrmGroups::createCRMMembers Create standard groups ...")
        CiviGroupScheme(credentials, civicrm).createStandardGroups()

        runBlocking {
            val userGroupID = civicrm.getGroupByName(CiviCRMNames.USER_GROUP_NAME)!!.id
            val orgaContacts = civicrm.getGroupByName(CiviCRMNames.ORGANIZATION_GROUP_NAME)!!
            val personContacts = civicrm.getGroupByName(CiviCRMNames.PERSON_GROUP_NAME)!!

            backgroundJobManager.setJobStatus(jobId, "Create -crm group for each regional group")
            createCRMforRGs(opPlausibility, groups, userGroupID, orgaContacts.id, personContacts.id)
            backgroundJobManager.setJobStatus(jobId, "Create -crm group for program bila")
            createCRMforProgramGroup(opPlausibility, userGroupID, orgaContacts.id, personContacts.id)
        }
    }

    private fun createCRMforRGs(
        opPlausibility: OpenProjectPlausibility,
        allGroups: Collection<Group>,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
    ) {
        logger.info("CreateCrmGroups::createCRMforRGs")
        val groupScheme = CiviGroupScheme(credentials, civicrm)
        allGroups.filter {
            it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) && it.name.startsWith(IogPluginConstants.RG_PREFIX)
        }.forEach {
            CreateCrmGroups.logger.info("CreateCrmGroups::createCRMforRGs check group based on ${it.name}")
            val pat = MemberCrmInterestedPattern(credentials)
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.INTERESTED_SUFFIX)
            // names.add(groupNameWithoutPrefix)

            val opDescription =
                opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(groupNameWithoutPrefix))

            val credentialGroup =
                pat.addCrmGroupIfNecessary(
                    CreateGroupConfig(
                        groupNameWithoutPrefix,
                        opDescription,
                        emptySet(),
                        GroupKind.REGIONAL_GROUP,
                        nextcloudPublicURL,
                    ),
                )
            groupScheme.createSingleCiviChapter(credentialGroup, userGroupId, orgaContacts, personContacts)
        }
    }

    private fun createCRMforProgramGroup(
        opPlausibility: OpenProjectPlausibility,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
    ) {
        logger.info { "CreateCrmGroups::createCRMforProgramGroup" }
        val groupScheme = CiviGroupScheme(credentials, civicrm)

        groups.filter {
            it.name.startsWith(IogPluginConstants.PROGRAM_PREFIX) && it.name.endsWith(IogPluginConstants.PR_FR_SUFFIX)
        }.forEach {
            logger.info { "CreateCrmGroups::createCRMforProgramGroup Create CRM for ${it.name}" }
            val pat = SimpleMemberAdminCRM(credentials)

            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(it.name))

            val credentialGroup =
                pat.addCrmGroupIfNecessary(
                    CreateGroupConfig(
                        it.name,
                        opDescription,
                        emptySet(),
                        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC,
                        nextcloudPublicURL,
                    ),
                )
            groupScheme.createSingleCiviChapter(credentialGroup, userGroupId, orgaContacts, personContacts)
        }
    }
}
