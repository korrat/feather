/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.trials

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.iog.api.bindings.TrialRequest
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheckAction
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import mu.KLogging
import java.time.Instant

internal class TrialProtocol(
    private val db: TrialMemberDB,
    val credentialProvider: ICredentialProvider,
    private val actionController: ActionController?,
) : UserDeletionEvent {
    enum class TrialState {
        TRIAL_POSSIBLE,
        REQUEST_PENDING,
        TRIAL_ACTIVE,
        EXTENSION_POSSIBLE,
        EXTENSION_ACTIVE,
        EXPIRED,
    }
    companion object : KLogging()

    val halfYearInSeconds: Long = (60 * 60 * 24 * 365 / 2).toLong()

    internal fun upgradeTrial(ldapID: Int) {
        val user = credentialProvider.getUser(ldapID)
        if (user != null) {
            logger.info { "TrialProtocol::upgradeTrial Try to upgrade ${user.displayName}" }
            val groupsWithReplacedMember = mutableListOf<Int>()
            user.groups.forEach {
                val group = credentialProvider.getGroup(it)!!
                val groupName = group.name
                if (groupName.contains(IogPluginConstants.INTERESTED_SUFFIX)) {
                    val groupWithTrialName =
                        groupName.replace(IogPluginConstants.INTERESTED_SUFFIX, IogPluginConstants.TRIAL_SUFFIX)
                    logger.info { "TrialProtocol::upgradeTrial replace $groupName by $groupWithTrialName" }
                    groupsWithReplacedMember.add(
                        credentialProvider.getGroups().first { g -> g.name == groupWithTrialName }.id,
                    )
                } else {
                    groupsWithReplacedMember.add(it)
                }
            }
            credentialProvider.updateUser(user.copy(groups = groupsWithReplacedMember.toSet()))
            actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
            logger.info { "TrialProtocol::upgradeTrial done" }
        } else {
            logger.warn { "TrialProtocol::upgradeTrial could not find user for ldap id $ldapID" }
        }
    }

    fun requestTrial(userId: Int): Boolean =
        when (val q = getUserState(userId)) {
            TrialState.EXTENSION_POSSIBLE, TrialState.TRIAL_POSSIBLE -> {
                db.createRequest(userId, Instant.now())
                true
            }
            TrialState.TRIAL_ACTIVE -> {
                logger.info { "Ignore Trial request. $userId is already active." }
                false
            }
            else -> {
                logger.info { "No trial request possible. Status of $userId is $q." }
                false
            }
        }

    fun acceptTrialRequest(
        userId: Int,
        ts: Instant = Instant.now(),
    ) {
        db.removeRequest(userId)
        val trialState = db.getTrialState(userId)
        if (trialState == null) {
            db.startFirstTrial(userId, ts)
        } else if (trialState.Started.isAfter(Instant.now().minusSeconds((60 * 60 * 24 * 135).toLong()))) {
            logger.info { "User with id $userId is not extended, because trial period just started" }
        } else {
            db.updateTrials(userId, trialState.Extensions + 1, trialState.Started)
        }
        upgradeTrial(userId)
    }

    fun rejectTrialRequest(userId: Int) {
        db.removeRequest(userId)
    }

    fun deleteAll() {
        db.deleteAll()
    }

    fun getAllRequests(
        nodeGroupBases: List<String>,
        isAdmin: Boolean,
    ): Set<TrialRequest> {
        val response = mutableListOf<TrialRequest>()
        db.getAllRequests().forEach { requestEntry ->
            credentialProvider.getUser(requestEntry.requestUserID)?.let { user ->
                val isNodeAdmin =
                    user.groups.any { rGroup ->
                        nodeGroupBases.any {
                            credentialProvider.getGroup(rGroup)?.name?.contains(it) == true
                        }
                    }
                if (isAdmin || isNodeAdmin) {
                    response.add(
                        TrialRequest(
                            user.id,
                            user.username,
                            user.displayName,
                            db.getTrialState(requestEntry.requestUserID)?.Extensions?.inc() ?: 0,
                            requestEntry.started,
                        ),
                    )
                }
            }
        }
        return response.toSet()
    }

    fun getUserState(id: Int): TrialState {
        val trialEntry = db.getTrialState(id)
        val requestState = db.getRequestState(id)

        if (trialEntry == null) {
            return if (requestState == null) {
                TrialState.TRIAL_POSSIBLE
            } else {
                TrialState.REQUEST_PENDING
            }
        } else {
            val expired =
                Instant.now()
                    .isAfter(trialEntry.Started.plusSeconds(halfYearInSeconds + trialEntry.Extensions.toLong() * halfYearInSeconds))
            if (expired) {
                return if (trialEntry.Extensions == 0) {
                    TrialState.EXTENSION_POSSIBLE
                } else {
                    TrialState.EXPIRED
                }
            }
            return if (trialEntry.Extensions == 0) {
                TrialState.TRIAL_ACTIVE
            } else {
                TrialState.EXTENSION_ACTIVE
            }
        }
    }

    fun getUserStates(users: Iterable<User>): Map<User, TrialState> {
        val trialEntries = db.getAllTrialStates()
        val requestEntries = db.getAllRequests()

        return users.associateWith { user ->
            val trialEntry = trialEntries.singleOrNull { it.ldapID == user.id }
            val requestState = requestEntries.singleOrNull { it.requestUserID == user.id }

            when {
                trialEntry != null -> {
                    val expired =
                        Instant.now()
                            .isAfter(trialEntry.Started.plusSeconds(halfYearInSeconds + trialEntry.Extensions.toLong() * halfYearInSeconds))

                    when {
                        expired && trialEntry.Extensions == 0 -> TrialState.EXTENSION_POSSIBLE
                        expired -> TrialState.EXPIRED
                        trialEntry.Extensions == 0 -> TrialState.TRIAL_ACTIVE
                        else -> TrialState.EXTENSION_ACTIVE
                    }
                }
                requestState == null -> TrialState.TRIAL_POSSIBLE
                else -> TrialState.REQUEST_PENDING
            }
        }
    }

    fun getNodeAdminGroups(user: User): List<String> {
        return user.groups.mapNotNull {
            if (credentialProvider.getGroup(it)?.name?.contains(IogPluginConstants.ADMIN_SUFFIX) == true) {
                credentialProvider.getGroup(it)?.name
            } else {
                null
            }
        }
    }

    override fun userDeleted(user: User) {
        if (db.getRequestState(user.id) == null) {
            db.removeRequest(user.id)
        }
        if (db.getTrialState(user.id) == null) {
            db.removeTrial(user.id)
        }
    }

    fun mayAddToGroup(
        adminUser: User,
        group: Group,
        toAdd: Int,
    ): Boolean {
        credentialProvider.getUser(toAdd)?.let { addedUser ->
            val q = getUserState(addedUser.id)
            if (group.name.endsWith(IogPluginConstants.TRIAL_SUFFIX)) {
                if (q == TrialState.TRIAL_POSSIBLE || q == TrialState.EXTENSION_POSSIBLE) {
                    logger.warn { "TrialProtocol::mayAddToGroup Add user ${addedUser.displayName} automatically to trial database by $adminUser." }
                    requestTrial(addedUser.id)
                    acceptTrialRequest(addedUser.id)
                } else if (q == TrialState.REQUEST_PENDING) {
                    logger.warn { "TrialProtocol::mayAddToGroup Automatically accept pending trial request for ${addedUser.displayName}." }
                    acceptTrialRequest(addedUser.id)
                }
                if (q == TrialState.EXPIRED) {
                    logger.warn { "TrialProtocol::mayAddToGroup reject to add user to trial group. He has EXPIRED state." }
                    return false
                }
            }
        } ?: return false

        return true
    }
}
