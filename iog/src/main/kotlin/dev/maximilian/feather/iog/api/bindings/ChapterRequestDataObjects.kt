/*
*    Copyright [2022] Feather development team, see AUTHORS.md
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package dev.maximilian.feather.iog.api.bindings

import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType

internal data class ChapterDetails(
    val uid: String,
    val maingroupName: String,
    val patterntype: IogGroupSchema.PatternTypes,
    val groupKind: GroupKind,
    val nextcloudFolder: String,
    val nextcloudPermissions: Set<PermissionType>,
    val parentProject: String,
    val projectName: String,
    val description: String,
    val archived: Boolean,
)

internal data class ChapterDetailsRequest(
    val chapters: List<String>,
)
