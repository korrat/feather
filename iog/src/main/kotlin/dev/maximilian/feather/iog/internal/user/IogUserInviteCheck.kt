/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.user

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.settings.CheckUserEvent

internal class IogUserInviteCheck(val credentialProvider: ICredentialProvider) : CheckUserEvent {
    override fun check(
        user: User,
        autoRepair: Boolean,
    ): String? {
        var isInAdminGroup = false
        user.groups.forEach {
            if (credentialProvider.getGroup(it)?.name?.endsWith(IogPluginConstants.ADMIN_SUFFIX) == true) {
                isInAdminGroup = true
            }
        }

        val allGroupNames = user.groups.mapNotNull { credentialProvider.getGroup(it)?.name }
        val groupsWithSuffixes = mutableListOf<String>()
        allGroupNames.forEach {
            var prefixedName = it
            IogGroupSchema.possibleSubGroupSuffixes.forEach { prefixedName = prefixedName.substringBefore(it) }
            groupsWithSuffixes.add(prefixedName)
        }

        var message = ""
        if (groupsWithSuffixes.distinct().size != groupsWithSuffixes.size) {
            message += "User is in two groups of same node."
        }

        if (!isInAdminGroup && user.permissions.contains(Permission.INVITE)) {
            if (autoRepair) credentialProvider.updateUser(user.copy(permissions = user.permissions.minus(Permission.INVITE)))
            message += "INVITE Permission not necessary."
        }

        return if (message.isEmpty()) null else message
    }
}
