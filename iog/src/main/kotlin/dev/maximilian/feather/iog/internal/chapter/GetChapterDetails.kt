/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.iog.api.bindings.ChapterDetails
import java.util.UUID

internal class GetChapterDetails(
    private val chapterDB: ChapterDB,
) {
    fun getChapterDetails(uid: UUID): ChapterDetails {
        return chapterDB.getChapterByUUID(uid)
            ?: throw IllegalArgumentException("GetGroupDetails::getGroupDetails Group with id ($uid) not found")
    }
}
