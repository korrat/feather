/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.Group
import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.authorization.enumContains
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.iog.api.bindings.IogGroupCreateRequest
import dev.maximilian.feather.iog.api.bindings.IogGroupDeleteRequest
import dev.maximilian.feather.iog.api.bindings.IogGroupDetailsRequest
import dev.maximilian.feather.iog.api.bindings.IogGroupsDeleteRequest
import dev.maximilian.feather.iog.internal.chapter.ChapterDB
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.DeleteGroup
import dev.maximilian.feather.iog.internal.group.GetGroupDetails
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.apibuilder.ApiBuilder.put
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import io.javalin.openapi.HttpMethod
import io.javalin.openapi.OpenApi
import io.javalin.openapi.OpenApiContent
import io.javalin.openapi.OpenApiRequestBody
import io.javalin.openapi.OpenApiResponse
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import org.eclipse.jetty.http.HttpStatus
import java.util.Locale
import java.util.UUID

internal object GroupConstraints {
    val DescriptionLength = IntRange(3, 60)
    val NameLength = IntRange(3, 40)
    val NameRegex = Regex("^[a-z][a-z0-9\\-]*\$")
    val DescriptionRegex = Regex("^[a-zA-Z][\\ a-zA-Z0-9äüößÄÖÜ\\-\\+\\,\\*\\!\\?\\(\\)\\:\\&]*\$")
}

internal class IogGroupAPI(
    app: Javalin,
    private val onc: OPNameConfig,
    private val synchronizer: GroupSynchronizationEvent,
    private val credentialProvider: ICredentialProvider,
    services: List<IControllableService>,
    private val backgroundJobManager: BackgroundJobManager,
    private val publicUrl: String,
    private val iogGroupSchema: IogGroupSchema,
    private val chapterDB: ChapterDB,
    private val userRemovedFromGroupEvent: List<UserRemovedFromGroupEvent>,
) {
    init {
        app.routes {
            path("bindings/multiservice/groups") {
                put("/", ::handleCreateGroup, Permission.ADMIN)
                get("/{groupId}", ::handleGetGroup)
                post("/get", ::handleGetGroups)
                delete("/{groupId}", ::handleDeleteGroup, Permission.ADMIN)
                post("/delete", ::handleDeleteGroups, Permission.ADMIN)
            }
        }
    }

    private val openProjectService = services.firstOrNull { it is OpenProjectService }
    private val openProject = openProjectService?.let { (openProjectService as OpenProjectService).openproject }

    private val nextcloudService = services.firstOrNull { it is NextcloudService }
    private val nextcloud = nextcloudService?.let { (nextcloudService as NextcloudService).nextcloud }

    private val civicrmService = services.firstOrNull { it is CiviCRMService }
    private val civicrm = civicrmService?.let { (civicrmService as CiviCRMService).civicrm }

    private val logger = KotlinLogging.logger {}

    @OpenApi(
        summary = "Create a group according to given groupkind scheme",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/multiservice/groups",
        methods = [HttpMethod.PUT],
    )
    private fun handleCreateGroup(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            logger.warn { "IogGroupApi::handleCreateGroup() permission denied. Admin required" }
            throw ForbiddenResponse()
        }

        val launchAsync = ctx.queryParam("async")?.toBoolean() ?: false

        val body: IogGroupCreateRequest
        try {
            body = ctx.bodyAsClass<IogGroupCreateRequest>()
        } catch (e: Exception) {
            logger.warn { "IogGroupApi::handleCreateGroup() malformed entity" }
            throw BadRequestResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"
        if (body.displayName.isEmpty()) errorMessages += "Ein Anzeigename ist benötigt"
        if (!GroupConstraints.NameLength.contains(body.displayName.length)) {
            errorMessages += "Ein Anzeigename benötigt mindestens ${GroupConstraints.NameLength.first} und maximal ${GroupConstraints.NameLength.last} Zeichen"
        }
        if (body.description.isEmpty()) errorMessages += "Eine Beschreibung ist benötigt"
        if (!GroupConstraints.DescriptionLength.contains(body.description.length)) {
            errorMessages += "Eine Beschreibung benötigt mindestens ${GroupConstraints.DescriptionLength.first} und maximal ${GroupConstraints.DescriptionLength.last} Zeichen"
        }
        if (!(body.displayName matches GroupConstraints.NameRegex)) errorMessages += "Ein Anzeigename darf nur Kleinbuchstaben und Zahlen enthalten. Darf nicht mit einer Zahl starten."
        if (!(body.description matches GroupConstraints.DescriptionRegex)) errorMessages += "Die Beschreibung darf nur Groß- und Kleinbuchstaben, sowie Umlaute, Leerzeichen, Zahlen und die folgenden Sonderzeichen enthalten: , + - * ! ? ( ) : &. Muss mit einem Buchstaben starten."
        body.owners.forEach {
            if (credentialProvider.getUser(it) == null) errorMessages += "Der angebene Besitzer existiert nicht"
        }
        if (!enumContains<GroupKind>(body.kind.uppercase(Locale.getDefault()))) {
            val allowedKindValues = enumValues<GroupKind>().joinToString(", ") { it.name }
            errorMessages += "Die Gruppenart kann nur einer dieser Einträge sein: $allowedKindValues"
        }

        val groupKind = enumValueOf<GroupKind>(body.kind.uppercase(Locale.getDefault()))
        val groupKindDefinition = IogServiceApiConstants.GROUP_KINDS[groupKind]
        if (groupKindDefinition!!.adminNeeded) {
            if (body.owners.isEmpty()) {
                errorMessages += "Die angebene Gruppenart erfordert mindestens einen Gruppenadministrator"
            }
        }
        if (groupKindDefinition.relatedGroupNeeded) {
            if (body.owners.isNotEmpty()) {
                errorMessages += "Die angebene Gruppenart kann nicht mit einem Gruppenadministrator angelegt werden"
            }
            if (credentialProvider.getGroupByName(body.displayName) == null) errorMessages += "Dieser Anzeigename existiert noch nicht, ist aber benötigt"
        } else {
            if (credentialProvider.getGroupByName(body.displayName) != null) errorMessages += "Dieser Anzeigename existiert schon, bitte einen anderen wählen"
        }

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not create group with name ${body.displayName} due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        } else {
            val createGroupService =
                CreateGroup(
                    openProject,
                    nextcloud,
                    civicrm,
                    onc,
                    synchronizer,
                    backgroundJobManager,
                    iogGroupSchema,
                    chapterDB,
                    credentialProvider,
                )

            try {
                val config = CreateGroupConfig(body.displayName, body.description, body.owners, groupKind, publicUrl)

                val job: suspend (UUID?, CreateGroupConfig) -> List<Group> =
                    if (groupKindDefinition.relatedGroupNeeded) {
                        logger.info { "About to create a related group for the one with name ${body.displayName} only in OpenProject and Nextcloud." }

                        createGroupService::createOPandNCOnly
                    } else {
                        logger.info { "About to create a new group with name ${body.displayName}" }

                        createGroupService::createOPandNCandLDAPandCiviCRM
                    }

                if (launchAsync) {
                    logger.info { "Create the new group as a background job" }

                    val newJob = backgroundJobManager.runBackgroundJob(job, config)

                    ctx.status(HttpStatus.CREATED_201)
                    ctx.json(newJob)
                } else {
                    runBlocking { job(null, config) }
                    ctx.status(HttpStatus.CREATED_201)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Will not create a new or related group with name ${body.displayName} due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT):" + e.message
                ctx.status(HttpStatus.BAD_REQUEST_400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    @OpenApi(
        summary = "Get a specific group",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/multiservice/groups/{groupId}",
        methods = [HttpMethod.GET],
    )
    private fun handleGetGroup(ctx: Context) {
        val session = ctx.session()

        val groupId = ctx.pathParamAsClass<Int>("groupId").getOrThrow { BadRequestResponse("Group id needs to be an integer") }

        val group = credentialProvider.getGroup(groupId) ?: throw NotFoundResponse()

        val viewer = session.user
        if (!viewer.permissions.contains(Permission.ADMIN) &&
            !group.hasOwnerRights(
                viewer.id,
                credentialProvider,
            ) &&
            !group.userMembers.contains(viewer.id)
        ) {
            logger.warn { "IogGroupApi::handleGetGroup() permission denied. Admin or owner rights required" }
            throw ForbiddenResponse()
        }

        logger.info { "About to get details for group with id $groupId" }

        val getGroupDetailsService =
            GetGroupDetails(
                openProject,
                nextcloud,
                credentialProvider,
            )

        try {
            val groupDetails = getGroupDetailsService.getGroupDetails(groupId)
            ctx.status(200)
            ctx.json(groupDetails)
        } catch (e: Exception) {
            logger.warn(e) { "Could not get details of group with id $groupId due to exception" }

            val errorMessage = "Interner Fehler (Benachrichtige die IT)"
            ctx.status(400)
            ctx.json(errorMessage)
        }
    }

    @OpenApi(
        summary = "Get all groups",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("200"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/multiservice/groups/get",
        methods = [HttpMethod.GET],
    )
    private fun handleGetGroups(ctx: Context) {
        ctx.session()
        // TODO: viewer should be either admin or group owner to see details
        /*val viewer = session.user
        if (!viewer.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }*/

        val body: IogGroupDetailsRequest
        try {
            body = ctx.bodyAsClass<IogGroupDetailsRequest>()
        } catch (e: Exception) {
            logger.warn { "IogGroupApi::handleGetGroups() malformed entity" }
            throw BadRequestResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()
        if (body.groups.isEmpty()) errorMessages += "Keine Gruppen angegeben"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not get group details due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        } else {
            val getGroupDetailsService =
                GetGroupDetails(
                    openProject,
                    nextcloud,
                    credentialProvider,
                )

            val groupDetails =
                body.groups.mapNotNull {
                    logger.info { "About to get details for group with id $it" }

                    try {
                        getGroupDetailsService.getGroupDetails(it)
                    } catch (e: Exception) {
                        logger.warn(e) { "Could not get details of group with id $it due to exception" }

                        errorMessages += "Interner Fehler (Benachrichtige die IT)"

                        null
                    }
                }

            if (errorMessages.isNotEmpty()) {
                ctx.status(HttpStatus.BAD_REQUEST_400)
                ctx.json(errorMessages)
            } else {
                ctx.status(HttpStatus.OK_200)
                ctx.json(groupDetails)
            }
        }
    }

    @OpenApi(
        summary = "Delete a specific group including related groups in iog scheme",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/multiservice/groups/{groupId}",
        methods = [HttpMethod.DELETE],
    )
    private fun handleDeleteGroup(ctx: Context) {
        val session = ctx.session()

        val groupId = ctx.pathParamAsClass<Int>("groupId").getOrThrow { BadRequestResponse("Group id needs to be an integer") }

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            logger.warn { "IogGroupApi::handleDeleteGroup() permission denied. Admin required" }
            throw ForbiddenResponse()
        }

        val body: IogGroupDeleteRequest
        try {
            body = ctx.bodyAsClass<IogGroupDeleteRequest>()
        } catch (e: Exception) {
            logger.warn { "IogGroupApi::handleDeleteGroup() malformed entity" }
            throw BadRequestResponse()
        }

        if (credentialProvider.authenticateUserByUsernameOrMail(creator.username, body.password) == null) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Das vollständige Löschen der Daten muss bestätigt werden"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not delete group $groupId due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to delete group with id $groupId" }

            val deleteGroupService =
                DeleteGroup(
                    credentialProvider,
                    openProject,
                    nextcloud,
                    civicrm,
                    chapterDB,
                    userRemovedFromGroupEvent,
                )

            try {
                deleteGroupService.deleteGroup(groupId)
            } catch (e: Exception) {
                logger.warn(e) { "Will not delete group with id $groupId due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(HttpStatus.BAD_REQUEST_400)
                ctx.json(errorMessages)
                return
            }

            // CacheManager.refreshCacheAsync()

            ctx.status(HttpStatus.CREATED_201)
        }
    }

    @OpenApi(
        summary = "Delete multiple groups",
        requestBody = OpenApiRequestBody([OpenApiContent(Unit::class)]),
        responses = [OpenApiResponse("201"), OpenApiResponse("400"), OpenApiResponse("403")],
        path = "/v1/bindings/multiservice/groups/delete",
        methods = [HttpMethod.POST],
    )
    private fun handleDeleteGroups(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            logger.warn { "IogGroupApi::handleDeleteGroups() permission denied. Admin required" }
            throw ForbiddenResponse()
        }
        val body: IogGroupsDeleteRequest
        try {
            body = ctx.bodyAsClass<IogGroupsDeleteRequest>()
        } catch (e: Exception) {
            logger.warn { "IogGroupApi::handleDeleteGroups() malformed entity" }
            throw BadRequestResponse()
        }

        if (credentialProvider.authenticateUserByUsernameOrMail(creator.username, body.password) == null) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Das vollständige Löschen der Daten muss bestätigt werden"
        if (body.groups.isEmpty()) errorMessages += "Keine Gruppen zum Löschen angegeben"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not delete groups due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST_400)
            ctx.json(errorMessages)
        } else {
            val deleteGroupService =
                DeleteGroup(
                    credentialProvider,
                    openProject,
                    nextcloud,
                    civicrm,
                    chapterDB,
                    userRemovedFromGroupEvent,
                )

            try {
                body.groups.forEach {
                    logger.info { "About to delete group with id $it" }
                    deleteGroupService.deleteGroup(it)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Will not delete groups due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(HttpStatus.BAD_REQUEST_400)
                ctx.json(errorMessages)
                return
            }

            ctx.status(HttpStatus.CREATED_201)
        }
    }
}
