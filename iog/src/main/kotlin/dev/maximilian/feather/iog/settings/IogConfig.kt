/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.settings

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import org.jetbrains.exposed.sql.Database
import redis.clients.jedis.JedisPool

enum class SupportMembershipFeature {
    SUPPORT_MEMBERSHIP_DISABLED,
    SUPPORT_MEMBERSHIP_ENABLED,
    SUPPORT_MEMBERSHIP_ENFORCED,
}

data class IogConfig(
    val database: Database,
    val supportMembership: SupportMembershipFeature,
    val credentialProvider: ICredentialProvider,
    val onc: OPNameConfig,
    val backgroundJobManager: BackgroundJobManager,
    val nextcloudPublicUrl: String,
    val pool: JedisPool,
    val sessionOperations: ISessionOperations,
    val syncEvent: GroupSynchronizationEvent,
    val supportMemberHashAlgorithm: String,
)
