/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.memberCheck

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.internal.supportmember.SupportMember
import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.iog.internal.trials.TrialProtocol
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.events.UserAddedToGroupEvent
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import mu.KLogging
import java.time.Duration
import java.time.Instant
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Date
import kotlin.concurrent.fixedRateTimer

internal class MemberCheck(
    private val credentials: ICredentialProvider,
    internal val smr: SupportMemberDB,
    internal val trial: TrialProtocol,
    internal val actionController: ActionController?,
    private val getUserFunction: () -> Iterable<User>,
    hashAlgorithm: String,
    private val centralOfficeGroupName: String,
) : UserCreationEvent, UserDeletionEvent, UserAddedToGroupEvent, UserRemovedFromGroupEvent {
    enum class RegistrationState {
        NO_SUPPORT_MEMBERSHIP_REQUIRED,
        REGISTERED_SUPPORT_MEMBER,
        SM_MAIL_REGISTRATION_POSSIBLE,
        SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED,
        INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE,
        TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE,
        INTERESTED_SM_UPGRADE_CAN_BE_OFFERED,
        TRIAL_REQUIRED,
        INTERESTED_TRIAL_PENDING,
        INTERESTED_TRIAL_UPGRADE_POSSIBLE,
        REGISTERED_TRIAL_MEMBER,
        TRIAL_EXPIRED,
        INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED,
    }

    data class UserIdAndState(val userId: Int, val result: RegistrationState)

    companion object : KLogging() {
        internal fun userIsBlocked(result: RegistrationState) =
            userIsBlockedBySM(result) || userIsBlockedByTrial(result)

        internal fun userIsBlockedBySM(result: RegistrationState) =
            (result == RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE) || (result == RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED)

        internal fun userIsBlockedByTrial(result: RegistrationState) =
            (result == RegistrationState.TRIAL_EXPIRED) || (result == RegistrationState.TRIAL_REQUIRED)

        internal fun userCanUpgrade(result: RegistrationState) =
            (result == RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE) || (result == RegistrationState.TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE) || (result == RegistrationState.INTERESTED_SM_UPGRADE_CAN_BE_OFFERED) || (result == RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED)

        internal fun userCanRequestTrial(result: RegistrationState) =
            (result == RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED)
    }

    val databaseValidation = DatabaseValidation(hashAlgorithm)

    fun makeHash(s: String): String = databaseValidation.makeHash(s)

    init {
        if (actionController != null) {
            // Register the MemberCheck Action
            logger.warn { "MemberCheck::addActionsToUsers register member action." }
            actionController.registerActionType(
                MemberCheckAction.NAME,
                ::setupTimerAndCheckExistingActions,
            ) { userId: Int, payload: String ->
                MemberCheckAction(userId, convertRegistrationState(payload))
            }
        }
    }

    private fun convertRegistrationState(payload: String): RegistrationState {
        val r: RegistrationState
        try {
            r = RegistrationState.valueOf(payload)
        } catch (ex: Exception) {
            logger.error { "MemberCheck::convertRegistrationState ActionType failed; Invalid payload? payload = <$payload>" }
            throw IllegalArgumentException("Cannot transform string <$payload> to Membership-Registrationstate.")
        }
        return r
    }

    internal fun addActionsToUsers() {
        if (actionController == null) {
            logger.warn { "MemberCheck::addActionsToUsers actionController is null." }
            return
        }
        val startTime = Instant.now()
        logger.info { "MemberCheck::addActionsToUsers call getUserFunction" }
        val users = getUserFunction()
        logger.info { "MemberCheck::addActionsToUsers map all users. found ${users.count()}" }
        val usersBlocked =
            getRegistrationState(users).map { UserIdAndState(it.key.id, it.value) }.filter { userIsBlocked(it.result) }
        logger.info { "MemberCheck::addActionsToUsers getOpenUserActions, ${usersBlocked.count()} are blocked." }
        val usersActionOpen = actionController.getOpenUserActions(MemberCheckAction.NAME).map { it.userId }.toSet()

        val usersNeedingAction = usersBlocked.filter { !usersActionOpen.contains(it.userId) }

        logger.info { "MemberCheck::addActionsToUsers insert missing actions (${usersNeedingAction.count()})" }
        actionController.insertActions(usersNeedingAction.map { MemberCheckAction(it.userId, it.result) })

        val duration = Duration.between(startTime, Instant.now()).toMillis()
        logger.info {
            "MemberCheck::addActionsToUsers created ${usersNeedingAction.count()} new actions in $duration ms."
        }
    }

    // Only one user is checked, so the result contains the user
    fun getRegistrationState(user: User): RegistrationState = getRegistrationState(listOf(user))[user]!!

    private fun getRegistrationState(userList: Iterable<User>): Map<User, RegistrationState> {
        val groups = credentials.getGroups()
        val groupByIdMap = groups.associateBy { it.id }
        val trialStates = trial.getUserStates(userList)
        val registeredMapIdToExternalId = smr.getAllRegistered().toMap()
        // Stores the support member by mail hash 1 and mail hash 2 into this map
        // No collision should occur, because hash function is safe
        // A collision can occur, if 2 users have the same mail hash
        val supportMemberByMailHash = smr.getAllSupportMember().map {
            listOf(it.MailHash1 to it, it.MailHash2 to it)
        }.flatten().groupBy { it.first }.mapValues { it.value.map { v -> v.second } }

        return userList.associateWith { user ->
            val userGroups = user.groups.mapNotNull { groupByIdMap[it] }
            val hasPermissionOrCentralOffice =
                user.permissions.contains(Permission.ADMIN) || user.permissions.contains(Permission.FUNCTION_ACCOUNT) || userGroups.any {
                    it.name.startsWith(centralOfficeGroupName)
                }

            val needsTrialMembership =
                !hasPermissionOrCentralOffice && userGroups.any { it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) }
            val upgradePossible = !hasPermissionOrCentralOffice && userGroups.any {
                it.name.endsWith(IogPluginConstants.INTERESTED_SUFFIX) || it.name.endsWith(
                    IogPluginConstants.TRIAL_SUFFIX,
                )
            }
            val needsSupportMembership = !hasPermissionOrCentralOffice && userGroups.any {
                it.name.endsWith(IogPluginConstants.MEMBER_SUFFIX) || it.name.endsWith(IogPluginConstants.ADMIN_SUFFIX) || (
                    it.name.startsWith(
                        IogPluginConstants.KG_PREFIX,
                    ) && !it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX)
                    ) || it.name.startsWith(IogPluginConstants.AS_PREFIX) || it.name.endsWith(
                    IogPluginConstants.CRM_SUFFIX,
                )
            }

            val t = requireNotNull(trialStates[user])

            if (needsSupportMembership) {
                if (registeredMapIdToExternalId[user.id] == null) {
                    val byMailHash = supportMemberByMailHash[makeHash(user.mail)]
                    if (byMailHash != null) {
                        if (byMailHash.size == 1) {
                            logger.debug {
                                "MemberCheck::getRegistrationState user  ${user.displayName} could be registered as support member by mail. SM_MAIL_REGISTRATION_POSSIBLE"
                            }
                            RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE
                        } else {
                            logger.debug {
                                "MemberCheck::getRegistrationState user  ${user.displayName} has same mail hash as other user. Manual registration necessary. SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED"
                            }
                            RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED
                        }
                    } else {
                        logger.debug {
                            "MemberCheck::getRegistrationState ${user.displayName} with LDAP ID ${user.id} could not be verified as support member by mail. SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED."
                        }
                        RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED
                    }
                } else {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} already registered. REGISTERED_SUPPORT_MEMBER" }
                    RegistrationState.REGISTERED_SUPPORT_MEMBER
                }
            } else if (needsTrialMembership) {
                val externID = supportMemberByMailHash[makeHash(user.mail)]?.singleOrNull()?.ExternalID
                if (externID != null) {
                    logger.debug { "MemberCheck::getRegistrationState ${user.displayName} could be upgraded. TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE" }
                    RegistrationState.TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE
                } else if (t == TrialProtocol.TrialState.TRIAL_POSSIBLE || t == TrialProtocol.TrialState.EXTENSION_POSSIBLE) {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName}. Trial registration necessary. TRIAL_REQUIRED" }
                    RegistrationState.TRIAL_REQUIRED
                } else if (t == TrialProtocol.TrialState.TRIAL_ACTIVE || t == TrialProtocol.TrialState.EXTENSION_ACTIVE) {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} trial active. REGISTERED_TRIAL_MEMBER" }
                    RegistrationState.REGISTERED_TRIAL_MEMBER
                } else {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} trial expired. TRIAL_EXPIRED" }
                    RegistrationState.TRIAL_EXPIRED
                }
            } else {
                val externalId = supportMemberByMailHash[makeHash(user.mail)]?.singleOrNull()?.ExternalID
                if (!hasPermissionOrCentralOffice && (externalId != null)) {
                    logger.debug { "MemberCheck::getRegistrationState ${user.displayName} could be upgraded. INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE" }
                    RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE
                } else if (!hasPermissionOrCentralOffice && (t == TrialProtocol.TrialState.TRIAL_ACTIVE || t == TrialProtocol.TrialState.EXTENSION_ACTIVE)) {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} Trial active but not used. INTERESTED_TRIAL_UPGRADE_POSSIBLE" }
                    RegistrationState.INTERESTED_TRIAL_UPGRADE_POSSIBLE
                } else if (!hasPermissionOrCentralOffice && (t == TrialProtocol.TrialState.EXTENSION_POSSIBLE || t == TrialProtocol.TrialState.TRIAL_POSSIBLE)) {
                    logger.debug {
                        "MemberCheck::getRegistrationState ${user.displayName} does not need support membership. INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED"
                    }
                    RegistrationState.INTERESTED_TRIAL_REQUEST_AND_SM_UPGRADE_ALLOWED
                } else if (!hasPermissionOrCentralOffice && (t == TrialProtocol.TrialState.REQUEST_PENDING)) {
                    logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName}: Trial registration pending. INTERESTED_TRIAL_PENDING" }
                    RegistrationState.INTERESTED_TRIAL_PENDING
                } else if (!hasPermissionOrCentralOffice && upgradePossible) {
                    logger.debug { "MemberCheck::getRegistrationState ${user.displayName} can choose to upgrade. INTERESTED_SM_UPGRADE_CAN_BE_OFFERED" }
                    RegistrationState.INTERESTED_SM_UPGRADE_CAN_BE_OFFERED
                } else {
                    logger.debug { "MemberCheck::getRegistrationState ${user.displayName} does not need support membership. NO_SUPPORT_MEMBERSHIP_REQUIRED" }
                    RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED
                }
            }
        }
    }

    internal fun registerByMail(user: User): Boolean {
        logger.debug { "MemberCheck::registerByMail for user ${user.displayName}" }
        val state = getRegistrationState(user)
        if (state == RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE) {
            upgrade(user.id)
        }
        return if (state == RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE || state == RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE) {
            val externID = smr.getExternalIDbyMail(makeHash(user.mail))
            smr.storeLdapAndExternalID(user.id, externID!!).apply {
                actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
            }
            true
        } else {
            logger.debug { "MemberCheck::registerByMail for user ${user.displayName} not possible. State is $state" }
            false
        }
    }

    internal fun registerByNameAndBirthdate(
        nameBirthdateHash: String,
        ldapID: Int,
    ): Boolean {
        val externalID = smr.getExternalIDbyNameHash(nameBirthdateHash)
        return if (externalID != null) {
            smr.storeLdapAndExternalID(ldapID, externalID).apply {
                credentials.getUser(ldapID)?.let {
                    val reg = getRegistrationState(it)
                    if (reg != RegistrationState.REGISTERED_TRIAL_MEMBER && reg != RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED && reg != RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE && reg != RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED) {
                        upgrade(ldapID)
                    }
                    actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, it)
                }
            }
            logger.info { "MemberCheck::registerByNameAndBirthdate successfull for user with LDAP ID $ldapID" }
            true
        } else {
            logger.info { "MemberCheck::registerByNameAndBirthdate for ldap id $ldapID failed." }
            false
        }
    }

    internal fun downgrade(ldapID: Int) {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::downgrade for user ${user.displayName}" }
            val groupsWithoutKGAS = mutableListOf<Int>()
            user.groups.forEach {
                val groupName = credentials.getGroup(it)!!.name
                if (!groupName.startsWith(IogPluginConstants.KG_PREFIX) && !groupName.startsWith(
                        IogPluginConstants.AS_PREFIX,
                    )
                ) {
                    groupsWithoutKGAS.add(it)
                    logger.info { "MemberCheck::downgrade remove user from group $groupName" }
                }
            }

            val groupsWithReplacedMember = mutableListOf<Int>()
            groupsWithoutKGAS.forEach {
                val group = credentials.getGroup(it)!!
                val groupName = group.name
                var interestedGroupName = ""
                if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.ADMIN_SUFFIX, IogPluginConstants.INTERESTED_SUFFIX)
                } else if (groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX)) {
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.MEMBER_SUFFIX, IogPluginConstants.INTERESTED_SUFFIX)
                } else if (groupName.endsWith(IogPluginConstants.TRIAL_SUFFIX)) {
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.TRIAL_SUFFIX, IogPluginConstants.INTERESTED_SUFFIX)
                } else if (groupName.endsWith(IogPluginConstants.CRM_SUFFIX)) {
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.CRM_SUFFIX, IogPluginConstants.INTERESTED_SUFFIX)
                }

                if (interestedGroupName != "") {
                    logger.info { "MemberCheck::downgrade try to replace membership $groupName with $interestedGroupName" }
                    val newGroup = credentials.getGroups().firstOrNull { it.name == interestedGroupName }?.id
                    newGroup?.let {
                        groupsWithReplacedMember.add(newGroup)
                    }
                } else {
                    groupsWithReplacedMember.add(it)
                    logger.info { "MemberCheck::downgrade keep group membership $groupName" }
                }
            }

            credentials.updateUser(user.copy(groups = groupsWithReplacedMember.toSet()))

            // now that the user was downgraded, we can remove any blocking member-check actions
            actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)

            logger.info { "MemberCheck::downgrade done" }
        } else {
            logger.warn { "MemberCheck::downgrade could not find user for ldap id $ldapID" }
        }
    }

    internal fun upgrade(ldapID: Int) {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::upgrade Try to upgrade ${user.displayName}" }
            val groupsWithReplacedMember = mutableListOf<Int>()
            user.groups.forEach {
                val group = credentials.getGroup(it)!!
                val groupName = group.name
                if (groupName.contains(IogPluginConstants.INTERESTED_SUFFIX)) {
                    val groupWithMemberName =
                        groupName.replace(IogPluginConstants.INTERESTED_SUFFIX, IogPluginConstants.MEMBER_SUFFIX)
                    logger.info { "MemberCheck::upgrade replace $groupName by $groupWithMemberName}" }
                    groupsWithReplacedMember.add(credentials.getGroups().first { it.name == groupWithMemberName }.id)
                } else if (groupName.contains(IogPluginConstants.TRIAL_SUFFIX)) {
                    val groupWithMemberName =
                        groupName.replace(IogPluginConstants.TRIAL_SUFFIX, IogPluginConstants.MEMBER_SUFFIX)
                    logger.info { "MemberCheck::upgrade replace $groupName by $groupWithMemberName}" }
                    groupsWithReplacedMember.add(credentials.getGroups().first { it.name == groupWithMemberName }.id)
                } else {
                    groupsWithReplacedMember.add(it)
                }
            }
            credentials.updateUser(user.copy(groups = groupsWithReplacedMember.toSet()))
            trial.rejectTrialRequest(user.id)
            actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
            logger.info { "MemberCheck::upgrade done" }
        } else {
            logger.warn { "MemberCheck::upgrade could not find user for ldap id $ldapID" }
        }
    }

    internal fun checkAndUpgradeSM(ldapID: Int): Boolean {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::checkAndUpgrade Try to upgrade user (${user.displayName})" }
            var externalID = smr.getExternalIDbyLDAP(ldapID)
            if (externalID != null) {
                logger.info { "MemberCheck::checkAndUpgrade user was already registered." }
            } else {
                logger.info { "MemberCheck::checkAndUpgrade try to register ${user.displayName} by mail." }
                externalID = smr.getExternalIDbyMail(makeHash(user.mail))
                return if (externalID != null) {
                    smr.storeLdapAndExternalID(ldapID, externalID)
                    upgrade(ldapID)
                    logger.info { "MemberCheck::checkAndUpgrade Upgrade done" }
                    true
                } else {
                    logger.info { "MemberCheck::checkAndUpgrade could not find mail in db, auto registration impossible." }
                    false
                }
            }
        } else {
            logger.warn { "MemberCheck::checkAndUpgrade could not find user for ldap id $ldapID" }
        }
        return false
    }

    private val formatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME

    internal fun updateDatabase(hashEntries: List<SupportMemberHashRow>) {
        val newIDs = hashEntries.map { it.externalID }
        val registeredIDs = smr.getAllRegisteredExternalID()
        registeredIDs.forEach { if (!newIDs.contains(it)) smr.deleteSupportMembership(it) }
        val supportMemberIDs = smr.getAllSupportMemberExternalID()
        supportMemberIDs.forEach { if (!newIDs.contains(it)) smr.deleteSupportMembership(it) }
        hashEntries.forEach { smr.storeHash(it.externalID, it.nameBirthdayHash, it.mailHash1, it.mailHash2) }

        val now = ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        smr.setMetadata(SupportMemberDB.MetadataKey.LAST_UPDATED, formatter.format(now))
    }

    internal fun getLastDatabaseUpdate(): ZonedDateTime? {
        return smr.getMetadata(SupportMemberDB.MetadataKey.LAST_UPDATED)
            ?.let { ZonedDateTime.from(formatter.parse(it)) }
    }

    internal fun getAllRegisteredSupportMember(): List<RegisteredSupportMember> {
        val idExternalIdMap = smr.getAllRegistered().toMap()
        val userIdMap = credentials.getUsers().associateBy { it.id }

        return idExternalIdMap.mapNotNull {
            // The null return should never happen because the database ensures that the id is also available in the user table
            // So if this throws, the foreign key constraint is broken
            // The only
            val user = checkNotNull(userIdMap[it.key]) {
                "MemberCheck::getAllRegisteredSupportMember could not find user with id=${it.value}, although registered as support member. Is the foreign key constraint of the database broken?"
            }

            RegisteredSupportMember(user.firstname, user.surname, it.key, it.value)
        }
    }

    internal fun getAllSupportMembersOfFundraisingBox(): List<SupportMember> = smr.getAllSupportMember()

    internal fun getSimulation(): List<SimulatedSupportMember> {
        val users = credentials.getUsers()

        // Stores the support member by mail hash 1 and mail hash 2 into this map
        // No collision should occur, because hash function is safe
        // A collision can occur, if 2 users have the same mail hash
        val supportMemberByMailHash = smr.getAllSupportMember().map {
            listOf(it.MailHash1 to it, it.MailHash2 to it)
        }.flatten().groupBy { it.first }.mapValues { it.value.map { v -> v.second } }

        return getRegistrationState(users).mapNotNull { (user, state) ->
            if (state == RegistrationState.SM_MAIL_REGISTRATION_POSSIBLE || state == RegistrationState.INTERESTED_AUTOMATIC_SM_UPGRADE_POSSIBLE || state == RegistrationState.TRIAL_AUTOMATIC_SM_UPGRADE_POSSIBLE) {
                val externalID = supportMemberByMailHash[makeHash(user.mail)]?.singleOrNull()?.ExternalID
                SimulatedSupportMember(
                    user.firstname,
                    user.surname,
                    user.id,
                    externalID,
                    state,
                )
            } else {
                if (state == RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED) {
                    SimulatedSupportMember(
                        user.firstname,
                        user.surname,
                        user.id,
                        null,
                        RegistrationState.SM_NAME_BIRTHDAY_REGISTRATION_REQUIRED,
                    )
                } else {
                    null
                }
            }
        }
    }

    private fun deleteFromSupportMemberRegistration(user: User) {
        val state = getRegistrationState(user)
        if (state == RegistrationState.REGISTERED_SUPPORT_MEMBER) {
            smr.deleteUserRegistration(user.id)
            logger.info {
                "MemberCheck::deleteFromSupportMemberRegistration user ${user.displayName} with ldapID=${user.id} deleted from support member registration database"
            }
        } else {
            logger.info {
                "MemberCheck::deleteFromSupportMemberRegistration did not find user ${user.displayName} with ldapID ${user.id} in support member registration."
            }
        }
    }

    override fun userCreated(user: User) {
        val result = getRegistrationState(user)
        if (result == RegistrationState.TRIAL_REQUIRED) {
            logger.info { "MemberCheck::userCreated user was inserted in TRIAL group. Autoadd acceptance." }
            trial.requestTrial(user.id)
            trial.acceptTrialRequest(user.id)
        } else {
            if (userIsBlocked(result)) {
                actionController?.insertAction(MemberCheckAction(user.id, result))
            }
        }
    }

    override fun userDeleted(user: User) {
        deleteFromSupportMemberRegistration(user)
        actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
    }

    override fun mayAddToGroup(
        adminUser: User,
        group: Group,
        toAdd: Int,
    ): Boolean {
        if (group.name.startsWith(centralOfficeGroupName)) {
            credentials.getUser(toAdd)?.let { addedUser ->
                logger.info { "MemberCheck::mayAddToGroup remove blocking events for freshly added GS member: ${addedUser.username}" }
                actionController?.getOpenUserActions(addedUser)
                    ?.forEach { actionController.deleteActionsByNameAndUser(it.name, addedUser) }
            }
        }
        return trial.mayAddToGroup(adminUser, group, toAdd)
    }

    override fun userRemovedFromGroup(
        userId: Int,
        groupId: Int,
    ) {
        credentials.getUser(userId)?.let { user ->
            val r = getRegistrationState(user).toString()
            logger.info { "MemberCheck::userRemovedFromGroup ${user.displayName} from group with id : $groupId. State now is now $r" }
            actionController?.getOpenUserActions(user)?.let { actions ->
                actions.filter { it.name == "membership" && it.payload.toString() != r }.onEach {
                    logger.info { "MemberCheck::userRemovedFromGroup Delete obsolete action ${it.payload} from action database for ${user.displayName}" }
                    actionController.deleteActionsByNameUserAndPayload(it.name, user, it.payload.toString())
                }
            }
        }
    }

    private fun setupTimerAndCheckExistingActions() {
        if (actionController != null) {
            // Periodically add the action to users which have not verified their membership state
            // Also check on application start
            fixedRateTimer(
                daemon = true,
                startAt = Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(1L, ChronoUnit.DAYS)),
                period = Duration.ofDays(1).toMillis(),
            ) { addActionsToUsers() }
            addActionsToUsers()
        }
    }
}
