/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.iog.internal.group.plausibility.RepairMessages
import dev.maximilian.feather.iog.internal.group.plausibility.SelfCheckingFileSystem
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.tools.PublicLinkConverter
import dev.maximilian.feather.nextcloud.INextcloud
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import mu.KLogging

internal data class InputInternalPublicFolder(
    private val nc: INextcloud,
    private val folderName: String,
    private val groupName: String,
    private val memberGroupName: String?,
    private val linkConverter: PublicLinkConverter,
    private val isProject: Boolean,
) {
    companion object : KLogging()

    private val repairable = RepairMessages.AUTO
    private val repaired = RepairMessages.REPAIR_DONE
    private val notRepairable = RepairMessages.MANUAL

    internal data class CopyInstruction(
        val source: String,
        val target: String,
    )

    internal data class CreationSequence(
        val createFolderCommands: List<String>,
        val copyCommands: List<CopyInstruction>,
        val readmePath: String,
        val inputFolder: String,
    )

    fun create() {
        val cmdSequence = getCmdSequence()
        cmdSequence.createFolderCommands.forEach { nc.createFolder(it) }
        cmdSequence.copyCommands.forEach { nc.copy(it.source, it.target) }

        createReadme(cmdSequence)
        /* TODO : ACL Links setzen
         * (needs nc:acl WebDAV properties extensions which Sardine does not have)
         *
         * ACLs to be set (permissions in parentheses are inherited):
         *
         * for input folder:
         * admins: (+read) (+write) (+create) (+delete) (+share)
         * interested people (if !inputFolderFix): -read +write +create -delete -share
         * interested people (if inputFolderFix): -read (-write) (-create) -delete -share
         * member-group: (+read) (+write) (+create) (+delete) -share
         * proko (if project): (+read) (+write) (+create) (+delete) (+share)
         *
         * for group-internal folder:
         * interested people: -read -write -create -delete -share
         * member-group: (+read) (+write) (+create) (+delete) -share
         * admins: (+read) (+write) (+create) (+delete) (+share)
         * proko (if project): (+read) (+write) (+create) (+delete) (+share)
         *
         * for public folder:
         * interested people: -read -write -create -delete -share
         * iogmembers: (+read) -write -create -delete (+share)
         * interested-group (if rg or project): (+read) -write -create -delete -share
         * member-group: (+read) (+write) (+create) (+delete) +share
         * admins: (+read) (+write) (+create) (+delete) (+share)
         * proko (if project): (+read) (+write) (+create) (+delete) (+share)
         */
    }

    fun verify(autoRepair: Boolean): List<String> {
        val errorList = mutableListOf<String>()
        val cmdSequence = getCmdSequence()
        val cachedFileSystem = nc as? SelfCheckingFileSystem ?: throw Exception("InputInternalPublicFolder::Verify must be called with SelfcheckingFilesystem and was not!")

        cmdSequence.createFolderCommands.forEach {
            if (!it.contains(NextcloudFolders.groupInternal) || it.endsWith(NextcloudFolders.groupInternal)) {
                if (!cachedFileSystem.folderExists(it)) {
                    errorList.add("Konnte Nextcloud-Ordner nicht finden: <$it>")
                    if (autoRepair) {
                        cachedFileSystem.createFolderRecursive(it)
                        errorList.add("$repaired. NC Permission Skript nicht vergessen!!!")
                    } else {
                        errorList.add("$repairable.")
                    }
                }
            }
        }
        cmdSequence.copyCommands.forEach {
            if (!it.target.contains(NextcloudFolders.groupInternal)) {
                if (!cachedFileSystem.folderExists(it.source)) {
                    errorList.add("Vorlageverzeichnis existiert nicht: ${it.source} $notRepairable.")
                } else if (!cachedFileSystem.folderExists(it.target)) {
                    errorList.add("Zielverzeichnis existiert nicht: ${it.target} $notRepairable.")
                } else {
                    errorList.addAll(compareFolderTree(it.source, it.target))
                }
            }
        }
        val files = cachedFileSystem.listFiles(folderName)
        val readme = cmdSequence.readmePath.substringAfterLast('/')
        if (!files.contains(readme)) {
            errorList.add("Konnte Readme.md nicht finden, erwarteter Ort : <$folderName/$readme>")
            if (autoRepair) {
                createReadme(cmdSequence)
                errorList.add("$repaired.")
            } else {
                errorList.add("$repairable.")
            }
        }
        return errorList
    }

    private fun createReadme(cmdSequence: CreationSequence) {
        Thread.sleep(10000) // wait to avoid "File does not exist" - Response from ShareAPI for freshly generated folders
        val inputShareid =
            nc.createShare(
                cmdSequence.inputFolder,
                ShareType.PublicLink,
                LdapNames.INTERESTED_PEOPLE,
                false,
                setOf(PermissionType.Create),
                "", // no expiration date
            )

        val link = nc.getSpecificShare(inputShareid)!!.url!!
        val publicLink = linkConverter.convert(link)

        nc.createFile(cmdSequence.readmePath, "$groupName\r\n\r\nEingang: <$publicLink>")
    }

    private fun getCmdSequence(): CreationSequence {
        val inputFolder = folderName + "/" + NextcloudFolders.inputFolder
        val groupInternalFolder = folderName + "/" + NextcloudFolders.groupInternal
        val publicFolder = folderName + "/" + NextcloudFolders.publicFolder

        val r = mutableListOf<String>()
        r.add(inputFolder)
        r.add(groupInternalFolder)
        r.add(publicFolder)

        val cpList = mutableListOf<CopyInstruction>()

        if (isProject) {
            cpList.add(
                CopyInstruction(
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}/${NextcloudFolders.template}/${NextcloudFolders.groupInternal}",
                    groupInternalFolder,
                ),
            )
            cpList.add(
                CopyInstruction(
                    "${NextcloudFolders.GroupShare}/${NextcloudFolders.Projects}/${NextcloudFolders.template}/${NextcloudFolders.publicFolder}",
                    publicFolder,
                ),
            )
        } else {
            NextcloudFolders.standardFolders.forEach {
                r.add("$groupInternalFolder/$it")
            }
        }

        val readme = "$folderName/Readme.md"
        return CreationSequence(r, cpList, readme, inputFolder)
    }

    private fun compareFolderTree(
        source: String,
        target: String,
    ): MutableList<String> {
        val q = mutableListOf<String>()
        val s: List<String>
        val t: List<String>
        try {
            s = nc.listFolders(source)
        } catch (ex: Exception) {
            logger.error { "InputInternalPublicFolder::compareFolderTree scanned for not existing source folder $source. $ex" }
            throw ex
        }
        try {
            t = nc.listFolders(target)
        } catch (ex: Exception) {
            logger.error { "InputInternalPublicFolder::compareFolderTree scanned for not existing target folder $target. $ex" }
            throw ex
        }
        s.forEach {
            if (!t.contains(it)) {
                q.add("Konnte <$target/$it> in NC nicht finden, obwohl es in der Vorlage <$source> definiert ist $notRepairable.")
            } else {
                q.addAll(compareFolderTree("$source/$it", "$target/$it"))
            }
        }
        return q
    }
}
