/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.iog.api.GroupPlausibilityApi
import dev.maximilian.feather.iog.api.IOGApi
import dev.maximilian.feather.iog.api.IogChapterApi
import dev.maximilian.feather.iog.api.IogGroupAPI
import dev.maximilian.feather.iog.api.SupportMembershipApi
import dev.maximilian.feather.iog.internal.chapter.ChapterDB
import dev.maximilian.feather.iog.internal.group.IogGroupSchema
import dev.maximilian.feather.iog.internal.memberCheck.MemberCheck
import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.trials.TrialMemberDB
import dev.maximilian.feather.iog.internal.trials.TrialProtocol
import dev.maximilian.feather.iog.internal.user.IogUserInviteCheck
import dev.maximilian.feather.iog.settings.IogConfig
import dev.maximilian.feather.iog.settings.SupportMembershipFeature
import dev.maximilian.feather.multiservice.events.UserAddedToGroupEvent
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.CheckUserEvent
import io.javalin.Javalin
import mu.KLogging

class IogPlugin(
    val config: IogConfig,
    actionControllerIn: ActionController,
    accountController: AccountController,
    externalUserDeletionEvents: List<UserDeletionEvent>,
    externalUserCreationEvents: List<UserCreationEvent>,
) {
    companion object : KLogging()

    val synchronizeEvent = config.syncEvent
    internal var trial: TrialProtocol? = null
    val userDeletionEvents: MutableList<UserDeletionEvent> = mutableListOf<UserDeletionEvent>().apply { addAll(externalUserDeletionEvents) }
    val userCreationEvents: MutableList<UserCreationEvent> = mutableListOf<UserCreationEvent>().apply { addAll(externalUserCreationEvents) }

    private var memberCheck: MemberCheck? = null
    internal val iogGroupSchema = IogGroupSchema(config.credentialProvider)

    val checkUserEvent = IogUserInviteCheck(config.credentialProvider) as CheckUserEvent
    val chapterDB = ChapterDB(config.database)
    val userRemovedFromGroupEvent: MutableList<UserRemovedFromGroupEvent> = mutableListOf<UserRemovedFromGroupEvent>()

    var userMayAddCheck: UserAddedToGroupEvent? = null

    init {
        if (config.supportMembership != SupportMembershipFeature.SUPPORT_MEMBERSHIP_DISABLED) {
            var actionController: ActionController? = null

            if (config.supportMembership == SupportMembershipFeature.SUPPORT_MEMBERSHIP_ENFORCED) {
                logger.info { "IogPlugin::init SupportMembership enforced." }
                actionController = actionControllerIn
            } else {
                logger.info { "IogPlugin::init SupportMembership enabled." }
            }
            trial = TrialProtocol(TrialMemberDB(config.database, accountController), config.credentialProvider, actionController)
            userDeletionEvents.add(trial!!)
            val smrDB = SupportMemberDB(config.database, accountController)
            logger.info { "IogPlugin::init create MemberCheck" }
            memberCheck =
                MemberCheck(
                    config.credentialProvider,
                    smrDB,
                    trial!!,
                    actionController,
                    config.credentialProvider::getUsers,
                    config.supportMemberHashAlgorithm,
                    config.onc.centralOfficeGroupName,
                )
            userMayAddCheck = memberCheck
            userRemovedFromGroupEvent.add(memberCheck as UserRemovedFromGroupEvent)
            logger.info { "IogPlugin::init add Events" }
            userCreationEvents.add(memberCheck!!)
            userDeletionEvents.add(memberCheck!!)
            logger.info { "IogPlugin::init finished." }
        } else {
            logger.warn { "IogPlugin::init SupportMembership disabled." }
        }
    }

    fun startApis(
        app: Javalin,
        services: List<IControllableService>,
    ) {
        memberCheck?.let {
            logger.info { "IogPlugin::startApis start MembershipAPI" }
            SupportMembershipApi(app, trial!!, it, config.credentialProvider)
        }

        logger.info { "IogPlugin::startApis start IogGroupAPI with ${services.count()} services, then GdprAPI." }

        IogGroupAPI(
            app, config.onc, synchronizeEvent,
            config.credentialProvider, services,
            config.backgroundJobManager, config.nextcloudPublicUrl,
            iogGroupSchema,
            chapterDB,
            userRemovedFromGroupEvent,
        )

        services.firstOrNull { it is OpenProjectService }?.let {
            logger.info { "IogPlugin::startApis OpenProject is configured, init IOGApi (Reset Sandbox)." }
            IOGApi(app, (it as OpenProjectService).openproject, config.backgroundJobManager, config.onc)
            val openProjectService = services.find { it is OpenProjectService } as OpenProjectService
            services.firstOrNull { it is NextcloudService }?.let {
                GroupPlausibilityApi(
                    app,
                    config.credentialProvider,
                    config.backgroundJobManager,
                    iogGroupSchema,
                    openProjectService,
                    config.syncEvent,
                    config.onc,
                    (it as NextcloudService).nextcloud,
                    config.nextcloudPublicUrl,
                )
                IogChapterApi(
                    app,
                    config.credentialProvider,
                    config.backgroundJobManager,
                    openProjectService,
                    config.syncEvent,
                    config.onc,
                    it.nextcloud,
                    config.nextcloudPublicUrl,
                    chapterDB,
                    iogGroupSchema,
                    services.firstOrNull { s -> s is CiviCRMService } as CiviCRMService?,
                )
            } ?: {
                logger.warn { "IogPlugin::startApis No Nextcloud configured. Skipping Plausibility and Chapter API." }
            }
        } ?: run {
            logger.warn { "IogPlugin::startApis No OpenProject configured. Skipping IOG API, GroupPlausibility and Chapter API." }
        }
    }
}
