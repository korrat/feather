/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import mu.KLogging

internal open class MemberAdminTrialPattern(
    protected open val credentialProvider: ICredentialProvider,
) : IGroupPattern {
    companion object : KLogging() {
    }

    protected val adminDescriptionPrefix = "Administratoren von "
    protected val memberDescriptionPrefix = "Fördermitglieder der Gruppe "
    protected val trialDescriptionPrefix = "Aktive Interessierte der Gruppe "
    protected val mainGroupDescriptionPrefix = "Alle Mitglieder von "

    override fun create(config: CreateGroupConfig): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val trials = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)
        requireNotNull(trials) { "Cannot find group '${LdapNames.TRIAL_MEMBERS}'" }

        val trialGroup = credentialProvider.createGroup(createTrialGroupBody(setOf(trials.id), config))
        val memberGroup = credentialProvider.createGroup(createMemberGroupBody(iogMembers.id, config))
        val masterGroup =
            credentialProvider.createGroup(
                createMainGroupBody(
                    setOf(memberGroup.id, trialGroup.id),
                    config,
                ),
            )
        val adminGroup =
            credentialProvider.createGroup(
                createAdminGroupBody(
                    setOf(
                        memberGroup.id,
                        trialGroup.id,
                    ),
                    config,
                ),
            )

        logger.info {
            "MemberAdminTrialPattern::create Created LDAP group ${masterGroup.name} with admin group ${adminGroup.name}, "
            "trial group ${trialGroup.name}  and member group ${memberGroup.name}"
        }
        return arrayOf(masterGroup, adminGroup, memberGroup, trialGroup).toList()
    }

    override fun search(prefixedName: String): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val memberGroup = credentialProvider.getGroupByName(getMemberGroupName(prefixedName))
        requireNotNull(memberGroup) { "Cannot find group '${getMemberGroupName(prefixedName)}'" }

        val rgGroup = credentialProvider.getGroupByName(prefixedName)
        requireNotNull(rgGroup) { "Cannot find group '$prefixedName'" }

        val adminGroup = credentialProvider.getGroupByName(getAdminGroupName(prefixedName))
        requireNotNull(adminGroup) { "Cannot find group '${getAdminGroupName(prefixedName)}'" }

        val trialGroup = credentialProvider.getGroupByName(getTrialGroupName(prefixedName))
        requireNotNull(trialGroup) { "Cannot find group '${getTrialGroupName(prefixedName)}'" }

        logger.info {
            "MemberAdminTrialPattern::search Found existing LDAP group" +
                " ${rgGroup.name} with admin group ${adminGroup.name}, " +
                "and member group ${memberGroup.name} as well as trial group ${trialGroup.name}"
        }

        return arrayOf(rgGroup, adminGroup, memberGroup, trialGroup).toList()
    }

    protected fun createMemberGroupBody(
        parentID: Int,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = getMemberGroupName(conf.ldapName),
            description = "$memberDescriptionPrefix${conf.description}",
            parentGroups = setOf(parentID),
            userMembers = emptySet(),
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet(),
        )
    }

    protected fun createTrialGroupBody(
        parentGroupIDs: Set<Int>,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = getTrialGroupName(conf.ldapName),
            description = "$trialDescriptionPrefix${conf.description}",
            parentGroups = parentGroupIDs,
            userMembers = emptySet(),
            groupMembers = emptySet(),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet(),
        )
    }

    protected fun createMainGroupBody(
        groupMemberIDs: Set<Int>,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = conf.ldapName,
            description = "$mainGroupDescriptionPrefix${conf.description}",
            parentGroups = emptySet(),
            userMembers = emptySet(),
            groupMembers = groupMemberIDs,
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet(),
        )
    }

    protected fun createAdminGroupBody(
        ownedGroupsG: Set<Int>,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = getAdminGroupName(conf.ldapName),
            description = "$adminDescriptionPrefix${conf.description}",
            parentGroups = emptySet(),
            userMembers = conf.ownerIDs,
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(),
            ownedGroups = ownedGroupsG,
        )
    }

    protected fun getTrialGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.TRIAL_SUFFIX
    }

    protected fun getMemberGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.MEMBER_SUFFIX
    }

    private fun getInterestedGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.INTERESTED_SUFFIX
    }

    protected fun getAdminGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.ADMIN_SUFFIX
    }

    override fun getMetaDescription(
        groupName: String,
        description: String,
    ): GroupMetaDescription {
        if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.ADMIN_SUFFIX.length)
            return GroupMetaDescription(
                name = getAdminGroupName(prefixedName),
                description = "$adminDescriptionPrefix$description",
                parentGroupNames = emptySet(),
                ownerNames = emptySet(),
                groupMemberNames = emptySet(),
                ownerGroupNames = emptySet(),
                ownedGroupNames =
                setOf(
                    getInterestedGroupName(prefixedName),
                    getMemberGroupName(prefixedName),
                    getTrialGroupName(prefixedName),
                ),
                descriptionWithoutPrefix = description,
            )
        } else if (groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.MEMBER_SUFFIX.length)
            return GroupMetaDescription(
                name = getMemberGroupName(prefixedName),
                description = "$memberDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.IOG_MEMBERS),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else if (groupName.endsWith(IogPluginConstants.TRIAL_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.TRIAL_SUFFIX.length)
            return GroupMetaDescription(
                name = getTrialGroupName(prefixedName),
                description = "$trialDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.TRIAL_MEMBERS, prefixedName),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else {
            return GroupMetaDescription(
                name = groupName,
                description = "$mainGroupDescriptionPrefix$description",
                parentGroupNames = setOf(),
                ownerNames = emptySet(),
                groupMemberNames =
                setOf(
                    getMemberGroupName(groupName),
                    getInterestedGroupName(groupName),
                    getTrialGroupName(groupName),
                ),
                ownerGroupNames = setOf(),
                ownedGroupNames = setOf(),
                descriptionWithoutPrefix = description,
            )
        }
    }

    override fun getAllMetaDescriptions(
        groupName: String,
        description: String,
    ): List<GroupMetaDescription> {
        val prefixedName = removeSuffixIfNecessary(groupName)
        return listOf(
            getMetaDescription(prefixedName, description),
            getMetaDescription(getAdminGroupName(prefixedName), description),
            getMetaDescription(getMemberGroupName(prefixedName), description),
            getMetaDescription(getTrialGroupName(prefixedName), description),
        )
    }

    open fun removeSuffixIfNecessary(groupName: String): String {
        var prefixedName = groupName
        if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.ADMIN_SUFFIX.length)
        } else if (groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.MEMBER_SUFFIX.length)
        } else if (groupName.endsWith(IogPluginConstants.TRIAL_SUFFIX)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.TRIAL_SUFFIX.length)
        }
        return prefixedName
    }

    override fun isAdminGroup(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)
    }

    fun isMemberGroup(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX)
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX) ||
            groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX) ||
            groupName.endsWith(IogPluginConstants.TRIAL_SUFFIX)
    }

    override fun delete(groupName: String) {
        credentialProvider.getGroupByName(getMemberGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getAdminGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getTrialGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(groupName)?.let { credentialProvider.deleteGroup(it) }
    }

    override fun isMainGroup(groupName: String): Boolean {
        return groupName == removeSuffixIfNecessary(groupName)
    }

    fun addTrialGroupIfNecessary(config: CreateGroupConfig): Group {
        val trialMembers = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)
        requireNotNull(trialMembers) { "Cannot find group '${LdapNames.TRIAL_MEMBERS}'" }

        val oldParent = credentialProvider.getGroupByName(config.ldapName)
        requireNotNull(oldParent) { "Cannot find group ${config.ldapName}" }

        val existingMemberGroup = credentialProvider.getGroupByName(getMemberGroupName(config.ldapName))
        existingMemberGroup?.let {
            logger.info { "MemberAdminTrialPattern::addTrialGroupIfNecessary group ${it.name} already exists" }
        }
        if (existingMemberGroup == null) {
            credentialProvider.updateGroup(oldParent.copy(name = getMemberGroupName(config.ldapName)))
            credentialProvider.createGroup(createMainGroupBody(setOf(oldParent.id), config))
        }
        val newParent = credentialProvider.getGroupByName(config.ldapName)
        requireNotNull(newParent) { "Cannot find group '${config.ldapName}'" }

        val generatedGroup =
            credentialProvider.getGroupByName(getTrialGroupName(config.ldapName))?.let { return it }
                ?: credentialProvider.createGroup(createTrialGroupBody(setOf(trialMembers.id, newParent.id), config))
                    .also { trialGroup ->
                        val adminName = getAdminGroupName(config.ldapName)
                        val adminGroup =
                            credentialProvider.getGroupByName(adminName)
                                ?: throw Exception("Could not find admin group $adminName to create trial group.")
                        credentialProvider.updateGroup(adminGroup.copy(ownedGroups = adminGroup.ownedGroups.plus(trialGroup.id)))
                    }

        return generatedGroup
    }
}
