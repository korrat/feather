/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.*

internal class DeleteKgTrials(
    private val credentials: ICredentialProvider,
    private val op: IOpenProject,
    private val backgroundJobManager: BackgroundJobManager,
    private val nc: Nextcloud,
    private val onc: OPNameConfig,
) {
    companion object : KLogging()

    suspend fun delete(
        jobId: UUID,
        param: Boolean,
    ): TrialOperationResult {
        logger.warn { "DeleteKgTrials::delete remove all kg-trial groups " }
        backgroundJobManager.setJobStatus(jobId, "Remove all kg-trial groups")

        val nct = nc.findGroupFolder(NextcloudFolders.GroupShare) ?: throw Exception("Could not find IOG share in NC")
        val trialGroups =
            credentials.getGroups().filter {
                it.name.startsWith(IogPluginConstants.KG_PREFIX) &&
                    it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX)
            }

        val allNames = nc.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull { it.key } ?: emptyList()
        trialGroups.forEach {
            credentials.getGroupByName(it.name.removeSuffix(IogPluginConstants.TRIAL_SUFFIX).plus(IogPluginConstants.MEMBER_SUFFIX))?.let {
                    memberGroup ->
                credentials.getGroupByName(memberGroup.name.removeSuffix(IogPluginConstants.MEMBER_SUFFIX))?.let {
                        parentGroup ->
                    credentials.deleteGroup(parentGroup)
                }
                // credentials.deleteGroup(it)
                credentials.getGroupByName(memberGroup.name)?.let { newMember ->
                    credentials.updateGroup(newMember.copy(name = memberGroup.name.removeSuffix(IogPluginConstants.MEMBER_SUFFIX)))
                }
            }
            credentials.deleteGroup(it)
        }
        runBlocking {
            op.getGroups()
                .filter {
                    it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.TRIAL_SUFFIX) ||
                        it.name.startsWith(IogPluginConstants.KG_PREFIX) && it.name.endsWith(IogPluginConstants.MEMBER_SUFFIX)
                }
                .asFlow()
                .collect { op.deleteGroup(it) }
        }

        allNames.filter {
            it.startsWith(IogPluginConstants.KG_PREFIX) && it.endsWith(IogPluginConstants.TRIAL_SUFFIX) ||
                it.startsWith(IogPluginConstants.KG_PREFIX) && it.endsWith(IogPluginConstants.MEMBER_SUFFIX)
        }.forEach { nc.removeGroupFromGroupFolder(nct, it) }

        logger.warn { "DeleteKgTrials::delete remove TrialMember group " }
        backgroundJobManager.setJobStatus(jobId, "Remove all TrialMember group")

        logger.warn { "DeleteKgTrials::delete finished." }
        backgroundJobManager.setJobStatus(jobId, "Finished.")
        return TrialOperationResult("OK", emptyList())
    }
}
