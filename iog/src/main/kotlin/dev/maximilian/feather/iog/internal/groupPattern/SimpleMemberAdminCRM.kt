package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal class SimpleMemberAdminCRM(
    credentialProvider: ICredentialProvider,
) : MemberAdminInterestedPattern(credentialProvider) {
    private val crmDescriptionPrefix = "CRM-Nutzer der Gruppe "

    override fun create(config: CreateGroupConfig): List<Group> {
        val groupList = super.create(config)

        val crmMembers = credentialProvider.getGroupByName(LdapNames.CRM_MEMBERS)
        requireNotNull(crmMembers) { "Cannot find group '${LdapNames.CRM_MEMBERS}'" }

        return groupList + addCrmGroupIfNecessary(config)
    }

    override fun search(prefixedName: String): List<Group> {
        val groupList = super.search(prefixedName)

        val crmGroup = credentialProvider.getGroupByName(getCrmGroupName(prefixedName))
        requireNotNull(crmGroup) { "Cannot find group '${getCrmGroupName(prefixedName)}'" }

        CreateGroup.logger.info {
            "MemberCrmInterestedPattern::search Found existing LDAP group with crm group ${crmGroup.name}"
        }
        return groupList + crmGroup
    }

    private fun createCrmGroupBody(
        parentIDs: Set<Int>,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = getCrmGroupName(conf.ldapName),
            description = "$crmDescriptionPrefix${conf.description}",
            parentGroups = parentIDs,
            userMembers = emptySet(),
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet(),
        )
    }

    private fun getCrmGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.CRM_SUFFIX
    }

    override fun getMetaDescription(
        groupName: String,
        description: String,
    ): GroupMetaDescription {
        if (groupName.endsWith(IogPluginConstants.CRM_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.CRM_SUFFIX.length)
            return GroupMetaDescription(
                name = getCrmGroupName(prefixedName),
                description = "$crmDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.CRM_MEMBERS, prefixedName),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else {
            val prefixedName = super.removeSuffixIfNecessary(groupName)
            val oldDescription = super.getMetaDescription(groupName, description)
            return if (isAdminGroup(groupName)) {
                oldDescription.copy(
                    ownedGroupNames = oldDescription.ownedGroupNames + getCrmGroupName(prefixedName),
                )
            } else if (isMemberGroup(groupName)) {
                oldDescription.copy(
                    groupMemberNames = oldDescription.groupMemberNames + getCrmGroupName(prefixedName),
                )
            } else {
                oldDescription
            }
        }
    }

    override fun getAllMetaDescriptions(
        groupName: String,
        description: String,
    ): List<GroupMetaDescription> {
        return super.getAllMetaDescriptions(
            groupName,
            description,
        ).plus(getMetaDescription(getCrmGroupName(removeSuffixIfNecessary(groupName)), description))
    }

    override fun removeSuffixIfNecessary(groupName: String): String {
        return if (groupName.endsWith(IogPluginConstants.CRM_SUFFIX)) {
            groupName.substring(0, groupName.length - IogPluginConstants.CRM_SUFFIX.length)
        } else {
            super.removeSuffixIfNecessary(groupName)
        }
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return super.userMembersAllowed(groupName) || groupName.endsWith(IogPluginConstants.CRM_SUFFIX)
    }

    override fun delete(groupName: String) {
        super.delete(groupName)
        credentialProvider.getGroupByName(getCrmGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
    }

    fun addCrmGroupIfNecessary(config: CreateGroupConfig): Group {
        val crm = credentialProvider.getGroupByName(LdapNames.CRM_MEMBERS)
        requireNotNull(crm) { "Cannot find group '${LdapNames.CRM_MEMBERS}'" }

        val parent = credentialProvider.getGroupByName(config.ldapName)
        requireNotNull(parent) { "Cannot find group '${config.ldapName}'" }

        return credentialProvider.getGroupByName(getCrmGroupName(config.ldapName))?.let { return it }
            ?: credentialProvider.createGroup(createCrmGroupBody(setOf(crm.id, parent.id), config))
                .also { crmGroup ->
                    val adminName = getAdminGroupName(config.ldapName)
                    val adminGroup =
                        credentialProvider.getGroupByName(adminName)
                            ?: throw Exception("Could not find admin group $adminName to create crm group.")
                    credentialProvider.updateGroup(adminGroup.copy(ownedGroups = adminGroup.ownedGroups.plus(crmGroup.id)))
                }
    }
}
