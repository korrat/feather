/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.internal.civicrm.ACLProtocol
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.iog.internal.settings.CiviCRMNames
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import kotlinx.coroutines.runBlocking

class CiviGroupScheme(private val credentials: ICredentialProvider, private val civicrm: ICiviCRM) {
    fun createStandardGroups() {
        createMemberGroup()

        runBlocking {
            val userGroupID = createUsersGroup()

            IogPluginConstants.CiviCRMCustomType.values().forEach { e -> civicrm.createContactType(e.protocolName, e.label, e.parent) }

            val contactMainGroup =
                civicrm.getGroupByName(CiviCRMNames.CONTACT_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.CONTACT_GROUP_NAME, "Personen- und Organisationenkontakte von Spendern")
            val orgaContacts =
                civicrm.getGroupByName(CiviCRMNames.ORGANIZATION_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.ORGANIZATION_GROUP_NAME, "Spenderorganisationen")
            val personContacts =
                civicrm.getGroupByName(CiviCRMNames.PERSON_GROUP_NAME)
                    ?: civicrm.createGroup(CiviCRMNames.PERSON_GROUP_NAME, "Kontaktpersonen")
            civicrm.updateParentOfGroup(orgaContacts.id, contactMainGroup.id)
            civicrm.updateParentOfGroup(personContacts.id, contactMainGroup.id)

            createEthicGroup(userGroupID, civicrm)
            createContactAdminGroup(
                LdapNames.CENTRAL_OFFICE,
                "Geschäftsstelle",
                IogPluginConstants.CiviCRMCustomType.Employee,
                userGroupID,
                contactMainGroup.id,
            )
            createContactAdminGroup(
                LdapNames.ASSOCIATION_BOARD,
                "Vorstand",
                IogPluginConstants.CiviCRMCustomType.IogMember,
                userGroupID,
                contactMainGroup.id,
            )
        }
    }

    internal fun createSingleCiviChapter(
        credentialGroup: Group,
        userGroupId: Int,
        orgaContacts: Int,
        personContacts: Int,
    ) {
        CreateCrmGroups.logger.info("CiviGroupScheme::createSingleCiviGroup create group for ${credentialGroup.name} in civicrm")

        runBlocking {
            val result =
                civicrm.createOrReplaceSubgroupWithACLPermission(
                    credentialGroup.name,
                    credentialGroup.description,
                    "Rolle für ${credentialGroup.name}",
                    CiviCRMNames.CRM_USER_ROLE_NAME,
                )
            civicrm.updateParentOfGroup(result.group.id, userGroupId)
            val contactsCreated =
                civicrm.createGroup(
                    credentialGroup.name + IogPluginConstants.CONTACT_SUFFIX,
                    "Personenkontakte der " + credentialGroup.description,
                )

            val organizationsCreated =
                civicrm.createGroup(
                    credentialGroup.name + IogPluginConstants.ORGA_SUFFIX,
                    "Organisationen der " + credentialGroup.description,
                )
            civicrm.updateParentOfGroup(contactsCreated.id, personContacts)
            civicrm.updateParentOfGroup(organizationsCreated.id, orgaContacts)

            civicrm.createACL(
                "Von " + credentialGroup.name + " verwaltete Personenkontakte",
                result.roleIndex,
                contactsCreated.id,
                ACLProtocol.Operations.EDIT,
            )
            civicrm.createACL(
                "Von " + credentialGroup.name + " verwaltete Organisationen",
                result.roleIndex,
                organizationsCreated.id,
                ACLProtocol.Operations.EDIT,
            )
        }
    }

    private fun createMemberGroup() {
        val iogMember = credentials.getGroupByName(LdapNames.IOG_MEMBERS)!!
        credentials.getGroupByName(LdapNames.CRM_MEMBERS) ?: credentials.createGroup(
            Group(
                0, LdapNames.CRM_MEMBERS, "Alle Fördermitglieder mit CRM-Zugriff",
                emptySet(), emptySet(), setOf(iogMember.id), emptySet(), emptySet(), emptySet(),
            ),
        )
    }

    private suspend fun createUsersGroup(): Int {
        CreateCrmGroups.logger.info("CiviGroupScheme::createUsersGroup Create group users.")
        val g =
            civicrm.createOrReplaceTopLevelGroupWithACLPermission(
                CiviCRMNames.USER_GROUP_NAME,
                "Alle CRM-Nutzer",
                CiviCRMNames.CRM_USER_ROLE_NAME,
            )
        civicrm.createACL("Nutzer können sich sehen", g.roleIndex, g.group.id, ACLProtocol.Operations.VIEW)
        return g.group.id
    }

    private suspend fun createEthicGroup(
        contactMainGroupId: Int,
        civicrm: ICiviCRM,
    ) {
        CreateCrmGroups.logger.info("CiviGroupScheme::createUsersGroup Create group ${LdapNames.ETHIC_VALIDATION_GROUP}.")
        civicrm.createOrReplaceSubgroupWithACLPermission(
            LdapNames.ETHIC_VALIDATION_GROUP,
            "Ethikprüfer",
            "Rolle für Ethikprüfer",
            CiviCRMNames.CRM_USER_ROLE_NAME,
        ).let {
                ethicGroup ->
            // civicrm.createACL("Ethikprüfer", ethicGroup.roleIndex, orgaContacts, ACLProtocol.Operations.EDIT)
            // logger.info("CreateCrmGroups::createCrmGroups create acl for Ethikprüfer with orga group id $orgaContacts ")
            civicrm.updateParentOfGroup(ethicGroup.group.id, contactMainGroupId)
        }
    }

    private suspend fun createContactAdminGroup(
        credentialName: String,
        description: String,
        contactType: IogPluginConstants.CiviCRMCustomType,
        userGroupId: Int,
        contactMainGroupId: Int,
    ) {
        val createGroupResult =
            civicrm.createOrReplaceSubgroupWithACLPermission(
                credentialName,
                description,
                "Rolle für $description",
                CiviCRMNames.CRM_USER_ROLE_NAME,
            )
        civicrm.updateParentOfGroup(createGroupResult.group.id, userGroupId)
        civicrm.createACL(credentialName, createGroupResult.roleIndex, contactMainGroupId, ACLProtocol.Operations.EDIT)
        credentials.getGroupByName(credentialName)?.let {
                credentialGroup ->
            CreateCrmGroups.logger.info {
                "CiviGroupScheme::createContactAdminGroup found ${credentialGroup.userMembers.count()} users in group $credentialName."
            }
            credentialGroup.userMembers.forEach {
                    userID ->
                val credentialUser = credentials.getUser(userID)
                credentialUser?.let {
                    CreateCrmGroups.logger.info { "CiviGroupScheme::createContactAdminGroup Import contact ${it.mail} for group $credentialName" }
                    val existingContactID = civicrm.getContactIdByEmail(it.mail)
                    if (existingContactID == null) {
                        CreateCrmGroups.logger.info {
                            "CiviGroupScheme::createContactAdminGroup Contact for ${it.mail} for group $credentialName does not exists. Create email, groupContact and contact with firstname ${credentialUser.firstname} and lastname ${credentialUser.surname} with type ${contactType.protocolName}."
                        }
                        runBlocking {
                            val createdContact =
                                civicrm.createContact(
                                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                                    contactType.protocolName,
                                    credentialUser.firstname,
                                    credentialUser.surname,
                                )
                            civicrm.createEmail(it.mail, createdContact.id)
                            civicrm.createGroupContact(createdContact.id, createGroupResult.group.id)
                        }
                    } else {
                        CreateCrmGroups.logger.info {
                            "CiviGroupScheme::createContactAdminGroup Contact for ${it.mail} for group $credentialName already exists. Create only groupContact."
                        }
                        civicrm.createGroupContact(existingContactID, createGroupResult.group.id)
                    }
                } ?: CreateCrmGroups.logger.warn { "User $userID does not exist in Credential Provider but is member of group." }
            }
        } ?: CreateCrmGroups.logger.warn { "$credentialName does not exist in Credential Provider." }
    }

    fun deleteChapter(civiGroupName: String) {
        runBlocking {
            civicrm.deleteRoleWithAclDependencies(civiGroupName)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName + IogPluginConstants.ORGA_SUFFIX)
            civicrm.deleteGroupWithAclAndContactDependencies(civiGroupName + IogPluginConstants.CONTACT_SUFFIX)
        }
    }
}
