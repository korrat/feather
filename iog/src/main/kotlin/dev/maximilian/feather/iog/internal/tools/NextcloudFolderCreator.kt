/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.InputInternalPublicFolder
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.INextcloud
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import java.util.UUID

internal class NextcloudFolderCreator {
    internal fun getParentFolder(
        groupKind: GroupKind,
        ldapName: String,
        description: String?,
    ): String {
        val fullGroupName = description ?: ldapName
        return when (groupKind) {
            GroupKind.COMPETENCE_GROUP, GroupKind.COMMITTEE ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.KG,
                    fullGroupName,
                )
            GroupKind.COLLABORATION ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.collaboration,
                    fullGroupName,
                )
            GroupKind.REGIONAL_GROUP ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.RgIntern,
                    fullGroupName,
                )
            GroupKind.BILA_GROUP ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.BiLa,
                    fullGroupName,
                )
            GroupKind.PR_FR_GROUP ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.PrFr,
                    fullGroupName,
                )
            GroupKind.PROJECT ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    NextcloudFolders.Projects,
                    fullGroupName,
                )
            else ->
                arrayOf(
                    NextcloudFolders.GroupShare,
                    fullGroupName,
                )
        }.joinToString("/")
    }

    fun createFolderStructureForGroupNextcloud(
        ldapName: String,
        description: String?,
        groupKind: GroupKind,
        createdLdapGroups: List<Group>,
        nextcloud: INextcloud,
        jobId: UUID?,
        backgroundJobManager: BackgroundJobManager,
        nextcloudPublicUrl: String,
    ) {
        jobId?.let { backgroundJobManager.setJobStatus(it, "Looking for groupfolder in Nextcloud") }
        val mainGroupFolder = nextcloud.findGroupFolder(NextcloudFolders.GroupShare)
        if (mainGroupFolder != null) {
            val accessToBeAssigned = getAccessToBeAssigned(groupKind, createdLdapGroups.size)
            createdLdapGroups.zip(accessToBeAssigned).forEach {
                if (it.second != null) {
                    val permissionType: Set<PermissionType> =
                        when {
                            it.second!! -> PermissionType.values().toSet()
                            else -> setOf(PermissionType.Read, PermissionType.Update, PermissionType.Create, PermissionType.Delete)
                        }
                    nextcloud.addGroupToGroupFolder(mainGroupFolder, it.first.name, permissionType)
                }
            }
        }

        val parentFolders = getParentFolder(groupKind, ldapName, description)

        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating main folder in Nextcloud") }
        CreateGroup.logger.info { "Create::createFolderStructureForGroupNextcloud Creating folder $parentFolders and its subfolders" }
        try {
            nextcloud.createFolder(parentFolders)
        } catch (e: Exception) {
            CreateGroup.logger.error(
                e,
            ) { "Create::createFolderStructureForGroupNextcloud Creating folder $parentFolders failed with exception." }
            throw RuntimeException("Failed to create group folder in Nextcloud")
        }
        val memberGroup = getMemberGroupName(groupKind, createdLdapGroups)

        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating subfolders in Nextcloud") }
        val groupName = description ?: ldapName
        val linkConverter = PublicLinkConverter(nextcloudPublicUrl)
        val sub =
            InputInternalPublicFolder(
                nextcloud,
                parentFolders,
                groupName,
                memberGroup.name,
                linkConverter,
                groupKind == GroupKind.PROJECT,
            )
        try {
            sub.create()
        } catch (e: Exception) {
            CreateGroup.logger.error(e) {
                "Create::createFolderStructureForGroupNextcloud Creating subfolders for $parentFolders failed with exception."
            }
            throw RuntimeException("Failed to create group subfolders in Nextcloud")
        }
    }

    internal fun getAccessToBeAssigned(
        groupKind: GroupKind,
        createdLdapGroupsSize: Int,
    ): Array<Boolean?> =
        when (groupKind) {
            GroupKind.REGIONAL_GROUP ->
                arrayOf(
                    // see arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup, trial, crm).toList() in createRegionalGroupSubGroups
                    null,
                    null,
                    false,
                    false,
                    false,
                    null,
                )
            GroupKind.PROJECT, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED ->
                arrayOf(
                    // see arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup, trial).toList() in createRegionalGroupSubGroups
                    null,
                    null,
                    false,
                    false,
                    false,
                )
            GroupKind.COLLABORATION,
            GroupKind.COMPETENCE_GROUP, GroupKind.COMMITTEE,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
            ->
                arrayOf(
                    // see arrayOf(group, adminGroup).toList() in createCompetenceGroupOrCommitteSubGroups
                    false,
                    null,
                )
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC ->
                arrayOf(
                    // see arrayOf(group, adminGroup).toList() in createCompetenceGroupOrCommitteSubGroups
                    false,
                    null,
                    null,
                )
            GroupKind.PR_FR_GROUP, GroupKind.BILA_GROUP ->
                if (createdLdapGroupsSize == IogServiceApiConstants.GROUPS_PER_MEMBER_CRM_INTERESTED_PATTERN) {
                    // BILA or PR/FR associated with a regional group
                    // see arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup).toList() in createRegionalGroupSubGroups
                    arrayOf(
                        null,
                        false,
                        null,
                        false,
                        null,
                    )
                } else if (createdLdapGroupsSize == IogServiceApiConstants.GROUPS_PER_MEMBER_ADMIN_INTERESTED_PATTERN) {
                    // BILA or PR/FR associated with a regional group
                    // see arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup).toList() in createRegionalGroupSubGroups
                    arrayOf(
                        null,
                        false,
                        null,
                        false,
                    )
                } else {
                    // standalone BILA orPR/FR group
                    // see arrayOf(group, adminGroup).toList() in createCompetenceGroupOrCommitteSubGroups
                    arrayOf(
                        false,
                        null,
                    )
                }
            GroupKind.LDAP_ONLY -> throw IllegalArgumentException("invalid group kind for nextcloud group")
        }

    internal fun getMemberGroupName(
        groupKind: GroupKind,
        createdLdapGroups: List<Group>,
    ) = when (groupKind) {
        GroupKind.REGIONAL_GROUP, GroupKind.PROJECT, GroupKind.NO_SPECIALITY_MEMBER_ADMIN_INTERESTED ->
            // see listOf(rgGroup, adminGroup, interestedGroup, memberGroup, ...) in createRegionalGroupSubGroups
            createdLdapGroups[3]
        GroupKind.COMPETENCE_GROUP,
        GroupKind.COMMITTEE,
        GroupKind.COLLABORATION,
        GroupKind.PR_FR_GROUP, GroupKind.BILA_GROUP,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_CRM_PUBLIC,
        GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
        ->
            // see arrayOf(group, adminGroup).toList() in createCompetenceGroupOrCommitteSubGroups
            createdLdapGroups[0]
        GroupKind.LDAP_ONLY -> throw IllegalArgumentException("invalid group kind for openproject group")
    }
}
