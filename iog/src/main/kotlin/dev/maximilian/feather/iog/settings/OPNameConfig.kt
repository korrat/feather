/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.settings

import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.internal.settings.OPRoleNames
import dev.maximilian.feather.multiservice.StringTools

class OPNameConfig(propertyMap: MutableMap<String, String>) {
    internal fun credentialGroupToOpenProjectGroup(ldapName: String): String {
        return when (ldapName) {
            LdapNames.IOG_MEMBERS -> iogMembersGroupName
            LdapNames.INTERESTED_PEOPLE -> interestedPeopleGroupName
            LdapNames.TRIAL_MEMBERS -> trialMemberGroupName
            LdapNames.CRM_MEMBERS -> crmMemberGroupName
            else -> ldapName
        }
    }

    internal val interestedPeopleGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.interestedPeople") { OPGroupNames.INTERESTED_PEOPLE.trimIndent() }
    internal val iogMembersGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.iogMembers") { OPGroupNames.IOG_MEMBERS.trimIndent() }
    internal val trialMemberGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.trialMembers") { OPGroupNames.TRIAL_MEMBERS.trimIndent() }
    internal val crmMemberGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.crmMembers") { OPGroupNames.CRM_MEMBERS.trimIndent() }
    internal val centralOfficeGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.centralOffice") { OPGroupNames.CENTRAL_OFFICE.trimIndent() }
    internal val projectAdminsGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.projectAdmins") { OPGroupNames.PROJECT_ADMINS.trimIndent() }
    internal val kgItGroupName =
        propertyMap.getOrPut("multiservice.openproject_group_names.kg_it") { OPGroupNames.KG_IT.trimIndent() }

    internal val interestedRoleName =
        propertyMap.getOrPut("multiservice.role_names.interested") { OPRoleNames.INTERESTED.trimIndent() }
    internal val readerRoleName =
        propertyMap.getOrPut("multiservice.role_names.reader") { OPRoleNames.READER.trimIndent() }
    internal val memberRoleName =
        propertyMap.getOrPut("multiservice.role_names.member") { OPRoleNames.MEMBER.trimIndent() }
    internal val nodeAdminRoleName =
        propertyMap.getOrPut("multiservice.role_names.node_admin") { OPRoleNames.NODE_ADMIN.trimIndent() }
    internal val projectAdminRoleName =
        propertyMap.getOrPut("multiservice.role_names.project_admin") { OPRoleNames.PROJECT_ADMIN.trimIndent() }

    internal val regionalGroupParentProject =
        propertyMap.getOrPut("multiservice.parent_project.regional_group") { OPPName.REGIONAL_GROUP.trimIndent() }
    internal val bilaGroupParentProject =
        propertyMap.getOrPut("multiservice.parent_project.bila_group") { OPPName.BILA.trimIndent() }
    internal val prFrGroupParentProject =
        propertyMap.getOrPut("multiservice.parent_project.pr_fr_group") { OPPName.PR_FR.trimIndent() }
    internal val competenceGroupParentProject =
        propertyMap.getOrPut("multiservice.parent_project.competence_group") { OPPName.COMPETENCE_GROUP.trimIndent() }
    internal val committeeParentProject =
        propertyMap.getOrPut("multiservice.parent_project.committee") { OPPName.COMMITTEE.trimIndent() }
    internal val collaborationParentProject =
        propertyMap.getOrPut("multiservice.parent_project.collaboration") { OPPName.COLLABORATION.trimIndent() }
    internal val projectParentProject =
        propertyMap.getOrPut("multiservice.parent_project.project") { OPPName.PROJECTS.trimIndent() }
    internal val otherParentProject =
        propertyMap.getOrPut("multiservice.parent_project.other") { OPPName.OTHER.trimIndent() }

    internal val regionalGroupTemplate =
        propertyMap.getOrPut("multiservice.project_template.regional_group") { IogPluginConstants.REGIONAL_GROUP_TEMPLATE.trimIndent() }
    internal val biLaGroupTemplate =
        propertyMap.getOrPut("multiservice.project_template.bila_group") { IogPluginConstants.BILA_GROUP_TEMPLATE.trimIndent() }
    internal val collaborationTemplate =
        propertyMap.getOrPut("multiservice.project_template.collaboration") { IogPluginConstants.COLLABORATION_TEMPLATE.trimIndent() }
    internal val prFrGroupTemplate =
        propertyMap.getOrPut("multiservice.project_template.pr_fr_group") { IogPluginConstants.PR_FR_GROUP_TEMPLATE.trimIndent() }
    internal val competenceGroupTemplate =
        propertyMap.getOrPut("multiservice.project_template.competence_group") { IogPluginConstants.COMPETENCE_GROUP_TEMPLATE.trimIndent() }
    internal val committeeTemplate =
        propertyMap.getOrPut("multiservice.project_template.committee") { IogPluginConstants.COMMITTEE_TEMPLATE.trimIndent() }
    internal val projectTemplate =
        propertyMap.getOrPut("multiservice.project_template.project") { IogPluginConstants.PROJECT_TEMPLATE.trimIndent() }

    internal val stuffToBeClonedFromTemplate =
        StringTools.toOpenProjectCloneFields(
            propertyMap.getOrPut("multiservice.project_template.only") { IogPluginConstants.TEMPLATE_ONLY.trimIndent() },
        )

    internal val sandboxId =
        propertyMap.getOrPut("multiservice.project_template.sandbox_id") {
            IogPluginConstants.SANDBOX_PROJECT_ID.trimIndent()
        }
}
