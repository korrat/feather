/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import mu.KLogging

internal open class MemberAdminInterestedPattern(
    override val credentialProvider: ICredentialProvider,
) : MemberAdminTrialPattern(credentialProvider) {
    companion object : KLogging() {
        fun concatOrIgnore(
            name: String,
            prefixAndSuffix: Pair<String?, String?>,
        ): String {
            var result = name
            val prefix: String? = prefixAndSuffix.first
            if (prefix != null) {
                result = concatPrefix(result, prefix)
            }
            val suffix: String? = prefixAndSuffix.second
            if (suffix != null) {
                result = concatSuffix(result, suffix)
            }
            return result
        }

        private fun concatPrefix(
            name: String,
            prefix: String,
        ): String {
            val dashedPrefix = "$prefix-"
            return if (name.startsWith(dashedPrefix)) {
                name
            } else {
                dashedPrefix + name
            }
        }

        private fun concatSuffix(
            name: String,
            suffix: String,
        ): String {
            val dashedSuffix = "-$suffix"
            return if (name.endsWith(dashedSuffix)) {
                name
            } else {
                name + dashedSuffix
            }
        }
    }

    private val interestedDescriptionPrefix = "Interessierte der Gruppe "

    override fun create(config: CreateGroupConfig): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val trials = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)
        requireNotNull(trials) { "Cannot find group '${LdapNames.TRIAL_MEMBERS}'" }

        val interestedGroup = credentialProvider.createGroup(createInterestedGroupBody(interested.id, config))
        val trialGroup = credentialProvider.createGroup(createTrialGroupBody(setOf(trials.id), config))
        val memberGroup = credentialProvider.createGroup(createMemberGroupBody(iogMembers.id, config))
        val masterGroup =
            credentialProvider.createGroup(
                createMainGroupBody(
                    setOf(
                        interestedGroup.id,
                        memberGroup.id,
                        trialGroup.id,
                    ),
                    config,
                ),
            )
        val adminGroup =
            credentialProvider.createGroup(
                createAdminGroupBody(
                    setOf(
                        interestedGroup.id,
                        memberGroup.id,
                        trialGroup.id,
                    ),
                    config,
                ),
            )

        CreateGroup.logger.info {
            "MemberAdminInterestedPattern::create Created LDAP group ${masterGroup.name} with admin group ${adminGroup.name}, "
            "trial group ${trialGroup.name}, interested group ${interestedGroup.name}, and member group ${memberGroup.name}"
        }
        return arrayOf(masterGroup, adminGroup, interestedGroup, memberGroup, trialGroup).toList()
    }

    override fun search(prefixedName: String): List<Group> {
        val q = super.search(prefixedName)
        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val interestedGroup = credentialProvider.getGroupByName(getInterestedGroupName(prefixedName))
        requireNotNull(interestedGroup) { "Cannot find group '${getInterestedGroupName(prefixedName)}'" }

        logger.info {
            "MemberAdminInterestedPattern::search Found existing LDAP group" +
                " interested group ${interestedGroup.name} "
        }

        return arrayOf(q[0], q[1], interestedGroup, q[2], q[3]).toList()
    }

    private fun createInterestedGroupBody(
        parentGroupID: Int,
        conf: CreateGroupConfig,
    ): Group {
        return Group(
            id = 0,
            name = getInterestedGroupName(conf.ldapName),
            description = "$interestedDescriptionPrefix${conf.description}",
            parentGroups = setOf(parentGroupID),
            userMembers = emptySet(),
            groupMembers = emptySet(),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet(),
        )
    }

    private fun getInterestedGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.INTERESTED_SUFFIX
    }

    override fun getMetaDescription(
        groupName: String,
        description: String,
    ): GroupMetaDescription {
        if (groupName.endsWith(IogPluginConstants.ADMIN_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.ADMIN_SUFFIX.length)
            return GroupMetaDescription(
                name = getAdminGroupName(prefixedName),
                description = "$adminDescriptionPrefix$description",
                parentGroupNames = emptySet(),
                ownerNames = emptySet(),
                groupMemberNames = emptySet(),
                ownerGroupNames = emptySet(),
                ownedGroupNames =
                setOf(
                    getInterestedGroupName(prefixedName),
                    getMemberGroupName(prefixedName),
                    getTrialGroupName(prefixedName),
                ),
                descriptionWithoutPrefix = description,
            )
        } else if (groupName.endsWith(IogPluginConstants.MEMBER_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.MEMBER_SUFFIX.length)
            return GroupMetaDescription(
                name = getMemberGroupName(prefixedName),
                description = "$memberDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.IOG_MEMBERS),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else if (groupName.endsWith(IogPluginConstants.INTERESTED_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.INTERESTED_SUFFIX.length)
            return GroupMetaDescription(
                name = getInterestedGroupName(prefixedName),
                description = "$interestedDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.INTERESTED_PEOPLE),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else if (groupName.endsWith(IogPluginConstants.TRIAL_SUFFIX)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.TRIAL_SUFFIX.length)
            return GroupMetaDescription(
                name = getTrialGroupName(prefixedName),
                description = "$trialDescriptionPrefix$description",
                parentGroupNames = setOf(LdapNames.TRIAL_MEMBERS, prefixedName),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description,
            )
        } else {
            return GroupMetaDescription(
                name = groupName,
                description = "$mainGroupDescriptionPrefix$description",
                parentGroupNames = setOf(),
                ownerNames = emptySet(),
                groupMemberNames =
                setOf(
                    getMemberGroupName(groupName),
                    getInterestedGroupName(groupName),
                    getTrialGroupName(groupName),
                ),
                ownerGroupNames = setOf(),
                ownedGroupNames = setOf(),
                descriptionWithoutPrefix = description,
            )
        }
    }

    override fun getAllMetaDescriptions(
        groupName: String,
        description: String,
    ): List<GroupMetaDescription> {
        val prefixedName = removeSuffixIfNecessary(groupName)
        return listOf(
            getMetaDescription(prefixedName, description),
            getMetaDescription(getAdminGroupName(prefixedName), description),
            getMetaDescription(getInterestedGroupName(prefixedName), description),
            getMetaDescription(getMemberGroupName(prefixedName), description),
            getMetaDescription(getTrialGroupName(prefixedName), description),
        )
    }

    override fun removeSuffixIfNecessary(groupName: String): String {
        var prefixedName = super.removeSuffixIfNecessary(groupName)
        if (groupName.endsWith(IogPluginConstants.INTERESTED_SUFFIX)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.INTERESTED_SUFFIX.length)
        }
        return prefixedName
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return super.userMembersAllowed(groupName) || groupName.endsWith(IogPluginConstants.INTERESTED_SUFFIX)
    }

    override fun delete(groupName: String) {
        super.delete(groupName)
        credentialProvider.getGroupByName(getInterestedGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
    }
}
