/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.openproject.OpenProjectGroup
import dev.maximilian.feather.openproject.OpenProjectRole

internal data class CreateGroupConfig(
    val ldapName: String,
    val description: String,
    val ownerIDs: Set<Int>,
    val groupKind: GroupKind,
    val publicUrl: String,
)

internal data class MembershipAssociationArguments(
    val roleName: String,
    val groupName: String,
)

internal data class MembershipAssociation(
    val role: OpenProjectRole,
    val group: OpenProjectGroup,
)
