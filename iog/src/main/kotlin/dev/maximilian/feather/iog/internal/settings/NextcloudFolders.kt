/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.settings

internal class NextcloudFolders {
    // Many of those are abbreviations or initials, so they're unsuitable for camelCase
    @Suppress("ktlint:standard:property-naming")
    companion object {
        const val Projects = "Projekte"
        const val template = "_Projektvorlage"
        const val KG = "KGs und Ausschüsse"
        const val PrFr = "PR-FR"
        const val BiLa = "BiLa"
        const val OMS = "Ordentliche Mitglieder"
        const val CentralOffice = "Geschäftsstelle"
        const val AssociationBoard = "Vorstand"
        const val APSprecherInnen = "AP SprecherInnen"
        const val RgIntern = "RG-Intern"
        const val collaboration = "Projektübergreifende Zusammenarbeit"

        const val inputFolder = "Eingang"
        const val publicFolder = "Öffentlich"
        const val groupInternal = "Gruppenintern"

        const val GroupShare = "IOG"

        val standardFolders = listOf("Archiv", "in Arbeit", "Allgemein")

        val parentCandidates =
            arrayOf(
                Projects,
                KG,
                PrFr,
                BiLa,
                RgIntern,
                collaboration,
            ).map { "$GroupShare/$it" }
    }
}
