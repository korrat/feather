/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern.metaDescription

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User

internal class GroupResolver(private val allUsers: Collection<User>, private val allGroups: Collection<Group>) {
    fun getMetaDescription(g: Group): GroupMetaDescription {
        return GroupMetaDescription(
            g.name,
            g.description,
            groupMemberNames = g.groupMembers.mapNotNull { getGroup(it)?.name }.toSet(),
            parentGroupNames = g.parentGroups.mapNotNull { getGroup(it)?.name }.toSet(),
            ownerNames = g.owners.mapNotNull { getUser(it)?.username }.toSet(),
            ownerGroupNames = g.ownerGroups.mapNotNull { getGroup(it)?.name }.toSet(),
            ownedGroupNames = g.ownedGroups.mapNotNull { getGroup(it)?.name }.toSet(),
            descriptionWithoutPrefix = "",
        )
    }

    fun getGroup(
        g: GroupMetaDescription,
        credentialProvider: ICredentialProvider,
    ): Group {
        return Group(
            0,
            g.name,
            g.description,
            emptySet(),
            g.groupMemberNames.mapNotNull { credentialProvider.getGroupByName(it)?.id }.toSet(),
            g.parentGroupNames.mapNotNull { credentialProvider.getGroupByName(it)?.id }.toSet(),
            g.ownerNames.mapNotNull { credentialProvider.getGroupByName(it)?.id }.toSet(),
            g.ownerGroupNames.mapNotNull { credentialProvider.getUserByUsername(it)?.id }.toSet(),
            g.ownedGroupNames.mapNotNull { credentialProvider.getGroupByName(it)?.id }.toSet(),
        )
    }

    private fun getUser(id: Int): User? {
        return allUsers.firstOrNull { it.id == id }
    }

    private fun getGroup(id: Int): Group? {
        return allGroups.firstOrNull { it.id == id }
    }
}
