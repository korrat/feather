/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lib"))
    implementation(project(":nextcloud"))
    implementation(project(":openproject"))
    implementation(project(":kubernetes"))
    implementation(project(":authorization"))
    implementation(project(":multiservice"))
    implementation(project(":civicrm"))

    // Database ORM Framework
    implementation(libs.bundles.exposed)

    // Database driver implementation
    implementation(libs.postgres)

    // Rest framework
    implementation(libs.javalin.core)
    implementation(libs.javalin.openapi.core)
    kapt(libs.javalin.openapi.kapt)

    // OpenApi Document Generator
    implementation(libs.swagger.core)
    implementation(libs.swagger.ui)

    // Redis driver
    implementation(libs.jedis)

    // Im memory database
    testImplementation(libs.h2)

    // Mail framework
    implementation(libs.mail)

    // Ktor core
    testImplementation(libs.ktor.client.core)

    // Http client for tests
    testImplementation(libs.bundles.jacksonAndUnirest)
}
