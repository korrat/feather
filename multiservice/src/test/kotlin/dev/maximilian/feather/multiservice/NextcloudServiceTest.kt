/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

@file:Suppress("ktlint:standard:no-empty-file")

package dev.maximilian.feather.multiservice
/*
import dev.maximilian.feather.multiservice.mockserver.CredentialMock
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudSettings
import io.javalin.Javalin
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class NextcloudServiceTest {
    val nextcloudSettings = NextcloudSettings(
        getEnv("NEXTCLOUD_USER_BINDING", "id"),
        getEnv("NEXTCLOUD_PUBLIC_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_USER", "ncadmin"),
        getEnv("NEXTCLOUD_PWD", "ncadminsecret")
    )

    val app = Javalin.create {
        it.showJavalinBanner = false
        it.enableCorsForAllOrigins()
        it.logIfServerNotStarted = false
        it.defaultContentType = "application/json"
        it.contextPath = "/v1"
    }
    val credentialProvider = CredentialMock()
    val ns = NextcloudService(app, MultiserviceTest.pool, nextcloudSettings, mutableMapOf(), credentialProvider)

    private val generalServiceTest = ServiceTestUtilities(ns)

    @Test
    fun `Nextcloud service name is Nextcloud`() {
        assertEquals("Nextcloud", ns.getServiceName())
    }

    @Test
    fun `Nextcloud service can create test user`() {
        var success = generalServiceTest.createSimpleUserTest()
        assertTrue(success)
    }

    @Test
    fun `Create user twice leads to error`() {
        val wasError = generalServiceTest.testCreatedUserTwice()
        assertTrue(wasError)
    }

    @Test
    fun `delete of freshly created test user returns TRUE `() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val r = ns.deleteUser(testUser)
        assertTrue(r)
    }

    @Test
    fun `delete nonexisting user returns FALSE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = ns.deleteUser(testUser)
        assertFalse(result)
    }

    @Test
    fun `activateUser(FALSE) of an active test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = ns.activateUser(testUser, false)
        assertEquals(true, success)
        ns.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an inactive test user leads to result TRUE (success)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = ns.activateUser(testUser, false)
        assertEquals(true, success)
        ns.deleteUser(testUser)
    }

    @Test
    fun `activateUser(FALSE) of an inactive test user leads to result FALSE (failed)`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val success = ns.activateUser(testUser, false)
        assertEquals(false, success)
        ns.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an active test to result FALSE (failed)`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val success = ns.activateUser(testUser, true)
        assertEquals(false, success)
        ns.deleteUser(testUser)
    }

    @Test
    fun `activateUser(TRUE) of an not existing user leads to FALSE (failed)`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val success = ns.activateUser(testUser, true)
        assertEquals(false, success)
        ns.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an existing user leads to TRUE`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val resultString = runBlocking { ns.checkUser(testUser) }
        assertEquals("OK", resultString)
        ns.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an existing user with wrong email leads to Mail address differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(mail = "somethingwrong@test.de")
        val resultString = runBlocking { ns.checkUser(testUserWrong) }
        assertEquals("Mail address differs: ${testUser.mail}", resultString)
        ns.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an enabled user leads with wrong status leads to status differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val testUserWrong = testUser.copy(disabled = false)
        val resultString = runBlocking { ns.checkUser(testUserWrong) }
        assertEquals("User status differs: ${testUser.disabled}", resultString)
        ns.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an disabled user with wrong status leads to status differs message`() {
        val testUser = generalServiceTest.createRandomTestUser(false)
        val testUserWrong = testUser.copy(disabled = true)
        val resultString = runBlocking { ns.checkUser(testUserWrong) }
        assertEquals("User status differs: ${testUser.disabled}", resultString)
        ns.deleteUser(testUser)
    }

    @Test
    fun `checkUser of an non existing user leads to not found message`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val resultString = runBlocking { ns.checkUser(testUser) }
        assertEquals("Not found", resultString)
    }

    @Test
    fun `ChangeMail of an existing user leads to TRUE result`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        val result = ns.changeMailAddress(testUser, "somethingnew@mail.de")
        assertTrue(result)
        ns.deleteUser(testUser)
    }

    @Test
    fun `ChangeMail and check user leads to OK message`() {
        val testUser = generalServiceTest.createRandomTestUser(true)
        ns.changeMailAddress(testUser, "somethingnew@mail.de")
        val resultString = runBlocking { ns.checkUser(testUser) }
        assertEquals("OK", resultString)
        ns.deleteUser(testUser)
    }

    @Test
    fun `ChangeMail of inexistant user leads to FALSE`() {
        val testUser = generalServiceTest.getRandomTestUser(true)
        val result = ns.changeMailAddress(testUser, "somethingnew@mail.de")
        assertFalse(result)
    }
}
*/
