/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.Group
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.GenericType
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import org.eclipse.jetty.http.HttpStatus
import kotlin.test.Test
import kotlin.test.assertEquals

class GroupApiTest {
    private val credentials = ServiceConfig.ACCOUNT_CONTROLLER
    private val apiTestUtilities = ApiTestUtilities()
    private val scenario = apiTestUtilities.startWithDummyService()

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

    private fun getGroups(): HttpResponse<List<Group>> {
        return apiTestUtilities.restConnection.get("${scenario.basePath}/groups").asObject()
    }

    private fun getGroup(id: Int): HttpResponse<Group> {
        return apiTestUtilities.restConnection.get("${scenario.basePath}/groups/$id").asObject()
    }

    @Test
    fun `Get groups delivers OK (200)`() {
        apiTestUtilities.createAndLoginUser()

        val ret = getGroups()
        assertEquals(HttpStatus.OK_200, ret.status)
    }

    @Test
    fun `Get groups delivers correct groups`() {
        val (user, _) = apiTestUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        val group1 = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        val group2 = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(otherUser.id)))
        val group3 = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf()))

        val ret = getGroups()
        assertEquals(
            setOf(group1, group2, group3),
            ret.body.filter { it.id == group1.id || it.id == group2.id || it.id == group3.id }.toSet(),
        )
    }

    @Test
    fun `Get group works`() {
        apiTestUtilities.createAndLoginUser()
        val group = credentials.createGroup(TestGroup.generateTestGroup())
        val ret = getGroup(group.id)
        assertEquals(HttpStatus.OK_200, ret.status)
        assertEquals(group, ret.body)
    }

    @Test
    fun `Add member to group delivers correct group`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(true)
        val group = credentials.createGroup(TestGroup.generateTestGroup())
        val q = executeAddMemberToGroup(group.id, user.id)
        assertEquals(group.copy(userMembers = setOf(user.id)), q.body)
        assertEquals(HttpStatus.OK_200, q.status)
    }

    @Test
    fun `Add member to group as owner delivers OK (200)`() {
        val (user, _) = apiTestUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())

        val userGroup = credentials.createGroup(TestGroup.generateTestGroup())
        credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id), ownedGroups = setOf(userGroup.id)))

        val ret =
            apiTestUtilities.restConnection.put(
                "${scenario.basePath}/groups/${userGroup.id}/member/",
            ).body(setOf(otherUser.id)).asObject<Group>()

        assertEquals(HttpStatus.OK_200, ret.status)
        assertEquals(credentials.getGroup(userGroup.id), ret.body)
        assert(ret.body.userMembers.contains(otherUser.id))
    }

    @Test
    fun `Add member to group by standard user delivers FORBIDDEN (403)`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(false)
        val group = credentials.createGroup(TestGroup.generateTestGroup())
        val q = executeAddMemberToGroup(group.id, user.id)
        assertEquals(HttpStatus.FORBIDDEN_403, q.status)
    }

    private fun executeAddMemberToGroup(
        groupId: Int,
        userId: Int,
    ): HttpResponse<Group> {
        return apiTestUtilities.restConnection.put("${scenario.basePath}/groups/$groupId/member").body(setOf(userId)).asObject()
    }

    @Test
    fun `Delete member from group by standard user delivers FORBIDDEN (403)`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(false)
        val group = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        val q = executeDeleteMemberFromGroup(group.id, user.id)
        assertEquals(HttpStatus.FORBIDDEN_403, q.status)
    }

    @Test
    fun `Delete member from group as owner delivers OK (200)`() {
        val (user, _) = apiTestUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())

        val userGroup = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(otherUser.id)))
        credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id), ownedGroups = setOf(userGroup.id)))

        val ret =
            apiTestUtilities.restConnection.delete(
                "${scenario.basePath}/groups/${userGroup.id}/member/${otherUser.id}",
            ).asObject<Group>()

        assertEquals(HttpStatus.OK_200, ret.status)
        assertEquals(credentials.getGroup(userGroup.id), ret.body)
        assert(!ret.body.userMembers.contains(otherUser.id))
    }

    @Test
    fun `Delete member from group calls correctly callback`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(true)
        val group = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        val q = executeDeleteMemberFromGroup(group.id, user.id)
        assertEquals(HttpStatus.OK_200, q.status)
        assertEquals(
            1,
            apiTestUtilities.removedUserChecker.callCount,
            "the userRemovedFromGroupCallback was not called only once but ${apiTestUtilities.removedUserChecker.callCount}",
        )
        assertEquals(
            user.id,
            apiTestUtilities.removedUserChecker.lastUserId,
            "the userRemovedFromGroupCallback called, but did not set param of deleted user id ${apiTestUtilities.removedUserChecker.lastUserId}",
        )
        assertEquals(
            group.id,
            apiTestUtilities.removedUserChecker.lastGroupID,
            "the userRemovedFromGroupCallback called, but did not use group ${apiTestUtilities.removedUserChecker.lastGroupID}",
        )
    }

    @Test
    fun `Delete member from group really deletes user`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(true)
        val group = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        executeDeleteMemberFromGroup(group.id, user.id)
        val freshGroup = credentials.getGroup(group.id)
        requireNotNull(freshGroup)
        assertEquals(0, freshGroup.userMembers.size)
    }

    @Test
    fun `Delete member from group returns correct group`() {
        val (user, _) = apiTestUtilities.createAndLoginUser(true)
        val group = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        val ret = executeDeleteMemberFromGroup(group.id, user.id)
        assertEquals(group.copy(userMembers = emptySet()), ret.body)
    }

    private fun executeDeleteMemberFromGroup(
        groupId: Int,
        userId: Int,
    ): HttpResponse<Group> {
        return apiTestUtilities.restConnection.delete("${scenario.basePath}/groups/$groupId/member/$userId").asObject()
    }
}
