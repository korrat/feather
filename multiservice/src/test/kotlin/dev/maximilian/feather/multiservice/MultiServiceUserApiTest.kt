/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUserCreateRequest
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUserDeleteRequest
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUsersDeleteRequest
import dev.maximilian.feather.multiservice.mockserver.ServiceMockUserEntry
import dev.maximilian.feather.testutils.ServiceConfig
import dev.maximilian.feather.testutils.TestGroup
import dev.maximilian.feather.testutils.TestUser
import kong.unirest.core.Empty
import kong.unirest.core.GenericType
import kong.unirest.core.GetRequest
import kong.unirest.core.HttpRequest
import kong.unirest.core.HttpResponse
import kong.unirest.core.RequestBodyEntity
import org.eclipse.jetty.http.HttpStatus
import java.time.Instant
import java.util.*
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class MultiServiceUserApiTest {
    private val credentials = ServiceConfig.ACCOUNT_CONTROLLER
    private val apiUtilities = ApiTestUtilities()
    private val scenario = apiUtilities.startWithDummyService()

    @Test
    fun `Create user by standard USER leads to FORBIDDEN (403)`() {
        apiUtilities.createAndLoginUser()
        val (_, request) = buildCreateRequest(emptySet())
        val result = request.asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `Create user by ADMIN leads to CREATED (201)`() {
        apiUtilities.createAndLoginUser(true)
        val (_, request) = buildCreateRequest(emptySet())
        val result = request.asEmpty()
        assertEquals(HttpStatus.CREATED_201, result.status)
    }

    @Test
    fun `Create user with ADMIN permission leads to FORBIDDEN (403)`() {
        apiUtilities.createAndLoginUser(true)
        val (_, request) = buildCreateRequest(setOf(Permission.ADMIN.toString()))
        val result = request.asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `Create user leads to one create user call`() {
        apiUtilities.createAndLoginUser(true)
        val (_, request) = buildCreateRequest(emptySet())
        request.asEmpty()
        assertEquals(1, scenario.serviceMock.createUserCounter)
    }

    @Test
    fun `Create user leads to correct user call`() {
        apiUtilities.createAndLoginUser(true)
        val (user, request) = buildCreateRequest(emptySet())
        request.asEmpty()
        assertTrue(scenario.serviceMock.users[0].active)
        assert(scenario.serviceMock.users.any { it.user.username == user.username })
        assertEquals(1, scenario.serviceMock.createUserCounter)
    }

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

    @Test
    fun `Get all users by ADMIN leads to all user info with login date`() {
        val password = UUID.randomUUID().toString()
        val users = (0 until 3).map { credentials.createUser(TestUser.generateTestUser(), password) }

        apiUtilities.loginUser(users[1].username, password)
        apiUtilities.loginUser(users[2].username, password)
        apiUtilities.createAndLoginUser(true)

        val filter = { it: User -> it.id in users.map { u -> u.id } }
        val dbUsers = credentials.getUsers().filter(filter)
        val result =
            getAllUsers().asObject<List<User>>().apply {
                assertEquals(HttpStatus.OK_200, status)
            }.body.filter(filter)

        assertEquals(dbUsers.toSet(), result.toSet())
    }

    @Test
    fun `Get all users with relevant group co-membership leads to some hidden info`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())

        val groupWithNormalUser = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        val groupWithOtherUser = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(otherUser.id)))
        val nestedGroupWithOtherUser = credentials.createGroup(TestGroup.generateTestGroup(groupMembers = setOf(groupWithOtherUser.id)))

        data class Scenario(val name: String, val members: Set<Int>, val groups: Set<Int>, val groupName: String)
        val scenarioName = UUID.randomUUID().toString()
        val randomProjectName = buildString {
            repeat(3) { _ -> append(('a'..'z').random()) }
            append("-iog")
            append(Random.nextInt(0, 100).toString().padStart(2, '0'))
        }
        val scenarios: Array<Scenario> = arrayOf(
            Scenario("Direct group co-membership", setOf(user.id, otherUser.id), setOf(), "kg-$scenarioName"),
            Scenario("otherUser member of subgroup", setOf(user.id), setOf(groupWithOtherUser.id), "rg-$scenarioName"),
            Scenario("otherUser member of sub-sub-group", setOf(user.id), setOf(nestedGroupWithOtherUser.id), randomProjectName),
            Scenario("user member of subgroup", setOf(otherUser.id), setOf(groupWithNormalUser.id), "kg-$scenarioName-members"),
            Scenario("both member of subgroup", setOf(), setOf(groupWithNormalUser.id, groupWithOtherUser.id), "rg-$scenarioName-interested"),
        )

        for (scenario in scenarios) {
            val commonGroup = credentials.createGroup(
                TestGroup.generateTestGroup(
                    name = scenario.groupName,
                    userMembers = scenario.members,
                    groupMembers = scenario.groups,
                ),
            )

            val result = getAllUsers().asObject<List<User>>()
            assertEquals(HttpStatus.OK_200, result.status)

            val retrievedOtherUser = result.body.find { it.id == otherUser.id }
            assertNotNull(retrievedOtherUser)

            checkUserDataRedacted(retrievedOtherUser, false)

            credentials.deleteGroup(commonGroup)
        }
    }

    @Test
    fun `Get all users with co-membership in irrelevant group leads to some hidden info`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())

        val irrelevantGroupNames = arrayOf("interestedpeople", "iogmembers")
        for (groupName in irrelevantGroupNames) {
            val commonGroup = credentials.createGroup(
                TestGroup.generateTestGroup(
                    name = groupName,
                    userMembers = setOf(user.id, otherUser.id),
                ),
            )

            val result = getAllUsers().asObject<List<User>>()
            assertEquals(HttpStatus.OK_200, result.status)

            val retrievedOtherUser = result.body.find { it.id == otherUser.id }
            assertNotNull(retrievedOtherUser)
            checkUserDataRedacted(retrievedOtherUser, true)

            credentials.deleteGroup(commonGroup)
        }
    }

    @Test
    fun `Get all users without ADMIN leads to all hidden info`() {
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        val (user, _) = apiUtilities.createAndLoginUser()
        val groupWithNormalUser = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id)))
        credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(otherUser.id), ownedGroups = setOf(groupWithNormalUser.id)))
        val result = getAllUsers().asObject<List<User>>()

        assertEquals(HttpStatus.OK_200, result.status)
        val retrievedUser = result.body.find { it.id == user.id }
        val retrievedOtherUser = result.body.find { it.id == otherUser.id }

        assertNotNull(retrievedUser)
        assertNotNull(retrievedOtherUser)

        checkUserDataRedacted(retrievedOtherUser, true)
        assertEquals(user.copy(groups = setOf(groupWithNormalUser.id)), retrievedUser)
    }

    @Test
    fun `Get all users by node admin leads to some hidden info`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        val normalUser = credentials.createUser(TestUser.generateTestUser())
        val testGroup = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(normalUser.id)))
        val adminGroup =
            credentials.createGroup(
                TestGroup.generateTestGroup(userMembers = setOf(user.id), ownedGroups = setOf(testGroup.id)),
            )

        val result = getAllUsers().asObject<List<User>>()
        assertEquals(HttpStatus.OK_200, result.status)
        val retrievedNormalUser = result.body.find { it.displayName == normalUser.displayName }
        val retrievedOtherUser = result.body.find { it.displayName == otherUser.displayName }
        val nodeAdminUser = result.body.find { it.displayName == user.displayName }

        assertNotNull(retrievedNormalUser, "normal user not found in response")
        assertNotNull(retrievedOtherUser, "other not found in response")
        assertNotNull(nodeAdminUser, "node admin not found in response")

        checkUserDataRedacted(retrievedNormalUser, false)
        checkUserDataRedacted(retrievedOtherUser, true)
        assertEquals(user.copy(groups = setOf(adminGroup.id)), nodeAdminUser, "Own user was redacted too much")
    }

    @Test
    fun `DELETE of foreign accounts by standard user leads to FORBIDDEN`() {
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        val (_, password) = apiUtilities.createAndLoginUser()
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(password, true)
        val result = executeDeleteRequest(otherUser.id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `Get user by ADMIN leads to user info with login date`() {
        val (user, _) = apiUtilities.createAndLoginUser()

        // Now login with admin to check if admin is allowed to see the updatedDate
        apiUtilities.createAndLoginUser(true)

        // Get the updated user entry from database and retrieve data from api
        val updatedUser = credentials.getUser(user.id)
        val result = getSingleUser(user.id).asObject<User>()

        assertEquals(HttpStatus.OK_200, result.status)
        assertEquals(updatedUser, result.body)
    }

    @Test
    fun `Get user of not existing user leads to NOT FOUND`() {
        apiUtilities.createAndLoginUser(true)
        val result = getSingleUser(-42).asObject<User>()
        assertEquals(HttpStatus.NOT_FOUND_404, result.status)
    }

    @Test
    fun `Get user of own identity leads to full info`() {
        val (user, _) = apiUtilities.createAndLoginUser()

        val updatedUser = requireNotNull(credentials.getUser(user.id))
        val apiResult = getSingleUser(user.id).asObject<User>()

        assertEquals(HttpStatus.OK_200, apiResult.status)
        assertEquals(user, updatedUser)
        assertEquals(updatedUser, apiResult.body)
    }

    @Test
    fun `Get user of foreign identity leads to hidden info`() {
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        apiUtilities.createAndLoginUser()

        val retrievedUser = getSingleUser(otherUser.id).asObject<User>()
        checkUserDataRedacted(retrievedUser.body, true)
    }

    @Test
    fun `Get user by node admin leads to some hidden info`() {
        val (user, _) = apiUtilities.createAndLoginUser()
        val otherUser = credentials.createUser(TestUser.generateTestUser())
        val testGroup = credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(otherUser.id)))
        credentials.createGroup(TestGroup.generateTestGroup(userMembers = setOf(user.id), ownedGroups = setOf(testGroup.id)))

        val result = getSingleUser(otherUser.id).asObject<User>()
        checkUserDataRedacted(result.body, false)
    }

    @Test
    fun `DELETE of USER of own account leads to SUCCESS (200)`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        scenario.serviceMock.users.add(ServiceMockUserEntry(user, true))
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(password, true)
        val result = executeDeleteRequest(user.id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.OK_200, result.status)
    }

    @Test
    fun `DELETE without verification leads to BAD REQUEST (400)`() {
        val (user, password) = apiUtilities.createAndLoginUser()
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(password, false)
        val result = executeDeleteRequest(user.id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `DELETE leads to one delete user call`() {
        val (user, password) = apiUtilities.createAndLoginUser()

        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(password, true)
        executeDeleteRequest(user.id, multiServiceUserDeleteRequest)
        assertEquals(1, scenario.serviceMock.deleteUserCounter)
    }

    @Test
    fun `DELETE of self deletes correct user`() {
        val (user, password) = apiUtilities.createAndLoginUser()

        scenario.serviceMock.users.add(ServiceMockUserEntry(user, true))
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(password, true)
        executeDeleteRequest(user.id, multiServiceUserDeleteRequest)
        assertEquals(0, scenario.serviceMock.users.count())
    }

    @Test
    fun `Multiple DELETE as Admin is CREATED (201)`() {
        val (_, password) = apiUtilities.createAndLoginUser(true)
        val testUser = credentials.createUser(TestUser.generateTestUser())

        scenario.serviceMock.users.add(ServiceMockUserEntry(testUser, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest(password, listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.CREATED_201, response.status)
    }

    @Test
    fun `Multiple DELETE with wrong password is FORBIDDEN (403)`() {
        apiUtilities.createAndLoginUser(true)
        val testUser = credentials.createUser(TestUser.generateTestUser())

        scenario.serviceMock.users.add(ServiceMockUserEntry(testUser, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest(UUID.randomUUID().toString(), listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Multiple DELETE as user is FORBIDDEN (403)`() {
        val (_, password) = apiUtilities.createAndLoginUser(false)
        val testUser = credentials.createUser(TestUser.generateTestUser())

        scenario.serviceMock.users.add(ServiceMockUserEntry(testUser, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest(password, listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    private fun checkUserDataRedacted(user: User, full: Boolean = true) {
        if (!full) {
            assertTrue(
                (user.firstname != "") and (user.surname != "") and (user.mail != ""),
                "User ${user.username} is redacted but should not be",
            )
        }

        val plainUser = User(
            id = user.id,
            registeredSince = Instant.ofEpochSecond(0),
            firstname = if (full) "" else user.firstname,
            surname = if (full) "" else user.surname,
            permissions = emptySet(),
            groups = user.groups,
            ownedGroups = user.ownedGroups,
            mail = if (full) "" else user.mail,
            displayName = user.displayName,
            username = user.username,
            lastLoginAt = Instant.ofEpochSecond(0),
        )

        assertEquals(plainUser, user, "User ${user.username} was not ${if (full) "fully " else ""}redacted")
    }

    private fun buildCreateRequest(permissions: Set<String>): Pair<User, RequestBodyEntity> {
        val user = TestUser.generateTestUser()
        val password = UUID.randomUUID().toString()
        val multiServiceUserCreateRequest =
            MultiServiceUserCreateRequest(
                user.displayName,
                password,
                password,
                user.mail,
                user.firstname,
                user.surname,
                setOf(),
                permissions,
                true,
                apiUtilities.gdprController.getLatestDocument()?.id,
            )

        scenario.serviceMock.users.add(ServiceMockUserEntry(user, true))

        return user to
            apiUtilities
                .restConnection
                .put("${scenario.basePath}/bindings/multiservice/users")
                .body(multiServiceUserCreateRequest)
    }

    private fun executeDeleteRequest(
        userID: Int,
        multiServiceUserDeleteRequest: MultiServiceUserDeleteRequest,
    ): HttpResponse<Empty> {
        return apiUtilities
            .restConnection
            .delete("${scenario.basePath}/bindings/multiservice/users/$userID")
            .body(multiServiceUserDeleteRequest)
            .asEmpty()
    }

    private fun executeMultipleDeleteRequest(multiServiceUsersDeleteRequest: MultiServiceUsersDeleteRequest): HttpResponse<Empty> {
        return apiUtilities
            .restConnection
            .post("${scenario.basePath}/bindings/multiservice/users/delete")
            .body(multiServiceUsersDeleteRequest)
            .asEmpty()
    }

    private fun getAllUsers(): GetRequest {
        return apiUtilities.restConnection
            .get("${scenario.basePath}/users")
    }

    private fun getSingleUser(userID: Int): GetRequest {
        return apiUtilities
            .restConnection
            .get("${scenario.basePath}/users/$userID")
    }
}
