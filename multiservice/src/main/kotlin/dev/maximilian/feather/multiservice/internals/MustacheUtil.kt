/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals

import com.github.mustachejava.DefaultMustacheFactory
import com.github.mustachejava.Mustache
import dev.maximilian.feather.multiservice.Multiservice
import java.io.StringReader
import java.io.StringWriter
import java.util.UUID

fun Mustache.execute(value: Any): String = StringWriter().also { execute(it, value) }.toString()

class FeatherMustache<T : Any> private constructor(value: String) {
    companion object {
        val MAIL_PASSWORD_FORGOT_CONTENT = FeatherMustache<MailPasswordForgotData>(Multiservice.multiServiceMailSettings!!.forgot.content)
        val MAIL_PASSWORD_FORGOT_SUBJECT = FeatherMustache<SitenameData>(Multiservice.multiServiceMailSettings!!.forgot.subject)

        val MAIL_INVITATION_CONTENT = FeatherMustache<MailInvitationData>(Multiservice.multiServiceMailSettings!!.invite.content)
        val MAIL_INVITATION_SUBJECT = FeatherMustache<SitenameData>(Multiservice.multiServiceMailSettings!!.invite.subject)

        val MAIL_CHANGE_MAIL_ADDRESS_CONTENT =
            FeatherMustache<MailChangeMailAddressData>(Multiservice.multiServiceMailSettings!!.change.content)
        val MAIL_CHANGE_MAIL_ADDRESS_SUBJECT = FeatherMustache<SitenameData>(Multiservice.multiServiceMailSettings!!.change.subject)

        val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1 =
            FeatherMustache<MailChangeMailAddressNotificationData>(Multiservice.multiServiceMailSettings!!.change.addressNotification1)
        val MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2 =
            FeatherMustache<MailChangeMailAddressNotificationData>(Multiservice.multiServiceMailSettings!!.change.addressNotification2)
    }

    private val mustache = DefaultMustacheFactory().compile(StringReader(value), UUID.randomUUID().toString())

    fun execute(on: T): String = StringWriter().also { mustache.execute(it, on) }.toString()
}

data class MailPasswordForgotData(
    val firstname: String,
    val surname: String,
    val username: String,
    val link: String,
    val botname: String,
)

data class MailInvitationData(
    val firstname: String,
    val surname: String,
    val link: String,
    val botname: String,
)

data class MailChangeMailAddressData(
    val firstname: String,
    val surname: String,
    val username: String,
    val link: String,
    val botname: String,
)

data class MailChangeMailAddressNotificationData(
    val firstname: String,
    val surname: String,
    val username: String,
    val oldMail: String,
    val newMail: String,
    val botname: String,
)

fun LostPassword.toMailPasswordForgotData() =
    MailPasswordForgotData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        link = "${Multiservice.multiServiceMailSettings!!.baseUrl}/reset/$id",
        botname = Multiservice.multiServiceMailSettings!!.senderAddress.personal,
    )

fun Invitation.toMailInvitationData() =
    MailInvitationData(
        firstname = invitee.firstname,
        surname = invitee.surname,
        link = "${Multiservice.multiServiceMailSettings!!.baseUrl}/invite/$id",
        botname = Multiservice.multiServiceMailSettings!!.senderAddress.personal,
    )

fun ChangeMailAddress.toMailChangeMailAddressData() =
    MailChangeMailAddressData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        link = "${Multiservice.multiServiceMailSettings!!.baseUrl}/change_mail/$id",
        botname = Multiservice.multiServiceMailSettings!!.senderAddress.personal,
    )

fun ChangeMailAddress.toMailChangeMailAddressNotificationData() =
    MailChangeMailAddressNotificationData(
        firstname = user.firstname,
        username = user.username,
        surname = user.surname,
        oldMail = user.mail,
        newMail = newMail,
        botname = Multiservice.multiServiceMailSettings!!.senderAddress.personal,
    )

data class SitenameData(
    val sitename: String,
)
