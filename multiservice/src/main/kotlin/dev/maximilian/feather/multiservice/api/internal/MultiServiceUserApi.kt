/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.StringTools
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.connectors.InvitationDatabaseConnector
import dev.maximilian.feather.multiservice.internals.useroperations.CreateUser
import dev.maximilian.feather.multiservice.internals.useroperations.CreateUserConfig
import dev.maximilian.feather.multiservice.internals.useroperations.DeleteUser
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.session
import dev.maximilian.feather.sessionOrMinisession
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.apibuilder.ApiBuilder.put
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import jakarta.mail.internet.AddressException
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.time.Instant

internal class MultiServiceUserApi(
    app: Javalin,
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
    private val events: MultiServiceEvents,
    private val invitationDatabaseConnector: InvitationDatabaseConnector,
    private val backgroundJobManager: BackgroundJobManager,
    private val gdprController: GdprController,
) {
    init {
        app.routes {
            path("bindings/multiservice/users") {
                put("/", ::handleCreateUser, Permission.ADMIN)
                delete("/{userId}", ::handleDeleteUser)
                post("/delete", ::handleDeleteUsers, Permission.ADMIN)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private fun handleCreateUser(ctx: Context) {
        val session = ctx.session()
        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val launchAsync = ctx.queryParam("async")?.toBoolean() ?: false

        val body = ctx.bodyAsClass<MultiServiceUserCreateRequest>()

        val newPermissions =
            try {
                body.permissions.map {
                    Permission.valueOf(it)
                }.toSet()
            } catch (e: IllegalArgumentException) {
                // string cannot be cast to the enum class
                throw ForbiddenResponse()
            }
        if (newPermissions.contains(Permission.ADMIN)) throw ForbiddenResponse()

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"
        if (body.firstname.isEmpty()) errorMessages += "Ein Vorname ist benötigt"
        if (body.firstname.length !in (2..40)) errorMessages += "Ein Vorname benötigt mindestens 2 und maximal 40 Zeichen"
        if (body.surname.isEmpty()) errorMessages += "Ein Nachname ist benötigt"
        if (body.surname.length !in (2..40)) errorMessages += "Ein Nachname benötigt mindestens 2 und maximal 40 Zeichen"
        if (body.mail.isEmpty()) errorMessages += "Eine E-Mail ist benötigt"
        if (body.mail.length !in (5..100)) errorMessages += "Eine E-Mail benötigt mindestens 5 und maximal 100 Zeichen"

        try {
            AsyncMailSender.validateMail(body.mail, "${body.firstname} ${body.surname}")
        } catch (e: AddressException) {
            val reason = e.toString()
            errorMessages += "Die angegebene E-Mail-Adresse ist nicht RFC822 konform (Grund: '$reason')"
        }
        if (credentialProvider.getUserByMail(body.mail) != null) errorMessages += "Diese E-Mail Adresse ist bereits registriert"
        if (invitationDatabaseConnector.getInvitationByMail(credentialProvider, body.mail) != null) errorMessages += "Diese E-Mail Adresse wurde bereits eingeladen"
        if (body.displayName.isEmpty()) errorMessages += "Ein Anzeigename ist benötigt"
        if (body.displayName.length !in (3..40)) errorMessages += "Ein Anzeigename benötigt mindestens 3 und maximal 40 Zeichen"
        if (!(body.displayName matches StringTools.userDisplayNameRegex)) errorMessages += "Ein Anzeigename darf nur Kleinbuchstaben und Zahlen enthalten. Darf nicht mit einer Zahl starten."
        if (credentialProvider.getUserByUsername(body.displayName) != null) errorMessages += "Dieser Anzeigename existiert schon, bitte einen anderen wählen"
        if (body.password1.isEmpty()) errorMessages += "Ein Passwort ist benötigt"
        if (body.password1.length !in (8..400)) errorMessages += "Ein Passwort benötigt mindestens 8 und maximal 400 Zeichen"
        if (body.password1 != body.password2) errorMessages += "Die Passwörter müssen identisch sein"

        val latestGdpr = gdprController.getLatestDocument()

        if (latestGdpr != null) {
            if (body.gdpr != latestGdpr.id && !body.permissions.contains(Permission.FUNCTION_ACCOUNT.toString())) {
                errorMessages += "Den aktuellen Datenschutzbestimmungen muss zugestimmt werden"
            }
        }

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not create user with name ${body.displayName} due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to create a new user with name ${body.displayName}" }

            val createUserService =
                CreateUser(
                    credentialProvider,
                    services,
                    backgroundJobManager,
                    events.userCreationEvents,
                    gdprController,
                )

            val job = createUserService::createUser
            val config =
                CreateUserConfig(
                    User(
                        id = 0,
                        username = body.displayName,
                        surname = body.surname,
                        mail = body.mail,
                        firstname = body.firstname,
                        displayName = "${body.firstname} ${body.surname}",
                        groups = body.groups,
                        ownedGroups = emptySet(),
                        registeredSince = Instant.ofEpochMilli(0),
                        permissions = newPermissions,
                        lastLoginAt = Instant.ofEpochSecond(0),
                    ),
                    body.password1,
                    body.gdpr,
                )

            try {
                if (launchAsync) {
                    logger.info { "Create the new user as a background job" }

                    val newJob = backgroundJobManager.runBackgroundJob(job, config)

                    ctx.status(201)
                    ctx.json(newJob)
                } else {
                    runBlocking { job(null, config) }
                    ctx.status(201)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Will not create user with name ${body.displayName} due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    private fun handleDeleteUser(ctx: Context) {
        val session = ctx.sessionOrMinisession()
        val userId = ctx.pathParamAsClass<Int>("userId").getOrThrow { BadRequestResponse("User id needs to be an integer") }

        val creator = session.user

        if (userId != creator.id && !(!session.miniSession && creator.permissions.contains(Permission.ADMIN))) {
            throw ForbiddenResponse()
        }

        val body = ctx.bodyAsClass<MultiServiceUserDeleteRequest>()
        if (credentialProvider.authenticateUserByUsernameOrMail(creator.username, body.password) == null) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Das vollständige Löschen der Daten muss bestätigt werden"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not delete user $userId due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to delete user with id $userId" }

            val deleteUserService =
                DeleteUser(
                    credentialProvider,
                    services,
                    events.userDeletionEvents,
                )

            try {
                deleteUserService.deleteUser(userId)
            } catch (e: Exception) {
                logger.warn(e) { "Will not delete user with id $userId due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }

            // CacheManager.refreshCacheAsync()

            if (userId == creator.id) {
                // finishes the context
                events.sessionTerminator.deleteSession(ctx)
            } else {
                events.sessionTerminator.deleteSessionsForUser(userId)
                ctx.status(201)
            }
        }
        // All ok
    }

    private fun handleDeleteUsers(ctx: Context) {
        val session = ctx.session()
        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val body = ctx.bodyAsClass<MultiServiceUsersDeleteRequest>()
        if (credentialProvider.authenticateUserByUsernameOrMail(creator.username, body.password) == null) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Das vollständige Löschen der Daten muss bestätigt werden"
        if (body.users.isEmpty()) errorMessages += "Keine Accounts zum Löschen angegeben"
        if (body.users.any { it == creator.id }) errorMessages += "Der eigene Account kann nicht in der Liste enthalten sein"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not delete users due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            val deleteUserService =
                DeleteUser(
                    credentialProvider,
                    services,
                    events.userDeletionEvents,
                )

            try {
                body.users.forEach {
                    logger.info { "About to delete user with id $it" }

                    deleteUserService.deleteUser(it)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Will not delete users due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }

            // CacheManager.refreshCacheAsync()

            ctx.status(201)
        }
    }
}
