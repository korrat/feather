/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.connectors

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.multiservice.internals.Invitation
import dev.maximilian.feather.multiservice.jsonb
import mu.KotlinLogging
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.less
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.Duration
import java.time.Instant
import java.util.Locale
import java.util.UUID

internal class InvitationDatabaseConnector(private val db: Database, accountController: AccountController) {
    private val logger = KotlinLogging.logger { }
    private val invitationTable = InvitationTable(accountController)

    init {
        logger.info { "InvitationDatabaseConnector::init, create MissingTables if necessary" }
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(
                invitationTable,
            )
        }
        deleteAllExpiredInvitations()
        logger.info { "InvitationDatabaseConnector::init finished" }
    }

    fun getValidInvitationsByInviter(inviter: User) =
        transaction(db) {
            invitationTable.select {
                invitationTable.inviterId eq inviter.id and (invitationTable.used eq false) and (invitationTable.validUntil greaterEq Instant.now())
            }.map { rowToInvitation(it, inviter) }
        }

    fun getAllValidInvitations(credentialProvider: ICredentialProvider) =
        transaction(db) {
            invitationTable.select {
                invitationTable.used eq false and (invitationTable.validUntil greaterEq Instant.now())
            }.map {
                credentialProvider.getUser(it[invitationTable.inviterId].value)
                    ?.let { user ->
                        rowToInvitation(
                            it,
                            user,
                        )
                    }
            }
        }

    fun getInvitationByMail(
        credentialProvider: ICredentialProvider,
        mail: String,
    ): Invitation? =
        transaction(db) {
            invitationTable.select {
                invitationTable.mail eq mail.lowercase(Locale.getDefault()) and (invitationTable.used eq false) and (invitationTable.validUntil greaterEq Instant.now())
            }.firstOrNull()?.let {
                credentialProvider.getUser(it[invitationTable.inviterId].value)
                    ?.let { user ->
                        rowToInvitation(
                            it,
                            user,
                        )
                    }
            }
        }

    fun getInvitation(
        uuid: UUID,
        credentialProvider: ICredentialProvider,
        oldOrUsed: Boolean = false,
    ): Invitation? =
        transaction(
            db,
        ) {
            invitationTable.select {
                invitationTable.id eq
                    EntityID(
                        uuid,
                        invitationTable,
                    )
            }.firstOrNull()?.let {
                credentialProvider.getUser(it[invitationTable.inviterId].value)
                    ?.let { user ->
                        rowToInvitation(
                            it,
                            user,
                        )
                    }
            }?.takeIf { oldOrUsed || (!it.used && it.validUntil >= Instant.now()) }
        }

    fun createInvitation(
        invitation: Invitation,
        credentialProvider: ICredentialProvider,
    ): Invitation =
        getInvitation(
            transaction(db) {
                invitationTable.insertAndGetId {
                    it[inviterId] = invitation.inviter.id
                    it[validUntil] = invitation.validUntil
                    it[used] = false
                    it[firstname] = invitation.invitee.firstname
                    it[surname] = invitation.invitee.surname
                    it[mail] = invitation.invitee.mail.lowercase(Locale.getDefault())
                    it[groups] = invitation.invitee.groups
                    it[permissions] = invitation.invitee.permissions.map { p -> p.toString() }.toSet()
                }.value
            },
            credentialProvider,
        )!!

    fun updateValidUntil(
        invitation: Invitation,
        newValidTo: Instant,
    ) {
        transaction {
            invitationTable.update({ invitationTable.id eq invitation.id }) {
                it[validUntil] = newValidTo
            }
        }
    }

    fun finalize(invitation: Invitation) {
        transaction {
            invitationTable.update({ invitationTable.id eq invitation.id }) {
                it[used] = true
            }
        }
    }

    fun deleteInvitation(invitation: Invitation) {
        transaction(db) { invitationTable.deleteWhere { invitationTable.id eq invitation.id } }
    }

    fun deleteAllExpiredInvitations() {
        /* Would not get invitations from deleted inviters:
        getAllExpiredInvitations.forEach{
            deleteInvitation(it)
        }*/

        logger.info { "InvitationDatabaseConnector::deleteAllExpiredInvitations" }
        val deadline = Instant.now().minus(Duration.ofDays(30))
        transaction(db) { invitationTable.deleteWhere { invitationTable.used eq false and (invitationTable.validUntil less deadline) } }
    }

    private fun rowToInvitation(
        it: ResultRow,
        inviter: User,
    ): Invitation =
        Invitation(
            inviter = inviter,
            validUntil = it[invitationTable.validUntil],
            used = it[invitationTable.used],
            id = it[invitationTable.id].value,
            invitee =
            User(
                id = 0,
                surname = it[invitationTable.surname],
                username = "",
                displayName = "",
                mail = it[invitationTable.mail],
                ownedGroups = emptySet(),
                groups = it[invitationTable.groups],
                permissions = it[invitationTable.permissions].map { Permission.valueOf(it) }.toSet(),
                firstname = it[invitationTable.firstname],
                registeredSince = Instant.ofEpochSecond(0),
                lastLoginAt = Instant.ofEpochSecond(0),
            ),
        )

    class InvitationTable(accountController: AccountController) : UUIDTable("invites") {
        private val objectMapper = jacksonObjectMapper()

        val inviterId: Column<EntityID<Int>> = reference("inviterId", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val validUntil: Column<Instant> = timestamp("validUntil")
        val used: Column<Boolean> = bool("used")
        val firstname: Column<String> = varchar("firstname", 30)
        val surname: Column<String> = varchar("surname", 30)
        val mail: Column<String> = varchar("mail", 100)
        val groups: Column<Set<Int>> = jsonb("groups", objectMapper)
        val permissions: Column<Set<String>> = jsonb("permissions", objectMapper)
    }
}
