/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.FeatherMustache
import dev.maximilian.feather.multiservice.internals.Mail
import dev.maximilian.feather.multiservice.internals.SitenameData
import dev.maximilian.feather.multiservice.internals.connectors.LostPasswordDatabaseConnector
import dev.maximilian.feather.multiservice.internals.toMailPasswordForgotData
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import jakarta.mail.internet.InternetAddress
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import mu.KotlinLogging
import java.util.UUID

internal class ForgotApi(
    app: Javalin,
    private val credentialProvider: ICredentialProvider,
    private val lostPasswordDatabaseConnector: LostPasswordDatabaseConnector,
    private val asyncMailSender: AsyncMailSender,
    private val mailSettings: MultiServiceMailSettings,
) {
    private val logger = KotlinLogging.logger {}

    init {
        app.routes {
            path("/forgot") {
                post("/step1/{mail}", ::sendLostPassword)
                get("/step2/{token}", ::getLostPasswordToken)
                post("/step2/{token}", ::setNewPassword)
            }
        }
    }

    private fun sendLostPassword(ctx: Context) {
        val mail = ctx.pathParam("mail")
        ctx.status(201)

        GlobalScope.launch {
            val user = credentialProvider.getUserByMail(mail)
            // TODO also log token (or not because it is effectively granting log reading users complete access to all users!
            logger.info("$mail wants to recover password, user entry: ${user?.id}")

            if (user != null) {
                val tokenData = lostPasswordDatabaseConnector.createLostPassword(user, credentialProvider)
                val mailData = FeatherMustache.MAIL_PASSWORD_FORGOT_CONTENT.execute(tokenData.toMailPasswordForgotData())
                val mailSubject =
                    FeatherMustache.MAIL_PASSWORD_FORGOT_SUBJECT.execute(SitenameData(mailSettings.siteName))

                asyncMailSender.submitMail(
                    Mail(
                        sender = mailSettings.senderAddress,
                        body = mailData,
                        receiver = setOf(InternetAddress(user.mail, "${user.firstname} ${user.surname}")),
                        subject = mailSubject,
                    ),
                )
            }
        }
    }

    private fun getLostPasswordToken(ctx: Context) {
        val uuid = ctx.pathParamAsClass<UUID>("{token}").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val token = uuid.let { lostPasswordDatabaseConnector.getLostPassword(it, credentialProvider) } ?: throw NotFoundResponse("$uuid not found")

        ctx.json(token)
    }

    private fun setNewPassword(ctx: Context) {
        val uuid = ctx.pathParamAsClass<UUID>("{token}").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val token = uuid.let { lostPasswordDatabaseConnector.getLostPassword(it, credentialProvider) } ?: throw NotFoundResponse("$uuid not found")
        val passwordBody = ctx.bodyAsClass<ChangePasswordData>()

        // Sanity checks!
        val errorList = mutableListOf<String>()

        if (passwordBody.password1.length < 8 || passwordBody.password1.length > 400) {
            errorList.add("Das Passwort muss aus mindestens 8 und maximal 400 Zeichen bestehen")
        }

        if (passwordBody.password1 != passwordBody.password2) {
            errorList.add("Die Passwörter müssen identisch sein")
        }

        if (errorList.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorList)
        } else {
            logger.info { "Password of User ${token.user.mail} (${token.user.id}) successfully recovered" }
            credentialProvider.updateUserPassword(token.user, passwordBody.password1)
            lostPasswordDatabaseConnector.removeLostPassword(token)
            // CacheManager.refreshCacheAsync()
            ctx.status(204)
        }
    }

    private data class ChangePasswordData(
        val password1: String,
        val password2: String,
    )
}
