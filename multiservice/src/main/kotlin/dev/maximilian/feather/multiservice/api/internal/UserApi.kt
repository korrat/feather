/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.connectors.InvitationDatabaseConnector
import dev.maximilian.feather.multiservice.internals.useroperations.ActivateUser
import dev.maximilian.feather.multiservice.internals.useroperations.ChangeMailAddress
import dev.maximilian.feather.multiservice.internals.useroperations.ChangeUserName
import dev.maximilian.feather.multiservice.internals.useroperations.GetUser
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import jakarta.mail.internet.AddressException
import mu.KotlinLogging

private data class UserChangeNameRequest(
    val displayName: String,
    val firstname: String,
    val surname: String,
    val verified: Boolean,
)

private data class UserChangeMailRequest(
    val mail: String,
    val verified: Boolean,
)

private data class ChangeUserStatusRequest(
    val disabled: Boolean,
)

internal class UserApi(
    app: Javalin,
    private val credentialProvider: ICredentialProvider,
    private val serviceList: List<IControllableService>,
    private val sessionTerminator: ISessionOperations,
    private val invitationDatabaseConnector: InvitationDatabaseConnector,
) {
    private val logger = KotlinLogging.logger {}

    init {
        app.routes {
            path("users") {
                get(::getAllUsers)
                path("{id}") {
                    get(::getUser)
                    get("/image", ::getUserImage)
                    post("/name", ::changeNameOfUser)
                    post("/mail", ::changeMailOfUser)
                    post("/status", ::changeStatusOfUser)
                    post("/permissions", ::changePermissionsOfUser)
                }
            }
        }
    }

    private fun getAllUsers(ctx: Context) {
        val session = ctx.session()
        ctx.status(200)

        val g = GetUser(credentialProvider)
        ctx.json(g.getAllUsers(session.user))
    }

    private fun getUser(ctx: Context) {
        val session = ctx.session()
        val id = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        val user = credentialProvider.getUser(id) ?: throw NotFoundResponse("User $id not found")
        val g = GetUser(credentialProvider)
        ctx.json(g.getSingleUser(session.user, user))
        ctx.status(200)
    }

    private fun getUserImage(ctx: Context) {
        val id = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        val user = credentialProvider.getUser(id) ?: throw NotFoundResponse("User $id not found")
        val image: ByteArray = credentialProvider.getPhotoForUser(user) ?: throw NotFoundResponse("No image")
        ctx.result(image)
        ctx.contentType("image/jpeg")
        ctx.status(200)
    }

    private fun changeNameOfUser(ctx: Context) {
        val session = ctx.session()
        val changingUser = session.user
        val affectedUserId = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        val affectedUser = credentialProvider.getUser(affectedUserId) ?: throw NotFoundResponse("User $affectedUserId not found")

        val body = ctx.bodyAsClass<UserChangeNameRequest>()

        if (!changingUser.isAdmin()) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"
        if (body.firstname.isEmpty()) errorMessages += "Ein Vorname ist benötigt"
        if (body.firstname.length !in (2..40)) errorMessages += "Ein Vorname benötigt mindestens 2 und maximal 40 Zeichen"
        if (body.surname.isEmpty()) errorMessages += "Ein Nachname ist benötigt"
        if (body.surname.length !in (2..40)) errorMessages += "Ein Nachname benötigt mindestens 2 und maximal 40 Zeichen"
        if (body.displayName.isEmpty()) errorMessages += "Ein voller Name ist benötigt"
        if (body.displayName.length !in (3..80)) errorMessages += "Ein voller Name benötigt mindestens 3 und maximal 80 Zeichen"
        if ((body.firstname == affectedUser.firstname) &&
            (body.surname == affectedUser.surname) &&
            (body.displayName == affectedUser.displayName)
        ) {
            errorMessages += "Die Namen haben sich nicht geändert"
        }

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not change user name ${body.displayName} due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            val changeUserNameService = ChangeUserName(credentialProvider, serviceList)

            try {
                logger.info { "About to change name for user with id ${affectedUser.id} to ${body.surname}, ${body.firstname} (${body.displayName})" }
                changeUserNameService.changeUserName(
                    affectedUser,
                    body.firstname,
                    body.surname,
                    body.displayName,
                )
            } catch (e: Exception) {
                logger.warn(e) { "Will not change name for user with id ${affectedUser.id} due to exception" }

                errorMessages += arrayOf("Interner Fehler (Benachrichtige die IT)")
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
            ctx.json(credentialProvider.getUser(affectedUser.id)!!)
        }
    }

    private fun changeMailOfUser(ctx: Context) {
        val session = ctx.session()
        val changingUser = session.user
        val affectedUserId = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        val affectedUser = credentialProvider.getUser(affectedUserId) ?: throw NotFoundResponse("User $affectedUserId not found")

        val body = ctx.bodyAsClass<UserChangeMailRequest>()

        if (!changingUser.isAdmin()) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"
        if (body.mail.isEmpty()) errorMessages += "Eine E-Mail ist benötigt"
        if (body.mail.length !in (5..100)) errorMessages += "Eine E-Mail benötigt mindestens 5 und maximal 100 Zeichen"
        try {
            AsyncMailSender.validateMail(body.mail, "${affectedUser.firstname} ${affectedUser.surname}")
        } catch (e: AddressException) {
            val reason = e.toString()
            errorMessages += "Die angegebene E-Mail-Adresse ist nicht RFC822 konform (Grund: '$reason')"
        }
        if (body.mail == affectedUser.mail) errorMessages += "Die E-Mail hat sich nicht geändert"
        if (credentialProvider.getUserByMail(body.mail) != null) errorMessages += "Diese E-Mail Adresse ist bereits registriert"
        if (invitationDatabaseConnector.getInvitationByMail(credentialProvider, body.mail) != null) errorMessages += "Diese E-Mail Adresse wurde bereits eingeladen"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not change user mail ${body.mail} due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            val changeMailAddressService = ChangeMailAddress(credentialProvider, serviceList)

            try {
                logger.info { "About to change mail address for user with id ${affectedUser.id} to ${body.mail}" }

                changeMailAddressService.changeMailAddress(affectedUser, body.mail)
            } catch (e: Exception) {
                logger.warn(e) { "Will not change mail address for user with id ${affectedUser.id} due to exception" }

                errorMessages += arrayOf("Interner Fehler (Benachrichtige die IT)")
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }

            ctx.json(credentialProvider.getUser(affectedUser.id)!!)
        }
    }

    private fun User.isAdmin() = permissions.contains(Permission.ADMIN)

    private fun changeStatusOfUser(ctx: Context) {
        val session = ctx.session()
        val changingUser = session.user
        val affectedUserId = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }

        if (!changingUser.isAdmin()) {
            throw ForbiddenResponse()
        } else if (changingUser.id == affectedUserId) {
            throw ForbiddenResponse()
        }

        val affectedUser = credentialProvider.getUser(affectedUserId) ?: throw NotFoundResponse("User $affectedUserId not found")

        val newStatus = ctx.bodyAsClass<ChangeUserStatusRequest>()

        val activateUserService = ActivateUser(credentialProvider, serviceList)

        try {
            logger.info { "About to (de)activate user with id ${affectedUser.id}" }

            activateUserService.activateUser(
                affectedUser.id,
                !newStatus.disabled,
                sessionTerminator,
            )
        } catch (e: Exception) {
            logger.warn(e) { "Will not (de)activate user with id ${affectedUser.id} due to exception" }

            val errorMessages = arrayOf("Interner Fehler (Benachrichtige die IT)")
            ctx.status(400)
            ctx.json(errorMessages)
            return
        }

        ctx.json(credentialProvider.getUser(affectedUser.id)!!)
    }

    private fun changePermissionsOfUser(ctx: Context) {
        val session = ctx.session()
        val changingUser = session.user
        val affectedUserId = ctx.pathParamAsClass<Int>("id").getOrThrow { BadRequestResponse("User id needs to be an integer") }
        val affectedUser = credentialProvider.getUser(affectedUserId) ?: throw NotFoundResponse("User $affectedUserId not found")

        val newPermissionsAsStrings = ctx.bodyAsClass<Set<String>>()

        if (!changingUser.isAdmin()) {
            throw ForbiddenResponse()
        }

        val newPermissions =
            try {
                newPermissionsAsStrings.map {
                    Permission.valueOf(it)
                }.toSet()
            } catch (e: IllegalArgumentException) {
                // string cannot be cast to the enum class
                throw ForbiddenResponse()
            }

        if (newPermissions.contains(Permission.ADMIN)) {
            /* just as for invitations, ADMIN permissions cannot be granted
             * from inside Feather (only in Free-IPA) */
            throw ForbiddenResponse()
        }

        credentialProvider.updateUser(affectedUser.copy(permissions = newPermissions))
        ctx.json(credentialProvider.getUser(affectedUser.id)!!.permissions)
    }
}
