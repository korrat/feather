/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.plausibility

import dev.maximilian.feather.multiservice.BackgroundJobManager
import java.util.UUID

class BackgroundJobStatusHelper(
    private val jobId: UUID,
    private val stepDescription: String,
    private val totalEntries: Int,
    backgroundJobManager: BackgroundJobManager,
) {
    var percent: Double = 0.0
    val backgroundJobManager = backgroundJobManager

    init {
        setJobStatus()
    }

    fun updateStatus(currentEntry: Int) {
        val newPercent = 100.0 * currentEntry.toDouble() / totalEntries.toDouble()
        if (newPercent - percent > 5.0) {
            percent = newPercent
            setJobStatus()
        }
    }

    private fun setJobStatus() {
        val percentString = "%.1f".format(percent)
        backgroundJobManager.setJobStatus(jobId, "$stepDescription - $percentString%")
    }
}
