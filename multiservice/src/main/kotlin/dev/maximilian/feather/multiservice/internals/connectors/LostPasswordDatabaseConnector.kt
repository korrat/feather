/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.connectors

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.multiservice.internals.LostPassword
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

internal class LostPasswordDatabaseConnector(private val db: Database, accountController: AccountController) {
    private val lostPasswordTable = LostPasswordTable(accountController)

    init {
        transaction(db) { SchemaUtils.createMissingTablesAndColumns(lostPasswordTable) }
    }

    fun getLostPassword(
        uuid: UUID,
        credentialProvider: ICredentialProvider,
    ): LostPassword? =
        transaction(db) {
            lostPasswordTable.select {
                lostPasswordTable.id eq EntityID(uuid, lostPasswordTable)
            }.firstOrNull()?.let {
                credentialProvider.getUser(it[lostPasswordTable.user].value)
                    ?.let { user -> rowToLostPassword(it, user) }
            }?.takeIf { it.validUntil >= Instant.now() }
        }

    private fun calculateValidUntil(): Instant {
        return Instant.now().plus(30, ChronoUnit.MINUTES)
    }

    fun createLostPassword(
        user: User,
        credentialProvider: ICredentialProvider,
    ): LostPassword =
        getLostPassword(
            transaction(db) {
                lostPasswordTable.insertAndGetId {
                    it[lostPasswordTable.user] = user.id
                    it[validUntil] = calculateValidUntil()
                }.value
            },
            credentialProvider,
        )!!

    fun removeLostPassword(lostPassword: LostPassword) {
        transaction(db) { lostPasswordTable.deleteWhere { lostPasswordTable.id eq lostPassword.id } }
    }

    private fun rowToLostPassword(
        it: ResultRow,
        user: User,
    ): LostPassword =
        LostPassword(
            validUntil = it[lostPasswordTable.validUntil],
            user = user,
            id = it[lostPasswordTable.id].value,
        )

    class LostPasswordTable(accountController: AccountController) : UUIDTable("lost_password") {
        val user: Column<EntityID<Int>> = reference("inviter", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val validUntil: Column<Instant> = timestamp("validUntil")
    }
}
