/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.bodyAsClass

internal class AccountApi(app: Javalin, val credentialProvider: ICredentialProvider) {
    init {
        app.routes {
            path("/account") {
                post("/changepass", ::changePassword)
                post("/setimage", ::setImage)
                delete("/image", ::deleteImage)
            }
        }
    }

    private fun changePassword(ctx: Context) {
        val user = ctx.session().user
        val request = ctx.bodyAsClass<ChangePasswordRequest>()

        if (credentialProvider.authenticateUserByUsernameOrMail(user.username, request.passwordOld) == null) throw ForbiddenResponse()
        // Sanity checks!
        val errorList = mutableListOf<String>()

        if (request.password1.length < 8 || request.password1.length > 400) {
            errorList.add("Das Passwort muss aus mindestens 8 und maximal 400 Zeichen bestehen")
        }

        if (request.password1 != request.password2) {
            errorList.add("Die Passwörter müssen identisch sein")
        }

        if (errorList.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorList)
        } else {
            credentialProvider.updateUserPassword(user, request.password1)
            // CacheManager.refreshCacheAsync()
            ctx.status(204)
        }
    }

    companion object {
        private const val MAX_IMAGE_SIZE_KIB = 20
    }

    private fun setImage(ctx: Context) {
        val user = ctx.session().user

        val uploadedFile = ctx.uploadedFile("profileImage") ?: throw BadRequestResponse("Parameter profileImage is required")
        if (uploadedFile.contentType() != "image/jpeg") throw BadRequestResponse("Image must be in JPEG format")
        if (uploadedFile.size() > MAX_IMAGE_SIZE_KIB * 1024) {
            throw BadRequestResponse(
                "Image size must be less than 20 KiB (is: ${uploadedFile.size()} B)",
            )
        }
        credentialProvider.updatePhotoForUser(user, uploadedFile.content().readAllBytes())
        ctx.status(204)
    }

    private fun deleteImage(ctx: Context) {
        val user = ctx.session().user
        if (credentialProvider.getPhotoForUser(user) == null) {
            ctx.status(404)
        } else {
            credentialProvider.deletePhotoForUser(user)
            ctx.status(204)
        }
    }
}
