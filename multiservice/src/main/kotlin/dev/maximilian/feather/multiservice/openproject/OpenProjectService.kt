/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.openproject

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.openproject.internals.OpenProjectUserInfo
import dev.maximilian.feather.openproject.EmptyAvatar
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectFactory
import dev.maximilian.feather.openproject.OpenProjectUser
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.UUID

class OpenProjectService(
    private val backgroundJobManager: BackgroundJobManager,
    internal val synchronizer: GroupSynchronizationEvent,
    private val openProjectSettings: OpenProjectSettings,
    private val credentialProvider: ICredentialProvider,
    requestRetryCount: Int = 0,
) : IControllableService {
    companion object : KLogging()

    val openproject: IOpenProject =
        OpenProjectFactory().create(
            openProjectSettings.baseUrl,
            openProjectSettings.authUser,
            openProjectSettings.authPassword,
            "X-Remote-User",
            openProjectSettings.ssoAdmin,
            openProjectSettings.ssoSecret,
            requestRetryCount,
        )

    init {
        logger.info { "OpenProject binding enabled for ${openProjectSettings.baseUrl}" }
    }

    override val serviceName = "OpenProject"

    private suspend fun mapToOpenProjectUser(user: User): OpenProjectUser? =
        runBlocking {
            openproject.getUserByLogin(getOpenProjectUserId(user)) ?: openproject.getUserByEmail(user.mail)
        }

    override fun activateUser(
        user: User,
        activate: Boolean,
    ): Boolean {
        val openProjectUser = runBlocking { mapToOpenProjectUser(user) }

        if (openProjectUser == null) {
            logger.warn { "OpenProjectService::activateUser Could not find user (${user.id}) with mail \"${user.mail}\" in OpenProject." }
            return false
        } else {
            val currentlyEnabled = openProjectUser.status == "active" // != "locked"
            if (activate && !currentlyEnabled) {
                kotlin.runCatching {
                    runBlocking { openproject.unlockUser(openProjectUser) }
                }.onFailure {
                    logger.error(
                        it,
                    ) { "OpenProjectService::activateUser Unlocking user ${openProjectUser.id} in OpenProject not successful." }
                    return false
                }
            } else if (!activate && currentlyEnabled) {
                kotlin.runCatching {
                    runBlocking { openproject.lockUser(openProjectUser) }
                }.onFailure {
                    logger.error(it) { "OpenProjectService::activateUser Locking user ${openProjectUser.id} in OpenProject not successful" }
                    return false
                }
            } else {
                logger.info { "User state of ${openProjectUser.id} already as required" }
                return false
            }
        }
        return true
    }

    override fun setUsersActivated(
        users: List<User>,
        activated: Boolean,
    ) {
        val desiredStatus = if (activated) "active" else "locked"
        runBlocking {
            users.mapNotNull { mapToOpenProjectUser(it) }.filter { it.status != desiredStatus }.forEach {
                if (activated) {
                    openproject.unlockUser(it)
                } else {
                    openproject.lockUser(it)
                }
            }
        }
    }

    override fun changeMailAddress(
        user: User,
        newMail: String,
    ): Boolean {
        val openProjectUser = runBlocking { mapToOpenProjectUser(user) }

        when {
            openProjectUser == null -> {
                logger.warn { "OpenProjectService::changeMailAddress Could not find user (${user.id}) with mail \"${user.mail}\" in OpenProject." }
                return false
            }
            openProjectUser.email != newMail -> {
                kotlin.runCatching {
                    runBlocking { openproject.changeMailAddress(openProjectUser, newMail) }
                }.onFailure {
                    logger.error(it) {
                        "OpenProjectService::changeMailAddress Changing mail address for user ${openProjectUser.id} in OpenProject not successful."
                    }
                    return false
                }
            }
            else -> {
                logger.info { "Mail address of user ${openProjectUser.id} already as required" }
            }
        }
        return true
    }

    override fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String,
    ): Boolean {
        val openProjectUser = runBlocking { mapToOpenProjectUser(user) }

        when {
            openProjectUser == null -> {
                logger.warn { "OpenProjectService::changeUserName Could not find user (${user.id}) with mail \"${user.mail}\" in OpenProject." }
                return false
            }
            openProjectUser.firstName != newFirstname || openProjectUser.lastName != newSurname -> {
                kotlin.runCatching {
                    runBlocking { openproject.changeName(openProjectUser, newFirstname, newSurname) }
                }.onFailure {
                    logger.error(it) {
                        "OpenProjectService::changeUserName Changing mail address for user ${openProjectUser.id} in OpenProject not successful."
                    }
                    return false
                }
            }
            else -> {
                logger.info { "Name of user ${openProjectUser.id} already as required" }
            }
        }
        return true
    }

    override fun createUser(
        createdUser: User,
        jobId: UUID?,
    ): User {
        val login = getOpenProjectUserId(createdUser)

        jobId?.let { backgroundJobManager.setJobStatus(it, "Registriere Benutzer in OpenProject") }

        kotlin.runCatching {
            runBlocking {
                val user =
                    openproject.createAuthSourceUser(
                        OpenProjectUser(
                            id = 0,
                            login = login,
                            email = createdUser.mail,
                            firstName = createdUser.firstname,
                            lastName = createdUser.surname,
                            admin = createdUser.permissions.contains(Permission.ADMIN),
                            authSource = 1,
                            status = "active",
                            avatar = EmptyAvatar,
                        ),
                        1,
                    )
                if (createdUser.disabled) {
                    logger.info { "OpenProjectService::createUser User was created with disabled option." }
                    openproject.lockUser(user)
                }
            }
        }.onFailure {
            logger.error(it) { "OpenProjectService::createUser User creation in OpenProject not successful." }
            throw RuntimeException("Failed to create user in OpenProject")
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Synchronisiere Gruppenmitgliedschaften in OpenProject") }

        kotlin.runCatching {
            synchronizer.synchronize()
        }.onSuccess {
            logger.info { "OpenProjectService::createUser User group synchronisation in OpenProject successful." }
        }.onFailure {
            logger.error(it) { "OpenProjectService::createUser User group synchronisation in OpenProject not successful." }
            throw it
        }

        return createdUser
    }

    override fun deleteUser(user: User): Boolean {
        val openProjectUser = runBlocking { openproject.getUserByEmail(user.mail) }

        if (openProjectUser == null) {
            logger.warn { "OpenProjectService::deleteUser Could not find user (${user.id}) with mail \"${user.mail}\" in OpenProject." }
            // ignore this
            return true
        } else {
            kotlin.runCatching {
                runBlocking { openproject.deleteUser(openProjectUser) }
            }.onFailure {
                logger.error(it) { "OpenProjectService::deleteUser User deletion in OpenProject not successful." }
                return false
            }
        }
        return true
    }

    override suspend fun checkUser(
        user: User,
        autoRepair: Boolean,
    ): String {
        // val openProjectUser = openproject.getUserByEmail(user.mail)
        // we use to login name to be faster (filters on server side, while search by mail filters on client)
        val openProjectUser = openproject.getUserByLogin(getOpenProjectUserId(user))
        return openProjectUser ?.let {
            when {
                // user.disabled != (openProjectUser.status != "active") /* == "locked" */
                user.disabled != (openProjectUser.status != "active") -> {
                    if (autoRepair) {
                        if (activateUser(user, !user.disabled)) {
                            "OK"
                        } else {
                            "Error: failed to (de)activate user!"
                        }
                    } else {
                        "User status differs: ${openProjectUser.status}"
                    }
                }
                user.mail != openProjectUser.email -> {
                    if (autoRepair) {
                        if (changeMailAddress(user, user.mail)) {
                            "OK"
                        } else {
                            "Error: failed to change mail address!"
                        }
                    } else {
                        "Mail address differs: ${openProjectUser.email}"
                    }
                }
                else -> "OK"
            }
        } ?: "Not found"
    }

    private fun getOpenProjectUserId(user: User): String =
        when (openProjectSettings.userBinding) {
            "username" -> user.username
            "id" ->
                requireNotNull(credentialProvider.getUserExternalId(user)) {
                    "User with id ${user.id} not found"
                }
            else -> throw IllegalStateException("Invalid OpenProject user binding ${openProjectSettings.userBinding}")
        }

    private fun getOpenProjectUserId(userId: Int): String =
        getOpenProjectUserId(
            requireNotNull(credentialProvider.getUser(userId)) {
                "User with id $userId not found"
            },
        )

    internal fun getOpenProjectWorkPackages(userId: Int): OpenProjectUserInfo? {
        val openprojectUserId = getOpenProjectUserId(userId)
        val openProjectUser = runBlocking { openproject.getUserByLogin(openprojectUserId) }

        return openProjectUser?.let {
            OpenProjectUserInfo(
                user = openProjectUser,
                workPackages = runBlocking { openproject.getWorkPackagesOfUser(openProjectUser, openproject.getStatus()) },
            )
        }
    }
}
