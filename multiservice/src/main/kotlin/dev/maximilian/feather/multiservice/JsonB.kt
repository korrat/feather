/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonTypeRef
import com.fasterxml.jackson.module.kotlin.readValue
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.statements.api.PreparedStatementApi
import org.jetbrains.exposed.sql.vendors.H2Dialect
import org.jetbrains.exposed.sql.vendors.PostgreSQLDialect
import org.jetbrains.exposed.sql.vendors.currentDialect
import org.postgresql.util.PGobject
import kotlin.reflect.KClass

inline fun <reified T : Any> Table.jsonb(
    name: String,
    jsonMapper: ObjectMapper,
): Column<T> = registerColumn(name, Json(T::class, jsonMapper, jacksonTypeRef()))

class Json<out T : Any>(
    private val clazz: KClass<T>,
    private val jsonMapper: ObjectMapper,
    private val typeRef: TypeReference<T>,
) : ColumnType() {
    override fun sqlType() =
        when (currentDialect) {
            is PostgreSQLDialect -> "jsonb"
            is H2Dialect -> "json"
            else -> throw UnsupportedOperationException("The jsonb data type is currently only supported for postgres and h2")
        }

    override fun setParameter(
        stmt: PreparedStatementApi,
        index: Int,
        value: Any?,
    ) {
        if (currentDialect is PostgreSQLDialect) {
            PGobject().apply {
                this.type = sqlType()
                this.value = value as String
                stmt[index] = this
            }
        } else {
            super.setParameter(stmt, index, value)
        }
    }

    override fun valueFromDB(value: Any): Any =
        when {
            value::class == clazz -> value
            currentDialect is H2Dialect -> jsonMapper.readValue(jsonMapper.readValue<String>(value as ByteArray), typeRef)
            currentDialect is PostgreSQLDialect -> jsonMapper.readValue(value.toString(), typeRef)
            else -> throw UnsupportedOperationException("The jsonb data type is currently only supported for postgres and h2")
        }

    override fun notNullValueToDB(value: Any): Any = jsonMapper.writeValueAsString(value)

    override fun nonNullValueToString(value: Any): String = jsonMapper.writeValueAsString(value)

    override fun valueToString(value: Any?): String =
        if (value == null) {
            super.valueToString(
                value,
            )
        } else {
            jsonMapper.writeValueAsString(value)
        }
}
