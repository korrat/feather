/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.nextcloud

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.api.NextcloudApi
import dev.maximilian.feather.multiservice.internals.NextcloudMappingCache
import dev.maximilian.feather.multiservice.internals.useroperations.ActivateUser
import dev.maximilian.feather.multiservice.internals.useroperations.ChangeMailAddress
import dev.maximilian.feather.multiservice.internals.useroperations.DeleteUser
import dev.maximilian.feather.nextcloud.NextcloudFactory
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity
import io.javalin.Javalin
import kotlinx.coroutines.runBlocking
import redis.clients.jedis.JedisPool
import java.util.UUID

class NextcloudService(
    app: Javalin,
    pool: JedisPool,
    private val nextcloudSettings: NextcloudSettings,
    private val credentialProvider: ICredentialProvider,
) : IControllableService {
    private val allowMissingAccount = true

    private val nextcloudMappingCache = NextcloudMappingCache(pool)

    val nextcloud = NextcloudFactory().create(nextcloudSettings.baseUrl, nextcloudSettings.username, nextcloudSettings.password)

    init {
        NextcloudApi(app, this)
    }

    override val serviceName = "Nextcloud"

    override fun activateUser(
        user: User,
        activate: Boolean,
    ): Boolean {
        val nextcloudUser = getNextcloudUser(user.id)

        if (nextcloudUser == null) {
            if (allowMissingAccount) {
                ActivateUser.logger.info {
                    "ActivateUser::activateUserInNextcloud User (${user.id}) with username ${user.username} does not exist in Nextcloud."
                }
                nextcloudMappingCache.deleteFromNextcloudMappingCache(user.id)
                return true
            } else {
                ActivateUser.logger.warn {
                    "ActivateUser::activateUserInNextcloud Could not find user (${user.id}) with username ${user.username} in Nextcloud."
                }
                return false
            }
        } else {
            if (activate && !nextcloudUser.enabled) {
                try {
                    nextcloud.enableUser(nextcloudUser.id)
                } catch (e: Exception) {
                    ActivateUser.logger.error(
                        e,
                    ) { "ActivateUser::activateUserInNextcloud Enabling user ${nextcloudUser.id} in Nextcloud not successfull." }
                    return false
                }
            } else if (!activate && nextcloudUser.enabled) {
                try {
                    nextcloud.disableUser(nextcloudUser.id)
                } catch (e: Exception) {
                    ActivateUser.logger.error(
                        e,
                    ) { "ActivateUser::activateUserInNextcloud Disabling user ${nextcloudUser.id} in Nextcloud not successfull." }
                    return false
                }
            } else {
                ActivateUser.logger.info { "User state of ${nextcloudUser.id} already as required" }
            }
        }

        nextcloudMappingCache.deleteFromNextcloudMappingCache(user.id)
        return true
    }

    override fun setUsersActivated(
        users: List<User>,
        activated: Boolean,
    ) {
        runBlocking {
            nextcloud.getAllUsersWithDetails(users.map { getNextcloudId(it.id) }.toSet()).filter { it.enabled != activated }.forEach {
                if (activated) {
                    nextcloud.enableUser(it.id)
                } else {
                    nextcloud.disableUser(it.id)
                }
            }
        }
    }

    override fun changeMailAddress(
        user: User,
        newMail: String,
    ): Boolean {
        val nextcloudUser = getNextcloudUser(user.id)

        when {
            nextcloudUser == null && allowMissingAccount -> {
                ChangeMailAddress.logger.info {
                    "ChangeMailAddress::changeMailAddressInNextcloud User (${user.id}) with username ${user.username} was not found in Nextcloud."
                }

                nextcloudMappingCache.deleteFromNextcloudMappingCache(user.id)
                return true
            }
            nextcloudUser == null -> {
                ChangeMailAddress.logger.warn {
                    "ChangeMailAddress::changeMailAddressInNextcloud Could not find user (${user.id}) with username ${user.username} in Nextcloud."
                }
                return false
            }
            nextcloudUser.email != newMail -> {
                try {
                    // Note that the "notify user about mail change by admin" should be disabled.
                    // Otherwise, the user will get more mails than necessary.
                    nextcloud.changeMailAddress(nextcloudUser.id, newMail)
                } catch (e: Exception) {
                    ChangeMailAddress.logger.error(e) {
                        "ChangeMailAddress::changeMailAddressInNextcloud Change mail of user ${nextcloudUser.id} in Nextcloud not successfull."
                    }
                    return false
                }
            }
            else -> {
                ChangeMailAddress.logger.info { "Mail address of user ${nextcloudUser.id} already as required" }
            }
        }

        nextcloudMappingCache.deleteFromNextcloudMappingCache(user.id)
        return true
    }

    override fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String,
    ): Boolean {
        /* Nothing to do in Nextcloud; NC takes name from LDAP and thus this changes automatically.
         * But remove from our cache so that we fetch the updated data next time. */
        nextcloudMappingCache.deleteFromNextcloudMappingCache(user.id)
        return true
    }

    override fun deleteUser(user: User): Boolean {
        val nextcloudUserId = getNextcloudId(user.id)

        try {
            nextcloud.deleteUser(nextcloudUserId)
        } catch (e: NoSuchElementException) {
            DeleteUser.logger.warn { "DeleteUser::deleteUser Could not find user $nextcloudUserId in Nextcloud." }
            // ignore this
            return true
        } catch (e: Exception) {
            DeleteUser.logger.error(e) { "DeleteUser::deleteUser User deletion in Nextcloud not successfull." }
            return false
        }
        return true
    }

    override fun createUser(
        createdUser: User,
        jobId: UUID?,
    ): User = createdUser

    override suspend fun checkUser(
        user: User,
        autoRepair: Boolean,
    ): String {
        val nextcloudUser = getNextcloudUser(user.id)
        return nextcloudUser?.let {
            when {
                user.disabled == nextcloudUser.enabled -> {
                    if (autoRepair) {
                        if (activateUser(user, !user.disabled)) {
                            "OK"
                        } else {
                            "Error: failed to (de)activate user!"
                        }
                    } else {
                        "User status differs: enabled=${nextcloudUser.enabled}"
                    }
                }
                user.mail != nextcloudUser.email -> {
                    if (autoRepair) {
                        if (changeMailAddress(user, user.mail)) {
                            "OK"
                        } else {
                            "Error: failed to change mail address!"
                        }
                    } else {
                        "Mail address differs: ${nextcloudUser.email}"
                    }
                }
                else -> "OK"
            }
        } ?: "Not found"
    }

    private fun getNextcloudId(userId: Int): String {
        val resolvedUser =
            requireNotNull(credentialProvider.getUser(userId)) {
                "User with id $userId not found"
            }

        return when (nextcloudSettings.userBinding) {
            "username" -> resolvedUser.username
            "id" ->
                requireNotNull(credentialProvider.getUserExternalId(resolvedUser)) {
                    "User with id $userId not found"
                }
            else -> throw IllegalStateException("Invalid Nextcloud user binding ${nextcloudSettings.userBinding}")
        }
    }

    internal fun getNextcloudUser(
        userId: Int,
        forceReload: Boolean = false,
    ): NextcloudUserDataEntity? {
        val cachedValue: String? = nextcloudMappingCache.getUser(userId)

        return if (cachedValue == null || forceReload) {
            val nextcloudUserId = getNextcloudId(userId)

            val userInfo =
                try {
                    nextcloud.getUserInfo(nextcloudUserId)
                } catch (e: Exception) {
                    null
                }

            // only save if data was given
            userInfo?.apply {
                nextcloudMappingCache.store(userId, this)
            }
        } else {
            return nextcloudMappingCache.decodeDatatEntity(cachedValue)
        }
    }
}
