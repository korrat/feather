/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.Group
import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.EMPTY_UUID
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.internals.AsyncMailSender
import dev.maximilian.feather.multiservice.internals.FeatherMustache
import dev.maximilian.feather.multiservice.internals.Invitation
import dev.maximilian.feather.multiservice.internals.Mail
import dev.maximilian.feather.multiservice.internals.SitenameData
import dev.maximilian.feather.multiservice.internals.connectors.InvitationDatabaseConnector
import dev.maximilian.feather.multiservice.internals.toMailInvitationData
import dev.maximilian.feather.multiservice.internals.useroperations.CreateUser
import dev.maximilian.feather.multiservice.internals.useroperations.CreateUserConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.session
import dev.maximilian.feather.sessionOrNull
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.NotFoundResponse
import io.javalin.http.bodyAsClass
import io.javalin.http.pathParamAsClass
import jakarta.mail.internet.AddressException
import jakarta.mail.internet.InternetAddress
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

internal class InvitationApi(
    app: Javalin,
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>,
    private val events: MultiServiceEvents,
    private val invitationDatabaseConnector: InvitationDatabaseConnector,
    private val backgroundJobManager: BackgroundJobManager,
    private val asyncMailSender: AsyncMailSender,
    private val mailSettings: MultiServiceMailSettings,
    private val gdprController: GdprController,
) {
    private val displayNameRegex = Regex("^[a-z][a-z0-9\\.]*\$")
    private val logger = KotlinLogging.logger {}

    init {
        app.routes {
            path("invitations") {
                get(::handleGetAllValidInvitations, Permission.ADMIN, Permission.INVITE)
                post(::handleCreateInvitation, Permission.ADMIN, Permission.INVITE)

                path("resend") {
                    post(::handleResendInvitation, Permission.ADMIN, Permission.INVITE)
                }

                path("withdraw") {
                    post(::handleWithdrawInvitation, Permission.ADMIN, Permission.INVITE)
                }

                path("{uuid}") {
                    get(::handleGetInvitation)
                    post(::finalizeInvitation)
                }
            }
        }
    }

    private fun handleGetAllValidInvitations(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val user = ctx.session().user
        val invitations =
            when {
                user.permissions.contains(Permission.ADMIN) -> invitationDatabaseConnector.getAllValidInvitations(credentialProvider)
                else -> invitationDatabaseConnector.getValidInvitationsByInviter(user).map { it.copy(id = EMPTY_UUID) }
            }

        ctx.status(200)
        ctx.json(invitations)
    }

    private fun handleGetInvitation(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val user = ctx.sessionOrNull()?.user
        val uuid: UUID =
            ctx.pathParamAsClass<UUID>("uuid").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val invitation = invitationDatabaseConnector.getInvitation(uuid, credentialProvider) ?: throw NotFoundResponse("$uuid not found")

        ctx.status(200)
        ctx.json(
            invitation.let {
                if (user != null && (user.permissions.contains(Permission.ADMIN) || user.id == invitation.inviter.id)) {
                    it
                } else {
                    it.copy(
                        inviter =
                        User(
                            id = 0,
                            ownedGroups = emptySet(),
                            groups = emptySet(),
                            mail = "",
                            username = "",
                            firstname = "",
                            surname = "",
                            permissions = emptySet(),
                            displayName = "",
                            registeredSince = Instant.ofEpochSecond(0),
                            lastLoginAt = Instant.ofEpochSecond(0),
                        ),
                    )
                }
            },
        )
    }

    private fun User.canInvite() =
        permissions.contains(Permission.INVITE) ||
            permissions.contains(Permission.ADMIN)

    private fun Group.isAdminOrAllowedToInvite(inviter: User) =
        inviter.permissions.contains(Permission.ADMIN) ||
            hasOwnerRights(inviter.id, credentialProvider)

    private fun handleCreateInvitation(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val inviter = ctx.session().user

        if (!inviter.canInvite()) {
            throw ForbiddenResponse()
        }

        val invitation =
            ctx.bodyAsClass<InvitationCreateRequest>().let {
                it.copy(
                    groups =
                    it.groups.mapNotNull { group ->
                        credentialProvider.getGroup(group)
                    }.filter { group ->
                        group.isAdminOrAllowedToInvite(inviter)
                    }.map { group ->
                        group.id
                    }.toSet(),
                )
            }

        if (invitation.permissions.isNotEmpty() && !inviter.permissions.contains(Permission.ADMIN)) throw ForbiddenResponse()
        val inviteePermissions =
            try {
                invitation.permissions.map {
                    Permission.valueOf(it)
                }.toSet()
            } catch (e: IllegalArgumentException) {
                // string cannot be cast to the enum class
                throw ForbiddenResponse()
            }
        if (inviteePermissions.contains(Permission.ADMIN)) throw ForbiddenResponse()
        val errorMessages = mutableSetOf<String>()

        if (!invitation.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"
        if (!invitation.instructed) errorMessages += "Der Datenverarbeitung muss zugestimmt werden"
        if (invitation.firstname.isEmpty()) errorMessages += "Ein Vorname ist benötigt"
        if (invitation.firstname.length !in (2..40)) errorMessages += "Ein Vorname benötigt mindestens 2 und maximal 40 Zeichen"
        if (invitation.surname.isEmpty()) errorMessages += "Ein Nachname ist benötigt"
        if (invitation.surname.length !in (2..40)) errorMessages += "Ein Nachname benötigt mindestens 2 und maximal 40 Zeichen"
        if (invitation.mail.isEmpty()) errorMessages += "Eine E-Mail ist benötigt"
        if (invitation.mail.length !in (5..100)) errorMessages += "Eine E-Mail benötigt mindestens 5 und maximal 100 Zeichen"

        try {
            AsyncMailSender.validateMail(invitation.mail, "${invitation.firstname} ${invitation.surname}")
        } catch (e: AddressException) {
            val reason = e.toString()
            errorMessages += "Die angegebene E-Mail-Adresse ist nicht RFC822 konform (Grund: '$reason')"
        }

        if (credentialProvider.getUserByMail(invitation.mail) != null) errorMessages += "Diese E-Mail Adresse ist bereits registriert"
        if (invitationDatabaseConnector.getInvitationByMail(credentialProvider, invitation.mail) != null) errorMessages += "Diese E-Mail Adresse wurde bereits eingeladen"

        if (errorMessages.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            val created =
                invitationDatabaseConnector.createInvitation(
                    Invitation(
                        id = EMPTY_UUID,
                        used = false,
                        validUntil = calculateValidUntil(),
                        inviter = inviter,
                        invitee =
                        User(
                            id = 0,
                            permissions = inviteePermissions,
                            displayName = "",
                            surname = invitation.surname,
                            firstname = invitation.firstname,
                            username = "",
                            mail = invitation.mail,
                            groups = invitation.groups,
                            ownedGroups = emptySet(),
                            registeredSince = Instant.ofEpochSecond(0),
                            lastLoginAt = Instant.ofEpochSecond(0),
                        ),
                    ),
                    credentialProvider,
                )

            sendInvitationMail(created)

            // TODO Write an Admin Only activity log?
            // TODO write Tests!!!

            ctx.status(201)
        }
    }

    private fun calculateValidUntil(): Instant {
        return Instant.now().plus(30, ChronoUnit.DAYS)
    }

    private fun sendInvitationMail(invitation: Invitation) {
        val mailData = FeatherMustache.MAIL_INVITATION_CONTENT.execute(invitation.toMailInvitationData())
        val mailSubject = FeatherMustache.MAIL_INVITATION_SUBJECT.execute(SitenameData(mailSettings.siteName))

        val invitee = invitation.invitee

        logger.info { "Sending invitation mail to ${invitee.mail}" }

        asyncMailSender.submitMail(
            Mail(
                sender = mailSettings.senderAddress,
                body = mailData,
                receiver = setOf(InternetAddress(invitee.mail, "${invitee.firstname} ${invitee.surname}")),
                subject = mailSubject,
            ),
        )
    }

    private fun handleResendInvitation(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val body = ctx.bodyAsClass<InvitationResendRequest>()

        if (body.mail.isEmpty()) {
            throw BadRequestResponse("mail address is not valid")
        }

        val inviter = ctx.session().user

        if (!inviter.canInvite()) {
            throw ForbiddenResponse()
        }

        val invitation =
            invitationDatabaseConnector.getInvitationByMail(credentialProvider, body.mail)
                ?: throw NotFoundResponse("invation for address ${body.mail} not found")

        if ((inviter != invitation.inviter) && !inviter.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val invitee = invitation.invitee

        if (invitee.groups.map {
                credentialProvider.getGroup(it)
            }.find {
                val isAllowed: Boolean? = it?.isAdminOrAllowedToInvite(inviter)
                !(isAllowed ?: false)
            } != null
        ) {
            throw ForbiddenResponse()
        }

        if (invitee.permissions.isNotEmpty() && !inviter.permissions.contains(Permission.ADMIN)) throw ForbiddenResponse()
        if (invitee.permissions.contains(Permission.ADMIN)) throw ForbiddenResponse()

        val errorMessages = mutableSetOf<String>()

        if (invitation.used) errorMessages += "Diese Einladung wurde bereits genutzt"
        if (!body.verified) errorMessages += "Dem erneuten Zusenden muss zugestimmt werden"

        if (errorMessages.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            invitationDatabaseConnector.updateValidUntil(invitation, calculateValidUntil())

            sendInvitationMail(invitation)

            ctx.status(200)
        }
    }

    private fun finalizeInvitation(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val uuid: UUID = ctx.pathParamAsClass<UUID>("uuid").getOrThrow { BadRequestResponse("token is not a valid uuid") }
        val invitation = invitationDatabaseConnector.getInvitation(uuid, credentialProvider) ?: throw NotFoundResponse("$uuid not found")

        val launchAsync = ctx.queryParam("async")?.toBoolean() ?: false

        val body = ctx.bodyAsClass<InvitationFinalizeRequest>()

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (body.displayName.isEmpty()) errorMessages += "Ein Anzeigename ist benötigt"
        if (body.displayName.length !in (3..40)) errorMessages += "Ein Anzeigename benötigt mindestens 3 und maximal 40 Zeichen"
        if (!(body.displayName matches displayNameRegex)) errorMessages += "Ein Anzeigename darf nur Kleinbuchstaben und Zahlen enthalten. Darf nicht mit einer Zahl starten."
        if (credentialProvider.getUserByUsername(body.displayName) != null) errorMessages += "Dieser Anzeigename existiert schon, bitte einen anderen wählen"
        if (body.password1.isEmpty()) errorMessages += "Ein Passwort ist benötigt"
        if (body.password1.length !in (8..400)) errorMessages += "Ein Passwort benötigt mindestens 8 und maximal 400 Zeichen"
        if (body.password1 != body.password2) errorMessages += "Die Passwörter müssen identisch sein"

        val latestGdpr = gdprController.getLatestDocument()

        if (latestGdpr != null) {
            if (body.gdpr != latestGdpr.id) {
                errorMessages += "Den aktuellen Datenschutzbestimmungen muss zugestimmt werden"
            }
        }

        if (errorMessages.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            logger.info { "Invitation ${invitation.id} was used" }

            val createUserService =
                CreateUser(
                    credentialProvider,
                    services,
                    backgroundJobManager,
                    events.userCreationEvents,
                    gdprController,
                )

            val job: suspend (UUID?, CreateUserConfig) -> User = { jobId: UUID?, config: CreateUserConfig ->
                val user = createUserService.createUser(jobId, config)
                invitationDatabaseConnector.finalize(invitation)
                user
            }

            val config =
                CreateUserConfig(
                    User(
                        id = 0,
                        username = body.displayName,
                        surname = invitation.invitee.surname,
                        mail = invitation.invitee.mail,
                        firstname = invitation.invitee.firstname,
                        displayName = "${invitation.invitee.firstname} ${invitation.invitee.surname}",
                        groups = invitation.invitee.groups,
                        ownedGroups = invitation.invitee.ownedGroups,
                        registeredSince = Instant.ofEpochMilli(0),
                        permissions = invitation.invitee.permissions,
                        lastLoginAt = Instant.ofEpochSecond(0),
                    ),
                    body.password1,
                    body.gdpr,
                )

            try {
                if (launchAsync) {
                    logger.info { "Registering user and finalizing invitation as a background job" }

                    val newJob =
                        backgroundJobManager.runBackgroundJob(
                            job,
                            config,
                            null, // anonymous access
                        )

                    ctx.status(201)
                    ctx.json(newJob)
                } else {
                    runBlocking { job(null, config) }
                    ctx.status(201)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Will not create user with name ${body.displayName} due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }

    private fun handleWithdrawInvitation(ctx: Context) {
        invitationDatabaseConnector.deleteAllExpiredInvitations()

        val body = ctx.bodyAsClass<InvitationResendRequest>()

        if (body.mail.isEmpty()) {
            throw BadRequestResponse("mail address is not valid")
        }

        val inviter = ctx.session().user

        if (!inviter.canInvite()) {
            throw ForbiddenResponse()
        }

        val invitation =
            invitationDatabaseConnector.getInvitationByMail(credentialProvider, body.mail)
                ?: throw NotFoundResponse("invation for address ${body.mail} not found")

        if ((inviter != invitation.inviter) && !inviter.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val errorMessages = mutableSetOf<String>()

        if (invitation.used) errorMessages += "Diese Einladung wurde bereits genutzt"
        if (!body.verified) errorMessages += "Dem Zurückziehen muss zugestimmt werden"

        if (errorMessages.isNotEmpty()) {
            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            invitationDatabaseConnector.deleteInvitation(invitation)

            ctx.status(200)
        }
    }

    private data class InvitationCreateRequest(
        val mail: String,
        val firstname: String,
        val surname: String,
        val groups: Set<Int> = emptySet(),
        val permissions: Set<String> = emptySet(),
        val verified: Boolean,
        val instructed: Boolean,
    )

    private data class InvitationResendRequest(
        val mail: String,
        val verified: Boolean,
    )

    private data class InvitationFinalizeRequest(
        val displayName: String,
        val password1: String,
        val password2: String,
        val gdpr: Int?,
    )
}
