/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.api.internal

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.internals.plausibility.UserPlausibility
import dev.maximilian.feather.multiservice.internals.plausibility.UserPlausibilityConfig
import dev.maximilian.feather.multiservice.settings.CheckUserEvent
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.bodyAsClass
import mu.KotlinLogging

internal class MultiServicePlausibilityApi(
    app: Javalin,
    private val services: List<IControllableService>,
    val backgroundJobManager: BackgroundJobManager,
    private val credentialProvider: ICredentialProvider,
    private val checkUserEvent: CheckUserEvent,
) {
    init {
        app.routes {
            path("bindings/multiservice/plausibility") {
                post("/users", ::handleCreateUserPlausibilityJob, Permission.ADMIN)
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private fun handleCreateUserPlausibilityJob(ctx: Context) {
        val session = ctx.session()
        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val body = ctx.bodyAsClass<UserPlausibilityConfig>()

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!(body.checkNextcloud || body.checkOpenProject)) {
            errorMessages += "Ein Service muss überprüft werden"
        }

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not check the user plausibility for admin ${creator.displayName} due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to check the user plausibility for admin ${creator.displayName}" }

            val userPlausibilityService =
                UserPlausibility(
                    services,
                    backgroundJobManager,
                    credentialProvider,
                    checkUserEvent,
                )

            try {
                val newJob =
                    backgroundJobManager.runBackgroundJob(
                        userPlausibilityService::checkUserPlausibility,
                        body,
                    )

                ctx.status(201)
                ctx.json(newJob)
            } catch (e: Exception) {
                logger.warn(e) { "Will not check user plausibility for admin ${creator.displayName} due to exception" }

                errorMessages += "Interner Fehler (Siehe Feather Logs)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }
        }
    }
}
