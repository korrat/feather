/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.CreateGroupResult

public interface ICiviCRM :
    IACLApi,
    IACLEntityRoleApi,
    IContactApi,
    IGroupApi,
    IEmailApi,
    IGroupContactApi,
    IOptionGroupApi,
    IOptionValueApi,
    IContactTypeApi,
    ICustomGroupApi,
    ICustomFieldApi,
    ICiviRulesApi {
    public suspend fun getRoleIndexByName(n: String): Int?

    public suspend fun createOrReplaceTopLevelGroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
    ): CreateGroupResult

    public suspend fun createOrReplaceSubgroupWithACLPermission(
        name: String,
        title: String,
        roleName: String,
        topLevelRole: String,
    ): CreateGroupResult

    public suspend fun deleteGroupWithAclAndContactDependencies(civiGroupName: String)

    public suspend fun deleteRoleWithAclDependencies(civiRoleNameSuffix: String)
}
