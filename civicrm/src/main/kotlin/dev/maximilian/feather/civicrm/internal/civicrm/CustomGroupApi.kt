/*
 * Copyright [2023] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.CustomGroup
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface ICustomGroupApi {
    public suspend fun getCustomGroup(): List<CustomGroup>

    public suspend fun getCustomGroup(id: Int): CustomGroup?

    public suspend fun createCustomGroup(
        name: String,
        title: String,
    ): CustomGroup

    public suspend fun deleteCustomGroup(id: Int)
}

internal class CustomGroupApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : ICustomGroupApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "CustomGroup", this)

    override suspend fun getCustomGroup(): List<CustomGroup> = generalApi.get().body<GeneralAPI.Answer<CustomGroup>>().values

    override suspend fun getCustomGroup(id: Int): CustomGroup? =
        generalApi.get(
            id,
        ).body<GeneralAPI.Answer<CustomGroup>>().values.singleOrNull()

    override suspend fun createCustomGroup(
        name: String,
        title: String,
    ): CustomGroup {
        logger.info { "CiviCRM::CustomGroupApi::CustomGroupApi with name $name" }
        val s =
            mapOf<String, JsonElement>(
                "name" to JsonPrimitive(name),
                "title" to JsonPrimitive(title),
            )
        return generalApi.create(s).body<GeneralAPI.Answer<CustomGroup>>().values.single()
    }

    override suspend fun deleteCustomGroup(id: Int) {
        logger.info { "CiviCRM::CustomGroupApi::deleteCustomGroup with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getCustomGroup(id)
}
