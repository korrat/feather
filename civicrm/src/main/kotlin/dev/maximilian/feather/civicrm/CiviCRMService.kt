/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.Group
import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.civicrm.entities.Contact
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.UserAddedEvent
import dev.maximilian.feather.multiservice.events.UserRemovedFromGroupEvent
import kotlinx.coroutines.runBlocking
import mu.KLogging
import java.util.UUID

public class CiviCRMService(
    private val credentialProvider: ICredentialProvider,
    private val civiSettings: CiviCRMSettings,
    private val backgroundJobManager: BackgroundJobManager,
) : IControllableService, UserRemovedFromGroupEvent, UserAddedEvent {
    private companion object : KLogging()

    public override val serviceName: String = "CiviCRM"

    public val civicrm: ICiviCRM =
        CiviCRMFactory.create(
            civiSettings,
        )

    private var aclRoleIndex: Int = 0

    init {
        logger.info {
            "CiviCRM binding enabled for ${civiSettings.baseUrl}. Valid groups are defined by name ${civiSettings.validGroupNames.joinToString()} and groups ending with ${civiSettings.validGroupNameSuffix.joinToString()}."
        }
        runBlocking {
            try {
                aclRoleIndex = civicrm.findIndexOfACLRole()
                logger.info { "CiviCRMService::init ${CiviCRMConstants.ACL_ROLE} gefunden" }
            } catch (ex: Exception) {
                logger.error { "CiviCRMService::init ${CiviCRMConstants.ACL_ROLE} wurde nicht in CiviCRM definiert." }
            }
        }
    }

    private fun isUserInCredentialCrmGroup(user: User): Boolean {
        val g = user.groups.mapNotNull { credentialProvider.getGroup(it)?.name }
        logger.info { "CiviCRMService::isUserInCredentialCrmGroup user is in following groups: ${g.joinToString()}" }
        var ret = false
        if (user.permissions.contains(Permission.ADMIN)) {
            logger.info { "CiviCRMService::isUserInCredentialCrmGroup is admin, so grant access to CiviCRM" }
            ret = true
        } else if (g.any { groupName -> civiSettings.validGroupNameSuffix.any { groupName.endsWith(it) } }) {
            logger.info {
                "CiviCRMService::isUserInCredentialCrmGroup user is member of one of the by suffix definied groups ending with: ${civiSettings.validGroupNameSuffix.joinToString()}. Grant access to CiviCRM. "
            }
            ret = true
        } else if (g.any { groupName -> civiSettings.validGroupNames.any { groupName == it } }) {
            logger.info {
                "CiviCRMService::isUserInCredentialCrmGroup user is member of relevant crm groups, because it is part of: ${civiSettings.validGroupNames.joinToString()}. Grant access to CiviCRM. "
            }
            ret = true
        } else {
            logger.info {
                "CiviCRMService::isUserInCredentialCrmGroup user is not ending with ${civiSettings.validGroupNameSuffix.joinToString()} and not starting with ${civiSettings.validGroupNames.joinToString()}. No CiviCRM member."
            }
        }
        return ret
    }

    override fun activateUser(
        user: User,
        activate: Boolean,
    ): Boolean {
        if (!isUserInCredentialCrmGroup(user)) {
            logger.info { "User with mail ${user.mail} does not exist in civicrm. Ignore." }
            return true
        }
        var success = true
        runBlocking {
            if (activate) {
                civicrm.getContactIdByEmail(user.mail)?.let {
                    civicrm.unblockContact(it)
                    logger.info { "CiviCRMService::activateUser User with mail ${user.mail} unblocked." }
                } ?: {
                    createUser(user, null)
                    logger.info { "CiviCRMService::activateUser User with mail: ${user.mail} is now created by activation." }
                    success = true
                }
            } else {
                civicrm.getContactIdByEmail(user.mail)?.let {
                    civicrm.blockContact(it)
                    success = true
                }
            }
        }
        return success
    }

    override fun setUsersActivated(
        users: List<User>,
        activated: Boolean,
    ) {
        users.forEach {
            activateUser(it, activated)
        }
    }

    override fun changeMailAddress(
        user: User,
        newMail: String,
    ): Boolean {
        logger.info { "CiviCRMService::changeMailAddress from ${user.mail} to $newMail" }
        var result = true
        runBlocking {
            mapToCiviContact(user)?.let { contact ->
                civicrm.getEmailByContactId(contact.id)?.let { mail ->
                    civicrm.updateEmail(mail.id, newMail)?.let { changedMail ->
                        result =
                            if (changedMail.email == newMail) {
                                true
                            } else {
                                logger.error {
                                    "CiviCRMService::changeMailAddress Changing mail address for user ${contact.id} in CiviCRM not successful. ChangeMail returned ${changedMail.email} but should be $mail"
                                }
                                false
                            }
                    }
                        ?: {
                            logger.error {
                                "CiviCRMService::changeMailAddress Changing mail address for user ${contact.id} in CiviCRM not successful. ChangeMail returned null."
                            }
                            result = false
                        }
                } ?: {
                    logger.error {
                        "CiviCRMService::changeMailAddress Changing mail address for user ${contact.id} in CiviCRM not successful. Found user but no mail."
                    }
                    result = false
                }
            } ?: {
                logger.info { "CiviCRMService::changeMailAddress Could not find user ${user.mail}" }
                result = true
            }
        }
        return result
    }

    override fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String,
    ): Boolean {
        if (!isUserInCredentialCrmGroup(user)) {
            logger.info { "CiviCRMService::changeUserName User with mail ${user.mail} does not exist. Ignore." }
            return true
        }
        val civiContact = mapToCiviContact(user)

        when {
            civiContact == null -> {
                logger.warn { "CiviCRMService::changeUserName Could not find user (${user.id}) with mail \"${user.mail}\" in Drupal." }
                return false
            }

            civiContact.firstName != newFirstname || civiContact.lastName != newSurname -> {
                kotlin.runCatching {
                    runBlocking {
                        civicrm.changeName(civiContact, newDisplayName, newFirstname, newSurname)
                        logger.info { "CiviCRMService::changeUserName Name changed for use with mail ${user.mail}." }
                    }
                }.onFailure {
                    logger.error(
                        it,
                    ) { "CiviCRMService::changeUserName Changing mail address for user ${civiContact.id} in Drupal not successful." }
                    return false
                }
            }

            else -> {
                logger.info { "Name of user ${civiContact.id} already as required" }
            }
        }
        return true
    }

    override fun createUser(
        createdUser: User,
        jobId: UUID?,
    ): User {
        if (!isUserInCredentialCrmGroup(createdUser)) {
            logger.info { "CiviCRMService::createUser User not created: ${createdUser.displayName}. Not in any civicrm relevant group." }
            return createdUser
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Registriere Benutzer in Drupal/CiviCRM") }

        kotlin.runCatching {
            runBlocking {
                val contactId = createGroupContactAndEmail(createdUser)
                createGroupContactsForUser(createdUser, contactId)
                logger.info { "CiviCRMService::createUser User created: ${createdUser.displayName}" }
            }
        }.onFailure {
            logger.error(it) { "CiviCRMService::createUser User creation in Drupal not successful." }
            throw RuntimeException("Failed to create user in Drupal")
        }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Synchronisiere Gruppenmitgliedschaften in Drupal") }

        return createdUser
    }

    private suspend fun createGroupContactsForUser(
        createdUser: User,
        contactId: Int,
    ) {
        val existingContacts = civicrm.getGroupContactsByContactId(contactId)
        createdUser.groups.forEach { groupID ->
            credentialProvider.getGroup(groupID)?.let {
                civicrm.getGroupByName(it.name)?.let { civiGroup ->
                    if (!existingContacts.any { contact -> contact.groupId == civiGroup.id }) {
                        civicrm.createGroupContact(contactId, civiGroup.id)
                        logger.info { "CiviCRMService::createGroupContactsForUser ${createdUser.displayName} in ${it.name} created." }
                    } else {
                        logger.info {
                            "CiviCRMService::createGroupContactsForUser GroupContact for ${createdUser.displayName} in ${it.name} already exists. Ignore."
                        }
                    }
                }
            }
        }
    }

    override fun deleteUser(user: User): Boolean {
        runBlocking {
            civicrm.getContactIdByEmail(user.mail)?.let { contactId ->
                civicrm.getGroupContacts().filter { it.contactId == contactId }
                    .forEach { civicrm.deleteGroupContact(it.id) }
                civicrm.getEmailByContactId(contactId)?.let { civicrm.deleteEmail(it.id) }
                civicrm.getContact(contactId)?.let {
                    civicrm.deleteContact(it.id)
                }
            }
                ?: logger.info { "CiviCRMService::deleteUser Could not find user (${user.id}) with mail \"${user.mail}\" in Drupal." }
        }
        return true
    }

    override suspend fun checkUser(
        user: User,
        autoRepair: Boolean,
    ): String =
        if (autoRepair) {
            if (isUserInCredentialCrmGroup(user)) {
                repairCiviCRMUser(user)
            } else {
                repairNonCiviCRMUser(user)
            }
        } else {
            if (isUserInCredentialCrmGroup(user)) {
                checkCiviCRMUser(user)
            } else {
                checkNonCiviCRMUser(user)
            }
        }

    private suspend fun checkCiviCRMUser(user: User): String =
        civicrm.getContactIdByEmail(user.mail)?.let { contactId ->
            val a = civicrm.getContact(contactId)
            val deleted = a?.deleted ?: false
            if (deleted) {
                if (user.disabled) {
                    "OK"
                } else {
                    "Blocked in CiviCRM"
                }
            } else if (user.disabled) {
                "Not blocked in CiviCRM"
            } else {
                a?.let { "OK" }
                    ?: "Not found"
            }
        } ?: "Not found"

    private suspend fun checkNonCiviCRMUser(user: User): String =
        civicrm.getContactIdByEmail(user.mail)?.let { contactID ->
            civicrm.getContact(contactID)?.let {
                val deleted = it.deleted ?: false
                if (deleted) {
                    "OK"
                } else {
                    "User is defined in CIVICRM but should not"
                }
            } ?: "Mail without contact"
        } ?: "OK"

    private suspend fun repairCiviCRMUser(user: User): String {
        civicrm.getContactIdByEmail(user.mail)?.let { contactId ->
            val a = civicrm.getContact(contactId)
            val deleted = a?.deleted ?: false
            if (deleted && !user.disabled) {
                civicrm.unblockContact(contactId)
            } else if (!deleted && user.disabled) {
                civicrm.blockContact(contactId)
            }
        } ?: createUser(user, null)
        return checkCiviCRMUser(user)
    }

    private suspend fun repairNonCiviCRMUser(user: User): String {
        civicrm.getContactIdByEmail(user.mail)?.let { contactID ->
            civicrm.getContact(contactID)?.let {
                val deleted = it.deleted ?: false
                if (!deleted) {
                    civicrm.blockContact(it.id)
                }
            } ?: civicrm.getEmails().filter { it.email == user.mail }.forEach { civicrm.deleteEmail(it.id) }
        }
        return checkNonCiviCRMUser(user)
    }

    public override fun userRemovedFromGroup(
        userId: Int,
        groupId: Int,
    ) {
        logger.info { "CiviCRMService::userRemovedFromGroup user $userId group $groupId" }
        runBlocking {
            credentialProvider.getUser(userId)?.let { user ->
                civicrm.getContactIdByEmail(user.mail)?.let { contactId ->
                    credentialProvider.getGroup(groupId)?.let { credentialGroup ->
                        runBlocking {
                            civicrm.getGroupByName(credentialGroup.name)?.let { civiGroup ->
                                civicrm.getGroupContacts()
                                    .filter { it.contactId == contactId && it.groupId == civiGroup.id }
                                    .forEach { groupContact ->
                                        civicrm.deleteGroupContact(groupContact.id)
                                    }
                                if (!isUserInCredentialCrmGroup(user)) {
                                    civicrm.getEmailByContactId(contactId)?.let { civicrm.deleteEmail(it.id) }
                                    civicrm.deleteContact(contactId)
                                }
                            }
                                ?: logger.info { "CiviCRMService::userRemovedFromGroup Group <${credentialGroup.name}> does not exist in civicrm. No action required." }
                        }
                    }
                        ?: logger.error { "CiviCRMService::userRemovedFromGroup Group $groupId does not exist in credential provider" }
                }
                    ?: logger.info { "CiviCRMService::userRemovedFromGroup ${user.mail} does not exist in civicrm. No action required." }
            }
        }
    }

    public override fun userAddedToGroup(
        adminUser: User,
        group: Group,
        toAdd: Int,
    ) {
        credentialProvider.getUser(toAdd)?.let { user ->
            if (!isUserInCredentialCrmGroup(user)) {
                logger.info {
                    "CiviCRMService::added ignore user ${user.displayName} to group ${group.name} by ${adminUser.displayName}. Group is not valid CiviCRM group."
                }
                return
            }

            logger.info { "CiviCRMService::added user index $toAdd to group ${group.name} by ${adminUser.displayName}" }
            runBlocking {
                val id = civicrm.getContactIdByEmail(user.mail) ?: createGroupContactAndEmail(user)
                createGroupContactsForUser(user, id)
            }
        } ?: logger.warn { "CiviCRMService::added user with id $toAdd not found in credential system." }
    }

    private fun createGroupContactAndEmail(user: User): Int {
        logger.info { "CiviCRMService::createGroupContactAndEmail create group contact and mail for ${user.displayName}" }
        val isEmployee = user.groups.any { credentialProvider.getGroup(it)?.name == civiSettings.employeeGroup }
        val subtype =
            if (isEmployee) {
                civiSettings.employeeContactType
            } else {
                civiSettings.defaultContactType
            }
        return runBlocking {
            val c =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    subtype,
                    user.firstname,
                    user.surname,
                )
            civicrm.createEmail(user.mail, c.id)
            if (user.disabled) {
                civicrm.blockContact(c.id)
            }
            return@runBlocking c.id
        }
    }

    private fun mapToCiviContact(user: User): Contact? =
        runBlocking {
            if (isUserInCredentialCrmGroup(user)) {
                civicrm.getContactIdByEmail(user.mail)?.let { civicrm.getContact(it) }
            } else {
                null
            }
        }
}
