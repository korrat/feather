/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.internal.civicrm.CiviCRM
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.defaultRequest
import io.ktor.client.request.header
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import java.net.URL

public class CiviCRMFactory {
    public companion object {
        private val logger = KotlinLogging.logger {}

        public fun create(civiCRMSettings: CiviCRMSettings): ICiviCRM {
            logger.info { "CiviCRM::CiviCRMFactor::create ${civiCRMSettings.baseUrl}" }
            // Check baseUrl
            val validatedURL =
                civiCRMSettings.baseUrl.trim().removeSuffix("/").takeIf { it.isNotBlank() }
                    ?: throw IllegalArgumentException("Drupal::create Drupal baseUrl not configured")

            kotlin.runCatching { URL(validatedURL) }.onFailure {
                throw IllegalArgumentException("Drupal::create Drupal base url malformed", it)
            }

            // Check api user and password
            val validatedAuthUser = civiCRMSettings.apikey.trim()
            require(validatedAuthUser.isNotBlank()) { "Drupal::create Drupal api key must not be empty" }

            kotlin.runCatching { URL(validatedURL) }
                .onFailure {
                    throw IllegalArgumentException("Drupal::create Drupal base url malformed", it)
                }

            val civiCRM =
                CiviCRM(
                    client = createApiHttpClient(validatedAuthUser),
                    baseUrl = validatedURL,
                    apiKey = validatedAuthUser,
                    siteKey = "",
                )

            return kotlin.runCatching {
                runBlocking {
                    logger.info { "CiviCRM::CiviCRMFactor::create check precondition: group ${civiCRMSettings.adminGroup} exists" }
                    val asyncAdminGroup =
                        async {
                            civiCRM.getGroupByName(civiCRMSettings.adminGroup) // "Administrators"
                        }
                    logger.info {
                        "CiviCRM::CiviCRMFactor::create check precondition: apiuser exists with name ${civiCRMSettings.adminFirstName} ${civiCRMSettings.adminLastName}  exists"
                    }
                    val asyncApiUser =
                        async {
                            civiCRM.getContactByName(civiCRMSettings.adminFirstName, civiCRMSettings.adminLastName) // "API","User"
                        }

                    asyncAdminGroup.await()
                    asyncApiUser.await()

                    // val asyncContactGroup = async {
                    //    civiCRM.getGroupContactsByContactId(apiUser!!.id)
                    // }
                    // val apiGroupContact = asyncContactGroup.await()
                    // Check if the api user has admin permissions
                    // require(apiGroupContact.any { it.group_id == adminGroup!!.id }) { "To use the Drupal API functionality a valid user with admin permissions must be configured" }
                }

                logger.info { "Drupal binding - enabled" }

                civiCRM
            }.onFailure { }.getOrThrow()
        }

        internal fun createApiHttpClient(apikey: String): HttpClient =
            HttpClient {
                expectSuccess = true
                configureJson()
                defaultRequest {
                    header("OCS-APIRequest", "true")
                    header("Accept", "application/json")
                    header("X-Civi-Auth", "Bearer $apikey")
                }

                defaultRequest {
                    contentType(ContentType.Application.Json)
                }

                install(HttpTimeout) {
                    requestTimeoutMillis = 200000
                }
            }

        private fun HttpClientConfig<*>.configureJson() =
            install(ContentNegotiation) {
                json(
                    Json {
                        isLenient = true
                        ignoreUnknownKeys = true
                    },
                )
            }
    }
}
