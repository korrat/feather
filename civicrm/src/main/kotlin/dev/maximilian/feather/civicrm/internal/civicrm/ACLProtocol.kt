/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

public class ACLProtocol {
    public enum class Operations(public val protocolName: String) {
        ALL("All"),
        EDIT("Edit"),
        VIEW("View"),
        DELETE("View"),
        CREATE("Create"),
    }

    public enum class ACLEntityType(public val protocolName: String) { ACL_ROLE("civicrm_acl_role"), CRM_GROUP("civicrm_group") }

    public enum class TargetType(public val protocolName: String) {
        GROUP("GROUP"),
        SAVED_SEARCH("civicrm_saved_search"),
        PROFILE("civicrm_uf_group"),
        CIVICRM_GROUP("civicrm_group"),
        CUSTOM_GROUP("civicrm_custom_group"),
        EVENT("civicrm_event"),
    }
}
