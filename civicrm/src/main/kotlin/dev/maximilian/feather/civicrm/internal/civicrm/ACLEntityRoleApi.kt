/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IACLEntityRoleApi {
    public suspend fun getACLEntityRoles(): List<ACLEntityRole>

    public suspend fun getACLEntityRole(id: Int): ACLEntityRole?

    public suspend fun createACLEntityRole(aclEntityRole: ACLEntityRole): ACLEntityRole

    public suspend fun deleteACLEntityRole(id: Int)
}

internal class ACLEntityRoleApi(
    client: HttpClient,
    baseUrl: String,
) : IACLEntityRoleApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "ACLEntityRole", this)

    override suspend fun getACLEntityRoles(): List<ACLEntityRole> = generalApi.get().body<GeneralAPI.Answer<ACLEntityRole>>().values

    override suspend fun getACLEntityRole(id: Int): ACLEntityRole? =
        generalApi.get(id).body<GeneralAPI.Answer<ACLEntityRole>>().values.singleOrNull()

    override suspend fun createACLEntityRole(aclEntityRole: ACLEntityRole): ACLEntityRole {
        logger.info {
            "CiviCRM::ACLEntityRoleApi::createACLEntityRole entity role id ${aclEntityRole.id}. entityID ${aclEntityRole.entityId}, entityTable ${aclEntityRole.entityTable}"
        }
        val s =
            mapOf<String, JsonElement>(
                "acl_role_id" to JsonPrimitive(aclEntityRole.aclRoleId),
                "entity_id" to JsonPrimitive(aclEntityRole.entityId),
                "entity_table" to JsonPrimitive(aclEntityRole.entityTable),
                "is_active" to JsonPrimitive(aclEntityRole.active),
            )

        return generalApi.create(s).body<GeneralAPI.Answer<ACLEntityRole>>().values.single()
    }

    override suspend fun deleteACLEntityRole(id: Int) {
        logger.info { "CiviCRM::ACLEntityRoleApi::deleteACLEntityRole with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getACLEntityRole(id)
}
