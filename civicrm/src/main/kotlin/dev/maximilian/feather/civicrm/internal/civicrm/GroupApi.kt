/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IGroupApi {
    public suspend fun getGroups(): List<Group>

    public suspend fun getGroup(id: Int): Group?

    public suspend fun getGroupByName(name: String): Group?

    public suspend fun createGroup(group: Group): Group

    public suspend fun createGroup(
        name: String,
        title: String,
    ): Group

    public suspend fun updateParentOfGroup(
        id: Int,
        newParent: Int?,
    ): Group?

    public suspend fun deleteGroup(id: Int)
}

internal class GroupApi(
    private val apiClient: HttpClient,
    private val baseUrl: String,
) : IGroupApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(apiClient, baseUrl, "Group", this)

    override suspend fun getGroups(): List<Group> = generalApi.get().body<GeneralAPI.Answer<Group>>().values

    override suspend fun getGroup(id: Int): Group? = generalApi.get(id).body<GeneralAPI.Answer<Group>>().values.singleOrNull()

    override suspend fun getGroupByName(name: String): Group? =
        apiClient.get(
            generalApi.url(baseUrl, GeneralAPI.Action.GET) + "?params=${ParamStringBuilder.where(setOf(Triple("name", "=", name)), "")}",
        )
            .body<GeneralAPI.Answer<Group>>().values.singleOrNull()

    override suspend fun createGroup(group: Group): Group {
        logger.info { "CiviCRM::GroupApi::createGroup for ${group.name}" }
        val s = mutableMapOf<String, JsonElement>()
        s["name"] = JsonPrimitive(group.name)
        group.description?.let { s["description"] = JsonPrimitive(group.description) }
        group.title?.let { s["title"] = JsonPrimitive(group.title) }
        group.source?.let { s["source"] = JsonPrimitive(group.source) }
        group.groupType?.let { s["group_type"] = JsonPrimitive(group.groupType.first()) }
        return generalApi.create(s).body<GeneralAPI.Answer<Group>>().values.single()
    }

    override suspend fun deleteGroup(id: Int) {
        logger.info { "CiviCRM::GroupApi::deleteGroup with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getGroup(id)

    override suspend fun createGroup(
        name: String,
        title: String,
    ): Group {
        val g =
            Group(
                0,
                name,
                title,
                title,
                null,
            )
        return createGroup(g)
    }

    override suspend fun updateParentOfGroup(
        id: Int,
        newParent: Int?,
    ): Group? {
        val m = mapOf("parents" to JsonArray(listOf(JsonPrimitive(newParent))))
        generalApi.update(id, m)
        return getGroup(id)
    }
}
