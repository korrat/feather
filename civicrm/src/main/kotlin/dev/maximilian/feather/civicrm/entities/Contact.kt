/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class Contact(
    val id: Int,
    @SerialName("contact_type")
    val contactType: String,
    // @SerialName("contact_sub_type")
    // val contactSubType: Set<String>?,
    @SerialName("is_deleted")
    val deleted: Boolean? = null,
    @SerialName("created_date")
    val createdDate: String? = null,
    @SerialName("modified_date")
    val modifiedDate: String? = null,
    @SerialName("first_name")
    val firstName: String? = null,
    @SerialName("last_name")
    val lastName: String? = null,
)
