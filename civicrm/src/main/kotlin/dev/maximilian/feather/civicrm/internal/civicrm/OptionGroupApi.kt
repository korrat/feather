/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.entities.OptionGroup
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IOptionGroupApi {
    public suspend fun getOptionGroups(): List<OptionGroup>

    public suspend fun getOptionGroup(id: Int): OptionGroup?

    public suspend fun getOptionGroup(name: String): OptionGroup?

    public suspend fun createOptionGroup(optionGroup: OptionGroup): OptionGroup

    public suspend fun deleteOptionGroup(id: Int)

    public fun findIndexOfACLRole(): Int
}

internal class OptionGroupApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IOptionGroupApi, IGetElement {
    private companion object : KLogging() {
        var aclRoleIndexStored: Int? = null
    }

    private val generalApi = GeneralAPI(client, baseUrl, "OptionGroup", this)

    override suspend fun getOptionGroups(): List<OptionGroup> = generalApi.get().body<GeneralAPI.Answer<OptionGroup>>().values

    override suspend fun getOptionGroup(id: Int): OptionGroup? =
        generalApi.get(id).body<GeneralAPI.Answer<OptionGroup>>().values.singleOrNull()

    override suspend fun getOptionGroup(name: String): OptionGroup? =
        client.get(
            generalApi.url(
                baseUrl,
                GeneralAPI.Action.GET,
            ) + "?params=${ParamStringBuilder.where(setOf(Triple("name", "=", name)), "")}",
        )
            .body<GeneralAPI.Answer<OptionGroup>>().values.singleOrNull()

    override suspend fun createOptionGroup(optionGroup: OptionGroup): OptionGroup {
        logger.info { "CiviCRM::OptionGroupApi::createOptionGroup with name ${optionGroup.name}" }
        val s = mutableMapOf<String, JsonPrimitive>()
        s["name"] = JsonPrimitive(optionGroup.name)
        optionGroup.title?.let { s["title"] = JsonPrimitive(optionGroup.title) }
        optionGroup.description?.let { s["description"] = JsonPrimitive(optionGroup.description) }
        optionGroup.isActive?.let { s["is_active"] = JsonPrimitive(optionGroup.isActive) }
        s["is_locked"] = JsonPrimitive(optionGroup.isLocked)
        // optionGroup.dataType?.let { s.add(Pair("data_type", optionGroup.dataType)) }
        // optionGroup.isReserved?.let { s.add(Pair("is_reserved", optionGroup.isReserved)) }
        // optionGroup.optionValueFields?.let { s.add(Pair("option_value_fields", optionGroup.optionValueFields)) }

        return generalApi.create(s).body<GeneralAPI.Answer<OptionGroup>>().values.single()
    }

    override suspend fun deleteOptionGroup(id: Int) {
        logger.info { "CiviCRM::OptionGroupApi::deleteOptionGroup with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getOptionGroup(id)

    override fun findIndexOfACLRole(): Int {
        aclRoleIndexStored ?: runBlocking {
            aclRoleIndexStored = getOptionGroup(CiviCRMConstants.ACL_ROLE)?.id
                ?: throw Exception("ACL Role not existant in CiviCRM. This is a system role.")
        }
        return aclRoleIndexStored!!
    }
}
