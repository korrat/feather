/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.GroupContact
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IGroupContactApi {
    public suspend fun getGroupContacts(): List<GroupContact>

    public suspend fun getGroupContact(id: Int): GroupContact?

    public suspend fun getGroupContactsByContactId(groupContactId: Int): List<GroupContact>

    public suspend fun getGroupContactsByGroupId(groupId: Int): List<GroupContact>

    public suspend fun createGroupContact(
        contactId: Int,
        groupId: Int,
    ): GroupContact

    public suspend fun deleteGroupContact(id: Int)
}

internal class GroupContactApi(
    client: HttpClient,
    baseUrl: String,
) : IGroupContactApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "GroupContact", this)

    override suspend fun getGroupContacts(): List<GroupContact> = generalApi.get().body<GeneralAPI.Answer<GroupContact>>().values

    override suspend fun getGroupContact(id: Int): GroupContact? =
        generalApi.get(
            id,
        ).body<GeneralAPI.Answer<GroupContact>>().values.singleOrNull()

    override suspend fun getGroupContactsByContactId(contactId: Int): List<GroupContact> =
        generalApi.getInt(setOf(Triple("contact_id", "=", contactId))).body<GeneralAPI.Answer<GroupContact>>().values

    override suspend fun getGroupContactsByGroupId(groupId: Int): List<GroupContact> =
        generalApi.getInt(setOf(Triple("group_id", "=", groupId))).body<GeneralAPI.Answer<GroupContact>>().values

    override suspend fun createGroupContact(
        contactId: Int,
        groupId: Int,
    ): GroupContact {
        logger.info { "CiviCRM::GroupContactApi::createGroupContact with id $contactId and groupID $groupId" }
        return generalApi.create(
            mapOf(
                "group_id" to JsonPrimitive(groupId.toString()),
                "contact_id" to JsonPrimitive(contactId.toString()),
            ),
        ).body<GeneralAPI.Answer<GroupContact>>().values.single()
    }

    override suspend fun deleteGroupContact(id: Int) {
        logger.info { "CiviCRM::GroupContactApi::deleteGroupContact with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getGroupContact(id)
}
