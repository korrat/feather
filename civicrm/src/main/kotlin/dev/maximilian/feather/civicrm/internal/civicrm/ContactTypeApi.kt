/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.CiviCRMConstants
import dev.maximilian.feather.civicrm.entities.ContactType
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IContactTypeApi {
    public suspend fun getContactTypes(): List<ContactType>

    public suspend fun getContactType(id: Int): ContactType?

    public suspend fun createContactType(
        name: String,
        label: String,
        parent: CiviCRMConstants.ContactTypeDefaults,
    ): ContactType

    public suspend fun deleteContactType(id: Int)
}

internal class ContactTypeApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IContactTypeApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "ContactType", this)

    override suspend fun getContactTypes(): List<ContactType> = generalApi.get().body<GeneralAPI.Answer<ContactType>>().values

    override suspend fun getContactType(id: Int): ContactType? =
        generalApi.get(
            id,
        ).body<GeneralAPI.Answer<ContactType>>().values.singleOrNull()

    override suspend fun createContactType(
        name: String,
        label: String,
        parent: CiviCRMConstants.ContactTypeDefaults,
    ): ContactType {
        logger.info { "CiviCRM::ContactTypeApi::createContactType with name $name" }
        val s =
            mapOf<String, JsonElement>(
                "name" to JsonPrimitive(name),
                "label" to JsonPrimitive(label),
                "parent_id.name" to JsonPrimitive(parent.protocolName),
                "icon" to JsonPrimitive(parent.icon),
            )
        return generalApi.create(s).body<GeneralAPI.Answer<ContactType>>().values.single()
    }

    override suspend fun deleteContactType(id: Int) {
        logger.info { "CiviCRM::ContactTypeApi::deleteContactType with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getContactType(id)
}
