package dev.maximilian.feather.civicrm.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class OptionGroup(
    val id: Int,
    val name: String,
    val title: String?,
    val description: String?,
    // @SerialName("data_type")
    // val dataType: String?,
    // @SerialName("is_reserved")
    // val isReserved: Boolean?,
    @SerialName("is_active")
    val isActive: Boolean?,
    @SerialName("is_locked")
    val isLocked: Boolean,
    // @SerialName("option_value_fields")
    // val optionValueFields: Set<String>?,
)
