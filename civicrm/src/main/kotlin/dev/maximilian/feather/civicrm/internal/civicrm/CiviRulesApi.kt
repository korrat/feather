/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.CiviRuleAction
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging
import java.time.Duration

public interface ICiviRulesApi {
    public suspend fun getCiviRules(): List<CiviRuleAction>

    public suspend fun getCiviRule(id: Int): CiviRuleAction?

    public suspend fun createCiviRule(
        contactId: Int,
        contactType: String,
        contactSubType: String,
    ): CiviRuleAction

    public suspend fun deleteCiviRule(id: Int)
}
public class CiviRulesApi(
    private val client: HttpClient,
    private val baseUrl: String,
    private val apiKey: String,
    private val siteKey: String,
) : ICiviRulesApi, IGetElement {
    private companion object : KLogging()

    // private val generalAPI = GeneralAPI(client, baseUrl, "CiviRuleAction", this)
    internal fun url(
        baseUrl: String,
        action: GeneralAPI.Action,
    ) = "$baseUrl/civicrm/core/extern/rest.php?entity=CiviRuleRule&action=$action&json=%7B%22sequential%22%3A1%7D&api_key=$apiKey"
    public override suspend fun getCiviRules(): List<CiviRuleAction> = client.get(url(baseUrl, GeneralAPI.Action.GET) + "&limit=10000000").body<GeneralAPI.Answer<CiviRuleAction>>().values

    public override suspend fun getCiviRule(id: Int): CiviRuleAction? = get(setOf(Triple("id", "=", id))).body<GeneralAPI.Answer<CiviRuleAction>>().values.singleOrNull()

    private suspend fun get(s: Set<Triple<String, String, Any>>): io.ktor.client.statement.HttpResponse =
        client.get(
            url(baseUrl, GeneralAPI.Action.GET) + "&params=${ParamStringBuilder.where(s, "")}",
        )
    public override suspend fun createCiviRule(
        contactId: Int,
        contactType: String,
        contactSubType: String,
    ): CiviRuleAction {
        val s = mutableMapOf<String, JsonElement>()
        s["contact_id"] = JsonPrimitive(contactId)
        s["contact_type"] = JsonPrimitive(contactType)
        s["contact_sub_type"] = JsonPrimitive(contactSubType)
        logger.info { "body is params=${ParamStringBuilder.value(s)}" }

        return createInternal(s).body<GeneralAPI.Answer<CiviRuleAction>>().values.single()
    }

    private suspend fun createInternal(s: Map<String, JsonElement>): io.ktor.client.statement.HttpResponse {
        try {
            return client.post(url(baseUrl, GeneralAPI.Action.CREATE) + "&params=${ParamStringBuilder.value(s)}")
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.UnprocessableEntity) {
                throw IllegalArgumentException(e.response.body<GeneralAPI.DrupalErrorAnswer>().message)
            } else {
                throw e
            }
        }
    }
    public override suspend fun deleteCiviRule(id: Int) {
        logger.info { "CiviCRM::CiviRulesApi::deleteCiviRule with id $id" }
        deleteWithLoop(id)
    }

    private suspend fun deleteWithLoop(
        id: Int,
        suffix: String = "",
    ) {
        client.post(
            url(baseUrl, GeneralAPI.Action.DELETE) +
                "&params=${ParamStringBuilder.where(setOf(Triple("id", "=", id)), suffix)}",
        )

        val waitingFunction: suspend () -> Unit = {
            require(getElement(id) == null) { "Deletion exceeds time limit" }
        }
        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    internal suspend fun delete(
        id: Int,
        suffix: String = "",
    ) {
        client.post(
            url(baseUrl, GeneralAPI.Action.DELETE) +
                "&params=${ParamStringBuilder.where(setOf(Triple("id", "=", id)), suffix)}",
        )
    }
    override suspend fun getElement(id: Int): Any? = getCiviRule(id)
}
