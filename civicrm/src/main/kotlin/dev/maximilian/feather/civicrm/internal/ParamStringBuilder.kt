package dev.maximilian.feather.civicrm.internal

import kotlinx.serialization.json.JsonElement
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

internal class ParamStringBuilder {
    companion object {
        // internal fun where(filters: Set<Triple<String, String, Any>>, suffix: String): String =
        //    URLEncoder.encode(ParamJsonBuilder.whereString(filters, suffix), "utf-8")

        internal fun valueWhere(
            values: Map<String, JsonElement>,
            filters: Set<Triple<String, String, Int>>,
        ): String = URLEncoder.encode(ParamJsonBuilder.valueWhere(values, filters), "utf-8")

        internal fun where(
            filters: Set<Triple<String, String, Any>>,
            suffix: String,
        ): String = URLEncoder.encode(ParamJsonBuilder.where(filters, suffix), StandardCharsets.UTF_8)

        internal fun value(values: Map<String, JsonElement>): String =
            URLEncoder.encode(ParamJsonBuilder.value(values), StandardCharsets.UTF_8)
    }
}
