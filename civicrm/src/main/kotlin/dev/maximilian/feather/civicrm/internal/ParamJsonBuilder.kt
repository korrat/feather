package dev.maximilian.feather.civicrm.internal

import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

internal class ParamJsonBuilder {
    companion object {
        internal fun where(
            filters: Set<Triple<String, String, Any>>,
            suffix: String,
        ): String {
            val q = if (suffix == "") "" else ",$suffix"
            return "{\"where\":[" + whereInnerPart(filters) + "]$q}"
        }

        internal fun valueWhere(
            values: Map<String, JsonElement>,
            filters: Set<Triple<String, String, Any>>,
        ): String = "{\"values\":" + valueInnerPart(values) + ",\"where\":[" + whereInnerPart(filters) + "]}"

        internal fun value(filters: Map<String, JsonElement>): String = "{\"values\":" + valueInnerPart(filters) + "}"

        private fun whereInnerPart(filters: Set<Triple<String, String, Any>>): String =
            filters.joinToString(separator = ",") {
                "[\"${it.first}\",\"${it.second}\"," +
                    if (it.third is String) {
                        "\"${it.third}\""
                    } else {
                        it.third.toString()
                    } + "]"
            }

        private fun valueInnerPart(filters: Map<String, JsonElement>): String {
            return JsonObject(filters).toString()
        }
    }
}
