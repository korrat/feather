/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.entities

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
public data class ACL(
    val id: Int,
    val name: String,
    val deny: Boolean,
    @SerialName("entity_table")
    val entityTable: String,
    @SerialName("entity_id")
    val entityId: Int?,
    val operation: String,
    @SerialName("object_table")
    val objectTable: String?,
    @SerialName("object_id")
    val objectId: Int? = null,
    // @SerialName("acl_table")
    // val aclTable: String?,
    @SerialName("acl_id")
    val aclId: Int? = null,
    @SerialName("is_active")
    val active: Boolean,
)
