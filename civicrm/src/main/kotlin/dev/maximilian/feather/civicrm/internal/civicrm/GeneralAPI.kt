package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import dev.maximilian.feather.civicrm.internal.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ClientRequestException
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import java.time.Duration

public interface IGetElement {
    public suspend fun getElement(id: Int): Any?
}

internal class GeneralAPI(
    private val client: HttpClient,
    private val baseUrl: String,
    private val subFunction: String,
    private val parentApi: IGetElement?,
) {
    internal enum class Action(val protocolName: String) { GET("get"), CREATE("create"), DELETE("delete"), UPDATE("update") }

    internal fun url(
        baseUrl: String,
        action: Action,
    ) = "$baseUrl/civicrm/ajax/api4/$subFunction/" + action.protocolName

    internal suspend fun get(): io.ktor.client.statement.HttpResponse = client.get(url(baseUrl, Action.GET) + "?limit=10000000")

    internal suspend fun get(id: Int): io.ktor.client.statement.HttpResponse = get(setOf(Triple("id", "=", id)))

    suspend fun get(s: Set<Triple<String, String, Any>>): io.ktor.client.statement.HttpResponse =
        client.get(
            url(baseUrl, Action.GET) + "?params=${ParamStringBuilder.where(s, "")}",
        )

    internal suspend fun getString(s: Set<Triple<String, String, String>>): io.ktor.client.statement.HttpResponse =
        client.get(
            url(baseUrl, Action.GET) +
                "?params=${ParamStringBuilder.where(s, "")}",
        )

    internal suspend fun create(s: Map<String, JsonElement>): io.ktor.client.statement.HttpResponse {
        try {
            return client.post(url(baseUrl, Action.CREATE) + "?params=${ParamStringBuilder.value(s)}")
        } catch (e: ClientRequestException) {
            if (e.response.status == HttpStatusCode.UnprocessableEntity) {
                throw IllegalArgumentException(e.response.body<DrupalErrorAnswer>().message)
            } else {
                throw e
            }
        }
    }

    internal suspend fun delete(
        id: Int,
        suffix: String = "",
    ) {
        client.post(
            url(baseUrl, Action.DELETE) +
                "?params=${ParamStringBuilder.where(setOf(Triple("id", "=", id)), suffix)}",
        )
    }

    internal suspend fun deleteWithLoop(
        id: Int,
        suffix: String = "",
    ) {
        client.post(
            url(baseUrl, Action.DELETE) +
                "?params=${ParamStringBuilder.where(setOf(Triple("id", "=", id)), suffix)}",
        )

        val waitingFunction: suspend () -> Unit = {
            require(parentApi?.getElement(id) == null) { "Deletion exceeds time limit" }
        }
        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    internal suspend fun update(
        id: Int,
        s: Map<String, JsonElement>,
    ): io.ktor.client.statement.HttpResponse {
        val cmd =
            url(baseUrl, Action.UPDATE) +
                "?params=${ParamStringBuilder.valueWhere(s, setOf(Triple("id", "=", id)))}"
        return client.post(cmd)
    }

    internal suspend fun getInt(s: Set<Triple<String, String, Int>>): io.ktor.client.statement.HttpResponse =
        client.post(url(baseUrl, Action.GET) + "?params=${ParamStringBuilder.where(s, "")}")

    internal data class DrupalErrorAnswer(
        val message: String,
    )

    @Serializable
    internal data class Answer<ElementType>(
        val values: List<ElementType>,
    )
}
