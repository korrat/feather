/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.civicrm.internal.civicrm

import dev.maximilian.feather.civicrm.entities.OptionValue
import dev.maximilian.feather.civicrm.internal.ParamStringBuilder
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonPrimitive
import mu.KLogging

public interface IOptionValueApi {
    public suspend fun getOptionValues(): List<OptionValue>

    public suspend fun getOptionValue(id: Int): OptionValue?

    public suspend fun getOptionValueByOptionGroupId(id: Int): List<OptionValue>

    public suspend fun createOptionValue(optionValue: OptionValue): OptionValue

    public suspend fun deleteOptionValue(id: Int)
}

internal class OptionValueApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IOptionValueApi, IGetElement {
    private companion object : KLogging()

    private val generalApi = GeneralAPI(client, baseUrl, "OptionValue", this)

    override suspend fun getOptionValues(): List<OptionValue> = generalApi.get().body<GeneralAPI.Answer<OptionValue>>().values

    override suspend fun getOptionValue(id: Int): OptionValue? =
        generalApi.get(
            id,
        ).body<GeneralAPI.Answer<OptionValue>>().values.singleOrNull()

    override suspend fun getOptionValueByOptionGroupId(id: Int): List<OptionValue> =
        client.get(
            generalApi.url(baseUrl, GeneralAPI.Action.GET) + "?params=${ParamStringBuilder.where(setOf(Triple("option_group_id", "=", id)), "")}",
        )
            .body<GeneralAPI.Answer<OptionValue>>().values

    override suspend fun createOptionValue(optionValue: OptionValue): OptionValue {
        logger.info { "CiviCRM::OptionGroupApi::createOptionValue with name ${optionValue.name}" }
        val s = mutableMapOf<String, JsonElement>()
        s["option_group_id"] = JsonPrimitive(optionValue.optionGroupId)
        optionValue.label?.let { s["label"] = JsonPrimitive(optionValue.label) }
        optionValue.name?.let { s["name"] = JsonPrimitive(optionValue.name) }
        optionValue.value?.let { s["value"] = JsonPrimitive(optionValue.value) }
        optionValue.weight?.let { s["weight"] = JsonPrimitive(optionValue.weight) }
        optionValue.description?.let { s["description"] = JsonPrimitive(optionValue.description) }
        optionValue.isActive?.let { s["is_active"] = JsonPrimitive(optionValue.isActive) }
        // optionValue.isReserved?.let { s.add(Pair("is_reserved", optionValue.isReserved)) }
        // optionValue.grouping?.let { s.add(Pair("grouping", optionValue.grouping)) }
        // optionValue.isOptGroup?.let { s.add(Pair("is_optgroup", optionValue.isOptGroup)) }
        // optionValue.isDefault?.let { s.add(Pair("is_default", optionValue.isDefault)) }
        // optionValue.isLocked?.let { s.add(Pair("is_locked", optionValue.isLocked)) }
        // optionValue.componentId?.let { s.add(Pair("component_id", optionValue.componentId)) }
        // optionValue.domainId?.let { s.add(Pair("domain_id", optionValue.domainId)) }
        // optionValue.visibilityId?.let { s.add(Pair("visibility_id", optionValue.visibilityId)) }
        // optionValue.icon?.let { s.add(Pair("icon", optionValue.icon)) }
        // optionValue.color?.let { s.add(Pair("color", optionValue.color)) }

        return generalApi.create(s).body<GeneralAPI.Answer<OptionValue>>().values.single()
    }

    override suspend fun deleteOptionValue(id: Int) {
        logger.info { "CiviCRM::OptionGroupApi::deleteOptionValue with id $id" }
        generalApi.deleteWithLoop(id)
    }

    override suspend fun getElement(id: Int): Any? = getOptionValue(id)
}
