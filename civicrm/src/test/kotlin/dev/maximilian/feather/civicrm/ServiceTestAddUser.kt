/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.ContactSnapshot
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals

class ServiceTestAddUser {
    @Test
    fun `CiviCRMService adds active user to crm group`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        ServiceTest.createContactTypes(a.civicrm)
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "add")
        runBlocking {
            ServiceTest.createContactTypes(a.civicrm)
            val civiGroup = a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")

            val old = ContactSnapshot(a.civicrm)
            a.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            val new = ContactSnapshot(a.civicrm)

            credentialScenario.deleteScenario()
            a.civicrm.deleteGroup(civiGroup.id)

            assertEquals(old.contacts.count() + 1, new.contacts.count(), "Contact was not added")
            assertEquals(old.groupContacts.count() + 1, new.groupContacts.count(), "GroupContact was not added")
            assertEquals(old.emails.count() + 1, new.emails.count(), "Email was not added")
            assertEquals(old.blockedContacts.count(), new.blockedContacts.count(), "Blocked contacts was added")

            assertEquals("Individual", new.contacts.last().contactType, "Contact.ContactTyp is not defined properly")
            assertEquals("test$randomNumber", new.contacts.last().lastName, "Contact.LastName is not defined properly")
            assertEquals("civi$randomNumber", new.contacts.last().firstName, "Contact.FirstName is not defined properly")

            assertEquals(
                new.contacts.last().id,
                new.groupContacts.last().contactId,
                "GroupContact.ContactID is not defined properly",
            )
            assertEquals(civiGroup.id, new.groupContacts.last().groupId, "GroupContact.GroupID is not defined properly")

            assertEquals(new.contacts.last().id, new.emails.last().contactId, "Email.ContactID is not defined properly")
            assertEquals("civi$randomNumber@example.org", new.emails.last().email, "Email.Email is not defined properly")
        }
    }

    @Test
    fun `CiviCRMService adds blocked crm user`() {
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        ServiceTest.createContactTypes(a.civicrm)
        val randomNumber = Random.nextInt()
        val testScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "user-blocked")

        runBlocking {
            val civiGroup = a.civicrm.createGroup(testScenario.group.name, "CRM users of RG Test")
            ServiceConfig.CREDENTIAL_PROVIDER.updateUser(testScenario.testUser.copy(disabled = true))
            testScenario.updateUser()
            val old = ContactSnapshot(a.civicrm)
            a.userAddedToGroup(testScenario.admin, testScenario.group, testScenario.testUser.id)

            val new = ContactSnapshot(a.civicrm)
            testScenario.deleteScenario()
            a.civicrm.deleteGroup(civiGroup.id)

            assertEquals(
                old.contacts.sortedBy {
                    it.id
                }.map {
                    Triple(it.id, it.lastName, it.deleted)
                },
                new.contacts.sortedBy { it.id }.map { Triple(it.id, it.lastName, it.deleted) },
                "Contacts was changed",
            )
            assertEquals(old.groupContacts.count() + 1, new.groupContacts.count(), "GroupContacts was not changed")
            assertEquals(old.emails.count() + 1, new.emails.count(), "Emails was changed")
            assertEquals(old.blockedContacts.count() + 1, new.blockedContacts.count(), "Blocked user was not added")
        }
    }

    @Test
    fun `CiviCRMService ignores user add to interested group`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        ServiceTest.createContactTypes(a.civicrm)
        val testScenario = CredentialScenario().createInterestedGroupWithUser(randomNumber, "user-ignoreB")

        runBlocking {
            // val civiGroup = a.civicrm.createGroup("rg-test-interested", "interested von RG Test")

            val old = ContactSnapshot(a.civicrm)

            a.userAddedToGroup(testScenario.admin, testScenario.group, testScenario.testUser.id)

            val new = ContactSnapshot(a.civicrm)

            testScenario.deleteScenario()

            assertEquals(old.contacts, new.contacts, "Contacts was changed")
            assertEquals(old.groupContacts, new.groupContacts, "GroupContacts was changed")
            assertEquals(old.emails, new.emails, "Emails was changed")
            assertEquals(old.blockedContacts, new.blockedContacts, "Blocked contacts was changed")
        }
    }
}
