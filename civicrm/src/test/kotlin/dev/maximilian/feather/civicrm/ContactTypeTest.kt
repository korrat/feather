/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.CiviCRMConstants.ContactTypeDefaults
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ContactTypeTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create contact type of Organization`() {
        runBlocking {
            val name = "contactTypeTest${Random.nextInt(1000)}"
            val response = civicrm.createContactType(name, "superlabel", ContactTypeDefaults.Organization)
            assertEquals(name, response.name)
            assertEquals("superlabel", response.label)
            civicrm.deleteContactType(response.id)
        }
    }

    @Test
    fun `Create contact type of Individual`() {
        runBlocking {
            val name = "contactTypeTestI${Random.nextInt(1000)}"
            val response = civicrm.createContactType(name, "Perso", ContactTypeDefaults.Individual)
            assertEquals(name, response.name)
            assertEquals("Perso", response.label)
            civicrm.deleteContactType(response.id)
        }
    }

    @Test
    fun `Get contact types`() {
        runBlocking {
            val t = civicrm.getContactTypes()
            val number = t.count()
            assertTrue(number >= 3, "Number of contacts could not be read.")
        }
    }

    @Test
    fun `Get contact type by contact id`() {
        runBlocking {
            val t = civicrm.getContactType(2)
            assertEquals(2, t!!.id)
            assertEquals("Household", t.name)
            assertEquals("Haushalt", t.label)
        }
    }

    @Test
    fun `Delete contact type`() {
        runBlocking {
            val name = "contactTypeTestB${Random.nextInt(1000)}"
            val response = civicrm.createContactType(name, "superlabel2", ContactTypeDefaults.Organization)

            civicrm.deleteContactType(response.id)
            assertEquals(0, civicrm.getContactTypes().count { it.name == name })
        }
    }
}
