/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GroupContactTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create groupcontact`() {
        runBlocking {
            val responseContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Harry",
                    "Schmidt",
                )
            val adminGroup = civicrm.getGroupByName("Administrators")
            val t = civicrm.createGroupContact(responseContact.id, adminGroup!!.id)
            assertEquals(responseContact.id, t.contactId)
            assertEquals(adminGroup.id, t.groupId)
        }
    }

    @Test
    fun `Delete groupcontact`() {
        runBlocking {
            val originalList = civicrm.getGroupContacts()
            val responseContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Harry",
                    "Schmidt",
                )
            val adminGroup = civicrm.getGroupByName("Administrators")
            val t = civicrm.createGroupContact(responseContact.id, adminGroup!!.id)
            civicrm.deleteGroupContact(t.id)
            val newList = civicrm.getGroupContacts()
            assertEquals(originalList, newList, "Original group contact list differs from list after create and delete")
        }
    }

    @Test
    fun `Read all groupcontact return at least one result`() {
        runBlocking {
            val responseContact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "First",
                    "Account",
                )
            val adminGroup = civicrm.getGroupByName("Administrators")
            civicrm.createGroupContact(responseContact.id, adminGroup!!.id)

            val t = civicrm.getGroupContacts()
            assertTrue(t.isNotEmpty())
        }
    }
}
