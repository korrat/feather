/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm.helper

import dev.maximilian.feather.civicrm.entities.Contact
import dev.maximilian.feather.civicrm.entities.Email
import dev.maximilian.feather.civicrm.entities.GroupContact
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import kotlinx.coroutines.runBlocking

class ContactSnapshot(private val a: ICiviCRM) {
    var contacts = emptyList<Contact>()
    var emails = emptyList<Email>()
    var groupContacts = emptyList<GroupContact>()
    var blockedContacts = emptyList<Contact>()

    init {
        runBlocking {
            groupContacts = a.getGroupContacts()
            contacts = a.getContacts()
            blockedContacts = a.getBlockedContacts()
            emails = a.getEmails()
        }
    }
}
