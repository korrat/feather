/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.OptionGroup
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OptionGroupTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create OptionGroup`() {
        runBlocking {
            val r = Random.nextInt()
            val optionGroup =
                OptionGroup(
                    1,
                    "lalalaA$r",
                    "SuperTest$r",
                    "testdescriptionA$r",
                    true,
                    false,
                )
            val response = civicrm.createOptionGroup(optionGroup)
            assertEquals(optionGroup.name, response.name)
            assertEquals(optionGroup.description, response.description)
            assertEquals(optionGroup.isLocked, response.isLocked)
            assertEquals(optionGroup.isActive, response.isActive)
            // assertNull(response.optionValueFields)
            // assertNull(response.dataType)
        }
    }

    @Test
    fun `Get OptionGroup by id`() {
        runBlocking {
            val r = Random.nextInt()
            val optionGroup =
                OptionGroup(
                    1,
                    "lalala$r",
                    "SuperTest$r",
                    "testdescription$r",
                    true,
                    false,
                )
            val response = civicrm.createOptionGroup(optionGroup)
            val response2 = civicrm.getOptionGroup(response.id)
            assertEquals(optionGroup.name, response2!!.name)
            civicrm.deleteOptionGroup(response.id)
        }
    }

    @Test
    fun `Get OptionGroup finds groupt with acl_role`() {
        runBlocking {
            val response2 = civicrm.getOptionGroups()
            val q = response2.single { it.name == CiviCRMConstants.ACL_ROLE }
            assertEquals("ACL-Rolle", q.title)
        }
    }

    @Test
    fun `findIndexOfACLRole finds twice`() {
        runBlocking {
            val aclRoleIndex = civicrm.findIndexOfACLRole()
            val aclRoleIndex2 = civicrm.findIndexOfACLRole()
            assertEquals(8, aclRoleIndex)
            assertEquals(8, aclRoleIndex2)
        }
    }

    @Test
    fun `Get OptionGroup finds groupt with acl_role by name`() {
        runBlocking {
            val response2 = civicrm.getOptionGroups()
            val groupByFilter = response2.single { it.name == CiviCRMConstants.ACL_ROLE }
            val groupByName = civicrm.getOptionGroup(CiviCRMConstants.ACL_ROLE)

            assertEquals(groupByFilter, groupByName, "Optiongroup for aclRole not found properly")
        }
    }

    @Test
    fun `Delete OptionGroup`() {
        runBlocking {
            val r = Random.nextInt()
            val originalList = civicrm.getOptionGroups()
            val optionGroup =
                OptionGroup(
                    1,
                    "lalalaB$r",
                    "SuperTest$r",
                    "testdescription$r",
                    true,
                    false,
                )
            val response = civicrm.createOptionGroup(optionGroup)
            civicrm.deleteOptionGroup(response.id)
            val newList = civicrm.getOptionGroups()
            assertEquals(originalList, newList, "Original Groupt list differs from list after create and delete")
        }
    }

    @Test
    fun `Read all OptionGroup return at least one result`() {
        runBlocking {
            val t = civicrm.getOptionGroups()
            assertTrue(t.isNotEmpty())
        }
    }
}
