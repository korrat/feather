/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.ACLEntityRole
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ACLEntitityRoleTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `create ACLEntityRole`() {
        runBlocking {
            val acl = ACLEntityRole(0, 1, "civicrm_group", 1, true)
            val n = civicrm.createACLEntityRole(acl)
            assertEquals("civicrm_group", n.entityTable)
            assertEquals(1, n.entityId)
            assertEquals(1, n.aclRoleId)
            assertEquals(true, n.active)
        }
    }

    @Test
    fun `Delete ACLEntityRole`() {
        runBlocking {
            val acl = ACLEntityRole(0, 1, "civicrm_group", 1, true)
            val n = civicrm.createACLEntityRole(acl)
            civicrm.deleteACLEntityRole(n.id)
        }
    }

    @Test
    fun `Get acl role by id`() {
        runBlocking {
            val n = civicrm.getACLEntityRole(1)
            assertNotNull(n)
            assertEquals(1, n.aclRoleId)
            assertEquals(true, n.active)
            assertEquals(1, n.entityId)
            assertEquals(1, n.id)
        }
    }

    @Test
    fun `get all acl roles returns at least one`() {
        runBlocking {
            val t = civicrm.getACLEntityRoles()
            assertTrue(t.isNotEmpty())
        }
    }
}
