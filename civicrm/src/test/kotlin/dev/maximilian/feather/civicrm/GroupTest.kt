/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.ContactSnapshot
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.Random
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class GroupTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create group`() {
        runBlocking {
            val testname = "testgroup${Random().nextInt(1000)}"
            val testtitle = "testtitle${Random().nextInt(1000)}"
            val group = Group(0, testname, testtitle, "testdescription")
            val response = civicrm.createGroup(group)
            assertEquals(testname, response.name)
            assertEquals(testtitle, response.title)
            assertEquals("testdescription", response.description)
        }
    }

    @Test
    fun `Delete group`() {
        runBlocking {
            val originalList = civicrm.getGroups()
            val testname = "testgroup${Random().nextInt(1000)}"
            val testtitle = "testtitle${Random().nextInt(1000)}"
            val group = Group(0, testname, testtitle, "testdescription")
            val genGroup = civicrm.createGroup(group)
            civicrm.deleteGroup(genGroup.id)
            val newList = civicrm.getGroups()
            assertEquals(originalList.count(), newList.count(), "Group count differs")
            assertEquals(originalList, newList, "Original Group list differs from list after create and delete")
        }
    }

    @Test
    fun `Create group with permission`() {
        runBlocking {
            val testname = "testgroup${Random().nextInt(1000)}"
            val testtitle = "testtitle${Random().nextInt(1000)}"
            val group = Group(0, testname, testtitle, "testdescription", source = null, groupType = setOf(1))
            val response = civicrm.createGroup(group)
            assertEquals(testname, response.name)
            assertEquals(testtitle, response.title)
            assertEquals("testdescription", response.description)
        }
    }

    @Test
    fun `Delete group with permission`() {
        runBlocking {
            val originalList = civicrm.getGroups()
            val testname = "testgroup${Random().nextInt(1000)}"
            val testtitle = "testtitle${Random().nextInt(1000)}"
            val group = Group(0, testname, testtitle, "testdescription", source = null, groupType = setOf(1))
            val genGroup = civicrm.createGroup((group))
            civicrm.deleteGroup(genGroup.id)
            val newList = civicrm.getGroups()
            assertEquals(originalList.count(), newList.count(), "Group count differs")
            assertEquals(originalList, newList, "Original Group list differs from list after create and delete")
        }
    }

    @Test
    fun `Read all groups return at least one result`() {
        runBlocking {
            val t = civicrm.getGroups()
            assertTrue(t.isNotEmpty())
        }
    }

    @Test
    fun `Create group with acl and role`() {
        runBlocking {
            val testname = "testgroup${Random().nextInt(1000)}"
            val testtitle = "testtitle${Random().nextInt(1000)}"
            val response = civicrm.createOrReplaceTopLevelGroupWithACLPermission(testname, testtitle, "Rolle für $testname")
            assertTrue { response.roleIndex >= 3 }
            assertEquals(testname, response.group.name)
            assertEquals(testtitle, response.group.title)
        }
    }

    @Test
    fun `Get group of not existing is returning null`() {
        runBlocking {
            val response = civicrm.getGroup(999999)

            assertNull(response, "Get group shall deliver null if group does not exist!")
        }
    }

    @Test
    fun `Add group as children`() {
        runBlocking {
            val testname = "testgroupA${Random().nextInt(1000)}"
            val testtitle = "testtitleA${Random().nextInt(1000)}"
            val testname2 = "testgroupB${Random().nextInt(1000)}"
            val testtitle2 = "testtitleB${Random().nextInt(1000)}"
            val parentGroup = civicrm.createOrReplaceTopLevelGroupWithACLPermission(testname, testtitle, "Rolle für $testname")
            val childGroup =
                civicrm.createOrReplaceSubgroupWithACLPermission(
                    testname2,
                    testtitle2,
                    "Rolle für $testname2",
                    "Rolle für $testname",
                )
            val response = civicrm.updateParentOfGroup(childGroup.group.id, parentGroup.group.id)

            val readGroup = civicrm.getGroup(childGroup.group.id)
            assertEquals(setOf(parentGroup.group.id.toString()), response!!.parents, "Native response of update did not contain parents")
            assertEquals(setOf(parentGroup.group.id.toString()), readGroup!!.parents, "Get response did not contain parents")
        }
    }

    @Test
    fun `Delete group with dependencies`() {
        val randomNumber = kotlin.random.Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()

        runBlocking {
            ServiceTest.createContactTypes(a.civicrm)
            val old = ContactSnapshot(a.civicrm)
            val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "delt")
            a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")
            civicrm.deleteGroupWithAclAndContactDependencies(credentialScenario.group.name)

            val new = ContactSnapshot(a.civicrm)
            val acl = civicrm.getACLs()
            credentialScenario.deleteScenario()

            assertFalse(
                civicrm.getGroups().any { it.name == credentialScenario.group.name },
                "Group ${credentialScenario.group.name} was not deleted",
            )
            assertFalse(
                acl.any { it.name.contains(credentialScenario.group.name) },
                "ACLs ${credentialScenario.group.name} were not deleted",
            )

            assertEquals(old.contacts.count(), new.contacts.count(), "Contact were not deleted")
            assertEquals(old.groupContacts.count(), new.groupContacts.count(), "GroupContacts were not deleted")
            assertEquals(old.emails.count(), new.emails.count(), "Email was not deleted")
            assertEquals(old.blockedContacts.count(), new.blockedContacts.count(), "Block contacts was not deleted")
        }
    }

    @Test
    fun `Delete role with dependencies`() {
        val randomNumber = kotlin.random.Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()

        runBlocking {
            ServiceTest.createContactTypes(a.civicrm)
            val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "delt")
            a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")
            civicrm.deleteRoleWithAclDependencies(credentialScenario.group.name)

            val acl = civicrm.getACLs()
            val optionValues = civicrm.getOptionValues()
            credentialScenario.deleteScenario()

            assertFalse(
                optionValues.any { it.name?.contains(credentialScenario.group.name) ?: false },
                "OptionValues ${credentialScenario.group.name} was not deleted",
            )
            assertFalse(
                acl.any { it.name.contains(credentialScenario.group.name) },
                "ACLs ${credentialScenario.group.name} were not deleted",
            )
        }
    }
}
