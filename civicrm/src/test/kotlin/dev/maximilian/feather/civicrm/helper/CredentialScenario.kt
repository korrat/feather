package dev.maximilian.feather.civicrm.helper

import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.testutils.ServiceConfig
import java.time.Instant

public class CredentialScenario() {
    var group: Group =
        Group(0, "no init", "NO INIT", emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet())
    var admin: User =
        User(
            0,
            "no init",
            "NO INIT",
            "firstname - NO INIT",
            "lastname - NO INIT",
            "noini@example.prg",
            emptySet(),
            Instant.now(),
            emptySet(),
            emptySet(),
            false,
            Instant.now(),
        )
    var testUser: User =
        User(
            0,
            "no init",
            "NO INIT",
            "firstname - NO INIT",
            "lastname - NO INIT",
            "noini@example.prg",
            emptySet(),
            Instant.now(),
            emptySet(),
            emptySet(),
            false,
            Instant.now(),
        )

    fun createCRMGroupWithUser(
        randomNumber: Int,
        uniquePrefix: String,
    ): CredentialScenario {
        val adminUser =
            User(
                0, "admin", "test admin", "Peter", "Pan", "civiservice-$uniquePrefix@example.com", emptySet(),
                Instant.now(), emptySet(), setOf(Permission.ADMIN), false, Instant.now(),
            )
        val testUser =
            User(
                0,
                "civi-testuser$uniquePrefix$randomNumber",
                "civi.user$randomNumber",
                "civi$randomNumber",
                "test$randomNumber",
                "civi$randomNumber@example.org",
                emptySet(),
                Instant.now(),
                emptySet(),
                emptySet(),
                false,
                Instant.now(),
            )

        val testUserCreated = ServiceConfig.CREDENTIAL_PROVIDER.createUser(testUser)

        val adminCreated =
            ServiceConfig.CREDENTIAL_PROVIDER.getUserByMail(adminUser.mail)
                ?: ServiceConfig.CREDENTIAL_PROVIDER.createUser(adminUser)
        val group =
            Group(
                0,
                "rg-test-$uniquePrefix$randomNumber-crm",
                "CRM Nutzer von RG Test $uniquePrefix$randomNumber",
                setOf(testUserCreated.id),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
            )
        val groupCreated = ServiceConfig.CREDENTIAL_PROVIDER.createGroup(group)
        val testUserRead = ServiceConfig.CREDENTIAL_PROVIDER.getUser(testUserCreated.id)

        this.testUser = testUserRead!!
        this.admin = adminCreated
        this.group = groupCreated
        return this
    }

    fun createInterestedGroupWithUser(
        randomNumber: Int,
        uniquePrefix: String,
    ): CredentialScenario {
        val adminUser =
            User(
                0, "admin", "test admin", "Peter", "Pan", "civiservice-$uniquePrefix@example.com", emptySet(),
                Instant.now(), emptySet(), setOf(Permission.ADMIN), false, Instant.now(),
            )
        val testUser =
            User(
                0,
                "civi-testuser$uniquePrefix$randomNumber",
                "civi.user$randomNumber",
                "civi$randomNumber",
                "test$randomNumber",
                "civi$randomNumber@example.org",
                emptySet(),
                Instant.now(),
                emptySet(),
                emptySet(),
                false,
                Instant.now(),
            )

        val testUserCreated = ServiceConfig.CREDENTIAL_PROVIDER.createUser(testUser)

        val adminCreated =
            ServiceConfig.CREDENTIAL_PROVIDER.getUserByMail(adminUser.mail)
                ?: ServiceConfig.CREDENTIAL_PROVIDER.createUser(adminUser)
        val group =
            Group(
                0,
                "rg-test-$uniquePrefix$randomNumber-interested",
                "InterestedNutzer von RG Test $uniquePrefix$randomNumber",
                setOf(testUserCreated.id),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
                emptySet(),
            )
        val groupCreated = ServiceConfig.CREDENTIAL_PROVIDER.createGroup(group)
        val testUserRead = ServiceConfig.CREDENTIAL_PROVIDER.getUser(testUserCreated.id)

        this.testUser = testUserRead!!
        this.admin = adminCreated
        this.group = groupCreated
        return this
    }

    fun deleteScenario() {
        ServiceConfig.CREDENTIAL_PROVIDER.deleteUser(testUser)
        ServiceConfig.CREDENTIAL_PROVIDER.deleteUser(admin)
        ServiceConfig.CREDENTIAL_PROVIDER.deleteGroup(group)
    }

    fun updateUser() {
        testUser = ServiceConfig.CREDENTIAL_PROVIDER.getUser(testUser.id)!!
    }
}
