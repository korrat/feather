/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.ACL
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.internal.civicrm.ACLProtocol
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ACLTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create ACL returns proper response`() {
        runBlocking {
            val id = Random().nextInt(1000)
            val acl =
                ACL(
                    0, "testacl$id", false, ACLProtocol.ACLEntityType.ACL_ROLE.toString(),
                    1, ACLProtocol.Operations.VIEW.protocolName,
                    ACLProtocol.TargetType.GROUP.toString(),
                    1, null, true,
                )
            val n = civicrm.createACL(acl)
            assertEquals("testacl$id", n.name)
            assertEquals(false, n.deny) // ignore true response, strange behaviour of civicrm
            assertEquals(ACLProtocol.ACLEntityType.ACL_ROLE.protocolName, n.entityTable)
            assertEquals(ACLProtocol.Operations.VIEW.protocolName, n.operation)
            assertEquals(ACLProtocol.TargetType.GROUP.protocolName, n.objectTable)
            assertEquals(1, n.objectId)
            assertEquals(true, n.active)
        }
    }

    @Test
    fun `Create denied ACL returns proper response`() {
        runBlocking {
            val id = Random().nextInt(1000)
            val acl =
                ACL(
                    0, "testacl$id", true, ACLProtocol.ACLEntityType.ACL_ROLE.toString(),
                    1, ACLProtocol.Operations.VIEW.protocolName,
                    ACLProtocol.TargetType.GROUP.toString(),
                    1, null, true,
                )
            val n = civicrm.createACL(acl)
            assertEquals("testacl$id", n.name)
            assertEquals(true, n.deny) // ignore true response, strange behaviour of civicrm
            assertEquals(ACLProtocol.ACLEntityType.ACL_ROLE.protocolName, n.entityTable)
            assertEquals(ACLProtocol.Operations.VIEW.protocolName, n.operation)
            assertEquals(ACLProtocol.TargetType.GROUP.protocolName, n.objectTable)
            assertEquals(1, n.objectId)
            assertEquals(true, n.active)
        }
    }

    @Test
    fun `Delete ACL does not crash`() {
        runBlocking {
            val id = Random().nextInt(1000)
            val acl =
                ACL(
                    0, "test$id", false, ACLProtocol.ACLEntityType.ACL_ROLE.protocolName,
                    1, ACLProtocol.Operations.VIEW.protocolName,
                    ACLProtocol.TargetType.GROUP.protocolName,
                    1, null, true,
                )
            val n = civicrm.createACL(acl)
            civicrm.deleteACL(n.id)
        }
    }

    @Test
    fun `Get acl by id 1 delivers correct ACL Edit All Contacts`() {
        runBlocking {
            val n = civicrm.getACL(1)
            assertEquals("Edit All Contacts", n!!.name)
            assertEquals(false, n.deny)
            assertEquals(ACLProtocol.ACLEntityType.ACL_ROLE.protocolName, n.entityTable)
            assertEquals("Edit", n.operation)
            assertEquals(ACLProtocol.TargetType.CIVICRM_GROUP.protocolName, n.objectTable)
            assertEquals(0, n.objectId)
            assertEquals(true, n.active)
        }
    }

    @Test
    fun `Get all ACL returns at least one`() {
        runBlocking {
            val t = civicrm.getACLs()
            assertTrue(t.isNotEmpty())
        }
    }
}
