/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CustomGroupTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create custom group`() {
        runBlocking {
            val g = civicrm.createCustomGroup("testgroupBBB", "testtitleBBB")
            assertEquals("testgroupBBB", g.name)
            assertEquals("testtitleBBB", g.title)
        }
    }

    @Test
    fun `Delete custom group`() {
        runBlocking {
            civicrm.getCustomGroup().onEach { civicrm.deleteCustomGroup(it.id) }
            val g = civicrm.createCustomGroup("testgroupCCC", "testtitleCCC")
            civicrm.deleteCustomGroup(g.id)
            val newList = civicrm.getCustomGroup()
            assertTrue(newList.isEmpty(), "List not empty after deletion")
        }
    }

    @Test
    fun `Read all custom groups return at least one result`() {
        runBlocking {
            val g = civicrm.createCustomGroup("testgroupDDD", "testtitleDDD")

            val t = civicrm.getCustomGroup()
            assertTrue(t.isNotEmpty())
        }
    }
}
