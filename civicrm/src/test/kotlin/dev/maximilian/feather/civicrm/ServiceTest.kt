/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.User
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.helper.ContactSnapshot
import dev.maximilian.feather.civicrm.helper.CredentialScenario
import dev.maximilian.feather.civicrm.internal.civicrm.ICiviCRM
import dev.maximilian.feather.testutils.ServiceConfig
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ServiceTest {
    companion object {
        private val defaultMember = "IogMember"
        private val employeeName = "Employee"

        fun createContactTypes(civi: ICiviCRM) {
            runBlocking {
                civi.getContactTypes().firstOrNull { it.name == employeeName }?.let { civi.deleteContactType(it.id) }

                civi.createContactType(
                    employeeName,
                    "Mitarbeiter",
                    CiviCRMConstants.ContactTypeDefaults.Individual,
                )

                civi.getContactTypes().firstOrNull { it.name == defaultMember }?.let { civi.deleteContactType(it.id) }

                civi.createContactType(
                    defaultMember,
                    "Iog-Mitglied",
                    CiviCRMConstants.ContactTypeDefaults.Individual,
                )
            }
        }
    }

    @Test
    fun `Create CiviCRMService`() {
        CiviCRMTestSetup().civiService
    }

    @Test
    fun `CiviCRMService returns correct name`() {
        val name = CiviCRMTestSetup().civiService.serviceName
        assertEquals("CiviCRM", name)
    }

    /*
    @Test
    fun `CiviCRMService checkUser returns ok`() {
        val randomNumber = Random.nextInt()

        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "add")
        runBlocking {
            val testSetup = CiviCRMTestSetup()
            val a = testSetup.civiService
            testSetup.reset()

            createContactTypes(a.civicrm)
            val civiGroup = a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")

            a.added(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            val res = a.checkUser(credentialScenario.testUser)

            val groupContacts = a.civicrm.getGroupContacts()
            val contacts = a.civicrm.getContacts()
            val emails = a.civicrm.getEmails()

            credentialScenario.deleteScenario()
            a.civicrm.deleteGroup(civiGroup.id)

            assertEquals("OK", res)

            a.civicrm.deleteContact(contacts.last().id)
            a.civicrm.deleteGroupContact(groupContacts.last().id)
            a.civicrm.deleteEmail(emails.last().id)
        }
    }

     */

    @Test
    fun `CiviCRMService checkUser without civi group returns Not found`() {
        val randomNumber = Random.nextInt()
        val a = CiviCRMTestSetup().civiService
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "add")
        runBlocking {
            val res = a.checkUser(credentialScenario.testUser)
            credentialScenario.deleteScenario()
            assertEquals("Not found", res)
        }
    }

    @Test
    fun `CiviCRMService checkUser of non in civicrm existing user return OK`() {
        val a = CiviCRMTestSetup().civiService
        val user =
            ServiceConfig.CREDENTIAL_PROVIDER.createUser(
                User(
                    0,
                    "testuser111", "t1", "f1", "l1", "bl@example.org",
                    emptySet(), Instant.now(), emptySet(), emptySet(), false, Instant.now(),
                ),
                "topsecret",
            )
        runBlocking {
            val res = a.checkUser(user)
            ServiceConfig.CREDENTIAL_PROVIDER.deleteUser(user)
            assertEquals("OK", res)
        }
    }

    @Test
    fun `CiviCRMService changeMail returns true and changed mail`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "addB")
        runBlocking {
            createContactTypes(a.civicrm)
            a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")

            a.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            val old = ContactSnapshot(a.civicrm)
            val res = a.changeMailAddress(credentialScenario.testUser, "newmail$randomNumber@example.org")
            val new = ContactSnapshot(a.civicrm)

            credentialScenario.deleteScenario()

            assertEquals(old.contacts.count(), new.contacts.count(), "Contact changed")
            assertEquals(old.groupContacts, old.groupContacts, "GroupContact changed")
            assertEquals(old.emails.count(), new.emails.count(), "Number of Emails changed ")
            assertEquals(old.blockedContacts.count(), new.blockedContacts.count(), "Number of blocked contacts changed ")

            assertEquals("newmail$randomNumber@example.org", new.emails.last().email, "Email is not defined properly")
            assertTrue(res, "Result should be true")
        }
    }

    @Test
    fun `CiviCRMService block user soft deletes in civicrm`() {
        val randomNumber = Random.nextInt()
        val testSetup = CiviCRMTestSetup()
        val a = testSetup.civiService
        testSetup.reset()
        val credentialScenario = CredentialScenario().createCRMGroupWithUser(randomNumber, "addC")

        createContactTypes(a.civicrm)
        runBlocking {
            a.civicrm.createGroup(credentialScenario.group.name, "CRM Nutzer von RG Test $randomNumber")
            val old = ContactSnapshot(a.civicrm)
            a.userAddedToGroup(credentialScenario.admin, credentialScenario.group, credentialScenario.testUser.id)
            a.activateUser(credentialScenario.testUser, false)
            val new = ContactSnapshot(a.civicrm)
            credentialScenario.deleteScenario()

            assertEquals(old.contacts.count(), new.contacts.count(), "Contact changed")
            assertEquals(old.groupContacts.count() + 1, new.groupContacts.count(), "GroupContact was not increased")
            assertEquals(old.emails.count() + 1, new.emails.count(), "Number of Emails was not increased ")
            assertTrue(
                new.blockedContacts.any {
                    it.lastName == credentialScenario.testUser.surname && it.firstName == credentialScenario.testUser.firstname
                },
            )
        }
    }
}
