/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.entities.Contact
import dev.maximilian.feather.civicrm.entities.Group
import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import dev.maximilian.feather.civicrm.internal.civicrm.GeneralAPI
import io.ktor.client.call.body
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class GeneralAPITest {
    @Test
    fun createClient() {
        CiviCRMFactory.createApiHttpClient(CiviCRMTestSetup.apikey)
    }

    @Test
    fun createGeneralApi() {
        val client = CiviCRMFactory.createApiHttpClient(CiviCRMTestSetup.apikey)

        GeneralAPI(
            client = client,
            baseUrl = CiviCRMTestSetup.host,
            "Contact",
            null,
        )
    }

    @Test
    fun get() {
        val client = CiviCRMFactory.createApiHttpClient(CiviCRMTestSetup.apikey)

        val api =
            GeneralAPI(
                client = client,
                baseUrl = CiviCRMTestSetup.host,
                "Contact",
                null,
            )
        runBlocking {
            val response = api.get().body<GeneralAPI.Answer<Contact>>().values
            assertTrue(response.isNotEmpty())
        }
    }

    @Test
    fun checkAdminGroupPrecondition() {
        val client = CiviCRMFactory.createApiHttpClient(CiviCRMTestSetup.apikey)

        val apiGroup =
            GeneralAPI(
                client = client,
                baseUrl = CiviCRMTestSetup.host,
                "Group",
                null,
            )
        runBlocking {
            val q =
                apiGroup.getString(setOf(Triple("name", "=", "Administrators")))
                    .body<GeneralAPI.Answer<Group>>().values
            assertNotNull(q)
        }
    }

    @Test
    fun checkPreconditions() {
        val client = CiviCRMFactory.createApiHttpClient(CiviCRMTestSetup.apikey)

        val apiContact =
            GeneralAPI(
                client = client,
                baseUrl = CiviCRMTestSetup.host,
                "Contact",
                null,
            )
        runBlocking {
            val apiUser =
                apiContact.getString(
                    setOf(
                        Triple("first_name", "=", "API"),
                        Triple("last_name", "=", "User"),
                    ),
                ).body<GeneralAPI.Answer<Contact>>().values
            assertTrue(apiUser.isNotEmpty())
        }
    }
}
