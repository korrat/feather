/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.civicrm

import dev.maximilian.feather.civicrm.helper.CiviCRMTestSetup
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import java.util.Random
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class EmailTest {
    val civicrm = CiviCRMFactory.create(CiviCRMTestSetup.civiCRMSettings)

    @Test
    fun `Create email`() {
        runBlocking {
            val testMail = "email${Random().nextInt(1000)}@example.org"
            val response = civicrm.createEmail(testMail, 1)
            assertEquals(testMail, response.email)
        }
    }

    @Test
    fun `Get email by id`() {
        runBlocking {
            val testMail = "email${Random().nextInt(1000)}@example.org"
            val response = civicrm.createEmail(testMail, 1)
            val response2 = civicrm.getEmail(response.id)
            assertEquals(testMail, response2!!.email)
            civicrm.deleteEmail(response.id)
        }
    }

    @Test
    fun `Get email by contact id`() {
        runBlocking {
            val testMail = "email${Random().nextInt(1000)}@example.org"
            val contact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Dudu",
                    "Müller",
                )
            civicrm.createEmail(testMail, contact.id)
            val response2 = civicrm.getEmailByContactId(contact.id)
            assertEquals(testMail, response2!!.email)
            civicrm.deleteEmail(response2.id)
        }
    }

    @Test
    fun `Get contact id by existing mail is valid`() {
        runBlocking {
            val testMail = "emailZ${Random().nextInt(1000)}@example.org"

            val contact =
                civicrm.createContact(
                    CiviCRMConstants.ContactTypeDefaults.Individual.protocolName,
                    null,
                    "Dudu",
                    "Müller${Random().nextInt()}",
                )
            val created = civicrm.createEmail(testMail, contact.id)
            val response2 = civicrm.getContactIdByEmail(testMail)
            civicrm.deleteEmail(created.id)
            civicrm.deleteContact(contact.id)
            assertEquals(contact.id, response2)
        }
    }

    @Test
    fun `Get contact id of non existing mail is null`() {
        runBlocking {
            val response2 = civicrm.getContactIdByEmail("norealmail@example.org")
            assertNull(response2, "Response of invalid email must be null")
        }
    }

    @Test
    fun `Delete email`() {
        runBlocking {
            val originalList = civicrm.getEmails()
            val response = civicrm.createEmail("blablab@example.org", 1)
            civicrm.deleteEmail(response.id)
            val newList = civicrm.getEmails()
            assertEquals(originalList, newList, "Original Groupt list differs from list after create and delete")
        }
    }

    @Test
    fun `Read all emails return at least one result`() {
        runBlocking {
            val t = civicrm.getEmails()
            assertTrue(t.isNotEmpty())
        }
    }

    @Test
    fun `Update emails`() {
        runBlocking {
            val testMail = "emailB${Random().nextInt(1000)}@example.org"
            val response = civicrm.createEmail(testMail, 1)
            val testMail2 = "emailc${Random().nextInt(1000)}@example.org"
            val updatedMail = civicrm.updateEmail(response.id, testMail2)
            assertEquals(testMail2, updatedMail!!.email)
        }
    }
}
