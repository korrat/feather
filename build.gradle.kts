/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import org.gradle.api.tasks.testing.logging.TestExceptionFormat

val gitVersion: groovy.lang.Closure<String> by extra

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.git.version)
    alias(libs.plugins.kotlin.kapt)
    `java-library`
}

allprojects {
    group = "dev.maximilian.feather"
    version = gitVersion()

    repositories {
        mavenCentral()
    }
}

subprojects {
    apply(plugin = "kotlin")
    apply(plugin = "java-library")
    apply(plugin = "kotlin-kapt")

    dependencies {
        // Kotlin with stdlib and coroutines
        implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
        implementation(rootProject.libs.kotlinx.coroutines)

        // Logging framework
        implementation(rootProject.libs.slf4j.api)
        runtimeOnly(rootProject.libs.slf4j.simple)
        implementation(rootProject.libs.microutils)

        // Testing framework
        testImplementation(rootProject.libs.kotlin.junit)
        testImplementation(rootProject.libs.junit.core)
        testRuntimeOnly(rootProject.libs.junit.engine)

        if (name != "test_utils") {
            testImplementation(project(":test_utils"))
        }
    }

    tasks {
        compileJava {
            sourceCompatibility = "17"
            targetCompatibility = "17"
        }

        compileTestJava {
            sourceCompatibility = "17"
            targetCompatibility = "17"
        }

        compileKotlin {
            kotlinOptions.jvmTarget = "17"
        }

        compileTestKotlin {
            kotlinOptions.jvmTarget = "17"
        }

        test {
            useJUnitPlatform()
            testLogging {
                events("passed", "skipped", "failed")

                exceptionFormat = TestExceptionFormat.FULL
            }
        }
    }
}
