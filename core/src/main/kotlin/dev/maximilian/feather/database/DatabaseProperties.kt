/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.database

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

internal class DatabaseProperties(private val db: Database) : MutableMap<String, String> {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(PropertyTable)
        }
    }

    override val size: Int = transaction(db) { PropertyEntry.count().toInt() }

    override fun containsKey(key: String): Boolean =
        transaction(db) {
            PropertyTable.select { PropertyTable.id eq key }.count() != 0L
        }

    override fun containsValue(value: String): Boolean =
        transaction(db) {
            PropertyTable.select { PropertyTable.value eq value }.count() != 0L
        }

    override fun get(key: String): String? =
        transaction(db) {
            PropertyEntry.findById(key)?.value
        }

    override fun isEmpty(): Boolean = size == 0

    override val entries: MutableSet<MutableMap.MutableEntry<String, String>> = DatabasePropertiesEntrySet(this)

    override val keys: MutableSet<String> =
        object : AbstractMutableSet<String>() {
            override val size: Int = this@DatabaseProperties.size

            override fun add(element: String): Boolean = throw UnsupportedOperationException()

            override fun iterator(): MutableIterator<String> = MutableDelegateIterator(entries.iterator()) { it.key }
        }

    override val values: MutableCollection<String> =
        object : AbstractMutableCollection<String>() {
            override val size: Int = this@DatabaseProperties.size

            override fun add(element: String): Boolean = throw UnsupportedOperationException()

            override fun iterator(): MutableIterator<String> = MutableDelegateIterator(entries.iterator()) { it.value }
        }

    override fun clear() {
        transaction(db) { PropertyTable.deleteAll() }
    }

    override fun put(
        key: String,
        value: String,
    ): String? =
        transaction {
            PropertyEntry.findById(key).apply {
                if (this != null) {
                    this.value = value
                } else {
                    PropertyTable.insert {
                        it[PropertyTable.id] = EntityID(key, PropertyTable)
                        it[PropertyTable.value] = value
                    }
                }
            }?.value
        }

    override fun putAll(from: Map<out String, String>): Unit =
        transaction {
            val databaseKeys = PropertyEntry.forIds(from.keys.toList())

            // Create all not existing
            from.minus(databaseKeys.map { it.id.value }).forEach { entry ->
                PropertyTable.insert {
                    it[PropertyTable.id] = EntityID(entry.key, PropertyTable)
                    it[value] = entry.value
                }
            }

            // Update all existing
            databaseKeys.forEach {
                it.value = from[it.id.value]!!
            }
        }

    override fun remove(key: String): String? = transaction(db) { PropertyEntry.findById(key)?.apply { delete() }?.value }

    internal object PropertyTable : StringIdTable("properties", "key") {
        val value: Column<String> = text("value")
    }

    internal class PropertyEntry(id: EntityID<String>) : StringEntity(id) {
        companion object : StringEntityClass<PropertyEntry>(PropertyTable)

        var value by PropertyTable.value
    }
}

private class MutableDelegateIterator<T, Y>(private val delegate: MutableIterator<Y>, private val next: (Y) -> T) :
    MutableIterator<T> {
    override fun hasNext(): Boolean = delegate.hasNext()

    override fun next(): T = next(delegate.next())

    override fun remove() = delegate.remove()
}

private class DatabasePropertiesEntrySet(private val props: DatabaseProperties) :
    AbstractMutableSet<MutableMap.MutableEntry<String, String>>() {
    override val size: Int = props.size

    override fun add(element: MutableMap.MutableEntry<String, String>): Boolean {
        val sizeBefore = size
        props[element.key] = element.value
        return sizeBefore != size
    }

    override fun iterator(): MutableIterator<MutableMap.MutableEntry<String, String>> =
        object : MutableIterator<MutableMap.MutableEntry<String, String>> {
            private val delegate =
                transaction {
                    DatabaseProperties.PropertyEntry.all().associate { it.id.value to it.value }.toMutableMap()
                }.iterator()
            private var last: MutableMap.MutableEntry<String, String>? = null

            override fun hasNext(): Boolean = delegate.hasNext()

            override fun next(): MutableMap.MutableEntry<String, String> {
                val next = delegate.next().let { entry -> SpecialEntry(props, entry.key, entry.value) }
                last = next
                return next
            }

            override fun remove() {
                requireNotNull(last).also {
                    delegate.remove()
                    props.remove(it.key, it.value)
                    last = null
                }
            }
        }
}

private class SpecialEntry(private val props: DatabaseProperties, override val key: String, override var value: String) :
    MutableMap.MutableEntry<String, String> {
    override fun setValue(newValue: String): String =
        value.also {
            value = newValue
            props[key] = newValue
        }
}
