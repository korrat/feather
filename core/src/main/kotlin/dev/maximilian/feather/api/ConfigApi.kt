/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.api

import dev.maximilian.feather.AuthorizerType
import dev.maximilian.feather.IAuthorizer
import dev.maximilian.feather.ICredentialAuthorizer
import dev.maximilian.feather.IOidcAuthorizer
import dev.maximilian.feather.ITokenAuthorizer
import dev.maximilian.feather.iog.ServiceFactory
import dev.maximilian.feather.iog.api.bindings.MultiServiceGroupKindDefinition
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.iog.settings.SupportMembershipFeature
import io.javalin.Javalin
import io.javalin.http.Context

internal class ConfigApi(
    app: Javalin,
    private val authorizer: Set<IAuthorizer>,
    private val version: String,
    private val openshiftEnabled: Boolean,
) {
    init {
        app.get("/config", ::getConfig)
    }

    private fun getConfig(ctx: Context) {
        if (authorizer.isEmpty()) {
            // no authorization service active => not a valid configuration
            ctx.status(503)
            return
        }
        ctx.json(
            Config(
                version = version,
                bindings =
                BindingConfig(
                    nextcloud =
                    NextcloudConfig(
                        ServiceFactory.isNextcloudEnabled(),
                        ServiceFactory.getNextcloudUrl(),
                    ),
                    openproject =
                    OpenProjectConfig(
                        ServiceFactory.isOpenProjectEnabled(),
                        ServiceFactory.getOpenProjectUrl(),
                    ),
                    openshift = openshiftEnabled,
                    multiservice =
                    MultiServiceConfig(
                        enabled = true,
                        groupKindDefinitions = IogServiceApiConstants.GROUP_KINDS.values.toList(),
                        plugin =
                        IoGPluginConfig(
                            ServiceFactory.getSupportMembershipFeature() != SupportMembershipFeature.SUPPORT_MEMBERSHIP_DISABLED,
                        ),
                    ),
                ),
                authorizations = authorizer.map(IAuthorizer::toAuthorizationConfig).toSet(),
            ),
        )
    }
}

private data class Config(
    val version: String,
    val bindings: BindingConfig,
    val authorizations: Set<AuthorizationConfig>,
)

private data class NextcloudConfig(
    val enabled: Boolean,
    val publicUrl: String,
)

private data class OpenProjectConfig(
    val enabled: Boolean,
    val publicUrl: String,
)

private abstract class MultiServicePluginConfig(
    val multiServicePlugin: String,
)

private data class IoGPluginConfig(
    val supportMembershipDB: Boolean,
) : MultiServicePluginConfig("iog")

private data class MultiServiceConfig(
    val enabled: Boolean,
    val groupKindDefinitions: List<MultiServiceGroupKindDefinition>,
    val plugin: MultiServicePluginConfig,
)

private data class BindingConfig(
    val nextcloud: NextcloudConfig,
    val openproject: OpenProjectConfig,
    val openshift: Boolean,
    val multiservice: MultiServiceConfig,
)

private fun IAuthorizer.toAuthorizationConfig() =
    when (this) {
        is IOidcAuthorizer -> AuthorizationConfig(id, type, name, iconUrl, color)
        is ICredentialAuthorizer, is ITokenAuthorizer -> AuthorizationConfig(id, type, null, null, null)
    }

private data class AuthorizationConfig(
    val id: String,
    val type: AuthorizerType,
    val name: String?,
    val iconUrl: String?,
    val color: String?,
)
