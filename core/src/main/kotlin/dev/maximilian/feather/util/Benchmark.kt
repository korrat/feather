package dev.maximilian.feather.util

import mu.KotlinLogging
import java.time.Duration
import java.time.Instant
import java.util.UUID
import java.util.concurrent.ConcurrentHashMap
import kotlin.concurrent.timer

internal class Benchmark {
    internal data class Entry(
        val count: Int,
        val minTimeMs: Long,
        val maxTimeMs: Long,
        val sumTimeMs: Long,
        val lastDuration: Long,
    )

    init {
        timer(daemon = true, period = 60 * 60 * 1000L, initialDelay = 4 * 60 * 1000L) { logReport() }
    }

    private val logger = KotlinLogging.logger {}
    private val runningJobs = ConcurrentHashMap<UUID, Instant>()
    internal val timeEntries = ConcurrentHashMap<String, Entry>()

    fun reset() {
        timeEntries.clear()
        runningJobs.clear()
    }

    fun start(): UUID {
        val uuid = UUID.randomUUID()
        runningJobs[uuid] = Instant.now()
        return uuid
    }

    fun stop(
        uuid: UUID,
        functionName: String,
    ) {
        runningJobs[uuid]?.let {
            val duration = Duration.between(it, Instant.now()).toMillis()
            if (timeEntries.containsKey(functionName)) {
                val q = timeEntries[functionName]!!
                val minTimeMs = minOf(q.minTimeMs, duration)
                val maxTimeMs = maxOf(q.maxTimeMs, duration)
                val sumTimeMs = q.sumTimeMs + duration
                timeEntries[functionName] = Entry(q.count + 1, minTimeMs, maxTimeMs, sumTimeMs, duration)
            } else {
                timeEntries[functionName] = Entry(1, duration, duration, duration, duration)
            }
            runningJobs.remove(uuid)
        }
    }

    fun logReport() {
        var summary = "Benchmark Report: \r\n"
        timeEntries.forEach {
            if (it.value.count > 0) {
                summary += "Function: ${it.key}\tMin:${it.value.minTimeMs}ms\tMax:${it.value.maxTimeMs}ms\tAvg:${it.value.sumTimeMs / it.value.count}ms\tCalls:${it.value.count}\tLast:${it.value.lastDuration}ms\r\n"
            }
        }
        logger.info { summary }
    }
}
