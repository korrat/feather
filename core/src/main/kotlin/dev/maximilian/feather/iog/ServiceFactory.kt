/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Main
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.civicrm.CiviCRMService
import dev.maximilian.feather.civicrm.CiviCRMSettings
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.IogPluginConstants.CRM_SUFFIX
import dev.maximilian.feather.iog.settings.SupportMembershipFeature
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.Multiservice
import dev.maximilian.feather.multiservice.api.OpenProjectApi
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudSettings
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.openproject.OpenProjectSettings
import dev.maximilian.feather.multiservice.settings.ChangeMailSetting
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.multiservice.settings.SMTPSetting
import dev.maximilian.feather.multiservice.settings.SendMailContent
import dev.maximilian.feather.util.FeatherProperties
import io.javalin.Javalin
import jakarta.mail.internet.InternetAddress
import mu.KLogging

internal class ServiceFactory(private val app: Javalin) {
    companion object : KLogging() {
        fun isOpenProjectEnabled(): Boolean {
            return FeatherProperties.OPENPROJECT_BASE_URL.isNotBlank() && FeatherProperties.OPENPROJECT_API_KEY.isNotBlank()
        }

        fun isNextcloudEnabled(): Boolean {
            return FeatherProperties.NEXTCLOUD_USERNAME.isNotBlank() && FeatherProperties.NEXTCLOUD_PASSWORD.isNotBlank()
        }

        fun isCiviCRMEnabled(): Boolean {
            return FeatherProperties.CIVICRM_BASE_URL.isNotBlank() && FeatherProperties.CIVICRM_APIKEY.isNotBlank()
        }

        fun getNextcloudUrl(): String =
            when {
                FeatherProperties.NEXTCLOUD_PUBLIC_URL.isNotBlank() -> FeatherProperties.NEXTCLOUD_PUBLIC_URL
                else -> FeatherProperties.BASE_URL
            }

        fun getOpenProjectUrl(): String =
            when {
                FeatherProperties.OPENPROJECT_PUBLIC_URL.isNotBlank() -> FeatherProperties.OPENPROJECT_PUBLIC_URL
                else -> FeatherProperties.BASE_URL
            }

        fun getSupportMembershipFeature(): SupportMembershipFeature {
            return if (!FeatherProperties.IOG_SUPPORT_MEMBERSHIP_ENABLED) {
                SupportMembershipFeature.SUPPORT_MEMBERSHIP_DISABLED
            } else if (FeatherProperties.IOG_SUPPORT_MEMBERSHIP_ENFORCED) {
                SupportMembershipFeature.SUPPORT_MEMBERSHIP_ENFORCED
            } else {
                SupportMembershipFeature.SUPPORT_MEMBERSHIP_ENABLED
            }
        }
    }

    fun createMultiService(
        backgroundJobManager: BackgroundJobManager,
        gdprController: GdprController,
        actionController: ActionController,
        events: MultiServiceEvents,
    ): Multiservice {
        val invitationMailSettings =
            SendMailContent(FeatherProperties.MAIL_INVITATION, FeatherProperties.MAIL_INVITATION_SUBJECT)

        val internetAddress = InternetAddress(FeatherProperties.MAIL_FROM, FeatherProperties.BOTNAME)

        val forgotMailSettings =
            SendMailContent(
                FeatherProperties.MAIL_PASSWORD_FORGOT_CONTENT,
                FeatherProperties.MAIL_PASSWORD_FORGOT_SUBJECT,
            )
        val changeMailSetting =
            ChangeMailSetting(
                FeatherProperties.MAIL_CHANGE_MAIL_ADDRESS_CONTENT,
                FeatherProperties.MAIL_CHANGE_MAIL_ADDRESS_SUBJECT,
                FeatherProperties.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION1,
                FeatherProperties.MAIL_CHANGE_MAIL_ADDRESS_NOTIFICATION2,
            )
        val smtp =
            SMTPSetting(
                FeatherProperties.MAIL_HOST,
                FeatherProperties.MAIL_PORT,
                FeatherProperties.MAIL_AUTH,
                FeatherProperties.MAIL_STARTTLS,
                FeatherProperties.MAIL_USERNAME,
                FeatherProperties.MAIL_PASSWORD,
            )

        val mailSetting =
            MultiServiceMailSettings(
                FeatherProperties.SITENAME,
                smtp,
                internetAddress,
                forgotMailSettings,
                invitationMailSettings,
                changeMailSetting,
                FeatherProperties.BASE_URL,
            )
        var extendedEvents = events
        var civi: CiviCRMService? = null
        if (isCiviCRMEnabled()) {
            logger.info { "ServiceFactory::createMultiService CiviCRM is enabled." }
            try {
                civi =
                    createCiviCrmService(
                        Main.CREDENTIAL_MANAGER,
                        backgroundJobManager,
                        actionController,
                    )
                extendedEvents =
                    events.copy(
                        userAdded = events.userAdded.plus(civi),
                        userRemovedFromGroupEvent = events.userRemovedFromGroupEvent.plus(civi),
                    )
            } catch (ex: Exception) {
                logger.error(ex) { "CiviCRM could not be started" }
            }
        } else {
            logger.warn { "ServiceFactory::createMultiService CiviCRM disabled" }
            if (FeatherProperties.CIVICRM_BASE_URL.trim().removeSuffix("/").isBlank()) {
                logger.warn { "ServiceFactory::createMultiService CIVICRM_BASE_URL not configured" }
            }
            if (FeatherProperties.CIVICRM_APIKEY.isBlank()) {
                logger.warn { "ServiceFactory::createMultiService CIVICRM_APIKEY not configured" }
            }
        }

        val multiServiceConfig =
            MultiServiceConfig(
                Main.CREDENTIAL_MANAGER,
                backgroundJobManager,
                Main.DATABASE,
                mailSetting,
                extendedEvents,
                gdprController,
            )
        logger.info { "ServiceFactory::createMultiService " }
        val multiservice = Multiservice(multiServiceConfig, Main.ACCOUNT_CONTROLLER)

        if (isNextcloudEnabled()) {
            logger.info { "ServiceFactory::createMultiService Nextcloud is enabled." }
            try {
                multiservice.services.add(createNextcloudService(actionController))
            } catch (ex: Exception) {
                logger.error(ex) { "NEXTCLOUD could not be started." }
            }
        } else {
            logger.warn { "ServiceFactory::createMultiService Nextcloud disabled" }
            if (FeatherProperties.NEXTCLOUD_USERNAME.isBlank()) {
                logger.warn { "ServiceFactory::createMultiService NEXTCLOUD_USERNAME not configured. " }
            }
            if (FeatherProperties.NEXTCLOUD_PASSWORD.isBlank()) {
                logger.warn { "ServiceFactory::createMultiService NEXTCLOUD_PASSWORD not configured." }
            }
        }

        if (isOpenProjectEnabled()) {
            logger.info { "ServiceFactory::createMultiService OpenProject is enabled." }
            try {
                multiservice.services.add(
                    createOpenProjectService(
                        backgroundJobManager,
                        events.syncAllServicesEvent,
                        actionController,
                    ),
                )
            } catch (ex: Exception) {
                logger.error(ex) { "OpenProject could not be started" }
            }
        } else {
            logger.warn { "ServiceFactory::createMultiService OpenProject disabled" }
            if (FeatherProperties.OPENPROJECT_BASE_URL.trim().removeSuffix("/").isBlank()) {
                logger.warn { "ServiceFactory::createMultiService OPENPROJECT_BASE_URL not configured" }
            }
            if (FeatherProperties.OPENPROJECT_API_KEY.isBlank()) {
                logger.warn { "ServiceFactory::createMultiService OPENPROJECT_API_KEY not configured" }
            }
        }

        civi?.also { multiservice.services.add(it) }

        logger.info { "ServiceFactory::createMultiService startApis" }
        multiservice.startApis(app)

        return multiservice
    }

    private fun createOpenProjectService(
        backgroundJobManager: BackgroundJobManager,
        ldapGroupSynchronizer: GroupSynchronizationEvent,
        actionController: ActionController,
    ): OpenProjectService {
        val openProjectSettings =
            OpenProjectSettings(
                FeatherProperties.OPENPROJECT_USER_BINDING,
                Main.PROPERTIES.getOrPut("openproject.base_url") { OpenProjectConstants.BASEURL.trimIndent() },
                Main.PROPERTIES.getOrPut("openproject.automation_username") { OpenProjectConstants.AUTOMATION_USERNAME.trimIndent() },
                Main.PROPERTIES.getOrPut("openproject.sso_secret") { OpenProjectConstants.SSO_SECRET.trimIndent() },
                "apikey",
                Main.PROPERTIES.getOrPut("openproject.api_key") { OpenProjectConstants.APIKEY.trimIndent() },
            )

        logger.info {
            "ServiceFactory::createOpenProjectService with ssoAdmin=${openProjectSettings.ssoAdmin}, base url=${openProjectSettings.baseUrl}, userBinding=${openProjectSettings.userBinding},"
        }

        val openProjectService =
            OpenProjectService(backgroundJobManager, ldapGroupSynchronizer, openProjectSettings, Main.CREDENTIAL_MANAGER)
        OpenProjectApi(app, openProjectService)
        return openProjectService.also {
            actionController.addService(it)
        }
    }

    private fun createCiviCrmService(
        credentialProvider: ICredentialProvider,
        backgroundJobManager: BackgroundJobManager,
        actionController: ActionController,
    ): CiviCRMService {
        val civiCRMSettings =
            CiviCRMSettings(
                "API",
                "User",
                "Administrators",
                FeatherProperties.CIVICRM_BASE_URL,
                FeatherProperties.CIVICRM_APIKEY,
                setOf(LdapNames.CENTRAL_OFFICE, LdapNames.ETHIC_VALIDATION_GROUP, LdapNames.ASSOCIATION_BOARD),
                setOf(CRM_SUFFIX),
                LdapNames.CENTRAL_OFFICE,
                IogPluginConstants.CiviCRMCustomType.Employee.protocolName,
                IogPluginConstants.CiviCRMCustomType.IogMember.protocolName,
            )

        logger.info {
            "ServiceFactory::createCiviCRMService with " +
                "Admin=${civiCRMSettings.adminFirstName}, ${civiCRMSettings.adminLastName}, " +
                " AdminGroup=${civiCRMSettings.adminGroup}, baseUrl=${civiCRMSettings.baseUrl}"
        }

        return CiviCRMService(credentialProvider, civiCRMSettings, backgroundJobManager).also {
            actionController.addService(it)
        }
    }

    private fun createNextcloudService(actionController: ActionController): NextcloudService {
        val nextcloudSettings =
            NextcloudSettings(
                FeatherProperties.NEXTCLOUD_USER_BINDING,
                FeatherProperties.NEXTCLOUD_PUBLIC_URL,
                FeatherProperties.NEXTCLOUD_BASE_URL,
                FeatherProperties.NEXTCLOUD_USERNAME,
                FeatherProperties.NEXTCLOUD_PASSWORD,
            )

        logger.info {
            "ServiceFactory::createNextcloudService with public url=${nextcloudSettings._publicUrl}, userbinding=${nextcloudSettings.userBinding}, base url=${nextcloudSettings.baseUrl}, username=${nextcloudSettings.username},"
        }
        return NextcloudService(app, Main.REDIS, nextcloudSettings, Main.CREDENTIAL_MANAGER).also {
            actionController.addService(it)
        }
    }
}
