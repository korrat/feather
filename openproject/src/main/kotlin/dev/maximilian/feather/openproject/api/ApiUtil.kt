/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ResponseException
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.http.takeFrom
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import org.jsoup.Jsoup
import java.net.URI
import kotlin.math.max

internal fun buildFilter(filters: Set<Triple<String, String, Set<String>>>): String =
    filters.joinToString(prefix = "[", postfix = "]", separator = ",") {
        val innerList =
            it.third.joinToString(prefix = "[", postfix = "]", separator = ",") { s ->
                """"$s""""
            }
        """{"${it.first}":{"operator":"${it.second}","values":$innerList}}"""
    }

internal suspend inline fun <reified T> HttpClient.getOrNull(
    urlString: String,
    block: HttpRequestBuilder.() -> Unit = {},
): T? =
    try {
        get(urlString, block).body()
    } catch (e: ResponseException) {
        if (e.response.status == HttpStatusCode.NotFound) {
            null
        } else {
            throw e
        }
    }

internal suspend fun HttpClient.getCsrfToken(urlString: String): String =
    Jsoup.parse(get(urlString).bodyAsText()).select("meta[name=csrf-token]")
        .firstOrNull()
        ?.attr("content")
        ?: throw IllegalStateException("Cannot read csrf token of $urlString")

private suspend inline fun <reified T> HttpClient.getPageAndCount(
    urlString: String,
    offset: Int,
    block: HttpRequestBuilder.() -> Unit = {},
): Pair<Int, List<T>> =
    get {
        url.takeFrom(urlString).apply {
            parameter("offset", "$offset")
            parameter("pageSize", "${overridePageSize ?: PAGE_SIZE}")
        }
        block()
    }.body<OpenProjectCollectionAnswer<T>>().let {
        max(it.total - 1, 0) / max(overridePageSize ?: PAGE_SIZE, it.count) + 1 to it.embedded.elements
    }

internal const val PAGE_SIZE = 500
internal var overridePageSize: Int? = null

internal suspend inline fun <reified T> HttpClient.getAllPages(
    urlString: String,
    block: HttpRequestBuilder.() -> Unit = {},
): List<T> =
    getPageAndCount<T>(urlString, 1, block).let {
        it.second + (2..it.first).map { offset -> getPageAndCount<T>(urlString, offset, block).second }.flatten()
    }

internal fun String.idFromURL(): Int? = if (this.contains("/")) this.substringAfterLast("/").toIntOrNull() else null

@Serializable
private data class OpenProjectCollectionAnswer<T>(
    @SerialName("_type")
    val type: String,
    val total: Int,
    val count: Int,
    val pageSize: Int? = null,
    @SerialName("_embedded")
    val embedded: OpenProjectElementsAnswer<T>,
) : List<T> by embedded

@Serializable
private data class OpenProjectElementsAnswer<T>(
    val elements: List<T>,
) : List<T> by elements

internal fun String.pathParts() = URI(this).path.removePrefix("/").split("/")
