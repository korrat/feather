/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.GroupApi
import dev.maximilian.feather.openproject.api.IOpenProjectGroupApi
import dev.maximilian.feather.openproject.api.IOpenProjectInfoApi
import dev.maximilian.feather.openproject.api.IOpenProjectJobStatusApi
import dev.maximilian.feather.openproject.api.IOpenProjectMembershipApi
import dev.maximilian.feather.openproject.api.IOpenProjectProjectApi
import dev.maximilian.feather.openproject.api.IOpenProjectRoleApi
import dev.maximilian.feather.openproject.api.IOpenProjectStatusApi
import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import dev.maximilian.feather.openproject.api.IOpenProjectWorkPackageApi
import dev.maximilian.feather.openproject.api.InfoApi
import dev.maximilian.feather.openproject.api.JobStatusApi
import dev.maximilian.feather.openproject.api.MembershipApi
import dev.maximilian.feather.openproject.api.ProjectApi
import dev.maximilian.feather.openproject.api.RoleApi
import dev.maximilian.feather.openproject.api.StatusApi
import dev.maximilian.feather.openproject.api.UserApi
import dev.maximilian.feather.openproject.api.WorkPackageApi
import io.ktor.client.HttpClient
import io.ktor.utils.io.core.Closeable

internal class OpenProject(
    internal val apiClient: HttpClient,
    private val webpageClientFunction: () -> HttpClient,
    internal val baseUrl: String,
    jobStatusApi: IOpenProjectJobStatusApi = JobStatusApi(apiClient, baseUrl),
    roleApi: IOpenProjectRoleApi = RoleApi(apiClient, baseUrl),
    userApi: IOpenProjectUserApi = UserApi(apiClient, webpageClientFunction, baseUrl),
    statusApi: IOpenProjectStatusApi = StatusApi(apiClient, baseUrl),
    projectApi: IOpenProjectProjectApi = ProjectApi(apiClient, baseUrl, jobStatusApi),
    workPackageApi: IOpenProjectWorkPackageApi = WorkPackageApi(apiClient, baseUrl),
    infoApi: IOpenProjectInfoApi = InfoApi(webpageClientFunction, baseUrl),
    groupApi: IOpenProjectGroupApi = GroupApi(apiClient, webpageClientFunction, baseUrl, userApi, infoApi),
    membershipApi: IOpenProjectMembershipApi = MembershipApi(apiClient, baseUrl, projectApi, userApi, groupApi),
) : IOpenProject,
    IOpenProjectRoleApi by roleApi,
    IOpenProjectUserApi by userApi,
    IOpenProjectStatusApi by statusApi,
    IOpenProjectProjectApi by projectApi,
    IOpenProjectWorkPackageApi by workPackageApi,
    IOpenProjectGroupApi by groupApi,
    IOpenProjectMembershipApi by membershipApi,
    IOpenProjectJobStatusApi by jobStatusApi,
    IOpenProjectInfoApi by infoApi,
    Closeable by apiClient
