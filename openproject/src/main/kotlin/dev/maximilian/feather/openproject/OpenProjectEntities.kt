/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import java.awt.image.BufferedImage

public class IdLazyEntity<out T>(
    public val id: Int,
    function: (Int) -> T,
) : Lazy<T> by lazy(initializer = { function(id) }) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as IdLazyEntity<*>

        if (id != other.id) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int = value.hashCode()

    override fun toString(): String = if (isInitialized()) {
        value.toString()
    } else {
        "Lazy value not initialized yet. ID: $id"
    }
}

public sealed class OpenProjectEntity {
    public abstract val id: Int
}

public data class OpenProjectRole(
    override val id: Int,
    val name: String,
) : OpenProjectEntity()

public enum class OpenProjectPrincipalType {
    USER,
    GROUP,
}

public sealed class OpenProjectPrincipal : OpenProjectEntity() {
    public val type: OpenProjectPrincipalType by lazy {
        when (this) {
            is OpenProjectUser -> OpenProjectPrincipalType.USER
            is OpenProjectGroup -> OpenProjectPrincipalType.GROUP
        }
    }
}

public class OpenProjectPrincipalLazyEntity(
    public val id: Int,
    public val type: OpenProjectPrincipalType,
    function: (Int) -> OpenProjectPrincipal?,
) : Lazy<OpenProjectPrincipal?> by lazy(initializer = { function(id) }) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as OpenProjectPrincipalLazyEntity

        if (id != other.id) return false
        if (type != other.type) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int = value.hashCode()

    override fun toString(): String = if (isInitialized()) {
        value.toString()
    } else {
        "Lazy value not initialized yet. ID: $id, type: $type"
    }
}

public data class OpenProjectUser(
    override val id: Int,
    val login: String,
    val email: String?,
    val firstName: String,
    val lastName: String,
    val admin: Boolean,
    val status: String,
    val authSource: Int,
    val avatar: OpenProjectUserAvatar,
) : OpenProjectPrincipal()

public sealed interface OpenProjectUserAvatar : Lazy<BufferedImage?>

public data class OpenProjectGroup(
    override val id: Int,
    val name: String,
    val members: List<IdLazyEntity<OpenProjectUser?>>,
) : OpenProjectPrincipal()

public data class OpenProjectStatus(
    override val id: Int,
    val name: String,
    val closed: Boolean,
) : OpenProjectEntity()

public data class OpenProjectDescription(
    val format: String,
    val raw: String? = null,
    val html: String,
)

public data class OpenProjectProject(
    override val id: Int,
    val name: String,
    val identifier: String,
    val description: OpenProjectDescription,
    val parent: IdLazyEntity<OpenProjectProject?>?,
) : OpenProjectEntity() {
    public constructor(
        id: Int,
        name: String,
        identifier: String,
        description: OpenProjectDescription,
        parent: OpenProjectProject?,
    ) : this(
        id,
        name,
        identifier,
        description,
        parent?.let { IdLazyEntity<OpenProjectProject?>(it.id) { _ -> it } },
    )

    init {
        require(name.isNotBlank()) { "Project name must be not blank" }
    }
}

public data class OpenProjectWorkPackage(
    override val id: Int,
    val subject: String,
    val description: OpenProjectDescription,
    val startDate: String?,
    val dueDate: String?,
    val percentageDone: Int,
    val status: OpenProjectStatus,
    // Here to test getWorkPackagesOfUser(), not production ready / well implemented
    internal val lockVersion: Int,
) : OpenProjectEntity()

public data class OpenProjectMembership(
    override val id: Int,
    val project: IdLazyEntity<OpenProjectProject?>,
    val principal: OpenProjectPrincipalLazyEntity,
    val roles: List<OpenProjectRole>,
) : OpenProjectEntity() {
    public constructor(
        id: Int,
        project: OpenProjectProject,
        principal: OpenProjectPrincipal,
        roles: List<OpenProjectRole>,
    ) : this(
        id,
        IdLazyEntity(project.id) { project },
        OpenProjectPrincipalLazyEntity(principal.id, principal.type) { principal },
        roles,
    )
}

public enum class OpenProjectCloneFields {
    BOARDS,
    CATEGORIES,
    FORUMS,
    MEMBERS,
    OVERVIEW,
    QUERIES,
    VERSIONS,
    WIKI,
    WIKI_PAGE_ATTACHMENTS,
    WORK_PACKAGE_ATTACHMENTS,
    WORK_PACKAGES,
}

public enum class OpenProjectJobStatusStatus {
    // See https://github.com/opf/openproject/blob/1af65d3b6c73573a55b61a0bc4f817a932d51e28/modules/job_status/app/models/job_status/status.rb#L1
    // Currently (OP 11.3.2) not documented :(
    IN_QUEUE,
    ERROR,
    IN_PROCESS,
    SUCCESS,
    FAILURE,
    CANCELLED,
}

internal data class OpenProjectJobStatus<T : OpenProjectEntity>(
    val id: String,
    val status: OpenProjectJobStatusStatus,
    val message: String?,
    val errors: Set<String>,
    val refObject: IdLazyEntity<T?>?,
)

public object EmptyAvatar : OpenProjectUserAvatar {
    private val hashCode = LazyLoadingAvatar({ throw IllegalStateException("Never called") }, "").hashCode()

    override val value: BufferedImage? = null

    override fun isInitialized(): Boolean = true

    override fun equals(other: Any?): Boolean =
        if (other is LazyLoadingAvatar) other.source.isBlank() else super.equals(other)

    override fun hashCode(): Int = hashCode
}
