/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.IdLazyEntity
import dev.maximilian.feather.openproject.OpenProjectEntity
import dev.maximilian.feather.openproject.OpenProjectJobStatus
import dev.maximilian.feather.openproject.OpenProjectJobStatusStatus
import io.ktor.client.HttpClient
import kotlinx.serialization.Serializable
import java.util.Locale

internal interface IOpenProjectJobStatusApi {
    suspend fun <T : OpenProjectEntity> getJob(
        id: String,
        supplier: suspend (id: Any) -> T?,
    ): OpenProjectJobStatus<T>?
}

internal class JobStatusApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IOpenProjectJobStatusApi {
    companion object {
        private fun jobStatusUrl(
            baseUrl: String,
            id: String,
        ) = "$baseUrl/api/v3/job_statuses/$id"
    }

    override suspend fun <T : OpenProjectEntity> getJob(
        id: String,
        supplier: suspend (id: Any) -> T?,
    ): OpenProjectJobStatus<T>? = client.getOrNull<JobStatusAnswer>(jobStatusUrl(baseUrl, id))?.toOpenProjectEntity(supplier)
}

private suspend fun <T : OpenProjectEntity> JobStatusAnswer.toOpenProjectEntity(supplier: suspend (id: Any) -> T?) =
    OpenProjectJobStatus(
        id = jobId,
        status = OpenProjectJobStatusStatus.valueOf(status.uppercase(Locale.getDefault())),
        message = message,
        errors = payload.errors ?: emptySet(),
        refObject =
        payload.redirect?.let {
            val pathParts = it.pathParts()

            require(pathParts.size >= 2) { "The redirect path must minimally contain 2 parts (The entity type and identifier)" }

            val type = pathParts[pathParts.size - 2]
            val id = pathParts.last()

            require(type == "projects") { "Currently only projects are supported for the job status endpoint" }
            require(id.isNotBlank()) { "Blank identifier found in project redirect url" }

            // Since we do not know the id, we are loading the refObject directly and not lazy
            val project = supplier(id)

            requireNotNull(project) { "Project which was redirected to does not exist" }

            IdLazyEntity(project.id) { project }
        },
    )

@Serializable
private data class JobStatusAnswer(
    val jobId: String,
    val status: String,
    val message: String? = null,
    val payload: JobStatusAnswerPayload,
)

@Serializable
private data class JobStatusAnswerPayload(
    val title: String,
    val errors: Set<String>? = null,
    val redirect: String? = null,
)
