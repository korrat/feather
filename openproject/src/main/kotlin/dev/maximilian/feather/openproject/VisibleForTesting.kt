package dev.maximilian.feather.openproject

@RequiresOptIn(message = "This API is only available for usage in tests")
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
/**
 * Each method annotated with this annotation should only be used in tests, nowhere else
 */
internal annotation class VisibleForTesting
