/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.OpenProjectRole
import io.ktor.client.HttpClient
import kotlinx.serialization.Serializable

public interface IOpenProjectRoleApi {
    public suspend fun getRoles(): List<OpenProjectRole>
}

internal class RoleApi(
    private val client: HttpClient,
    private val baseUrl: String,
) : IOpenProjectRoleApi {
    companion object {
        internal fun roleUrl(
            baseUrl: String,
            id: Int? = null,
        ) = "$baseUrl/api/v3/roles" + (id?.let { "/$it" } ?: "")
    }

    override suspend fun getRoles(): List<OpenProjectRole> =
        client.getAllPages<OpenProjectRoleAnswer>(roleUrl(baseUrl)).toOpenProjectEntity()
}

@Serializable
private data class OpenProjectRoleAnswer(
    val id: Int,
    val name: String,
)

private fun OpenProjectRoleAnswer.toOpenProjectEntity() =
    OpenProjectRole(
        id = id,
        name = name,
    )

private fun Iterable<OpenProjectRoleAnswer>.toOpenProjectEntity() = map { it.toOpenProjectEntity() }
