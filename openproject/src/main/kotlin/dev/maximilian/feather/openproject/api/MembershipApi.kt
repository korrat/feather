/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.IdLazyEntity
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectPrincipalLazyEntity
import dev.maximilian.feather.openproject.OpenProjectPrincipalType
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.OpenProjectRole
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.delete
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

public interface IOpenProjectMembershipApi {
    public suspend fun createMembership(memberShip: OpenProjectMembership): OpenProjectMembership

    public suspend fun getMemberships(roles: List<OpenProjectRole>): List<OpenProjectMembership>

    public suspend fun getMembership(
        memberShipID: Int,
        roles: List<OpenProjectRole>,
    ): OpenProjectMembership?

    public suspend fun deleteMembership(memberShip: OpenProjectMembership)
}

internal class MembershipApi(
    private val client: HttpClient,
    private val baseUrl: String,
    private val projectApi: IOpenProjectProjectApi,
    private val userApi: IOpenProjectUserApi,
    private val groupApi: IOpenProjectGroupApi,
) : IOpenProjectMembershipApi {
    companion object {
        private fun memberShipUrl(
            baseUrl: String,
            id: Int? = null,
        ) = "$baseUrl/api/v3/memberships" + (id?.let { "/$it" } ?: "")
    }

    override suspend fun createMembership(memberShip: OpenProjectMembership): OpenProjectMembership {
        val answer =
            client.post(memberShipUrl(baseUrl)) {
                setBody(
                    MembershipCreateRequest(
                        links =
                        MembershipAnswerLinks(
                            roles = memberShip.roles.map { HrefAnswer(RoleApi.roleUrl(baseUrl, it.id)) },
                            project = HrefAnswer(ProjectApi.projectUrl(baseUrl, memberShip.project.id)),
                            principal =
                            HrefAnswer(
                                when (memberShip.principal.type) {
                                    OpenProjectPrincipalType.USER -> UserApi.userUrl(baseUrl, memberShip.principal.id)
                                    OpenProjectPrincipalType.GROUP -> GroupApi.groupUrl(baseUrl, memberShip.principal.id)
                                },
                            ),
                        ),
                    ),
                )
            }.body<MembershipAnswer>()

        // Don't touch or lazy load any dependencies, as this is not necessary to perform this operation
        return memberShip.copy(id = answer.id)
    }

    override suspend fun getMemberships(roles: List<OpenProjectRole>): List<OpenProjectMembership> =
        client.getAllPages<MembershipAnswer>(memberShipUrl(baseUrl)).toOpenProjectEntity(roles.associateBy { it.id })

    override suspend fun getMembership(
        memberShipID: Int,
        roles: List<OpenProjectRole>,
    ): OpenProjectMembership? =
        client.getOrNull<MembershipAnswer>(memberShipUrl(baseUrl, memberShipID))
            ?.toOpenProjectEntity(roles.associateBy { it.id }, mutableMapOf(), mutableMapOf())

    override suspend fun deleteMembership(memberShip: OpenProjectMembership) {
        client.delete(memberShipUrl(baseUrl, memberShip.id)) {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            setBody("")
        }.body<Unit>()
    }

    private fun MembershipAnswer.toOpenProjectEntity(
        roles: Map<Int, OpenProjectRole>,
        lazyLoadPrincipal: MutableMap<Pair<Int, OpenProjectPrincipalType>, OpenProjectPrincipalLazyEntity>,
        lazyLoadProject: MutableMap<Int, IdLazyEntity<OpenProjectProject?>>,
    ) = OpenProjectMembership(
        id = id,
        principal =
        links.principal.href?.idFromURL()?.let {
            val type = links.principal.href.toOpenProjectPrincipalType() ?: return@let null

            lazyLoadPrincipal.getOrPut(it to type) {
                OpenProjectPrincipalLazyEntity(it, type) {
                    runBlocking {
                        when (type) {
                            OpenProjectPrincipalType.GROUP -> groupApi.getGroup(it)
                            OpenProjectPrincipalType.USER -> userApi.getUser(it)
                        }
                    }
                }
            }
        }
            ?: throw IllegalStateException("The linked project does not contain an id, answer was: ${links.project.href}"),
        project =
        links.project.href?.idFromURL()?.let {
            lazyLoadProject.getOrPut(it) {
                IdLazyEntity(it) { runBlocking { projectApi.getProject(it) } }
            }
        }
            ?: throw IllegalStateException("The linked project does not contain an id, answer was: ${links.project.href}"),
        roles =
        links.roles.map {
            it.href?.idFromURL()?.let { id -> roles[id] } ?: OpenProjectRole(-1, "unknown")
        },
    )

    private fun Iterable<MembershipAnswer>.toOpenProjectEntity(roles: Map<Int, OpenProjectRole>): List<OpenProjectMembership> {
        val lazyLoadPrincipal = mutableMapOf<Pair<Int, OpenProjectPrincipalType>, OpenProjectPrincipalLazyEntity>()
        val lazyLoadProject = mutableMapOf<Int, IdLazyEntity<OpenProjectProject?>>()

        return map { it.toOpenProjectEntity(roles, lazyLoadPrincipal, lazyLoadProject) }
    }
}

@Serializable
private data class MembershipAnswer(
    val id: Int,
    @SerialName("_links")
    val links: MembershipAnswerLinks,
)

@Serializable
private data class MembershipCreateRequest(
    @SerialName("_links")
    val links: MembershipAnswerLinks,
)

@Serializable
private data class MembershipAnswerLinks(
    val project: HrefAnswer,
    val principal: HrefAnswer,
    val roles: List<HrefAnswer>,
)

private fun String.toOpenProjectPrincipalType(): OpenProjectPrincipalType? =
    when {
        contains("groups") -> OpenProjectPrincipalType.GROUP
        contains("users") -> OpenProjectPrincipalType.USER
        else -> null
    }
