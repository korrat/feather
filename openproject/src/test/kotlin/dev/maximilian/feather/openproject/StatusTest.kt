/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectStatusApi
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class StatusTest {
    private val api: IOpenProjectStatusApi = TestUtil.openProject

    @Test
    fun `Get All status returns OpenProject standard statuses`() {
        val actual = runBlocking { api.getStatus().map { it.copy(id = 0) }.toSet() }
        val expected =
            setOf(
                OpenProjectStatus(id = 0, name = "New", closed = false),
                OpenProjectStatus(id = 0, name = "In specification", closed = false),
                OpenProjectStatus(id = 0, name = "Specified", closed = false),
                OpenProjectStatus(id = 0, name = "Confirmed", closed = false),
                OpenProjectStatus(id = 0, name = "To be scheduled", closed = false),
                OpenProjectStatus(id = 0, name = "Scheduled", closed = false),
                OpenProjectStatus(id = 0, name = "In progress", closed = false),
                OpenProjectStatus(id = 0, name = "Developed", closed = false),
                OpenProjectStatus(id = 0, name = "In testing", closed = false),
                OpenProjectStatus(id = 0, name = "Tested", closed = false),
                OpenProjectStatus(id = 0, name = "Test failed", closed = false),
                OpenProjectStatus(id = 0, name = "Closed", closed = true),
                OpenProjectStatus(id = 0, name = "On hold", closed = false),
                OpenProjectStatus(id = 0, name = "Rejected", closed = true),
            )

        assertEquals(expected, actual, "Expected the standard OpenProject statuses")
    }
}
