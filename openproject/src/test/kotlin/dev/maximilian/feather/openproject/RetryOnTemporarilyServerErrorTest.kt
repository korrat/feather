/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openproject

import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.server.application.call
import io.ktor.server.engine.embeddedServer
import io.ktor.server.engine.stop
import io.ktor.server.netty.Netty
import io.ktor.server.response.respond
import io.ktor.server.routing.get
import io.ktor.server.routing.routing
import kotlinx.coroutines.runBlocking
import java.net.ServerSocket
import java.util.UUID
import java.util.concurrent.TimeUnit
import kotlin.test.Test
import kotlin.test.assertEquals

class RetryOnTemporarilyServerErrorTest {
    @OptIn(VisibleForTesting::class)
    @Test
    fun `2 times retry is working`() {
        val responseContent = UUID.randomUUID().toString()
        var times = 0
        val port = getFreePort()

        val server =
            embeddedServer(Netty, host = "0.0.0.0", port = port) {
                routing {
                    get("/") {
                        times++

                        // Times is incremented before it is used
                        if (times < 3) {
                            call.response.status(HttpStatusCode.BadGateway)
                        } else {
                            call.respond(responseContent)
                            call.response.status(HttpStatusCode.OK)
                        }
                    }
                }
            }.start()

        val client =
            HttpClient {
                expectSuccess = true
                OpenProjectFactory().configureRetryForTesting(this)
            }
        val response = runBlocking { client.get("http://127.0.0.1:$port").bodyAsText() }

        assertEquals(responseContent, response)
        assertEquals(3, times) // times is incremented once more than needed
        server.stop(1, 5, TimeUnit.SECONDS)
    }

    private fun getFreePort(): Int {
        val socket = ServerSocket(0)
        val port = socket.localPort
        socket.close()
        return port
    }
}
