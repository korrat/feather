/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.testutils.getEnv
import java.util.Locale
import java.util.UUID
import kotlin.random.Random

object TestUtil {
    // Host and api baseUrl
    private val host: String = getEnv("OPENPROJECT_HOST", "http://127.0.0.1:8081")

    // Api auth
    private val authUser = getEnv("OPENPROJECT_USER", "admin")
    private val authPassword = getEnv("OPENPROJECT_PASSWORD", "admin")

    // SSO Webpage Auth
    private val ssoHeader: String = getEnv("OPENPROJECT_SSO_HEADER", "X-Remote-User")
    private val ssoSecret: String = getEnv("OPENPROJECT_SSO_SECRET", "s3cr3t")
    private val ssoAdmin: String = getEnv("OPENPROJECT_SSO_ADMIN", "sso_admin")

    internal val openProject =
        OpenProjectFactory().create(
            host,
            authUser,
            authPassword,
            ssoHeader,
            ssoAdmin,
            ssoSecret,
            3,
        )
}

internal fun generateName() = UUID.randomUUID().toString().replace("-", "").substring(2)

internal fun generateMailAddress() = "test+${UUID.randomUUID()}@example.org"

internal fun generateTestUser(
    id: Int = 0,
    firstName: String = "Test",
    lastName: String = generateName(),
    status: String = "active",
    email: String = generateMailAddress(),
    admin: Boolean = false,
    login: String = lastName.lowercase(Locale.getDefault()),
) = OpenProjectUser(id, login, email, firstName, lastName, admin, status, 0, EmptyAvatar)

internal fun generateTestGroup(
    id: Int = 0,
    name: String = generateName(),
    members: List<OpenProjectUser> = emptyList(),
) = OpenProjectGroup(id, name, members.map { m -> IdLazyEntity(m.id) { m } })

private val charPool = ('0'..'9') + ('a'..'z') + ('A'..'Z')

private fun randomString() =
    (1..32)
        .map { Random.nextInt(0, charPool.size) }
        .map(charPool::get)
        .joinToString("")

internal fun generateTestProject(
    id: Int = 0,
    name: String = randomString(),
    identifier: String = "lower-${name.lowercase(Locale.getDefault())}",
    parent: IdLazyEntity<OpenProjectProject?>? = null,
) = OpenProjectProject(
    id = id,
    name = name,
    identifier = identifier,
    description =
    OpenProjectDescription(
        html = "<p class=\"op-uc-p\">TestProject $name with identifier $identifier and parent ${parent?.id}</p>",
        raw = "TestProject $name with identifier $identifier and parent ${parent?.id}",
        format = "markdown",
    ),
    parent = parent,
)

internal val SCRUM_PROJECT =
    OpenProjectProject(
        id = 0,
        name = "Scrum project",
        identifier = "your-scrum-project",
        description =
        OpenProjectDescription(
            html = "<p class=\"op-uc-p\">This is a short summary of the goals of this demo Scrum project.</p>",
            raw = "This is a short summary of the goals of this demo Scrum project.",
            format = "markdown",
        ),
        parent = null as OpenProjectProject?,
    )

internal val DEMO_PROJECT =
    OpenProjectProject(
        id = 0,
        name = "Demo project",
        identifier = "demo-project",
        description =
        OpenProjectDescription(
            html = "<p class=\"op-uc-p\">This is a short summary of the goals of this demo project.</p>",
            raw = "This is a short summary of the goals of this demo project.",
            format = "markdown",
        ),
        parent = null as OpenProjectProject?,
    )
