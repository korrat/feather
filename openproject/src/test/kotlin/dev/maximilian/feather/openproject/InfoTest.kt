package dev.maximilian.feather.openproject

import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertDoesNotThrow
import kotlin.test.Test

class InfoTest {
    private val api = TestUtil.openProject

    @Test
    fun `getOpenProjectVersion returns valid semVer version`() {
        runBlocking {
            assertDoesNotThrow {
                api.getOpenProjectVersion()
            }
        }
    }
}
