/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectProjectApi
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.Locale
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ProjectTest {
    private val api: IOpenProjectProjectApi = TestUtil.openProject

    @Test
    fun `Default generated projects are existing`() {
        val allProjects = runBlocking { api.getProjects() }.map { it.copy(id = 0) }.toSet()

        assertTrue(allProjects.contains(DEMO_PROJECT))
    }

    @Test
    fun `GetProjectByIdentifier returns correct project`() {
        runBlocking {
            // Create some "noise"
            (0 until 3).map { async { generateTestProject().let { api.createProject(it) } } }.awaitAll()

            launch {
                assertEquals(
                    DEMO_PROJECT,
                    api.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)?.copy(id = 0),
                )
            }
            launch { assertEquals(DEMO_PROJECT, api.getProjectByIdentifierOrName(DEMO_PROJECT.name)?.copy(id = 0)) }

            launch {
                assertEquals(
                    SCRUM_PROJECT,
                    api.getProjectByIdentifierOrName(SCRUM_PROJECT.identifier)?.copy(id = 0),
                )
            }
            launch { assertEquals(SCRUM_PROJECT, api.getProjectByIdentifierOrName(SCRUM_PROJECT.name)?.copy(id = 0)) }
        }
    }

    @Test
    fun `Get project works`() {
        runBlocking {
            val created = api.createProject(generateTestProject())
            val retrieved = api.getProject(created.id)

            assertEquals(created, retrieved)
        }
    }

    @Test
    fun `Create project works`() {
        val toCreate = generateTestProject()
        val created = runBlocking { api.createProject(toCreate) }

        assertEquals(toCreate, created.copy(id = 0))
    }

    @Test
    fun `Create project with parent works`() {
        val parent = generateTestProject().let { runBlocking { api.createProject(it) } }
        val sub =
            generateTestProject(parent = IdLazyEntity(parent.id) { parent }).let { runBlocking { api.createProject(it) } }
        val subSub =
            generateTestProject(parent = IdLazyEntity(sub.id) { sub }).let { runBlocking { api.createProject(it) } }

        val subSubParent =
            subSub.parent.let {
                assertNotNull(it)
                assertFalse(it.isInitialized())
                it.value
            }

        assertNotNull(subSubParent)
        assertEquals(sub.id, subSubParent.id)

        val subSubParentParent =
            subSubParent.parent.let {
                assertNotNull(it)
                assertFalse(it.isInitialized())
                it.value
            }

        assertNotNull(subSubParentParent)
        assertEquals(parent.id, subSubParentParent.id)
    }

    @Test
    fun `Delete project works`() {
        runBlocking {
            val toCreate = generateTestProject()
            val created = api.createProject(toCreate)

            api.deleteProject(created)
            require(api.getProject(created.id) == null) { "Project was not deleted successfully" }
        }
    }

    @Test
    fun `Parents of projects retrieved in the same request are the same lazy load entity`() {
        runBlocking {
            val parent = api.createProject(generateTestProject())
            val sub1Async = async { api.createProject(generateTestProject(parent = IdLazyEntity(parent.id) { parent })) }
            val sub2Async = async { api.createProject(generateTestProject(parent = IdLazyEntity(parent.id) { parent })) }

            val (sub1, sub2) = sub1Async.await() to sub2Async.await()
            val projects = api.getProjects().filter { it.id == sub1.id || it.id == sub2.id }

            assertEquals(2, projects.size)
            assertTrue { projects[0].parent === projects[1].parent }
        }
    }

    @Test
    fun `Clone project works`() {
        runBlocking {
            val toClone = api.createProject(generateTestProject())
            val cloneName = UUID.randomUUID().toString().uppercase(Locale.getDefault())

            val clone = api.cloneProject(toClone, "a-$cloneName", null, listOf(OpenProjectCloneFields.WORK_PACKAGES))

            assertEquals(
                toClone,
                clone.copy(
                    id = toClone.id,
                    name = toClone.name,
                    identifier = toClone.identifier,
                    description = toClone.description.copy(raw = toClone.description.raw),
                ),
            )
        }
    }

    @Test
    fun `Clone subproject works`() {
        runBlocking {
            val toClone = api.createProject(generateTestProject())
            val subProject = api.createProject(generateTestProject(parent = IdLazyEntity(toClone.id) { toClone }))
            val cloneName = UUID.randomUUID().toString().uppercase(Locale.getDefault())

            val clone = api.cloneProject(subProject, "a-$cloneName", null, listOf(OpenProjectCloneFields.WORK_PACKAGES))

            assertEquals(
                subProject,
                clone.copy(
                    id = subProject.id,
                    name = subProject.name,
                    identifier = subProject.identifier,
                    description = subProject.description.copy(raw = subProject.description.raw),
                ),
            )
        }
    }
}
