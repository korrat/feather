/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.testutils

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

public object TestUser {
    private val setTime = Instant.now().truncatedTo(ChronoUnit.MINUTES).minusSeconds(180)

    public val NORMAL_USER: User =
        User(
            id = 0,
            username = "test",
            displayName = "Normal User",
            firstname = "Normal",
            surname = "User",
            mail = "normal.user@example.org",
            groups = emptySet(),
            registeredSince = setTime,
            ownedGroups = setOf(),
            permissions = setOf(Permission.USER),
            disabled = false,
            lastLoginAt = setTime,
        )

    public const val NORMAL_USER_PASSWORD: String = "secret"

    public val ADMIN_USER: User =
        User(
            id = 0,
            username = "admin",
            displayName = "Admin User",
            firstname = "Admin",
            surname = "User",
            mail = "admin.user@example.org",
            groups = emptySet(),
            registeredSince = setTime,
            ownedGroups = setOf(),
            permissions = setOf(Permission.ADMIN),
            disabled = false,
            lastLoginAt = setTime,
        )

    public const val ADMIN_USER_PASSWORD: String = "admin-secret"

    public fun generateTestUser(
        id: Int = 0,
        username: String = "Username-${UUID.randomUUID()}",
        displayName: String = "a${UUID.randomUUID().toString().replace("-", "")}",
        firstname: String = "${UUID.randomUUID()}",
        surname: String = "${UUID.randomUUID()}",
        mail: String = "test-${UUID.randomUUID()}@example.org",
        groups: Set<Int> = emptySet(),
        registeredSince: Instant = Instant.now(),
        ownedGroups: Set<Int> = emptySet(),
        permissions: Set<Permission> = emptySet(),
        disabled: Boolean = false,
        lastLoginAt: Instant = Instant.ofEpochSecond(0),
    ): User =
        User(
            id = id,
            username = username,
            displayName = displayName,
            firstname = firstname,
            surname = surname,
            mail = mail,
            groups = groups,
            registeredSince = registeredSince,
            ownedGroups = ownedGroups,
            permissions = permissions,
            disabled = disabled,
            lastLoginAt = lastLoginAt,
        )
}
