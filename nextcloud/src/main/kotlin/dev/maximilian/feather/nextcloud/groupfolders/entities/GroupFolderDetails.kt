/*
 *
 *  *    Copyright [2021] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders.entities

import dev.maximilian.feather.nextcloud.TransformEmptyArrayAsObjectSerializer
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.PermissionTypeSetSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer

@Serializable
public data class GroupFolderManageDetails(
    @SerialName("type")
    val type: String,
    @SerialName("id")
    val id: String,
    @SerialName("displayname")
    val displayName: String,
)

internal object GroupFolderGroupDetailsSerializer :
    TransformEmptyArrayAsObjectSerializer<Map<String, Set<PermissionType>>>(
        MapSerializer(
            String.serializer(),
            PermissionTypeSetSerializer,
        ),
    )

@Serializable
public data class GroupFolderDetails(
    @SerialName("id")
    val id: Int,
    @SerialName("mount_point")
    val mountPoint: String,
    // empty array "[]" or a map of groups with their permissions
    @SerialName("groups")
    @Serializable(with = GroupFolderGroupDetailsSerializer::class)
    val groups: Map<String, Set<PermissionType>>? = null,
    @SerialName("quota")
    val quota: Long,
    @SerialName("size")
    val size: Long, // needs 64-bit integer
    @SerialName("acl")
    val acl: Boolean,
    @SerialName("manage")
    val manage: List<GroupFolderManageDetails>? = null,
)
