package dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern

import com.github.sardine.Sardine
import java.io.InputStream
import javax.xml.namespace.QName

internal class WebdavReal(private val sardine: Sardine) : IWebdav {
    override fun createDirectory(path: String) {
        sardine.createDirectory(path)
    }

    override fun delete(path: String) {
        sardine.delete(path)
    }

    override fun copy(
        source: String,
        destination: String,
    ) {
        sardine.copy(source, destination)
    }

    override fun put(
        path: String,
        content: String,
    ) {
        sardine.put(path, content.toByteArray(), "text/markdown")
    }

    override fun get(path: String): InputStream {
        return sardine.get(path)
    }

    override fun list(
        path: String,
        withAcl: Boolean,
    ): List<DirectoryEntry> {
        val t =
            if (withAcl) {
                val props =
                    setOf(
                        QName("http://nextcloud.org/ns", "acl-list", "nc"),
                        QName("http://nextcloud.org/ns", "inherited-acl-list", "nc"),
                        QName("http://nextcloud.org/ns", "group-folder-id", "nc"),
                        QName("http://nextcloud.org/ns", "acl-enabled", "nc"),
                        QName("http://nextcloud.org/ns", "acl-can-manage", "nc"),
                    )
                sardine.list(path, 1, props)
            } else {
                sardine.list(path)
            }

        return t.map {
            DirectoryEntry(
                it.name,
                it.isDirectory,
            )
        }
    }
}
