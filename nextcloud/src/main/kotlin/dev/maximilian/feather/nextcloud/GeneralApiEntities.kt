package dev.maximilian.feather.nextcloud

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject

@Serializable
internal data class Ocs<T>(
    val ocs: OcsEntity<T>,
)

@Serializable
internal data class OcsEntity<T>(
    val meta: MetaEntity,
    val data: T,
)

@Serializable
internal data class MetaEntity(
    @SerialName("status")
    val status: String,
    @SerialName("statuscode")
    val statusCode: Int,
    @SerialName("message")
    val message: String,
)

@Serializable
internal data class SuccessResponse(
    val success: Boolean,
)

internal open class TransformEmptyArrayAsObjectSerializer<T : Any>(tSerializer: KSerializer<T>) : JsonTransformingSerializer<T>(
    tSerializer,
) {
    override fun transformDeserialize(element: JsonElement): JsonElement =
        if (element is JsonArray) {
            buildJsonObject { }
        } else {
            element
        }
}
