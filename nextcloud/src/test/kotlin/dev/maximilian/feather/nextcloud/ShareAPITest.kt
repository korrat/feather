/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import com.github.sardine.SardineFactory
import dev.maximilian.feather.nextcloud.ocs.GroupAPI
import dev.maximilian.feather.nextcloud.ocs.NextcloudShare
import dev.maximilian.feather.nextcloud.ocs.ShareAPI
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import dev.maximilian.feather.nextcloud.webdavAPI.WebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.WebdavReal
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeAll
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class ShareAPITest {
    companion object {
        private val shareAPI = ShareAPI(TestUtil.nextcloud.apiClient, TestUtil.baseUrl)
        private val webdavURL =
            getEnv(
                "NEXTCLOUD_WEBDAV",
                TestUtil.baseUrl + "/remote.php/dav/files/" + TestUtil.user + "/",
            )

        private val webdavAPI =
            WebdavAPI(
                webdavURL,
                WebdavReal(SardineFactory.begin(TestUtil.user, TestUtil.password)),
            )

        @BeforeAll
        @JvmStatic
        fun setup() {
            val groupAPI = GroupAPI(TestUtil.nextcloud.apiClient, TestUtil.baseUrl)
            runBlocking { groupAPI.addGroup(TestUtil.GROUP_TO_SHARE_WITH) }
        }
    }

    @Test
    fun `Test getAllShares and getSpecificShareEntity`() {
        var shares = runBlocking { shareAPI.getAllShares() }

        if (shares.isEmpty()) {
            // test at least once with a share
            createShareForFolder(generateTestShare())

            shares = runBlocking { shareAPI.getAllShares() }
        }

        assert(shares.isNotEmpty())

        shares.forEach {
            check(it.id != 0)
            check(it.path.isNotEmpty())
        }

        val t = runBlocking { shareAPI.getSpecificShareEntity(shares.first().id) }
        requireNotNull(t)
        check(t.id != 0)
        check(t.path.isNotEmpty())
    }

    @Test
    fun `Test createDeleteShare`() {
        val shareUrlID = createShareForFolder(generateTestShare())

        checkNotNull(shareUrlID > 0)

        val share = runBlocking { shareAPI.getSpecificShareEntity(shareUrlID) }
        checkNotNull(share)

        val shares = runBlocking { shareAPI.getAllShares() }
        check(shares.count { it.id == shareUrlID } == 1)

        runBlocking { shareAPI.deleteShare(shareUrlID) }

        assertNull(runBlocking { shareAPI.getSpecificShareEntity(shareUrlID) })

        val shares2 = runBlocking { shareAPI.getAllShares() }
        check(shares2.none { it.id == shareUrlID })
    }

    @Test
    fun `Test createShareForInvalidFolderFails`() {
        val testShare = generateTestShare()

        val folders = webdavAPI.listFolders("")
        // the folder of the test share should not exist
        check(!folders.contains(testShare.path.substring(1)))

        assertFailsWith<java.lang.Exception> {
            runBlocking { shareAPI.createShare(testShare) }
        }
    }

    @Test
    fun `Test createShareForInvalidUserFails`() {
        // generate share for fake user ID
        val testShare =
            generateTestShare(
                userOrGroupIDtoShareWith = UUID.randomUUID().toString().replace("-", ""),
            )

        // call createFolder without the leading "/"
        webdavAPI.createFolder(testShare.path.substring(1))

        assertFailsWith<java.lang.Exception> {
            runBlocking { shareAPI.createShare(testShare) }
        }
    }

    @Test
    fun `Test updateShare`() {
        val shareUrlID = createShareForFolder(generateTestShare())

        checkNotNull(shareUrlID > 0)

        val share = runBlocking { shareAPI.getSpecificShareEntity(shareUrlID) }
        checkNotNull(share)
        check(share.permissions == PermissionType.values().toSet())

        runBlocking { shareAPI.updateShare(shareUrlID, false, setOf(PermissionType.Read)) }

        val share2 = runBlocking { shareAPI.getSpecificShareEntity(shareUrlID) }
        checkNotNull(share2)
        check(share2.permissions == setOf(PermissionType.Read))
    }

    @Test
    fun `Test createPublicShare`() {
        // generate share with public link
        val testShare =
            generateTestShare(
                type = ShareType.PublicLink,
                publicUpload = false,
                permissions = setOf(PermissionType.Create),
            )

        val shareUrlID = createShareForFolder(testShare)
        checkNotNull(shareUrlID > 0)

        val share = runBlocking { shareAPI.getSpecificShareEntity(shareUrlID) }
        checkNotNull(share)

        assertEquals(
            setOf(PermissionType.Read, PermissionType.Create, PermissionType.Share),
            share.permissions,
        ) // or PermissionType.values().toSet() if publicUpload == true
        check(share.canEdit)
        checkNotNull(share.token)
        checkNotNull(share.url)
    }

    private fun createShareForFolder(share: NextcloudShare): Int {
        // call createFolder without the leading "/"
        webdavAPI.createFolder(share.path.substring(1))
        Thread.sleep(3000) // wait to avoid "File does not exist" - Response from ShareAPI on Git CI after Push
        val shareID = runBlocking { shareAPI.createShare(share).id }
        return shareID
    }
}
