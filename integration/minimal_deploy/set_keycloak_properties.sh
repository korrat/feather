#!/bin/bash
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

usage() {
  echo "usage: ./set_keycloak_properties.sh \$CLIENT_TOKEN" >&2
}

if [ -z "$1" ]; then
  usage
  echo "Error: client token is missing." >&2
  exit 1
fi
CLIENT_TOKEN="$1"


MY_DIR="$(dirname "$0")"
#docker-compose --project-directory "$MY_DIR" exec feather-postgres \
docker exec -it feather-postgres \
  psql -U postgres postgres \
  -c "UPDATE properties SET value='false' WHERE key='authorizer.credential'" \
  -c "UPDATE properties SET value='[\"keycloak\"]' WHERE key='authorizer.oidc'" \
  -c "INSERT INTO properties (key, value) \
  VALUES \
    ('authorizer.oidc.keycloak.apiProvider', 'AUTO_DISCOVER'), \
    ('authorizer.oidc.keycloak.name', 'keycloak'), \
    ('authorizer.oidc.keycloak.url.discover', 'http://localhost:8080/realms/master'), \
    ('authorizer.oidc.keycloak.url.icon', 'account-key'), \
    ('authorizer.oidc.keycloak.color', 'rgb(0, 169, 224)'), \
    ('authorizer.oidc.keycloak.apiKey', 'feather'), \
    ('authorizer.oidc.keycloak.apiSecret', '$CLIENT_TOKEN')"
