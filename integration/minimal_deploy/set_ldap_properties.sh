#!/bin/bash
#
#    Copyright [2022] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

usage() {
  echo "usage: ./set_ldap_properties.sh \$LDAP_HOST \$LDAP_BASE_DN \$LDAP_BIND_DN [\$LDAP_PORT] [\$LDAP_MODE]" >&2
}

if [ -z "$1" ]; then
  usage
  echo "Error: LDAP host is missing." >&2
  exit 1
fi
if [ -z "$2" ]; then
  usage
  echo "Error: LDAP base DN is missing." >&2
  exit 1
fi
if [ -z "$3" ]; then
  usage
  echo "Error: LDAP bind DN is missing." >&2
  exit 1
fi
LDAP_HOST="$1"
LDAP_BASE_DN="$2"
LDAP_BIND_DN="$3"
LDAP_PORT=${4:-389} # local instances do not have TLS
if [ $LDAP_PORT -eq 389 ]; then
  LDAP_TLS=false
else
  LDAP_TLS=true
fi
LDAP_MODE=${5-local} # 'local' (389ds) or 'cloud' (IPA)

if [[ ! ( "$LDAP_MODE" == "local" || "$LDAP_MODE" == "cloud" ) ]]; then
  echo "Invalid \$LDAP_MODE=$LDAP_MODE - choose 'local' or 'cloud'!"
  exit 1
fi

# Thanks to https://unix.stackexchange.com/a/223000
read_password() {
  REPLY="$(
    # always read from the tty even when redirected:
    exec < /dev/tty || exit # || exit only needed for bash

    # save current tty settings:
    tty_settings=$(stty -g) || exit

    # schedule restore of the settings on exit of that subshell
    # or on receiving SIGINT or SIGTERM:
    trap 'stty "$tty_settings"' EXIT INT TERM

    # disable terminal local echo
    stty -echo || exit

    # prompt on tty
    printf "Password: " > /dev/tty

    # read password as one line, record exit status
    IFS= read -r password; ret=$?

    # display a newline to visually acknowledge the entered password
    echo > /dev/tty

    # return the password for $REPLY
    printf '%s\n' "$password"
    exit "$ret"
  )"
}

echo "Enter LDAP bind password for $LDAP_BIND_DN"

read_password

if [ -z "$REPLY" ]; then
    echo "Password must be provided"
    exit 1
fi

LDAP_BIND_PW="$REPLY"
if [ "$LDAP_MODE" == "local" ]; then
  LDAP_DN_USERS="ou=users,{{ baseDn }}"
  LDAP_DN_GROUPS="ou=groups,{{ baseDn }}"
  LDAP_DN_ROLES="ou=roles,{{ baseDn }}"
else
  LDAP_DN_USERS="cn=users,{{ baseDn }}"
  LDAP_DN_GROUPS="cn=groups,{{ baseDn }}"
  LDAP_DN_ROLES="cn=roles,{{ baseDn }}"
fi

MY_DIR="$(dirname "$0")"
#docker-compose --project-directory "$MY_DIR" exec feather-postgres \
docker exec -it feather-postgres \
  psql -U postgres postgres \
  -c "DELETE FROM properties" \
  -c "INSERT INTO properties (key, value) \
  VALUES \
  ('MIGRATE_OLD_IDS', '1'), \
  ('ldap.host', '$LDAP_HOST'), \
  ('ldap.port', '$LDAP_PORT'), \
  ('ldap.tls', '$LDAP_TLS'), \
  ('ldap.dn.base', '$LDAP_BASE_DN'), \
  ('ldap.dn.users', '$LDAP_DN_USERS' ), \
  ('ldap.dn.groups',  '$LDAP_DN_GROUPS'), \
  ('ldap.dn.permissions', '$LDAP_DN_ROLES'), \
  ('ldap.bind.dn', '$LDAP_BIND_DN'), \
  ('ldap.rdn.user', 'uid'), \
  ('ldap.unique_groups', 'false'), \
  ('ldap.plain_text_passwords', 'true'), \
  ('ldap.bind.password', '$LDAP_BIND_PW'), \
  ('keycloak_actions.token', '3268b9c1-4b95-4f94-8383-3df45a37874f'), \
  ('additional_allowed_origin', '[\"http://localhost:3000\"]')"
