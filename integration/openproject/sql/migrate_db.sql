/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

-- Add/Ignore dummy auth source with id 1
INSERT INTO ldap_auth_sources
(id, name, host, port, attr_login, onthefly_register, created_at, updated_at)
VALUES (1, 'test', '127.0.0.1', '389', 'uid', false, NOW(), NOW())
    ON CONFLICT DO NOTHING;

-- Create auth source admin
INSERT INTO users (login, admin, type, ldap_auth_source_id) VALUES ('sso_admin', true, 'User', 1);

-- Disable admin password force change
UPDATE users SET force_password_change = FALSE WHERE login = 'admin';


----------- NOT Needed for tests, just quality of life for local usage
-- Admin and user created above are not logging in first time
UPDATE users SET first_login = FALSE WHERE login = 'admin';
UPDATE users SET first_login = FALSE WHERE login = 'sso_admin';
