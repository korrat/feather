#!/bin/sh

#
#    Copyright [2021] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

SCRIPT_DIR="$(dirname "$0")"
. "$SCRIPT_DIR/variables.sh"

docker-compose --project-directory "$SCRIPT_DIR" pull openproject
docker-compose --project-directory "$SCRIPT_DIR" rm -fsv openproject || true
docker-compose --project-directory "$SCRIPT_DIR" up -d openproject

"$SCRIPT_DIR/wait_ready.sh"

OPENPROJECT_MIGRATE_IOG=$OPENPROJECT_MIGRATE_IOG "$SCRIPT_DIR/migrate_db.sh"
