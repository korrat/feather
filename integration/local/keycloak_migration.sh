#!/bin/sh
#
#    Copyright [2023] [Ingenieure ohne Grenzen development team, see AUTHORS.md]
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

LDAP_HOST="$1"
LDAP_BASE_DN_USERS="$2"
LDAP_BASE_DN_GROUPS="$3"
LDAP_BIND_DN="$4"
LDAP_BIND_PW="$5"
FRONTEND_PORT="$6"

/opt/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/ --realm master --user "$KEYCLOAK_ADMIN" --password "$KEYCLOAK_ADMIN_PASSWORD" > /dev/null
/opt/keycloak/bin/kcadm.sh create realms -s enabled=true -s realm=feather-local-test
/opt/keycloak/bin/kcadm.sh create -r feather-local-test authentication/flows/browser/copy -s 'newName=Feather-Keycloak-Plugin'
EXECUTION_ID=$(/opt/keycloak/bin/kcadm.sh create -r feather-local-test -i authentication/flows/Feather-Keycloak-Plugin%20forms/executions/execution -s 'provider=feather-keycloak-plugin')
/opt/keycloak/bin/kcadm.sh update -r feather-local-test authentication/flows/Feather-Keycloak-Plugin/executions -s "id=$EXECUTION_ID" -s 'requirement=REQUIRED' --no-merge
/opt/keycloak/bin/kcadm.sh update -r feather-local-test realms/feather-local-test -s 'browserFlow=Feather-Keycloak-Plugin'

PARENT_ID=$(/opt/keycloak/bin/kcadm.sh create -r feather-local-test -i clients -s clientId=feather -s "redirectUris=[\"http://localhost:$FRONTEND_PORT/*\"]")
/opt/keycloak/bin/kcadm.sh create -r feather-local-test clients/$PARENT_ID/client-secret
CLIENT_SECRET=$(/opt/keycloak/bin/kcadm.sh get -r feather-local-test clients/$PARENT_ID/client-secret --fields value --format csv --noquotes)

/opt/keycloak/bin/kcadm.sh create -r feather-local-test clients/$PARENT_ID/protocol-mappers/models \
  -s name=uuid \
  -s protocol=openid-connect \
  -s protocolMapper=oidc-usermodel-attribute-mapper \
  -s 'config."user.attribute"="LDAP_ID"' \
  -s 'config."claim.name"="uuid"' \
  -s 'config."userinfo.token.claim"="true"' \
  -s 'config."id.token.claim"="true"' \
  -s 'config."access.token.claim"="true"' \
  -s 'config."jsonType.label"="String"'

/opt/keycloak/bin/kcadm.sh create -r feather-local-test roles -s name=admin

PARENT_ID=$(/opt/keycloak/bin/kcadm.sh create components -r feather-local-test -i \
    -s name="test-ldap" \
    -s providerId="ldap" \
    -s providerType="org.keycloak.storage.UserStorageProvider" \
    -s 'config.editMode=["READ_ONLY"]' \
    -s 'config.priority=["1"]' \
    -s 'config.vendor=["rhds"]' \
    -s 'config.batchSizeForSync=["1000"]' \
    -s 'config.usernameLDAPAttribute=["cn"]' \
    -s 'config.rdnLDAPAttribute=["cn"]' \
    -s 'config.uuidLDAPAttribute=["entryUUID"]' \
    -s 'config.userObjectClasses=["inetOrgPerson, organizationalPerson"]' \
    -s "config.connectionUrl=[\"$LDAP_HOST\"]" \
    -s "config.usersDn=[\"$LDAP_BASE_DN_USERS\"]" \
    -s 'config.authType=["simple"]' \
    -s "config.bindDn=[\"$LDAP_BIND_DN\"]" \
    -s "config.bindCredential=[\"$LDAP_BIND_PW\"]" \
    -s "config.searchScope=[\"1\"]" \
    -s "config.useTruststoreSpi=[\"ldapsOnly\"]" \
    -s 'config.fullSyncPeriod=["-1"]' \
    -s 'config.changedSyncPeriod=["-1"]')

/opt/keycloak/bin/kcadm.sh create components -r feather-local-test \
    -s name=admin-role \
    -s providerId=role-ldap-mapper \
    -s providerType=org.keycloak.storage.ldap.mappers.LDAPStorageMapper \
    -s parentId=$PARENT_ID \
    -s 'config."membership.attribute.type"=["DN"]' \
    -s "config.\"roles.dn\"=[\"$LDAP_BASE_DN_GROUPS\"]" \
    -s 'config."roles.ldap.filter"=["(cn=admins)"]' \
    -s 'config."mode"=["READ_ONLY"]' \
    -s 'config."use.realm.roles.mapping"=["true"]'

echo $CLIENT_SECRET
