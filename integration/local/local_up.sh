#!/bin/bash

#
#    Copyright [2022] Feather development team, see AUTHORS.md
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

set -e

DISABLE_FRONTEND_START=${DISABLE_FRONTEND_START:-0}
DISABLE_BACKEND_START=${DISABLE_BACKEND_START:-0}
IOG_ENABLED=${IOG_ENABLED:-0}
KEYCLOAK_ENABLED=${KEYCLOAK_ENABLED:-0}

KEYCLOAK_REGEX="--keycloak(=([0-9\.]+|current))?"
for var in "$@"; do
  if [ "$var" = "--no-frontend" ]; then
    DISABLE_FRONTEND_START=1
  elif [ "$var" = "--no-backend" ]; then
    DISABLE_BACKEND_START=1
  elif [ "$var" = "--iog" ]; then
      IOG_ENABLED=1
  elif [[ "$var" =~ $KEYCLOAK_REGEX ]]; then
        KEYCLOAK_ENABLED=1
        # renovate: datasource=gitlab-packages depName=ingenieure-ohne-grenzen/keycloak-plugin:dev/maximilian/feather/keycloak/feather-keycloak-plugin versioning=semver registryUrl=https://gitlab.com
        DEFAULT_FEATHER_PLUGIN_VERSION=1.0.20
        FEATHER_PLUGIN_VERSION=${BASH_REMATCH[2]:-$DEFAULT_FEATHER_PLUGIN_VERSION}
  else
    echo "Warning: ignoring invalid argument '$var'!" >&2
  fi
done

if [ "$DISABLE_BACKEND_START" -eq 0 ] && [ "$KEYCLOAK_ENABLED" -eq 1 ]; then
  echo "If you enable keycloak it's (currently) mandatory to disable the backend start with this script"
  exit 1;
fi;

SCRIPT_DIR="$(dirname "$0")"
cd "$SCRIPT_DIR"

. "./util.sh"

# Pull new image versions
docker compose pull

# Destroy everything, including volumes
docker compose down -v

POSTGRES_FEATHER_PASSWORD=$(openssl rand -hex 32)
export POSTGRES_FEATHER_PASSWORD
LDAP_ADMIN_PASSWORD=$(openssl rand -hex 32)
export LDAP_ADMIN_PASSWORD
KEYCLOAK_ADMIN_PASSWORD=$(openssl rand -hex 32)
export KEYCLOAK_ADMIN_PASSWORD
KEYCLOAK_FEATHER_TOKEN=$(openssl rand -hex 32)
export KEYCLOAK_FEATHER_TOKEN

# Create and migrate database
docker compose up -d postgres redis openldap
service_ready OpenLdap 3 15 "[ \$(docker ps -a -f \"name=feather-local-openldap\" --format \"{{ .State }}\") = \"running\" ]"

if [ "$IOG_ENABLED" -eq 1 ]; then
  docker cp "./iog.ldif" feather-local-openldap:/iog.ldif > /dev/null
  docker exec feather-local-openldap slapadd -F /opt/bitnami/openldap/etc/slapd.d -l iog.ldif -b "dc=example,dc=org" > /dev/null
fi;

service_ready "Postgres database" 3 15 "docker compose exec postgres pg_isready -q"
service_ready Redis 3 15 "[ \$(docker exec feather-local-redis redis-cli ping) = \"PONG\" ]"

docker exec feather-local-postgres psql -q -Upostgres -c "CREATE USER feather WITH PASSWORD '$POSTGRES_FEATHER_PASSWORD'"
docker exec feather-local-postgres psql -q -Upostgres -c "CREATE DATABASE feather WITH OWNER = feather"

if [ "$DISABLE_BACKEND_START" -eq 1 ]; then
  export LDAP_HOST=localhost
  export LDAP_PORT=16012
  export REDIS_HOST=localhost
  export REDIS_PORT=16011
  export API_PORT=7000
else
  export LDAP_HOST=openldap
  export LDAP_PORT=1389
  export REDIS_HOST=redis
  export REDIS_PORT=6379
  export API_PORT=16013
fi;

if [ "$DISABLE_FRONTEND_START" -eq 1 ]; then
  export FRONTEND_PORT=3000
else
  export FRONTEND_PORT=16014
fi;

< db_dump.sql envsubst | docker exec -i feather-local-postgres psql -q -Upostgres feather > /dev/null

if [ "$IOG_ENABLED" -eq 1 ]; then
  < iog_dump.sql envsubst | docker exec -i feather-local-postgres psql -q -Upostgres feather > /dev/null
fi;

if [ "$KEYCLOAK_ENABLED" -eq 1 ]; then
  echo "FEATHER_PLUGIN_VERSION: $FEATHER_PLUGIN_VERSION"
  if [ "$FEATHER_PLUGIN_VERSION" == "current" ]; then
    if [ ! -f ./feather-keycloak-plugin.jar ]; then
      echo "Please copy your current development artefact 'feather-keycloak-plugin.jar' in this folder."
      exit 1
    fi
  else
    # download version from Gitlab
    curl -sSL "https://gitlab.com/api/v4/groups/6691377/-/packages/maven/dev/maximilian/feather/keycloak/feather-keycloak-plugin/$FEATHER_PLUGIN_VERSION/feather-keycloak-plugin-$FEATHER_PLUGIN_VERSION-all.jar" -o "feather-keycloak-plugin.jar"
  fi
  BASE_URL=http://localhost:$FRONTEND_PORT API_BASE_URL=http://host.docker.internal:7000 KEYCLOAK_ADMIN_PASSWORD=$KEYCLOAK_ADMIN_PASSWORD FEATHER_API_TOKEN=$KEYCLOAK_FEATHER_TOKEN docker compose up -d feather-keycloak

  service_ready Keycloak 3 30 "curl -sSL http://127.0.0.1:16015 > /dev/null 2>&1"

  export CLIENT_TOKEN=$(docker exec feather-local-keycloak /migrate.sh "ldap://openldap:1389" "ou=users,dc=example,dc=org" "ou=groups,dc=example,dc=org" "cn=admin,dc=example,dc=org" "$LDAP_ADMIN_PASSWORD" "$FRONTEND_PORT")
  < keycloak_dump.sql envsubst | docker exec -i feather-local-postgres psql -q -Upostgres feather > /dev/null
fi;

echo ""
echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
echo "POSTGRES_FEATHER_PASSWORD=$POSTGRES_FEATHER_PASSWORD"
echo "LDAP_ADMIN_PASSWORD=$LDAP_ADMIN_PASSWORD"

if [ "$KEYCLOAK_ENABLED" -eq 1 ]; then
  echo "KEYCLOAK_ADMIN_PASSWORD=$KEYCLOAK_ADMIN_PASSWORD"
fi;

echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
echo "No dummy data was inserted. A new admin user is created on start of Feather, please read the password from the log output."

# Does the user want to develop the feather backend and start it locally instead of starting the backend container also?
if [ "$DISABLE_BACKEND_START" -eq 1 ]; then
  echo "To start the feather backend locally you can issue the following command"
  echo "You can also use the play button of your IDE. The main method is in core/src/main/dev/maximilian/feather/Main.kt"
  BACKEND_RUN_PATH=$(realpath "./../../")
  echo "DATABASE_URL=\"jdbc:postgresql://localhost:16010/feather?user=feather&password=$POSTGRES_FEATHER_PASSWORD&ssl=false\" $BACKEND_RUN_PATH/gradlew run"
else
  DATABASE_URL="jdbc:postgresql://postgres:5432/feather?user=feather&password=$POSTGRES_FEATHER_PASSWORD&ssl=false" docker compose up -d feather-backend
  echo "You can get the log output of the backend by issuing the following command"
  echo "docker compose --project-directory \"$SCRIPT_DIR\" logs -f feather-backend"
fi

# Does the user want to develop the feather frontend and start it locally instead of starting the frontend container also?
if [ "$DISABLE_FRONTEND_START" -eq 1 ]; then
  echo "To start the feather frontend locally you can issue the following command"
  echo "VITE_API_BASE_URL=http://localhost:$API_PORT yarn serve"
else
  BASE_URL=http://localhost:$FRONTEND_PORT API_BASE_URL=http://localhost:$API_PORT docker compose up -d feather-frontend
  echo "You can get the log output of the frontend by issuing the following command"
  echo "docker compose --project-directory \"$SCRIPT_DIR\" logs -f feather-frontend"
  echo "waiting for successful frontend start"
  # TODO make this better
  sleep 5
  echo "To navigate to the frontend open a browser on http://localhost:$FRONTEND_PORT/"
fi

echo "---------------------------------------------------------------------------------------------------------------------------------------------------"
