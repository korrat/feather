/*
 *    Copyright [2023] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

INSERT INTO public.properties (key, value) VALUES ('authorizer.credential', 'false');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc', '["keycloak"]');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.apiProvider', 'AUTO_DISCOVER');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.name', 'keycloak');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.url.discover', 'http://localhost:16015/realms/feather-local-test');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.url.icon', 'account-key');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.color', 'rgb(0, 169, 224)');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.apiKey', 'feather');
INSERT INTO public.properties (key, value) VALUES ('authorizer.oidc.keycloak.apiSecret', '$CLIENT_TOKEN');

INSERT INTO public.properties (key, value) VALUES ('keycloak_actions.token', '$KEYCLOAK_FEATHER_TOKEN');
