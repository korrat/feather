#!/bin/bash

LOG_LEVEL= #--info

function run_all_tests {
    ./gradlew $LOG_LEVEL cleanTest $1
}

function run_tests {
    ./gradlew $LOG_LEVEL cleanTest $1 --tests $2
}

TEST_ROOT=dev.maximilian.feather

run_all_tests :core:test

run_all_tests :multiservice:test

#run_all_tests :iog:test # needs an LDAP provider

#run_all_tests :openshift:test # no tests
