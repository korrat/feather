/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.plugin

import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.AuthorizationController
import io.javalin.Javalin
import org.jetbrains.exposed.sql.Database

public data class FeatherPluginInitialization(
    public val propertyMap: MutableMap<String, String>,
    public val baseUrl: String,
    public val db: Database,
    public val app: Javalin,
    public val accountController: AccountController,
    public val actionController: ActionController,
    public val authorizationController: AuthorizationController,
)
