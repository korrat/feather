/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.plugin

import mu.KotlinLogging
import java.util.ServiceLoader
import java.util.concurrent.locks.ReentrantLock

public class PluginLoader {
    private val logger = KotlinLogging.logger { }
    private val plugins = ServiceLoader.load(FeatherPlugin::class.java).map { it }
    private val initialized = mutableSetOf<FeatherPlugin>()
    private val started = mutableSetOf<FeatherPlugin>()
    private val lock = ReentrantLock()

    public fun initialize(featherPluginInitialization: FeatherPluginInitialization) {
        doLocked {
            (plugins - initialized).forEach {
                logger.info { "Initializing Plugin \"${it.name}\"" }
                it.initialize(featherPluginInitialization)
                initialized.add(it)
                logger.info { "Initialized Plugin \"${it.name}\"" }
            }
        }
    }

    public fun start() {
        doLocked {
            (plugins - initialized - started).forEach {
                logger.info { "Starting Plugin \"${it.name}\"" }
                it.start()
                started.add(it)
                logger.info { "Started Plugin \"${it.name}\"" }
            }
        }
    }

    public fun stop() {
        doLocked {
            started.forEach {
                logger.info { "Stopping Plugin \"${it.name}\"" }
                it.stop()
                started.remove(it)
                logger.info { "Stopped Plugin \"${it.name}\"" }
            }
        }
    }

    public fun dispose() {
        doLocked {
            (initialized - started).forEach {
                logger.info { "Disposing Plugin \"${it.name}\"" }
                it.dispose()
                initialized.remove(it)
                logger.info { "Disposed Plugin \"${it.name}\"" }
            }
        }
    }

    private fun <T> doLocked(block: () -> T) =
        try {
            lock.lock()
            block()
        } finally {
            lock.unlock()
        }
}
