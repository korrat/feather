/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

public sealed interface IAuthorizer {
    public val id: String
    public val type: AuthorizerType

    /**
     * The refresh needs to check the session validity
     */
    public fun refresh(session: Session): Session?

    public fun afterLogoutURL(session: Session): String

    public fun afterActionsRedirect(session: Session): String = "/"

    public fun afterActionsSessionModification(session: Session): Session? = session.copy(miniSession = false)
}

public interface ITokenAuthorizer : IAuthorizer {
    override val type: AuthorizerType
        get() = AuthorizerType.TOKEN

    public fun authorize(token: String): Session?
}

public interface ICredentialAuthorizer : IAuthorizer {
    override val type: AuthorizerType
        get() = AuthorizerType.CREDENTIAL

    public fun authorize(
        username: String,
        password: String,
    ): Session?
}

public interface IOidcAuthorizer : IAuthorizer {
    public val name: String
    public val color: String
    public val iconUrl: String

    override val type: AuthorizerType
        get() = AuthorizerType.OIDC

    public fun redirectUrl(): String

    public fun authorize(token: String): Session?
}

public enum class AuthorizerType {
    CREDENTIAL,
    OIDC,
    TOKEN,
}
