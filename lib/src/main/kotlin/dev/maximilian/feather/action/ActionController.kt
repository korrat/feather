package dev.maximilian.feather.action

import dev.maximilian.feather.IControllableService
import dev.maximilian.feather.User
import dev.maximilian.feather.account.AccountController
import org.jetbrains.exposed.sql.Database

public class ActionController(database: Database, private val accountController: AccountController) {
    private val actionSetupList = mutableListOf<() -> Unit>()
    private val actionProducerMap = mutableMapOf<String, (Int, String) -> Action<*>>()
    private val actionDatabase = ActionDatabase(database, accountController)
    private val services: MutableMap<String, IControllableService> = mutableMapOf()

    public fun addService(service: IControllableService) {
        services[service.serviceName] = service
    }

    public fun removeService(service: IControllableService) {
        services.remove(service.serviceName)
    }

    public fun registerActionType(
        type: String,
        setup: () -> Unit,
        producer: (Int, String) -> Action<*>,
    ) {
        actionProducerMap[type] = producer
        actionSetupList.add(setup)
    }

    public fun setupActions() {
        actionSetupList.forEach { it() }
    }

    private fun constructAction(
        type: String,
        userId: Int,
        payload: String,
    ): Action<*> {
        val producer =
            requireNotNull(actionProducerMap[type]) {
                "Action \"$type\" was not registered to ActionController and cannot be constructed"
            }

        return producer(userId, payload)
    }

    public fun getOpenUserActions(fromActionType: String): List<Action<*>> =
        actionDatabase.getActionsByName(fromActionType, ::constructAction)

    public fun getOpenUserActions(fromUser: User): List<Action<*>> = actionDatabase.getActionsByUser(fromUser.id, ::constructAction)

    public fun getOpenActions(): Map<Int, List<Action<*>>> = actionDatabase.getOpenActions(::constructAction)

    public fun insertActions(actions: List<Action<*>>) {
        val usersWithOpenActions = actionDatabase.getUsersWithOpenActions().toSet()
        val usersToBeChecked = actions.map { it.userId }.toSet() - usersWithOpenActions

        actionDatabase.insertActions(actions)

        if (usersToBeChecked.isNotEmpty()) {
            services.values.forEach {
                it.setUsersActivated(
                    usersToBeChecked.mapNotNull {
                            userId ->
                        accountController.getUser(userId)
                    },
                    false,
                )
            }
        }
    }

    public fun insertAction(action: Action<*>) {
        val hasUserAlreadyAction = actionDatabase.hasUserActions(action.userId)

        actionDatabase.insertAction(action)

        if (!hasUserAlreadyAction) {
            val user = accountController.getUser(action.userId) ?: return
            services.values.forEach { it.activateUser(user, false) }
        }
    }

    internal fun deleteActionsByName(name: String) {
        val usersWithOpenActions = actionDatabase.getUsersWithOpenActions().toSet()
        actionDatabase.deleteActionsByName(name)
        val usersToBeChecked = usersWithOpenActions - actionDatabase.getUsersWithOpenActions().toSet()

        if (usersToBeChecked.isNotEmpty()) {
            val usersToModify = usersToBeChecked.mapNotNull { userId -> accountController.getUser(userId) }.filter { !it.disabled }

            if (usersToModify.isNotEmpty()) {
                services.values.forEach { it.setUsersActivated(usersToModify, true) }
            }
        }
    }

    public fun deleteActionsByNameAndUser(
        name: String,
        user: User,
    ) {
        val hasUserAlreadyAction = actionDatabase.hasUserActions(user.id)

        actionDatabase.deleteActionsByNameAndUser(name, user.id)

        if (hasUserAlreadyAction && !actionDatabase.hasUserActions(user.id) && !user.disabled) {
            services.values.forEach { it.activateUser(user, true) }
        }
    }

    public fun deleteActionsByNameUserAndPayload(
        name: String,
        user: User,
        payload: String,
    ) {
        val hasUserAlreadyAction = actionDatabase.hasUserActions(user.id)
        actionDatabase.deleteActionsByNameUserAndPayload(name, user.id, payload)

        if (hasUserAlreadyAction && !actionDatabase.hasUserActions(user.id) && !user.disabled) {
            services.values.forEach { it.activateUser(user, true) }
        }
    }
}
