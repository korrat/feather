package dev.maximilian.feather.action

public interface Action<T> {
    public val name: String
    public val userId: Int
    public val description: String
    public val payload: T

    public fun payloadToString(): String
}
