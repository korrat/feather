/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.IAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.account.AccountController
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.inList
import org.jetbrains.exposed.sql.SqlExpressionBuilder.lessEq
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.concurrent.fixedRateTimer

internal class SessionDatabase(
    private val db: Database,
    private val accountController: AccountController,
    private val authorizerMap: Map<String, IAuthorizer>,
) {
    private val sessionTable = SessionTable(accountController)
    private val afterActionsRedirectTable = AfterActionsRedirectTable(sessionTable)

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(sessionTable, afterActionsRedirectTable)
        }

        fixedRateTimer(
            name = "KeycloakActionsDatabase Cleanup Job",
            daemon = true,
            period = Duration.ofHours(1).seconds,
        ) {
            cleanupJob()
        }
    }

    internal fun create(session: Session): Session {
        require(session.id == EMPTY_UUID) { "Can only create a session with an empty uuid" }

        return session.copy(
            id =
            transaction(db) {
                sessionTable.insertAndGetId {
                    it[userId] = session.user.id
                    it[validUntil] = session.validUntil
                    it[authorizer] = session.authorizer.id
                    it[refreshToken] = session.refreshToken
                    it[accessToken] = session.accessToken
                    it[idToken] = session.idToken
                    it[minisession] = session.miniSession
                }
            }.value,
        )
    }

    internal fun getSession(id: UUID): Session? =
        transaction(db) {
            sessionTable.select { sessionTable.id eq id }.firstOrNull()?.let {
                val user = accountController.getUser(it[sessionTable.userId].value) ?: return@transaction null
                val authorizer =
                    it[sessionTable.authorizer].let { authorizer -> authorizerMap[authorizer] } ?: return@transaction null
                Session(
                    id = it[sessionTable.id].value,
                    validUntil = it[sessionTable.validUntil],
                    refreshToken = it[sessionTable.refreshToken],
                    accessToken = it[sessionTable.accessToken],
                    idToken = it[sessionTable.idToken],
                    user = user,
                    authorizer = authorizer,
                    miniSession = it[sessionTable.minisession],
                )
            }
        }

    internal fun updateSession(session: Session): Session {
        val oldSession = getSession(session.id) ?: throw IllegalArgumentException("Cannot update not existing entity")
        require(
            oldSession.copy(
                validUntil = session.validUntil,
                refreshToken = session.refreshToken,
                accessToken = session.accessToken,
                idToken = session.idToken,
                miniSession = session.miniSession,
            ) == session,
        ) {
            "Can only update validUntil, refreshToken, idToken or minisession from session"
        }

        transaction(db) {
            sessionTable.update({ sessionTable.id eq session.id }) {
                it[validUntil] = session.validUntil
                it[accessToken] = session.accessToken
                it[idToken] = session.idToken
                it[refreshToken] = session.refreshToken
                it[minisession] = session.miniSession
            }
        }

        return session
    }

    internal fun deleteSession(session: Session) {
        transaction(db) {
            afterActionsRedirectTable.deleteWhere { afterActionsRedirectTable.sessionId eq session.id }
            sessionTable.deleteWhere { sessionTable.id eq session.id }
        }
    }

    internal fun deleteSessionsForUser(userId: Int) {
        transaction(db) {
            val sessionIds = sessionTable.slice(sessionTable.id).select { sessionTable.userId eq userId }.map { it[sessionTable.id].value }

            afterActionsRedirectTable.deleteWhere { afterActionsRedirectTable.sessionId inList sessionIds }
            sessionTable.deleteWhere { sessionTable.userId eq userId }
        }
    }

    internal fun getAfterActionsRedirect(session: Session): String? =
        transaction(db) {
            afterActionsRedirectTable.select { afterActionsRedirectTable.sessionId eq session.id }
                .firstOrNull()
                ?.let { it[afterActionsRedirectTable.redirectUrl] }
        }

    internal fun insertAfterActionsRedirect(
        session: Session,
        redirectUrl: String,
    ) {
        transaction(db) {
            afterActionsRedirectTable.insert {
                it[sessionId] = session.id
                it[afterActionsRedirectTable.redirectUrl] = redirectUrl
            }
        }
    }

    private fun cleanupJob() {
        transaction(db) {
            sessionTable.deleteWhere { sessionTable.validUntil lessEq Instant.now().minus(10, ChronoUnit.MINUTES) }
        }
    }

    private class SessionTable(accountController: AccountController) : UUIDTable("sessions") {
        val userId: Column<EntityID<Int>> = reference("userId", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val validUntil: Column<Instant> = timestamp("validUntil")
        val authorizer: Column<String> = varchar("authorizer", 255)
        val accessToken: Column<String?> = text("access_token").nullable()
        val refreshToken: Column<String?> = text("refresh_token").nullable()
        val idToken: Column<String?> = text("id_token").nullable()
        val minisession: Column<Boolean> = bool("minisession").default(false)
    }

    private class AfterActionsRedirectTable(SessionTable: SessionTable) : Table("after_actions") {
        val sessionId: Column<EntityID<UUID>> = reference("session_id", SessionTable.id, onDelete = ReferenceOption.CASCADE)
        val redirectUrl: Column<String> = varchar("redirect_url", 4096)

        override val primaryKey: PrimaryKey = PrimaryKey(sessionId)
    }
}
