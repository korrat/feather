package dev.maximilian.feather

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.javalin.Javalin
import io.javalin.http.BadRequestResponse
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import io.javalin.http.UnauthorizedResponse
import io.javalin.http.servlet.HttpResponseExceptionMapper
import io.javalin.json.JsonMapper
import io.javalin.json.PipedStreamUtil
import io.javalin.micrometer.MicrometerPlugin
import io.javalin.openapi.OpenApiInfo
import io.javalin.openapi.plugin.OpenApiConfiguration
import io.javalin.openapi.plugin.OpenApiPlugin
import io.javalin.openapi.plugin.redoc.ReDocConfiguration
import io.javalin.openapi.plugin.redoc.ReDocPlugin
import io.javalin.openapi.plugin.swagger.SwaggerConfiguration
import io.javalin.openapi.plugin.swagger.SwaggerPlugin
import io.javalin.validation.JavalinValidation
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.prometheus.client.exporter.common.TextFormat
import java.io.InputStream
import java.lang.reflect.Type
import java.util.UUID

public class AppBuilder {
    private companion object {
        init {
            JavalinValidation.register(UUID::class.java) { UUID.fromString(it) }
        }
    }

    public fun createApp(
        versionString: String,
        titleString: String,
        frontendBaseUrl: String,
        additionalAllowedOrigins: Set<String> = emptySet(),
        port: Int = 7000,
        metricsBasicAuthPassword: String? = null,
    ): Javalin {
        val prometheusRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)

        return Javalin.create {
            it.showJavalinBanner = false
            it.jsonMapper(
                object : JsonMapper {
                    private val objectMapper =
                        jacksonObjectMapper().registerModule(JavaTimeModule())
                            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)

                    override fun toJsonString(
                        obj: Any,
                        type: Type,
                    ): String = objectMapper.writeValueAsString(obj)

                    override fun toJsonStream(
                        obj: Any,
                        type: Type,
                    ): InputStream =
                        PipedStreamUtil.getInputStream { pipedOutputStream ->
                            objectMapper.factory.createGenerator(pipedOutputStream).writeObject(obj)
                        }

                    override fun <T : Any> fromJsonString(
                        json: String,
                        targetType: Type,
                    ): T = objectMapper.readValue(json, objectMapper.typeFactory.constructType(targetType))

                    override fun <T : Any> fromJsonStream(
                        json: InputStream,
                        targetType: Type,
                    ): T = objectMapper.readValue(json, objectMapper.typeFactory.constructType(targetType))
                },
            )
            it.http.defaultContentType = "application/json"
            it.http.maxRequestSize = 10000000L
            it.routing.contextPath = "/v1"
            it.plugins.register(
                OpenApiPlugin(
                    OpenApiConfiguration().apply {
                        info =
                            OpenApiInfo().apply {
                                version = versionString
                                title = titleString
                            }
                        documentationPath = "/openapi"
                    },
                ),
            )
            it.plugins.register(SwaggerPlugin(SwaggerConfiguration()))
            it.plugins.register(ReDocPlugin(ReDocConfiguration()))

            it.plugins.register(
                MicrometerPlugin.create { micrometerConfig ->
                    micrometerConfig.registry = prometheusRegistry
                },
            )

            if (additionalAllowedOrigins.contains("*")) {
                it.plugins.enableCors { cors ->
                    cors.add { pluginConfig ->
                        pluginConfig.anyHost()
                        pluginConfig.allowCredentials = true
                    }
                }
            } else {
                it.plugins.enableCors { cors ->
                    cors.add { pluginConfig ->
                        pluginConfig.allowHost(frontendBaseUrl, *(additionalAllowedOrigins).toTypedArray())
                        pluginConfig.allowCredentials = true
                    }
                }
            }

            it.accessManager { handler, ctx, permittedRoles ->
                if (permittedRoles.isEmpty() || ctx.session().user.permissions.intersect(permittedRoles).isNotEmpty()) {
                    handler.handle(ctx)
                } else {
                    throw PermissionException(
                        ctx.session().user,
                        "${ctx.method()} ${ctx.path()}",
                        permittedRoles.mapNotNull { r -> r as? Permission }.toSet(),
                    )
                }
            }
        }.start(port).apply {
            exception(PermissionException::class.java) { ex, ctx ->
                if (ex.unauthorized) {
                    HttpResponseExceptionMapper.handle(UnauthorizedResponse(ex.message!!), ctx)
                } else {
                    HttpResponseExceptionMapper.handle(ForbiddenResponse(ex.message!!), ctx)
                }
            }

            exception(MiniSessionException::class.java) { ex, ctx ->
                HttpResponseExceptionMapper.handle(ForbiddenResponse(ex.message!!), ctx)
            }

            exception(IllegalArgumentException::class.java) { ex, ctx ->
                HttpResponseExceptionMapper.handle(BadRequestResponse(ex.message ?: "Bad request"), ctx)
            }

            get("/metrics") {
                val session = it.sessionOrNull()

                // Admin user is allowed to see metrics
                // Also plain basic auth with metrics user and predefined token is allowed
                if (session?.user?.permissions?.contains(Permission.ADMIN) != true) {
                    val credentials = it.basicAuthCredentials()

                    if (metricsBasicAuthPassword == null || credentials == null) {
                        if (session != null) {
                            throw ForbiddenResponse()
                        } else {
                            throw UnauthorizedResponse()
                        }
                    }

                    if (credentials.username != "metrics" || credentials.password != metricsBasicAuthPassword) {
                        throw UnauthorizedResponse()
                    }
                }

                it.contentType(TextFormat.CONTENT_TYPE_004).result(prometheusRegistry.scrape())
            }
        }
    }
}

public fun Context.sessionOrMinisessionOrNull(): Session? = this.attribute<Session>("session")

public fun Context.sessionOrMinisession(): Session = this.sessionOrMinisessionOrNull() ?: throw UnauthorizedResponse()

public fun Context.session(): Session =
    sessionOrMinisessionOrNull()?.let {
        if (it.miniSession) throw ForbiddenResponse("User has only a miniSession but a full session is required") else it
    }
        ?: throw UnauthorizedResponse()

public fun Context.sessionOrNull(): Session? = sessionOrMinisessionOrNull()?.let { if (it.miniSession) null else it }
