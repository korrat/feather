/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import java.util.UUID

public interface IControllableService {
    public val serviceName: String

    public fun createUser(
        createdUser: User,
        jobId: UUID?,
    ): User

    public fun deleteUser(user: User): Boolean

    public fun activateUser(
        user: User,
        activate: Boolean,
    ): Boolean

    public fun setUsersActivated(
        users: List<User>,
        activated: Boolean,
    )

    public suspend fun checkUser(
        user: User,
        autoRepair: Boolean = false,
    ): String

    public fun changeMailAddress(
        user: User,
        newMail: String,
    ): Boolean

    public fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String,
    ): Boolean
}
