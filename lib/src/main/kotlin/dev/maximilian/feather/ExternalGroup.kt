package dev.maximilian.feather

public data class ExternalGroup(
    val externalId: String,
    val name: String,
    val description: String,
    val userMembers: Set<String>,
    val groupMembers: Set<String>,
    val parentGroups: Set<String>,
    val owners: Set<String>,
    val ownerGroups: Set<String>,
    val ownedGroups: Set<String>,
)
