package dev.maximilian.feather.gdpr

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.UserRequestInfo
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.requireValid
import mu.KLogging
import org.jetbrains.exposed.sql.Database
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date
import kotlin.concurrent.fixedRateTimer

public class GdprController(
    database: Database,
    private val actionController: ActionController,
    private val accountController: AccountController,
) {
    private companion object : KLogging()

    internal val gdprDatabase = GdprDatabase(database, accountController)

    init {
        // Register the GDPR Action
        actionController.registerActionType(GdprAction.NAME, ::setupTimerAndCheckExistingActions) { userId: Int, payload: String ->
            GdprAction(userId, payload.toInt())
        }
    }

    private fun addActionsToUsers() {
        val startTime = Instant.now()

        val latestDocument = getLatestDocument() ?: return
        if (latestDocument.forceDate >= Instant.now()) return

        val userList = accountController.getUsers()
        val usersAccepted =
            gdprDatabase.getAcceptancesOf(latestDocument, userList.associateBy { it.id }).map { it.user.id }
        val usersActionOpen = actionController.getOpenUserActions(GdprAction.NAME).map { it.userId }

        // Add needed actions
        val usersNeedingAction =
            userList.filterNot { it.permissions.contains(Permission.FUNCTION_ACCOUNT) }.map { it.id }
                .toSet() - usersAccepted.toSet() - usersActionOpen.toSet()

        actionController.insertActions(usersNeedingAction.map { GdprAction(it, latestDocument.id) })

        // Remove unneeded actions
        val usersNotNeedingActions = userList.filter { it.permissions.contains(Permission.FUNCTION_ACCOUNT) }

        actionController.getOpenUserActions(GdprAction.NAME)
            .filterIsInstance<GdprAction>()
            .mapNotNull { action -> usersNotNeedingActions.firstOrNull { user -> user.id == action.userId } }
            .forEach { actionController.deleteActionsByNameAndUser(GdprAction.NAME, it) }

        // Time footprint
        val duration = Duration.between(startTime, Instant.now()).toMillis()
        logger.info {
            "GdprController::addActionsToUsers created ${usersNeedingAction.count()} new actions in $duration ms."
        }
    }

    public fun handleUserCreated(user: User) {
        val latestDocument = getLatestDocument() ?: return
        if (latestDocument.forceDate >= Instant.now()) return
        val userAcceptance = gdprDatabase.getAcceptancesOf(latestDocument, user)

        if (userAcceptance == null) {
            actionController.insertAction(GdprAction(user.id, latestDocument.id))
        }
    }

    /**
     * TODO this code is currently unused
     */
    public fun handleUserUpdated(user: User) {
        // Two cases
        // 1. User was a functional account and is not anymore => check if we need to insert an action
        // 2. User was not a functional account and is now => check if we need to delete an action
        // Every other change is not important here

        val latestDocument = getLatestDocument() ?: return
        if (latestDocument.forceDate >= Instant.now()) return
        val userAcceptance = gdprDatabase.getAcceptancesOf(latestDocument, user)
        val userAction = actionController.getOpenUserActions(user).filterIsInstance<GdprAction>().firstOrNull()

        if (userAcceptance == null && !user.permissions.contains(Permission.FUNCTION_ACCOUNT) && userAction == null) {
            actionController.insertAction(GdprAction(user.id, latestDocument.id))
        }

        if (user.permissions.contains(Permission.FUNCTION_ACCOUNT) && userAction != null) {
            actionController.deleteActionsByNameAndUser(GdprAction.NAME, user)
        }
    }

    public fun getLatestDocument(): GdprDocument? = gdprDatabase.getLatestDocumentId()?.let { gdprDatabase.getDocument(it) }

    public fun getAcceptanceInfoOfUser(
        requestInfo: UserRequestInfo,
        documentId: String,
        user: User,
    ): GdprAcceptance? {
        if (user.id != requestInfo.user.id) {
            requestInfo.requireValid(
                "getAcceptanceInfoOfUser(${user.id})",
                setOf(Permission.MANAGE_GDPR, Permission.ADMIN),
            )
        }

        val document = documentId.toDocumentId()?.let { gdprDatabase.getDocument(it) } ?: return null

        return gdprDatabase.getAcceptancesOf(document, user)
    }

    /**
     * Only admin users and gdpr manager are allowed to check acceptance info of a gdpr document
     */
    public fun getAcceptanceInfo(
        requestInfo: UserRequestInfo,
        documentId: String,
    ): List<GdprAcceptance>? {
        requestInfo.requireValid("Gdpr.getAcceptanceInfo($documentId)", setOf(Permission.ADMIN, Permission.MANAGE_GDPR))

        val document = documentId.toDocumentId()?.let { gdprDatabase.getDocument(it) } ?: return null
        val users = accountController.getUsers()
        val userIdSet = users.map { it.id }.toSet()
        val accepted = gdprDatabase.getAcceptancesOf(document, users.associateBy { it.id })
        val acceptedIdSet = accepted.map { it.user.id }.toSet()
        val notNeededSet = users.filter { it.permissions.contains(Permission.FUNCTION_ACCOUNT) }
        val notNeededIdSet = notNeededSet.map { it.id }.toSet()
        val notAcceptedIdSet = userIdSet - acceptedIdSet - notNeededIdSet
        val notAccepted =
            users.filter { notAcceptedIdSet.contains(it.id) }
                .map { GdprAcceptance(it, Instant.ofEpochSecond(0), document, GdprAcceptanceType.NOT_ACCEPTED) }
        val notNeeded =
            notNeededSet.map { GdprAcceptance(it, Instant.ofEpochSecond(0), document, GdprAcceptanceType.NOT_NEEDED) }

        return accepted + notAccepted + notNeeded
    }

    public fun storeAcceptanceInfo(
        requestInfo: UserRequestInfo,
        documentId: Int,
    ): GdprAcceptance {
        val latest = gdprDatabase.getLatestDocumentId()?.let { gdprDatabase.getDocument(it) }

        requireNotNull(latest) { "No GDPR document exists" }
        require(latest.id == documentId) { "Only the latest version can be accepted" }

        return gdprDatabase.storeAcceptanceInfo(latest, requestInfo.user).apply {
            actionController.deleteActionsByNameAndUser(GdprAction.NAME, requestInfo.user)
        }
    }

    /**
     * Only registered users with a full session are allowed to view all documents
     * If unauthorized or mini session, only the latest gdpr is returned to the list
     */
    public fun getDocuments(requestInfo: UserRequestInfo?): List<GdprDocument> =
        if (requestInfo == null || requestInfo.miniSession) {
            listOfNotNull(gdprDatabase.getLatestDocumentId()?.let { gdprDatabase.getDocument(it) })
        } else {
            gdprDatabase.getAllDocuments()
        }

    /**
     * Only registered users with a full session are allowed to view all documents
     * If unauthorized or mini session, only the latest gdpr is returned, else Permission is denied
     */
    public fun getDocument(
        requestInfo: UserRequestInfo?,
        id: String,
    ): GdprDocument? {
        val latestGdprDocumentId = gdprDatabase.getLatestDocumentId()

        if (id == "latest" || id == latestGdprDocumentId?.toString()) {
            return latestGdprDocumentId?.let { gdprDatabase.getDocument(it) }
        }

        requestInfo.requireValid("Gdpr.getDocument($id)", emptySet())

        val intId = id.toIntOrNull()
        requireNotNull(intId) { "$id is not an integer or not \"latest\"" }

        return gdprDatabase.getDocument(intId)
    }

    /**
     * Admins or GDPR Manager are allowed to create documents
     */
    public fun createDocument(
        requestInfo: UserRequestInfo,
        document: GdprDocument,
    ): GdprDocument {
        requestInfo.requireValid("Gdpr.createDocument($document)", setOf(Permission.ADMIN, Permission.MANAGE_GDPR))

        val latest = gdprDatabase.getLatestDocumentId()?.let { gdprDatabase.getDocument(it) }

        if (latest != null) {
            require(latest.validDate < Instant.now()) { "Cannot create new gdpr document since the latest is not valid until now" }
        }

        require(document.forceDate >= Instant.now().plus(30, ChronoUnit.DAYS)) {
            "The force date must be at least 30 days in the future"
        }

        require(document.validDate >= Instant.now().plus(60, ChronoUnit.DAYS)) {
            "The valid date must be at least 60 days in the future"
        }

        require(document.content.length >= 10) { "Content length must be at least 10 characters" }

        return gdprDatabase.createDocument(document).apply {
            actionController.deleteActionsByName(GdprAction.NAME)
            // Document is not forced yet, so don't add actions
        }
    }

    private fun String.toDocumentId(): Int? =
        if (this == "latest") {
            gdprDatabase.getLatestDocumentId()
        } else {
            requireNotNull(this.toIntOrNull()) { "$this is not an integer or not \"latest\"" }
        }

    private fun setupTimerAndCheckExistingActions() {
        // Periodically add the action to users which have not checked the gdpr yet
        // Also check on application start
        fixedRateTimer(
            daemon = true,
            startAt = Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(1L, ChronoUnit.DAYS)),
            period = Duration.ofDays(1).toMillis(),
        ) { addActionsToUsers() }

        addActionsToUsers()
    }
}
