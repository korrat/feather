/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.gdpr

import dev.maximilian.feather.MiniSessionException
import dev.maximilian.feather.Permission
import dev.maximilian.feather.PermissionException
import dev.maximilian.feather.UserRequestInfo
import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.assertDoesNotThrow
import java.sql.DriverManager
import java.time.Instant
import java.util.UUID
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

/**
 * This class tests the business logic of the gdpr class
 * For the low level database functions test check the GdprDatabaseTest
 * @see dev.maximilian.feather.gdpr.GdprControllerDatabaseTest
 */
class GdprControllerTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") })

    private val accountController =
        AccountController(database, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    private val users =
        (0 until 5).map {
            accountController.createUser(
                TestUser.generateTestUser(
                    permissions =
                    if (it == 0) {
                        setOf(Permission.MANAGE_GDPR)
                    } else if (it == 3) {
                        setOf(Permission.FUNCTION_ACCOUNT)
                    } else {
                        emptySet()
                    },
                ),
            )
        }
    private val gdprController = GdprController(database, ActionController(database, accountController), accountController)
    private val gdprDatabase = gdprController.gdprDatabase

    @Test
    fun `Get not existing document returns correctly`() {
        assertFailsWith<PermissionException> { gdprController.getDocument(null, (-42).toString()) }.apply {
            assertEquals(
                "User with id NOT_LOGGED_IN is not allowed to execute action \"Gdpr.getDocument(-42)\".",
                message,
            )
        }

        assertNull(gdprController.getDocument(UserRequestInfo(users[0], false), (-42).toString()))
        assertNull(gdprController.getDocument(UserRequestInfo(users[1], false), (-42).toString()))

        assertFailsWith<MiniSessionException> {
            gdprController.getDocument(
                UserRequestInfo(users[0], true),
                (-42).toString(),
            )
        }.apply {
            assertEquals(
                "User with id ${users[0].id} is not allowed to execute action \"Gdpr.getDocument(-42)\" because the session currently active is a minimal session",
                message,
            )
        }

        assertFailsWith<MiniSessionException> {
            gdprController.getDocument(
                UserRequestInfo(users[1], true),
                (-42).toString(),
            )
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"Gdpr.getDocument(-42)\" because the session currently active is a minimal session",
                message,
            )
        }
    }

    @Test
    fun `Get not latest document returns correctly`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())
        gdprDatabase.createDocument(generateTestGdprDocument())

        assertFailsWith<PermissionException> { gdprController.getDocument(null, document.id.toString()) }.apply {
            assertEquals(
                "User with id NOT_LOGGED_IN is not allowed to execute action \"Gdpr.getDocument(${document.id})\".",
                message,
            )
        }

        assertEquals(document, gdprController.getDocument(UserRequestInfo(users[0], false), document.id.toString()))
        assertEquals(document, gdprController.getDocument(UserRequestInfo(users[1], false), document.id.toString()))

        assertFailsWith<MiniSessionException> {
            gdprController.getDocument(
                UserRequestInfo(users[0], true),
                document.id.toString(),
            )
        }.apply {
            assertEquals(
                "User with id ${users[0].id} is not allowed to execute action \"Gdpr.getDocument(${document.id})\" because the session currently active is a minimal session",
                message,
            )
        }

        assertFailsWith<MiniSessionException> {
            gdprController.getDocument(
                UserRequestInfo(users[1], true),
                document.id.toString(),
            )
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"Gdpr.getDocument(${document.id})\" because the session currently active is a minimal session",
                message,
            )
        }
    }

    @Test
    fun `Get latest document returns correctly`() {
        val document = gdprDatabase.createDocument(generateTestGdprDocument())

        listOf(document.id.toString(), "latest").forEach {
            assertEquals(document, gdprController.getDocument(null, it))
            assertEquals(document, gdprController.getDocument(UserRequestInfo(users[0], false), it))
            assertEquals(document, gdprController.getDocument(UserRequestInfo(users[1], false), it))
            assertEquals(document, gdprController.getDocument(UserRequestInfo(users[0], true), it))
            assertEquals(document, gdprController.getDocument(UserRequestInfo(users[1], true), it))
        }
    }

    @Test
    fun `Get documents returns correctly`() {
        val documents =
            (0 until 5).map { gdprDatabase.createDocument(generateTestGdprDocument()) }.sortedByDescending { it.id }

        assertEquals(listOf(documents.first()), gdprController.getDocuments(null))
        assertEquals(documents, gdprController.getDocuments(UserRequestInfo(users[0], false)))
        assertEquals(documents, gdprController.getDocuments(UserRequestInfo(users[1], false)))
        assertEquals(listOf(documents.first()), gdprController.getDocuments(UserRequestInfo(users[0], true)))
        assertEquals(listOf(documents.first()), gdprController.getDocuments(UserRequestInfo(users[1], true)))
    }

    @Test
    fun `Create document permission test`() {
        val document = generateTestGdprDocument()

        assertFailsWith<PermissionException> {
            gdprController.createDocument(
                UserRequestInfo(users[1], false),
                document,
            )
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"Gdpr.createDocument($document)\".",
                message,
            )
        }

        assertFailsWith<PermissionException> {
            gdprController.createDocument(
                UserRequestInfo(users[1], true),
                document,
            )
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"Gdpr.createDocument($document)\".",
                message,
            )
        }

        assertFailsWith<MiniSessionException> {
            gdprController.createDocument(
                UserRequestInfo(users[0], true),
                document,
            )
        }.apply {
            assertEquals(
                "User with id ${users[0].id} is not allowed to execute action \"Gdpr.createDocument($document)\" because the session currently active is a minimal session",
                message,
            )
        }
    }

    @Test
    fun `Create document requires force date at least 30 days in the future`() {
        val document = generateTestGdprDocument(forceDate = Instant.now().plusSeconds(15))
        val user = users[0]
        require(user.permissions.contains(Permission.MANAGE_GDPR))

        assertFailsWith<IllegalArgumentException> {
            gdprController.createDocument(
                UserRequestInfo(user, false),
                document,
            )
        }.apply {
            assertEquals("The force date must be at least 30 days in the future", message)
        }
    }

    @Test
    fun `Create document requires valid date at least 60 days in the future`() {
        val document = generateTestGdprDocument(validDate = Instant.now().plusSeconds(15))

        assertFailsWith<IllegalArgumentException> {
            gdprController.createDocument(UserRequestInfo(users[0], false), document)
        }.apply {
            assertEquals("The valid date must be at least 60 days in the future", message)
        }
    }

    @Test
    fun `Create document requires that previous document is now valid`() {
        gdprDatabase.createDocument(
            generateTestGdprDocument(
                forceDate = Instant.now().plusSeconds(1),
                validDate = Instant.now().plusSeconds(2),
            ),
        )

        val document = generateTestGdprDocument()

        assertFailsWith<IllegalArgumentException> {
            gdprController.createDocument(UserRequestInfo(users[0], false), document)
        }.apply {
            assertEquals("Cannot create new gdpr document since the latest is not valid until now", message)
        }
    }

    @Test
    fun `GDPR_MANAGER can create document`() {
        val document = generateTestGdprDocument()

        val user = users[0]
        require(user.permissions.contains(Permission.MANAGE_GDPR))

        assertDoesNotThrow {
            val created = gdprController.createDocument(UserRequestInfo(user, false), document)

            assertEquals(document, created.copy(id = 0, creationDate = Instant.ofEpochSecond(0)))
        }
    }

    @Test
    fun `User cannot accept not latest document`() {
        val beforeLatest =
            gdprDatabase.createDocument(
                generateTestGdprDocument(
                    forceDate = Instant.now().plusSeconds(1),
                    validDate = Instant.now().plusSeconds(2),
                ),
            )

        gdprDatabase.createDocument(
            generateTestGdprDocument(
                forceDate = Instant.now().plusSeconds(10),
                validDate = Instant.now().plusSeconds(20),
            ),
        )

        assertFailsWith<IllegalArgumentException> {
            gdprController.storeAcceptanceInfo(UserRequestInfo(users[1], false), beforeLatest.id)
        }.apply {
            assertEquals("Only the latest version can be accepted", message)
        }
    }

    @Test
    fun `User can accept latest document`() {
        val document =
            gdprDatabase.createDocument(
                generateTestGdprDocument(
                    forceDate = Instant.now().plusSeconds(1),
                    validDate = Instant.now().plusSeconds(2),
                ),
            )

        val created =
            assertDoesNotThrow {
                gdprController.storeAcceptanceInfo(UserRequestInfo(users[1], true), document.id)
            }

        assertEquals(
            GdprAcceptance(users[1], Instant.ofEpochSecond(0), document, GdprAcceptanceType.ACCEPTED),
            created.copy(timestamp = Instant.ofEpochSecond(0)),
        )
    }

    @Test
    fun `Function accounts do not need to accept gdpr`() {
        val document =
            gdprDatabase.createDocument(
                generateTestGdprDocument(
                    forceDate = Instant.now().plusSeconds(1),
                    validDate = Instant.now().plusSeconds(2),
                ),
            )

        val acceptanceInfo = gdprController.getAcceptanceInfo(UserRequestInfo(users[0], false), document.id.toString())
        val notNeeded = listOf(GdprAcceptance(users[3], Instant.ofEpochSecond(0), document, GdprAcceptanceType.NOT_NEEDED))
        val needed = (users - users[3]).map { GdprAcceptance(it, Instant.ofEpochSecond(0), document, GdprAcceptanceType.NOT_ACCEPTED) }

        assertEquals((notNeeded + needed).toSet(), acceptanceInfo?.toSet())
    }

    @Test
    fun `Only MANAGER_GDPR users can see full acceptance info`() {
        val document =
            gdprDatabase.createDocument(
                generateTestGdprDocument(
                    forceDate = Instant.now().plusSeconds(1),
                    validDate = Instant.now().plusSeconds(2),
                ),
            )

        val acceptUsers = listOf(users[0], users[2], users[4])
        val noAcceptsUsers = users - acceptUsers.toSet()
        val acceptances =
            acceptUsers.map { gdprController.storeAcceptanceInfo(UserRequestInfo(it, false), document.id) }

        // Test full list method
        assertFailsWith<PermissionException> {
            gdprController.getAcceptanceInfo(UserRequestInfo(users[1], false), document.id.toString())
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"Gdpr.getAcceptanceInfo(${document.id})\".",
                message,
            )
        }

        val acceptanceInfoUser0 =
            gdprController.getAcceptanceInfo(UserRequestInfo(users[0], false), document.id.toString())
        assertEquals(
            acceptances +
                noAcceptsUsers.map {
                    GdprAcceptance(
                        it,
                        Instant.ofEpochSecond(0),
                        document,
                        if (it == users[3]) GdprAcceptanceType.NOT_NEEDED else GdprAcceptanceType.NOT_ACCEPTED,
                    )
                },
            acceptanceInfoUser0,
        )

        // Test get one method
        assertFailsWith<PermissionException> {
            gdprController.getAcceptanceInfoOfUser(UserRequestInfo(users[1], false), document.id.toString(), users[2])
        }.apply {
            assertEquals(
                "User with id ${users[1].id} is not allowed to execute action \"getAcceptanceInfoOfUser(${users[2].id})\".",
                message,
            )
        }

        val selfAcceptance1 =
            gdprController.getAcceptanceInfoOfUser(UserRequestInfo(users[1], false), document.id.toString(), users[1])
        assertNull(selfAcceptance1)

        val selfAcceptance2 =
            gdprController.getAcceptanceInfoOfUser(UserRequestInfo(users[2], false), document.id.toString(), users[2])
        assertEquals(acceptances[1], selfAcceptance2)

        val adminAcceptanceCheck =
            gdprController.getAcceptanceInfoOfUser(UserRequestInfo(users[0], false), document.id.toString(), users[2])
        assertEquals(acceptances[1], adminAcceptanceCheck)
    }
}
