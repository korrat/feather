package dev.maximilian.feather

import dev.maximilian.feather.account.AccountController
import dev.maximilian.feather.action.Action
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.junit.jupiter.api.assertDoesNotThrow
import java.sql.DriverManager
import java.util.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails

class ActionControllerTest {
    private val dbName = UUID.randomUUID().toString()
    private val db by lazy { Database.connect(getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") }) }
    private val accountController by lazy {
        AccountController(db, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))
    }

    private fun actionController(
        db: Database,
        accountController: AccountController,
        registerAction1: Boolean,
        registerAction2: Boolean,
    ) = ActionController(db, accountController).apply {
        if (registerAction1) {
            registerActionType(TestAction1.NAME, { /* No-Op */ }) { userId: Int, payload: String -> TestAction1(userId, payload) }
        }

        if (registerAction2) {
            registerActionType(TestAction2.NAME, { /* No-Op */ }) { userId: Int, payload: String -> TestAction2(userId, payload.toInt()) }
        }
    }

    @Test
    fun `Insert actions works`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = false, registerAction2 = true)
        val action1s = (0 until 3).map { TestAction1(user.id, UUID.randomUUID().toString()) }
        val action2s = (0 until 3).map { TestAction2(user.id, it) }
        actionController.insertActions(action1s + action2s)
        actionController.registerActionType(
            TestAction1.NAME,
            { /* No-Op */ },
        ) { userId: Int, payload: String -> TestAction1(userId, payload) }
        val action1s2 = (0 until 3).map { TestAction1(user.id, UUID.randomUUID().toString()) }
        // +3 to be unique with the above ones
        val action2s2 = (0 until 3).map { TestAction2(user.id, it + 3) }
        actionController.insertActions(action1s2 + action2s2)

        assertEquals((action2s + action2s2 + action1s + action1s2).toSet(), actionController.getOpenUserActions(user).toSet())
    }

    @Test
    fun `Insert action with unregistered action does not throw`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = false, registerAction2 = false)

        assertDoesNotThrow {
            actionController.insertAction(TestAction1(user.id, UUID.randomUUID().toString()))
        }
    }

    @Test
    fun `Retrieve action with unregistered action throws`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = false, registerAction2 = true)
        actionController.insertAction(TestAction1(user.id, UUID.randomUUID().toString()))

        assertFails {
            actionController.getOpenActions()
        }.also {
            assertEquals("Action \"${TestAction1.NAME}\" was not registered to ActionController and cannot be constructed", it.message)
        }

        assertFails {
            actionController.getOpenUserActions(user)
        }.also {
            assertEquals("Action \"${TestAction1.NAME}\" was not registered to ActionController and cannot be constructed", it.message)
        }

        assertFails {
            actionController.getOpenUserActions(TestAction1.NAME)
        }.also {
            assertEquals("Action \"${TestAction1.NAME}\" was not registered to ActionController and cannot be constructed", it.message)
        }
    }

    @Test
    fun `Delete action by name works`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = false, registerAction2 = true)
        val action1s = (0 until 3).map { TestAction1(user.id, UUID.randomUUID().toString()) }
        val action2s = (0 until 3).map { TestAction2(user.id, it) }
        actionController.insertActions(action1s + action2s)
        actionController.registerActionType(
            TestAction1.NAME,
            { /* No-Op */ },
        ) { userId: Int, payload: String -> TestAction1(userId, payload) }
        val action1s2 = (0 until 3).map { TestAction1(user.id, UUID.randomUUID().toString()) }
        actionController.insertActions(action1s2)

        actionController.deleteActionsByName(TestAction1.NAME)

        assertEquals(action2s.toSet(), actionController.getOpenUserActions(user).toSet())
        assertEquals(emptySet(), actionController.getOpenUserActions(TestAction1.NAME).toSet())
    }

    @Test
    fun `Delete action by name and user works`() {
        val user1 = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val action1User1 = TestAction1(user1.id, UUID.randomUUID().toString())
        val action1User2 = TestAction1(user2.id, UUID.randomUUID().toString())
        val action2User1 = TestAction2(user1.id, 0)
        val action2User2 = TestAction2(user2.id, 1)

        actionController.insertActions(listOf(action1User1, action1User2, action2User1, action2User2))

        actionController.deleteActionsByNameAndUser(TestAction1.NAME, user1)

        assertEquals(setOf(action2User1), actionController.getOpenUserActions(user1).toSet())
        assertEquals(setOf(action1User2), actionController.getOpenUserActions(TestAction1.NAME).toSet())
    }

    @Test
    fun `Delete action by name, user and payload works`() {
        val user1 = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val payload = UUID.randomUUID().toString()
        val action1User1 = TestAction1(user1.id, payload)
        val action1User2 = TestAction1(user2.id, payload)
        val action2User1 = TestAction2(user1.id, 0)
        val action2User2 = TestAction2(user2.id, 0)

        actionController.insertActions(listOf(action1User1, action1User2, action2User1, action2User2))
        actionController.deleteActionsByNameUserAndPayload(TestAction1.NAME, user1, payload)

        assertEquals(setOf(action2User1), actionController.getOpenUserActions(user1).toSet())
        assertEquals(setOf(action1User2), actionController.getOpenUserActions(TestAction1.NAME).toSet())
    }

    @Test
    fun `getOpenActions works`() {
        val user1 = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val action1User1 = TestAction1(user1.id, UUID.randomUUID().toString())
        val action1User2 = TestAction1(user2.id, UUID.randomUUID().toString())
        val action2User1 = TestAction2(user1.id, 0)
        val action2User2 = TestAction2(user2.id, 1)

        actionController.insertActions(listOf(action1User1, action1User2, action2User1, action2User2))

        val openActions = actionController.getOpenActions()

        assertEquals(2, openActions.size)
        assertEquals(setOf(action1User1, action2User1), openActions[user1.id]?.toSet())
        assertEquals(setOf(action1User2, action2User2), openActions[user2.id]?.toSet())
    }

    @Test
    fun `Insert action sets user disabled in service`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val service = DummyService()
        actionController.addService(service)
        assert(service.userActivationStatus.isEmpty())
        actionController.insertAction(TestAction1(user.id, "0"))
        assertEquals(false, service.userActivationStatus[user.id])

        // Now check, that the user is not deactivated twice, because there is already an action for him/her
        val service2 = DummyService()
        actionController.addService(service2)
        assert(service2.userActivationStatus.isEmpty())
        actionController.insertAction(TestAction2(user.id, 0))
        assert(service2.userActivationStatus.isEmpty())
    }

    @Test
    fun `Insert actions sets users disabled in service`() {
        val users = (0 until 5).map { accountController.createUser(TestUser.generateTestUser()) }
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val service = DummyService()
        actionController.addService(service)
        assert(service.userActivationStatus.isEmpty())
        actionController.insertActions(users.map { user -> TestAction1(user.id, "0") })
        assertEquals(users.associate { it.id to false }, service.userActivationStatus)

        // Now check, that the user is not deactivated twice, because there is already an action for him/her
        val service2 = DummyService()
        actionController.addService(service2)
        assert(service2.userActivationStatus.isEmpty())
        actionController.insertActions(users.map { user -> TestAction2(user.id, 0) })
        assert(service2.userActivationStatus.isEmpty())
    }

    @Test
    fun `Delete actions by name enables users in service, if user is not disabled`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser(disabled = true))
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val service = DummyService()
        actionController.addService(service)

        actionController.insertAction(TestAction1(user.id, "0"))
        actionController.insertAction(TestAction2(user.id, 0))
        actionController.insertAction(TestAction2(user2.id, 0))
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByName(TestAction1.NAME)
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByName(TestAction2.NAME)
        assertEquals(mapOf(user.id to true, user2.id to false), service.userActivationStatus)
    }

    @Test
    fun `Delete actions by name and user enables users in service, if user is not disabled`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser(disabled = true))
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val service = DummyService()
        actionController.addService(service)

        actionController.insertAction(TestAction1(user.id, "0"))
        actionController.insertAction(TestAction2(user.id, 0))
        actionController.insertAction(TestAction2(user2.id, 0))
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameAndUser(TestAction1.NAME, user)
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameAndUser(TestAction2.NAME, user)
        assertEquals(mapOf(user.id to true, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameAndUser(TestAction1.NAME, user)
        assertEquals(mapOf(user.id to true, user2.id to false), service.userActivationStatus)
    }

    @Test
    fun `Delete actions by name, user and payload enables users in service, if user is not disabled`() {
        val user = accountController.createUser(TestUser.generateTestUser())
        val user2 = accountController.createUser(TestUser.generateTestUser(disabled = true))
        val actionController = actionController(db, accountController, registerAction1 = true, registerAction2 = true)
        val service = DummyService()
        actionController.addService(service)

        actionController.insertAction(TestAction1(user.id, "0"))
        actionController.insertAction(TestAction2(user.id, 0))
        actionController.insertAction(TestAction2(user2.id, 0))
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameUserAndPayload(TestAction1.NAME, user, "0")
        assertEquals(mapOf(user.id to false, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameUserAndPayload(TestAction2.NAME, user, "0")
        assertEquals(mapOf(user.id to true, user2.id to false), service.userActivationStatus)

        actionController.deleteActionsByNameUserAndPayload(TestAction1.NAME, user, "0")
        assertEquals(mapOf(user.id to true, user2.id to false), service.userActivationStatus)
    }

    private class TestAction1(override val userId: Int, override val payload: String) : Action<String> {
        companion object {
            const val NAME: String = "test_action_1"
        }

        override val name: String = NAME
        override val description: String = "Test action 1 for user with id $userId with payload $payload"

        override fun payloadToString(): String = payload

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as TestAction1

            if (userId != other.userId) return false
            if (payload != other.payload) return false
            return name == other.name
        }

        override fun hashCode(): Int {
            var result = userId
            result = 31 * result + payload.hashCode()
            result = 31 * result + name.hashCode()
            return result
        }
    }

    private class TestAction2(override val userId: Int, override val payload: Int) : Action<Int> {
        companion object {
            const val NAME: String = "test_action_2"
        }

        override val name: String = NAME
        override val description: String = "Test action 2 for user with id $userId with payload $payload"

        override fun payloadToString(): String = "$payload"

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as TestAction2

            if (userId != other.userId) return false
            if (payload != other.payload) return false
            return name == other.name
        }

        override fun hashCode(): Int {
            var result = userId
            result = 31 * result + payload.hashCode()
            result = 31 * result + name.hashCode()
            return result
        }
    }

    private class DummyService : IControllableService {
        override val serviceName: String = "dummy"

        val userActivationStatus: MutableMap<Int, Boolean> = mutableMapOf()

        override fun createUser(
            createdUser: User,
            jobId: UUID?,
        ): User {
            throw NotImplementedError("Not used for test")
        }

        override fun deleteUser(user: User): Boolean {
            throw NotImplementedError("Not used for test")
        }

        override fun activateUser(
            user: User,
            activate: Boolean,
        ): Boolean {
            userActivationStatus[user.id] = activate
            return true
        }

        override fun setUsersActivated(
            users: List<User>,
            activated: Boolean,
        ) {
            users.map { it.id }.forEach { userActivationStatus[it] = activated }
        }

        override suspend fun checkUser(
            user: User,
            autoRepair: Boolean,
        ): String {
            throw NotImplementedError("Not used for test")
        }

        override fun changeMailAddress(
            user: User,
            newMail: String,
        ): Boolean {
            throw NotImplementedError("Not used for test")
        }

        override fun changeUserName(
            user: User,
            newFirstname: String,
            newSurname: String,
            newDisplayName: String,
        ): Boolean {
            throw NotImplementedError("Not used for test")
        }
    }
}
