/*
 *    Copyright [2022] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.account

import dev.maximilian.feather.testutils.InMemoryExternalCredentialProvider
import dev.maximilian.feather.testutils.TestUser
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import java.sql.DriverManager
import java.time.Instant
import java.util.UUID
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

// TODO Extend with useful tests
class AccountControllerTest {
    private val dbName = UUID.randomUUID().toString()
    private val database =
        Database.connect(
            getNewConnection = { DriverManager.getConnection("jdbc:h2:mem:$dbName;DB_CLOSE_DELAY=-1") },
            DatabaseConfig.invoke { defaultRepetitionAttempts = 0 },
        )
    private val accountController =
        AccountController(database, InMemoryExternalCredentialProvider(), mutableMapOf("MIGRATE_OLD_IDS" to "42"))

    @Test
    fun `Test user creation`() {
        val now = Instant.now()
        val testUser =
            TestUser.generateTestUser(
                registeredSince = Instant.ofEpochSecond(100),
                lastLoginAt = Instant.ofEpochSecond(200),
            )

        val createdUser = accountController.createUser(testUser)
        assert(createdUser.id > 0)
        assertEquals(
            testUser.copy(
                id = createdUser.id,
                registeredSince = createdUser.registeredSince,
                lastLoginAt = createdUser.lastLoginAt,
            ),
            createdUser,
        )
        assertEquals(Instant.ofEpochSecond(0), createdUser.lastLoginAt)
        assert(createdUser.registeredSince >= now)

        val returnedUser = accountController.getUser(createdUser.id)
        assertNotNull(returnedUser)
        assertEquals(createdUser, returnedUser)
    }

    @Test
    fun `Test update and delete user photo`() {
        val testUser = TestUser.generateTestUser()
        val user = accountController.createUser(testUser)
        val externalUserId = assertNotNull(accountController.getUserExternalId(user))
        val photo = Random.nextBytes(10)

        accountController.updatePhotoForUser(user, photo)
        assertContentEquals(photo, accountController.getPhotoForUser(user))
        assertContentEquals(photo, accountController.externalUserProvider.getPhotoForUser(externalUserId))

        accountController.deletePhotoForUser(user)
        assertNull(accountController.getPhotoForUser(user))
        assertNull(accountController.externalUserProvider.getPhotoForUser(externalUserId))
    }
}
