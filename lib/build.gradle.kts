/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

dependencies {
    // Rest framework
    implementation(libs.bundles.javalin)
    implementation(libs.javalin.openapi.core)
    kapt(libs.javalin.openapi.kapt)

    // JSON (De-)Serializer
    implementation(libs.bundles.jackson)

    // Database ORM Framework
    implementation(libs.bundles.exposed)

    // Redis driver
    implementation(libs.jedis)

    // Micrometer metrics plugin
    implementation(libs.bundles.micrometer)

    // Graph library for finding cycles in group membership structure
    implementation(libs.jgrapht)

    // Im memory database
    testImplementation(libs.h2)
}

kotlin {
    explicitApi()
}
