package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.User
import java.util.UUID

internal data class KeycloakAction(
    val id: UUID,
    val user: User,
    val backUrl: String,
)
