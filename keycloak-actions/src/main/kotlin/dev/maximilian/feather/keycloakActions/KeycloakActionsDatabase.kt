/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.account.AccountController
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.lessEq
import org.jetbrains.exposed.sql.andWhere
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.CurrentTimestamp
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.javatime.timestampParam
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import kotlin.concurrent.fixedRateTimer

internal class KeycloakActionsDatabase(private val db: Database, private val accountController: AccountController) {
    private val keycloakActionsTable = KeycloakActionsTable(accountController)

    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(keycloakActionsTable)
        }

        fixedRateTimer(
            name = "KeycloakActionsDatabase Cleanup Job",
            daemon = true,
            period = java.time.Duration.ofHours(1).seconds,
        ) {
            cleanupJob()
        }
    }

    internal fun insertKeycloakAction(keycloakAction: KeycloakAction): KeycloakAction =
        transaction(db) {
            val resultId =
                keycloakActionsTable.insertAndGetId {
                    it[userId] = keycloakAction.user.id
                    it[backUrl] = keycloakAction.backUrl
                }.value

            keycloakAction.copy(id = resultId)
        }

    internal fun deleteActionById(id: UUID) {
        transaction(db) {
            keycloakActionsTable.deleteWhere { keycloakActionsTable.id eq id }
        }
    }

    internal fun getKeycloakActionById(
        id: UUID,
        checkTime: Boolean = true,
    ): KeycloakAction? =
        transaction(db) {
            // The token can only be used within 60 seconds to generate a session
            val untilIssuedAt = Instant.now().minusSeconds(60)
            val query = keycloakActionsTable.select { keycloakActionsTable.id eq id }

            if (checkTime) {
                query.andWhere { keycloakActionsTable.issuedAt greaterEq timestampParam(untilIssuedAt) }
            }

            query.firstOrNull()
                ?.let {
                    val user = accountController.getUser(it[keycloakActionsTable.userId].value) ?: return@let null

                    KeycloakAction(
                        id = it[keycloakActionsTable.id].value,
                        user = user,
                        backUrl = it[keycloakActionsTable.backUrl],
                    )
                }.apply {
                    keycloakActionsTable.update(where = { keycloakActionsTable.id eq id }) {
                        it[issuedAt] = Instant.ofEpochSecond(0)
                    }
                }
        }

    private fun cleanupJob() {
        transaction(db) {
            keycloakActionsTable.deleteWhere { keycloakActionsTable.issuedAt lessEq Instant.now().minus(10, ChronoUnit.MINUTES) }
        }
    }

    private class KeycloakActionsTable(accountController: AccountController) : UUIDTable("keycloak_actions") {
        val userId: Column<EntityID<Int>> = reference("user_id", accountController.userIdColumn, onDelete = ReferenceOption.CASCADE)
        val backUrl: Column<String> = varchar("back_url", 512)
        val issuedAt: Column<Instant> = timestamp("issued_at").defaultExpression(CurrentTimestamp())
    }
}
