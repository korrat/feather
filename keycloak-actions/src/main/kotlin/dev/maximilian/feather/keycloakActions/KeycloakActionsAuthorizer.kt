package dev.maximilian.feather.keycloakActions

import dev.maximilian.feather.ITokenAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.authorization.EMPTY_UUID
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

internal class KeycloakActionsAuthorizer(
    private val baseUrl: String,
    private val keycloakActionController: KeycloakActionsController,
    private val actionController: ActionController,
) : ITokenAuthorizer {
    override fun authorize(token: String): Session? {
        val uuidToken = kotlin.runCatching { UUID.fromString(token) }.getOrNull() ?: return null
        val action = keycloakActionController.getActionByIdAndUse(uuidToken) ?: return null
        val openUserActions = actionController.getOpenUserActions(action.user)

        if (openUserActions.isEmpty()) return null

        return Session(
            id = EMPTY_UUID,
            user = action.user,
            authorizer = this,
            validUntil = Instant.now().plus(60, ChronoUnit.MINUTES),
            miniSession = true,
            refreshToken = action.id.toString(),
            accessToken = action.id.toString(),
            idToken = action.id.toString(),
        )
    }

    override val id: String = "keycloak-actions-plugin"

    override fun refresh(session: Session): Session? = session.takeIf { it.validUntil >= Instant.now() }

    override fun afterLogoutURL(session: Session): String = "$baseUrl/login"

    override fun afterActionsRedirect(session: Session): String = keycloakActionController.getActionBySession(session)?.backUrl ?: "/login"

    override fun afterActionsSessionModification(session: Session): Session? {
        keycloakActionController.deleteActionBySession(session)
        return null
    }
}
